package ru.opresent.maven.minify;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

public class SourceFilesEnumeration implements Enumeration<InputStream> {

    private static final String DELIMITER = "\r\n\r\n";

    private List<File> files;

    private int current = 0;
    private boolean delimiterNext = false;

    public SourceFilesEnumeration(List<File> files) {
        this.files = files;
    }

    @Override
    public boolean hasMoreElements() {
        return current < files.size();
    }

    @Override
    public InputStream nextElement() {
        if (!hasMoreElements()) {
            throw new NoSuchElementException("No more files!");
        }
        if (delimiterNext) {
            delimiterNext = false;
            return new ByteArrayInputStream(DELIMITER.getBytes(StandardCharsets.UTF_8));
        }

        File nextFile = files.get(current);
        current++;
        delimiterNext = true;

        try {
            return new FileInputStream(nextFile);
        } catch (FileNotFoundException e) {
            throw new NoSuchElementException("The path [" + nextFile.getPath() + "] cannot be found.");
        }
    }
}
