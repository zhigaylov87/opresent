package ru.opresent.maven.minify;

import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.IOUtil;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Mojo(name = "minify", defaultPhase = LifecyclePhase.PROCESS_RESOURCES, threadSafe = true)
public class MinifyMojo extends AbstractMojo {

    private static final String TEMP_SUFFIX = ".tmp";
    private static final String CHARSET = "UTF-8";
    private static final int LINE_BREAK = 5000;

    @Parameter(property = "profile", defaultValue = "production")
    private Profile profile;

    @Parameter(property = "webappSourceDir", defaultValue = "${basedir}/src/main/webapp")
    private String webappSourceDir;

    @Parameter(property = "webappTargetDir", defaultValue = "${project.build.directory}/${project.build.finalName}")
    private String webappTargetDir;

    @Parameter(property = "mobileCssSrcFiles")
    private List<String> mobileCssSrcFiles;

    @Parameter(property = "mobileCssFinalFile")
    private String mobileCssFinalFile;

    @Parameter(property = "normalCssSrcFiles")
    private List<String> normalCssSrcFiles;

    @Parameter(property = "normalCssFinalFile")
    private String normalCssFinalFile;

    @Parameter(property = "mobileJsSrcFiles")
    private List<String> mobileJsSrcFiles;

    @Parameter(property = "mobileJsFinalFile")
    private String mobileJsFinalFile;

    @Parameter(property = "normalJsSrcFiles")
    private List<String> normalJsSrcFiles;

    @Parameter(property = "normalJsFinalFile")
    private String normalJsFinalFile;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            getLog().info("Profile: " + profile);

            Processor cssProcessor = new CssProcessor();
            getLog().info("Start of process mobile css");
            process(mobileCssSrcFiles, mobileCssFinalFile, cssProcessor);
            getLog().info("End of process mobile css");

            getLog().info("Start of process normal css");
            process(normalCssSrcFiles, normalCssFinalFile, cssProcessor);
            getLog().info("End of process normal css");

            Processor jsProcessor = new JsProcessor(getLog());
            getLog().info("Start of process mobile js");
            process(mobileJsSrcFiles, mobileJsFinalFile, jsProcessor);
            getLog().info("End of process mobile js");

            getLog().info("Start of process normal js");
            process(normalJsSrcFiles, normalJsFinalFile, jsProcessor);
            getLog().info("End of process normal js");

        } catch (Exception e) {
            throw new MojoExecutionException("Minify failed", e);
        }
    }

    private void process(List<String> srcFilesStr, String finalFileStr, Processor processor) throws IOException, MojoExecutionException {
        List<File> srcFiles = new ArrayList<>();
        for (String src : srcFilesStr) {
            File file = new File(webappSourceDir + src);
            if (file.exists()) {
                if (file.isDirectory()) {
                    getLog().info("Source dir: " + file.getPath());
                    srcFiles.addAll(getOrderingFiles(file));
                } else {
                    getLog().info("Source file: " + file.getPath());
                    srcFiles.add(file);
                }
            } else {
                throw new FileNotFoundException("Source file: " + file.getPath());
            }
        }

        String fullFinalFileStr = webappTargetDir + finalFileStr;

        File mergedFile = new File(webappTargetDir + finalFileStr + (profile == Profile.production ? TEMP_SUFFIX : ""));
        mergedFile.getParentFile().mkdirs();
        mergedFile.createNewFile();
        merge(srcFiles, mergedFile);
        getLog().info(MessageFormat.format("Merged file size: {0} bytes", mergedFile.length()));

        if (profile == Profile.production) {
            File minifiedFile = new File(fullFinalFileStr);
            processor.process(mergedFile, minifiedFile);
            getLog().info(MessageFormat.format("Minified file size: {0} bytes", minifiedFile.length()));
            getLog().info("Result file: " + minifiedFile.getPath());
            mergedFile.delete();
        } else {
            getLog().info("Result file: " + mergedFile.getPath());
        }
    }

    private void merge(List<File> files, File mergedFile) throws IOException {
        try (InputStream sequence = new SequenceInputStream(new SourceFilesEnumeration(files));
             OutputStream out = new FileOutputStream(mergedFile);
             InputStreamReader sequenceReader = new InputStreamReader(sequence, CHARSET);
             OutputStreamWriter outWriter = new OutputStreamWriter(out, CHARSET)) {

            IOUtil.copy(sequenceReader, outWriter);
        }
    }

    private List<File> getOrderingFiles(File dir) throws IOException, MojoExecutionException {
        final Path orderingPath = Paths.get(dir.getAbsolutePath(), "ordering.txt");
        final List<File> result = new ArrayList<>();

        for (String fileStr : Files.readAllLines(orderingPath, Charset.forName(CHARSET))) {
            if (fileStr.trim().isEmpty()) {
                continue;
            }
            File file = new File(dir.getAbsolutePath() + fileStr);
            if (!file.exists()) {
                throw new FileNotFoundException("Source file: " + file.getPath());
            }
            getLog().info("Source file: " + file.getPath());
            result.add(file);
        }

        final List<File> notProcessed = new ArrayList<>();
        Files.walkFileTree(dir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                File file = path.toFile();
                if (!file.equals(orderingPath.toFile()) && !result.contains(file)) {
                    notProcessed.add(file);
                }
                return FileVisitResult.CONTINUE;
            }
        });

        if (!notProcessed.isEmpty()) {
            StringBuilder fileStr = new StringBuilder();
            for (File file : notProcessed) {
                fileStr.append("\n").append(file.getPath());
            }
            throw new MojoExecutionException("Not processed files: " + fileStr.toString());
        }

        return result;
    }

    public enum Profile {
        production,
        development
    }

    private interface Processor {

        void process(File mergedFile, File minifiedFile) throws IOException;

    }

    private static class CssProcessor implements Processor {

        @Override
        public void process(File mergedFile, File minifiedFile) throws IOException {
            try (InputStream in = new FileInputStream(mergedFile);
                 OutputStream out = new FileOutputStream(minifiedFile);
                 InputStreamReader reader = new InputStreamReader(in, CHARSET);
                 OutputStreamWriter writer = new OutputStreamWriter(out, CHARSET)) {

                CssCompressor compressor = new CssCompressor(reader);
                compressor.compress(writer, LINE_BREAK);
            }
        }
    }

    private static class JsProcessor implements Processor {

        private Log log;

        private JsProcessor(Log log) {
            this.log = log;
        }

        @Override
        public void process(File mergedFile, File minifiedFile) throws IOException {
            try (InputStream in = new FileInputStream(mergedFile);
                 OutputStream out = new FileOutputStream(minifiedFile);
                 InputStreamReader reader = new InputStreamReader(in, CHARSET);
                 OutputStreamWriter writer = new OutputStreamWriter(out, CHARSET)) {

                JavaScriptCompressor compressor = new JavaScriptCompressor(reader,
                        new JavaScriptErrorReporter(log, mergedFile.getName()));
                compressor.compress(writer, LINE_BREAK, false, false, true, true);
            }
        }
    }
}