package ru.opresent.core.util;

import org.junit.Assert;
import org.junit.Test;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.test.AbstractTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 29.05.15
 * Time: 3:57
 */
public class CollectionUtilsTest extends AbstractTest {

    @Test
    public void testSort() {
        List<FooIdentifiable> sortedIdentifiables = new ArrayList<>();
        List<Long> sortedIds = new ArrayList<>();
        for (long id = 0; id <= 42; id++) {
            sortedIds.add(id);
            sortedIdentifiables.add(new FooIdentifiable(id));
        }

        List<FooIdentifiable> identifiables = new ArrayList<>(sortedIdentifiables);

        // test ten times
        for (int i = 0; i <= 10; i++) {
            Collections.shuffle(identifiables);
            CollectionUtils.sort(identifiables, sortedIds);
            Assert.assertArrayEquals(sortedIdentifiables.toArray(), identifiables.toArray());
        }

    }

    private static class FooIdentifiable implements Identifiable {

        final long id;

        FooIdentifiable(long id) {
            this.id = id;
        }

        @Override
        public long getId() {
            return id;
        }

        @Override
        public String toString() {
            return Long.toString(id);
        }
    }

}
