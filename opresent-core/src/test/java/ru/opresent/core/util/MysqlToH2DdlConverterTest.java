package ru.opresent.core.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * User: artem
 * Date: 12.03.2017
 * Time: 13:19
 */
public class MysqlToH2DdlConverterTest {

    private static final String MYSQL_ADD_INDEX = "ALTER TABLE product ADD INDEX index_product_status (status);";

    private static final String H2_ADD_INDEX = "CREATE INDEX index_product_status ON product(status);";

    @Test
    public void convertAddIndex() {
        Assert.assertEquals(H2_ADD_INDEX, MysqlToH2DdlConverter.convert(MYSQL_ADD_INDEX));
    }

    private static final String MYSQL_ADD_INDEX_AND_FOREIGN_KEY =
            "ALTER TABLE product\n" +
            "    ADD INDEX index_product_type_id (product_type_id),\n" +
            "    ADD CONSTRAINT index_product_type_id\n" +
            "    FOREIGN KEY (product_type_id)\n" +
            "    REFERENCES product_type (id);";

    private static final String H2_ADD_INDEX_AND_FOREIGN_KEY =
            "CREATE INDEX index_product_type_id ON product(product_type_id);\n" +
            "ALTER TABLE product ADD FOREIGN KEY (product_type_id) REFERENCES product_type(id);";

    @Test
    public void convertAddIndexAndForeignKey() {
        Assert.assertEquals(H2_ADD_INDEX_AND_FOREIGN_KEY, MysqlToH2DdlConverter.convert(MYSQL_ADD_INDEX_AND_FOREIGN_KEY));
    }

//    @Test
//    public void convertSchema() throws IOException {
//        for (String ddlFile : Arrays.asList("schema.sql", "schema_update.sql")) {
//            String mysqlDdl = FileUtils.readFileToString(new File(getClass().getResource("/sql/mysql/" + ddlFile).getFile()));
//            String h2Ddl = MysqlToH2DdlConverter.convert(mysqlDdl);
//
//            File h2DdkFile = new File(getClass().getResource("/sql/h2/" + ddlFile).getFile());
//            System.out.println("h2DdkFile = " + h2DdkFile);
//            FileUtils.write(h2DdkFile, h2Ddl);
//        }
//    }
}
