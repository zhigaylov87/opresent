package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.domain.Size;
import ru.opresent.core.service.api.SizeService;
import ru.opresent.test.TestDataHelper;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
* User: artem
* Date: 27.12.14
* Time: 5:42
*/
public class SizeServiceTest extends AbstractServiceTest {

    @Resource(name = "sizeService")
    private SizeService sizeService;

    @Resource(name = "testDataHelper")
    private TestDataHelper testDataHelper;

    @Test
    public void testConstraints() {
        final Size size = new Size();
        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                sizeService.save(size);
            }
        };

        testValidationMethodExecution(saveExecution, "{Size.width.empty}");
        size.setWidth(42);

        testValidationMethodExecution(saveExecution, "{Size.height.empty}");
        size.setHeight(100500);

        testValidationMethodExecution(saveExecution);

        testValidationMethodExecution(saveExecution, "{Size.sameExists}");
    }

}
