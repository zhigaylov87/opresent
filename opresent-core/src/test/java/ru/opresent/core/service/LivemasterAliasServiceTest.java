package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.core.service.api.ProductTypeService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
* User: artem
* Date: 27.12.14
* Time: 3:34
*/
public class LivemasterAliasServiceTest extends AbstractServiceTest {

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "livemasterAliasService")
    private LivemasterAliasService livemasterAliasService;

    @Test
    public void testConstraintsForProductType() {
        String productTypeAlias = "This is product type alias";

        LivemasterAliases productTypeAliases = createProductTypeAliases(productTypeAlias);
        livemasterAliasService.save(productTypeAliases);
        // save once again
        livemasterAliasService.save(productTypeAliases);

        final LivemasterAliases sameProductTypeAliases = createProductTypeAliases(productTypeAlias);
        testValidationMethodExecution(new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                livemasterAliasService.save(sameProductTypeAliases);
            }
        }, "{LivemasterAliases.productType.sameExists}");
    }

    @Test
    public void testConstraintsForCategory() {
        String categoryAlias = "This is category alias";

        LivemasterAliases categoryAliases = createCategoryAliases(categoryAlias);
        livemasterAliasService.save(categoryAliases);
        // save once again
        livemasterAliasService.save(categoryAliases);

        final LivemasterAliases sameCategoryAliases = createCategoryAliases(categoryAlias);
        testValidationMethodExecution(new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                livemasterAliasService.save(sameCategoryAliases);
            }
        }, "{LivemasterAliases.category.sameExists}");
    }

    private LivemasterAliases createProductTypeAliases(String alias) {
        long productTypeId = productTypeService.save(createAndSaveFooProductType());

        LivemasterAliases aliases = new LivemasterAliases(productTypeId, LivemasterAliasType.PRODUCT_TYPE);
        aliases.addAlias(alias);
        return aliases;
    }

    private LivemasterAliases createCategoryAliases(String alias) {
        long categoryId = categoryService.save(createAndSaveFooCategory());

        LivemasterAliases aliases = new LivemasterAliases(categoryId, LivemasterAliasType.CATEGORY);
        aliases.addAlias(alias);
        return aliases;
    }

}
