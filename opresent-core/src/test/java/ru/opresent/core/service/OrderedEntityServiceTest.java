package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.OrderedEntity;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.service.api.BannerService;
import ru.opresent.core.service.api.NewsService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * User: artem
 * Date: 10.05.15
 * Time: 4:14
 */
public class OrderedEntityServiceTest extends AbstractServiceTest {

    private static final int MAX_COUNT = 10;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Resource(name = "bannerService")
    private BannerService bannerService;

    @Resource(name = "newsService")
    private NewsService newsService;

    @Test
    public void testProductType() {
        test(new Support<ProductType>() {
            @Override
            public IdentifiableList<ProductType> getAll() {
                return productTypeService.getAll();
            }

            @Override
            public void addNew() {
                createAndSaveFooProductType();
            }

            @Override
            public void moveUp(ProductType entity) {
                productTypeService.moveUp(entity);
            }

            @Override
            public void moveDown(ProductType entity) {
                productTypeService.moveDown(entity);
            }

            @Override
            public void delete(ProductType entity) {
                productTypeService.delete(entity);
            }
        });
    }

    @Test
    public void testSection() {
        test(new Support<Section>() {
            @Override
            public IdentifiableList<Section> getAll() {
                return sectionService.getAll();
            }

            @Override
            public void addNew() {
                createFooSection();
            }

            @Override
            public void moveUp(Section entity) {
                sectionService.moveUp(entity);
            }

            @Override
            public void moveDown(Section entity) {
                sectionService.moveDown(entity);
            }

            @Override
            public void delete(Section entity) {
                sectionService.deleteSection(entity.getId());
            }
        });
    }

    @Test
    public void testBanner() {
        test(new Support<BannerItem>() {
            @Override
            public IdentifiableList<BannerItem> getAll() {
                return bannerService.getBanner().getAllItems();
            }

            @Override
            public void addNew() {
                createFooBannerItem();
            }

            @Override
            public void moveUp(BannerItem entity) {
                bannerService.moveUp(entity);
            }

            @Override
            public void moveDown(BannerItem entity) {
                bannerService.moveDown(entity);
            }

            @Override
            public void delete(BannerItem entity) {
                bannerService.delete(entity);
            }
        });
    }

    @Test
    public void testNews() {
        test(new Support<NewsItem>() {
            @Override
            public IdentifiableList<NewsItem> getAll() {
                return newsService.getNews().getAllItems();
            }

            @Override
            public void addNew() {
                createFooNewsItem();
            }

            @Override
            public void moveUp(NewsItem entity) {
                newsService.moveUp(entity);
            }

            @Override
            public void moveDown(NewsItem entity) {
                newsService.moveDown(entity);
            }

            @Override
            public void delete(NewsItem entity) {
                newsService.delete(entity);
            }
        });
    }

    @Test
    public void testSectionItems() {
        for (int i = 0; i < 5; i++) {
            createFooSection(SectionType.LIST);
        }

        for (Section section : sectionService.getAll().getAll()) {
            if (section.getType() == SectionType.LIST) {
                test(new SectionItemSupport(section));
            }
        }
    }

    private <T extends OrderedEntity> void test(Support<T> support) {
        // create entities
        for (int i = 0; i < MAX_COUNT; i++) {
            support.addNew();
            checkIndexes(support.getAll());
        }

        long movingEntityId = support.getAll().getFirst().getId();

        // try to move up first entity
        try {
            support.moveUp(support.getAll().getById(movingEntityId));
            fail();
        } catch (CoreException e) {
            // OK
        }

        // move entity from first to last
        for (int i = 0; i < MAX_COUNT - 1; i++) {
            support.moveDown(support.getAll().getById(movingEntityId));
            checkIndexes(support.getAll());
        }

        // try to move down last entity
        try {
            support.moveDown(support.getAll().getById(movingEntityId));
            fail();
        } catch (CoreException e) {
            // OK
        }

        // move entity from last to first
        for (int i = 0; i < MAX_COUNT - 1; i++) {
            support.moveUp(support.getAll().getById(movingEntityId));
            checkIndexes(support.getAll());
        }

        // delete first, middle, last until all entities deleted
        final int firstMode = 0;
        final int middleMode = 1;
        final int lastMode = 2;
        int mode = firstMode;
        while (!support.getAll().getAll().isEmpty()) {
            switch (mode) {
                case firstMode:
                    support.delete(support.getAll().getFirst());
                    mode = middleMode;
                    break;

                case middleMode:
                    int middleIndex = support.getAll().getAll().size() / 2;
                    support.delete(support.getAll().getAll().get(middleIndex));
                    mode = lastMode;
                    break;

                case lastMode:
                    support.delete(support.getAll().getLast());
                    mode = firstMode;
                    break;

                default:
                    throw new RuntimeException("Mode: " + mode);
            }
            checkIndexes(support.getAll());
        }
    }

    private void checkIndexes(IdentifiableList<? extends OrderedEntity> entities) {
        for (int i = 0; i < entities.getAll().size(); i++) {
            assertEquals(i, entities.getAll().get(i).getIndex());
        }
    }

    private interface Support<T extends OrderedEntity> {

        IdentifiableList<T> getAll();

        void addNew();

        void moveUp(T entity);

        void moveDown(T entity);

        void delete(T entity);
    }

    private class SectionItemSupport implements Support<SectionItem> {

        final Section section;

        SectionItemSupport(Section section) {
            this.section = section;
        }

        @Override
        public IdentifiableList<SectionItem> getAll() {
            return new IdentifiableList<>(sectionService.loadItems(section.getId()));
        }

        @Override
        public void addNew() {
            createFooSectionItem(section);
        }

        @Override
        public void moveUp(SectionItem entity) {
            sectionService.moveUp(entity);
        }

        @Override
        public void moveDown(SectionItem entity) {
            sectionService.moveDown(entity);
        }

        @Override
        public void delete(SectionItem entity) {
            sectionService.deleteSectionItem(entity.getId());
        }
    }
}
