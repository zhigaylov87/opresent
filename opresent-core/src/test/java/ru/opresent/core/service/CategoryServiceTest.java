package ru.opresent.core.service;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import ru.opresent.core.domain.Category;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.CategoryService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;

/**
* User: artem
* Date: 25.12.14
* Time: 6:33
*/
public class CategoryServiceTest extends AbstractServiceTest {

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Test
    public void testConstraints() {
        final Category category = new Category();

        ValidationMethodExecution saveExecution = new SaveExecution(category);
        ValidationMethodExecution saveExecutionWithAlises = new SaveExecutionWithAlises(category);
        List<ValidationMethodExecution> executions = Arrays.asList(saveExecution, saveExecutionWithAlises);

        testValidationMethodExecutions(executions, "{Category.name.empty}");
        category.setName(FOO);

        for (Pair<String, String> entityNameToMsgTemplate : ENTITY_NAME_TO_MESSAGE_TEMPLATE) {
            category.setEntityName(entityNameToMsgTemplate.getLeft());
            testValidationMethodExecutions(executions, entityNameToMsgTemplate.getRight());
        }
        String fooEntityName = FOO;
        category.setEntityName(fooEntityName);

        testValidationMethodExecutions(executions, "{Category.description.empty}");
        category.setDescription(FOO);

        testValidationMethodExecution(saveExecution);
        testValidationMethodExecution(saveExecution, "{Category.entityName.sameExists}");

        category.setName(category.getName() + FOO);
        category.setEntityName(fooEntityName + FOO);
        testValidationMethodExecution(saveExecutionWithAlises);
        testValidationMethodExecution(saveExecutionWithAlises, "{Category.entityName.sameExists}");
    }

    private class SaveExecution implements ValidationMethodExecution {

        private Category category;

        private SaveExecution(Category category) {
            this.category = category;
        }

        @Override
        public void execute() throws ConstraintViolationException {
            categoryService.save(category);
        }
    }

    private class SaveExecutionWithAlises implements ValidationMethodExecution {

        private Category category;

        private SaveExecutionWithAlises(Category category) {
            this.category = category;
        }

        @Override
        public void execute() throws ConstraintViolationException {
            categoryService.save(category, new LivemasterAliases(LivemasterAliasType.CATEGORY));
        }
    }
}
