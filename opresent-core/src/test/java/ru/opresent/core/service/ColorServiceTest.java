package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.domain.Color;
import ru.opresent.core.service.api.ColorService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
* User: artem
* Date: 26.12.14
* Time: 0:50
*/
public class ColorServiceTest extends AbstractServiceTest {

    @Resource(name = "colorService")
    private ColorService colorService;

    @Test
    public void testConstraints() {
        final Color color = new Color();

        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                colorService.save(color);
            }
        };

        testValidationMethodExecution(saveExecution, "{Color.rgb.empty}");

        int rgb = 100500;
        color.setRgb(rgb);
        testValidationMethodExecution(saveExecution);

        final Color savedColor = colorService.getAll().getAll().get(0);
        testValidationMethodExecution(new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                colorService.save(savedColor);
            }
        });

        final Color sameColor = new Color();
        sameColor.setRgb(rgb);
        testValidationMethodExecution(new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                colorService.save(sameColor);
            }
        }, "{Color.sameExists}");

        final Color notSameColor = new Color();
        notSameColor.setRgb(rgb + 1);
        testValidationMethodExecution(new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                colorService.save(notSameColor);
            }
        });
    }
}
