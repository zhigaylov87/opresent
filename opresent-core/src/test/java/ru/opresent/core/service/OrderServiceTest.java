// TODO: Актуализировать тест. Запилить тестовые наборы на json'ах
//package ru.opresent.core.service;
//
//import org.junit.Test;
//import ru.opresent.core.domain.cart.Cart;
//import ru.opresent.core.domain.order.DeliveryWay;
//import ru.opresent.core.domain.order.OrderInfo;
//import ru.opresent.core.service.api.OrderService;
//import ru.opresent.core.validation.EntitiesConstraints;
//
//import javax.annotation.Resource;
//import javax.validation.ConstraintViolationException;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Objects;
//
///**
// * User: artem
// * Date: 27.02.15
// * Time: 23:54
// */
//public class OrderServiceTest extends AbstractServiceTest {
//
//    @Resource(name = "orderService")
//    private OrderService orderService;
//
//    @Test
//    public void testOrderInfoConstraints() {
//        OrderInfo orderInfo = new OrderInfo();
//        Cart cart = createValidCart();
//
//        ValidationMethodExecution orderCreationExecution = createOrderCreationExecution(orderInfo, cart);
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerFullName.empty}");
//        orderInfo.setCustomerFullName(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_FULL_NAME_MAX_LENGTH + 1));
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerFullName.veryLong}");
//        orderInfo.setCustomerFullName(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_FULL_NAME_MAX_LENGTH));
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerPhone.empty}");
//        orderInfo.setCustomerPhone(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_PHONE_MAX_LENGTH + 1));
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerPhone.veryLong}");
//        orderInfo.setCustomerPhone(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_PHONE_MAX_LENGTH));
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerEmail.empty}");
//        orderInfo.setCustomerEmail(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_EMAIL_MAX_LENGTH + 1));
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerEmail.veryLong}");
//        orderInfo.setCustomerEmail(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_EMAIL_MAX_LENGTH));
//
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.deliveryWay.empty}");
//
//        for (DeliveryWayCase deliveryWayCase : createDeliveryWayCases()) {
//            orderInfo.setDeliveryWay(deliveryWayCase.deliveryWay);
//            orderInfo.setCustomerPostcode(deliveryWayCase.customerPostcode);
//            orderInfo.setCustomerAddress(deliveryWayCase.customerAddress);
//
//            testValidationMethodExecution(orderCreationExecution, deliveryWayCase.msgTemplate);
//        }
//
//        orderInfo.setCustomerComment(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_COMMENT_MAX_LENGTH + 1));
//        testValidationMethodExecution(orderCreationExecution, "{OrderInfo.customerComment.veryLong}");
//
//        orderInfo.setCustomerComment(createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_COMMENT_MAX_LENGTH));
//        testValidationMethodExecution(orderCreationExecution, null);
//    }
//
//    private ValidationMethodExecution createOrderCreationExecution(final OrderInfo orderInfo, final Cart cart) {
//        return new ValidationMethodExecution() {
//            @Override
//            public void execute() throws ConstraintViolationException {
//                orderService.create(orderInfo, cart);
//            }
//        };
//    }
//
//    private List<DeliveryWayCase> createDeliveryWayCases() {
//
//        final String validPostcode = createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_POSTCODE_MAX_LENGTH);
//        final String veryLongPostcode = createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_POSTCODE_MAX_LENGTH + 1);
//
//        final String validAddress = createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_ADDRESS_MAX_LENGTH);
//        final String veryLongAddress = createFooString(EntitiesConstraints.ORDER_INFO_CUSTOMER_ADDRESS_MAX_LENGTH + 1);
//
//        return Arrays.asList(
//                // invalid desiredDeliveryDate
//                new DeliveryWayCase(DeliveryWay.COURIER_INSIDE_MKAD, null, null, "{OrderInfo.desiredDeliveryDate.empty}"),
//                new DeliveryWayCase(DeliveryWay.COURIER_EXPRESS_SERVICE, null, null, "{OrderInfo.desiredDeliveryDate.empty}"),
//
//                new DeliveryWayCase(DeliveryWay.RUSSIAN_POST, null, null, "{OrderInfo.desiredDeliveryDate.notEmpty}"),
//                new DeliveryWayCase(DeliveryWay.SELF_EXPORT, null, null, "{OrderInfo.desiredDeliveryDate.notEmpty}"),
//
//                // invalid customerPostcode
//                new DeliveryWayCase(DeliveryWay.RUSSIAN_POST, null, null, "{OrderInfo.customerPostcode.empty}"),
//                new DeliveryWayCase(DeliveryWay.COURIER_EXPRESS_SERVICE, null, null, "{OrderInfo.customerPostcode.empty}"),
//
//                new DeliveryWayCase(DeliveryWay.RUSSIAN_POST, veryLongPostcode, null, "{OrderInfo.customerPostcode.veryLong}"),
//                new DeliveryWayCase(DeliveryWay.COURIER_EXPRESS_SERVICE, veryLongPostcode, null, "{OrderInfo.customerPostcode.veryLong}"),
//
//                new DeliveryWayCase(DeliveryWay.COURIER_INSIDE_MKAD, validPostcode, null, "{OrderInfo.customerPostcode.notEmpty}"),
//                new DeliveryWayCase(DeliveryWay.SELF_EXPORT, validPostcode, null, "{OrderInfo.customerPostcode.notEmpty}"),
//
//                // invalid customerAddress
//                new DeliveryWayCase(DeliveryWay.COURIER_INSIDE_MKAD, null, null, "{OrderInfo.customerAddress.empty}"),
//                new DeliveryWayCase(DeliveryWay.RUSSIAN_POST, validPostcode, null, "{OrderInfo.customerAddress.empty}"),
//                new DeliveryWayCase(DeliveryWay.COURIER_EXPRESS_SERVICE, validPostcode, null, "{OrderInfo.customerAddress.empty}"),
//
//                new DeliveryWayCase(DeliveryWay.COURIER_INSIDE_MKAD, validPostcode, veryLongAddress, "{OrderInfo.customerAddress.veryLong}"),
//                new DeliveryWayCase(DeliveryWay.RUSSIAN_POST, validPostcode, veryLongAddress, "{OrderInfo.customerAddress.veryLong}"),
//                new DeliveryWayCase(DeliveryWay.COURIER_EXPRESS_SERVICE, validPostcode, veryLongAddress, "{OrderInfo.customerAddress.veryLong}"),
//
//                new DeliveryWayCase(DeliveryWay.SELF_EXPORT, null, validAddress, "{OrderInfo.customerAddress.notEmpty}"),
//
//                // valid
//                new DeliveryWayCase(DeliveryWay.COURIER_INSIDE_MKAD, null, validAddress, null),
//                new DeliveryWayCase(DeliveryWay.RUSSIAN_POST, validPostcode, validAddress, null),
//                new DeliveryWayCase(DeliveryWay.COURIER_EXPRESS_SERVICE, validPostcode, validAddress, null),
//                new DeliveryWayCase(DeliveryWay.SELF_EXPORT, null, null, null)
//        );
//    }
//
//    private static class DeliveryWayCase {
//
//        final DeliveryWay deliveryWay;
//        final String customerPostcode;
//        final String customerAddress;
//        final String msgTemplate;
//
//        private DeliveryWayCase(DeliveryWay deliveryWay, String customerPostcode,
//                                String customerAddress, String msgTemplate) {
//            this.deliveryWay = Objects.requireNonNull(deliveryWay);
//            this.customerPostcode = customerPostcode;
//            this.customerAddress = customerAddress;
//            this.msgTemplate = msgTemplate;
//        }
//    }
//
//}
