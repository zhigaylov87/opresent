package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.domain.ProductLabel;
import ru.opresent.core.domain.ProductLabelType;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.api.ProductLabelService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
* User: artem
* Date: 27.12.14
* Time: 4:42
*/
public class ProductLabelServiceTest extends AbstractServiceTest {

    @Resource(name = "productLabelService")
    private ProductLabelService productLabelService;

    @Test
    public void testConstraints() {
        ProductLabel label = new ProductLabel();
        ValidationMethodExecution saveExecution = createSaveExecution(label);

        testValidationMethodExecution(saveExecution, "{ProductLabel.type.empty}");
        label.setType(ProductLabelType.NEW);

        testValidationMethodExecution(saveExecution, "{ProductLabel.image.empty}");
        ImageUse validImageUse = ImageUse.PRODUCT_LABEL_IMAGE;
        for (ImageUse imageUse : ImageUse.values()) {
            if (imageUse != validImageUse) {
                label.setImage(createAndSaveFooJpgImage(imageUse));
                testValidationMethodExecution(saveExecution, "ImageUseConstraint.invalid");
            }
        }
        label.setImage(createAndSaveFooJpgImage(validImageUse));

        testValidationMethodExecution(saveExecution, "{ProductLabel.defaultNote.empty}");
        label.setDefaultNote(FOO);

        label.setWithDiscount(true);
        testValidationMethodExecution(saveExecution, "{ProductLabel.emptyDiscountPercentForWithDiscount}");

        label.setDiscountPercent(ProductLabel.MIN_DISCOUNT_PERCENT - 1);
        testValidationMethodExecution(saveExecution, "{ProductLabel.discountPercent.lessThanMin}");

        label.setDiscountPercent(ProductLabel.MAX_DISCOUNT_PERCENT + 1);
        testValidationMethodExecution(saveExecution, "{ProductLabel.discountPercent.greaterThanMax}");

        label.setDiscountPercent(ProductLabel.MIN_DISCOUNT_PERCENT);
        testValidationMethodExecution(saveExecution);

        label.setDiscountPercent(ProductLabel.MAX_DISCOUNT_PERCENT);
        testValidationMethodExecution(saveExecution);
    }

    @Test
    public void testDefinedDiscountPercentForWithoutDiscount() {
        ProductLabel label = new ProductLabel();
        label.setType(ProductLabelType.NEW);
        label.setImage(createAndSaveFooJpgImage(ImageUse.PRODUCT_LABEL_IMAGE));
        label.setDefaultNote(FOO);
        label.setDiscountPercent(ProductLabel.MIN_DISCOUNT_PERCENT);

        testValidationMethodExecution(createSaveExecution(label), "Discount percent defined for label without discount");
    }

    private ValidationMethodExecution createSaveExecution(final ProductLabel label) {
        return new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                productLabelService.save(label);
            }
        };
    }
}
