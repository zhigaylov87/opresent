package ru.opresent.core.service;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.ProductTypeService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;

/**
* User: artem
* Date: 27.12.14
* Time: 1:56
*/
public class ProductTypeServiceTest extends AbstractServiceTest {

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Test
    public void testConstraints() {
        final ProductType productType = new ProductType();

        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                productTypeService.save(productType);
            }
        };

        ValidationMethodExecution saveWithAliasExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                productTypeService.save(productType, new LivemasterAliases(LivemasterAliasType.PRODUCT_TYPE));
            }
        };

        List<ValidationMethodExecution> executions = Arrays.asList(saveExecution, saveWithAliasExecution);

        testValidationMethodExecutions(executions, "{ProductType.name.empty}");
        String fooName = FOO;
        productType.setName(FOO);

        for (Pair<String, String> entityNameToMsgTemplate : ENTITY_NAME_TO_MESSAGE_TEMPLATE) {
            productType.setEntityName(entityNameToMsgTemplate.getLeft());
            testValidationMethodExecution(saveExecution, entityNameToMsgTemplate.getRight());
        }
        String fooEntityName = FOO;
        productType.setEntityName(fooEntityName);

        testValidationMethodExecution(saveExecution, "{ProductType.image.empty}");
        productType.setImage(createAndSaveFooJpgImage(ImageUse.PRODUCT_TYPE_IMAGE));

        testValidationMethodExecution(saveExecution);
        testValidationMethodExecution(saveExecution, "{ProductType.name.sameExists}");

        fooName += FOO;
        fooEntityName += FOO;
        productType.setName(fooName);
        productType.setEntityName(fooEntityName);
        testValidationMethodExecution(saveWithAliasExecution);
        testValidationMethodExecution(saveWithAliasExecution, "{ProductType.name.sameExists}");

        fooName = fooName + FOO;
        fooEntityName = fooEntityName + FOO;
        productType.setName(fooName);
        productType.setEntityName(fooEntityName);
        testValidationMethodExecution(saveExecution);
        fooName += FOO;
        productType.setName(fooName);
        testValidationMethodExecution(saveExecution, "{ProductType.entityName.sameExists}");

        fooName = fooName + FOO;
        fooEntityName = fooEntityName + FOO;
        productType.setName(fooName);
        productType.setEntityName(fooEntityName);
        testValidationMethodExecution(saveWithAliasExecution);
        fooName += FOO;
        productType.setName(fooName);
        testValidationMethodExecution(saveWithAliasExecution, "{ProductType.entityName.sameExists}");
    }
}
