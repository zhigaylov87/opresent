package ru.opresent.core.service;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.domain.*;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.domain.cart.Cart;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.service.api.*;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.test.AbstractTest;
import ru.opresent.test.ResourcesUtils;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.*;

/**
 * User: artem
 * Date: 25.12.14
 * Time: 6:34
 */
public abstract class AbstractServiceTest extends AbstractTest {

    public static final List<Pair<String, String>> ENTITY_NAME_TO_MESSAGE_TEMPLATE = Arrays.asList(
            Pair.<String, String>of(null, "{EntityName.empty}"),
            Pair.of("", "{EntityName.invalidLength}"),
            Pair.of(
                    createFooString(EntitiesConstraints.ENTITY_NAME_MAX_LENGTH + 1),
                    "{EntityName.invalidLength}"),
            Pair.of("каталог", "{EntityName.invalidFormat}"),
            Pair.of("cata log", "{EntityName.invalidFormat}"),
            Pair.of("cata+log", "{EntityName.invalidFormat}"),
            Pair.of("cata=log", "{EntityName.invalidFormat}"),
            Pair.of("cata;log", "{EntityName.invalidFormat}")
    );

    @Resource(name = "currentTimestampHelper")
    private CurrentTimestampHelper currentTimestampHelper;

    @Resource(name = "imageService")
    private ImageService imageService;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "sizeService")
    private SizeService sizeService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Resource(name = "bannerService")
    private BannerService bannerService;

    @Resource(name = "newsService")
    private NewsService newsService;

    @Resource(name = "cartService")
    private CartService cartService;

    protected void testValidationMethodExecution(ValidationMethodExecution execution, String expectedConstraintViolationMessageTemplate) {
        try {
            execution.execute();
            Assert.assertEquals(expectedConstraintViolationMessageTemplate, null);
        } catch (ConstraintViolationException e) {
            Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
            Assert.assertFalse(constraintViolations.isEmpty());

            Set<String> actualConstraintViolationsMessageTemplates = new HashSet<>();
            for (ConstraintViolation constraintViolation : constraintViolations) {
                actualConstraintViolationsMessageTemplates.add(constraintViolation.getMessageTemplate());
            }

            Assert.assertTrue(MessageFormat.format(
                    "Actual constraint violations message templates not contains expected message template." +
                            " Expected: ''{0}''. Actual: {1}.",
                    expectedConstraintViolationMessageTemplate,
                    Arrays.toString(actualConstraintViolationsMessageTemplates.toArray())),
                    actualConstraintViolationsMessageTemplates.contains(expectedConstraintViolationMessageTemplate));
        }
    }

    protected void testValidationMethodExecutions(List<? extends ValidationMethodExecution> executions, String expectedConstraintViolationMessageTemplate) {
        for (ValidationMethodExecution execution : executions) {
            testValidationMethodExecution(execution, expectedConstraintViolationMessageTemplate);
        }
    }

    protected void testValidationMethodExecution(ValidationMethodExecution execution) {
        testValidationMethodExecution(execution, null);
    }

    protected void testValidationMethodExecutions(List<? extends ValidationMethodExecution> executions) {
        testValidationMethodExecutions(executions, null);
    }

    protected Image createAndSaveFooPngImage(ImageUse imageUse) {
        return createAndSaveImage("foo.png", imageUse);
    }

    protected Image createAndSaveFooJpgImage(ImageUse imageUse) {
        return createAndSaveImage("foo.jpg", imageUse);
    }

    protected Image createAndSaveImage(String imageFileName, ImageUse imageUse) {
        Image image = ResourcesUtils.getImage("/ru/opresent/test/image/" + imageFileName, imageUse, HasCopyrightOption.INHERIT_FROM_IMAGE_USE);
        return imageService.save(image);
    }

    protected List<Image> createAndSaveFooPngImages(ImageUse imageUse, int count) {
        List<Image> images = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            images.add(createAndSaveFooPngImage(imageUse));
        }
        return images;
    }

    public static String createFooString(int length) {
        char[] chars = new char[length];
        Arrays.fill(chars, 'f');
        return new String(chars);
    }

    protected Product createValidFooProduct() {
        return createValidFooProduct(FOO);
    }

    protected Product createValidFooProduct(String productName) {
        Product product = new Product();

        product.setType(createAndSaveFooProductType());
        product.setName(productName);
        product.setStatus(ProductStatus.AVAILABLE);
        product.setPrice(42);
        product.setPreview(createAndSaveFooJpgImage(ImageUse.PRODUCT_PREVIEW_IMAGE));
        product.setSizeText(FOO);
        product.addCategory(createAndSaveFooCategory());
        product.setVisible(true);

        return product;
    }

    protected Product createAndSaveFooProduct() {
        return productService.save(createValidFooProduct());
    }

    protected Product createAndSaveFooProduct(ProductStatus status, int price) {
        Product product = createValidFooProduct();
        product.setStatus(status);
        product.setPrice(price);

        return productService.save(product);
    }

    protected ProductType createAndSaveFooProductType() {
        ProductType productType = new ProductType();
        productType.setName(FOO + nextId()); // unique name
        productType.setEntityName(FOO + nextId()); // unique entity name
        productType.setImage(createAndSaveFooJpgImage(ImageUse.PRODUCT_TYPE_IMAGE));
        long productTypeId = productTypeService.save(productType);
        return productTypeService.getAll().getById(productTypeId);
    }

    protected Size createAndSaveFooSize() {
        Size size = new Size();
        size.setWidth(42);
        size.setHeight(100500);
        sizeService.save(size);
        return sizeService.getAll().getAll().iterator().next();
    }

    protected Category createAndSaveFooCategory() {
        Category category = new Category();
        category.setName(FOO + nextId());
        category.setEntityName(FOO + nextId()); // unque entity name
        category.setDescription(FOO);

        long categoryId = categoryService.save(category);
        return categoryService.getAll().getById(categoryId);
    }

    protected Cart createValidCart() {
        String trackingId = UUID.randomUUID().toString();

        cartService.addProduct(trackingId, createAndSaveFooProduct(ProductStatus.AVAILABLE, 123).getId());
        cartService.addProduct(trackingId, createAndSaveFooProduct(ProductStatus.FOR_ORDER, 456).getId());
        cartService.addProduct(trackingId, createAndSaveFooProduct(ProductStatus.AVAILABLE, 789).getId());

        return cartService.load(trackingId);
    }

    protected Section createFooSection() {
        return createFooSection(SectionType.TEXT);
    }

    protected Section createFooSection(SectionType sectionType) {
        Section section = new Section(sectionType);
        section.setName(FOO + nextId());
        section.setEntityName(FOO + nextId());
        section.setText(FOO);

        sectionService.save(section);
        return sectionService.getAll().getLast();
    }

    protected SectionItem createFooSectionItem(Section section) {
        SectionItem item = new SectionItem();
        item.setName(FOO);
        item.setFullText(FOO);
        item.setShortText(FOO);
        item.setPreview(createAndSaveFooJpgImage(ImageUse.SECTION_ITEM_PREVIEW_IMAGE));
        item.setSection(section);

        return sectionService.save(item);
    }

    protected BannerItem createFooBannerItem() {
        BannerItem item = new BannerItem();
        item.setName(FOO);
        item.setNormalImage(createAndSaveFooJpgImage(ImageUse.BANNER_ITEM_NORMAL_IMAGE));
        item.setMobileImage(createAndSaveFooJpgImage(ImageUse.BANNER_ITEM_MOBILE_IMAGE));

        bannerService.save(item);
        return bannerService.getBanner().getAllItems().getLast();
    }

    protected NewsItem createFooNewsItem() {
        NewsItem item = new NewsItem();
        item.setDate(currentTimestampHelper.getTimestamp());
        item.setText(FOO);

        newsService.save(item);
        return newsService.getNews().getAllItems().getLast();
    }

    private static int CURRENT_ID = 0;

    private static int nextId() {
        return CURRENT_ID++;
    }
}
