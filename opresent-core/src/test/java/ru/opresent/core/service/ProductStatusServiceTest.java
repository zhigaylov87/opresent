package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductStatusInfo;
import ru.opresent.core.service.api.ProductStatusService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
* User: artem
* Date: 27.12.14
* Time: 5:37
*/
public class ProductStatusServiceTest extends AbstractServiceTest {

    @Resource(name = "productStatusService")
    private ProductStatusService productStatusService;

    @Test
    public void testConstraints() {
        final ProductStatusInfo statusInfo = new ProductStatusInfo(ProductStatus.AVAILABLE);

        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                productStatusService.save(statusInfo);
            }
        };

        testValidationMethodExecution(saveExecution, "{ProductStatusInfo.defaultNote.empty}");
        statusInfo.setDefaultNote(FOO);

        testValidationMethodExecution(saveExecution);
    }

}
