package ru.opresent.core.service;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.service.api.SectionService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
* User: artem
* Date: 27.12.14
* Time: 9:50
*/
public class SectionServiceTest extends AbstractServiceTest {

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Test
    public void testSectionItemConstraints() {
        final SectionItem sectionItem = new SectionItem();

        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                sectionService.save(sectionItem);
            }
        };

        testValidationMethodExecution(saveExecution, "{SectionItem.name.empty}");
        sectionItem.setName(FOO);

        testValidationMethodExecution(saveExecution, "{SectionItem.preview.empty}");
        ImageUse validPreviewImageUse = ImageUse.SECTION_ITEM_PREVIEW_IMAGE;
        for (ImageUse imageUse : ImageUse.values()) {
            if (imageUse != validPreviewImageUse) {
                sectionItem.setPreview(createAndSaveFooJpgImage(imageUse));
                testValidationMethodExecution(saveExecution, "ImageUseConstraint.invalid");
            }
        }
        sectionItem.setPreview(createAndSaveFooJpgImage(validPreviewImageUse));

        testValidationMethodExecution(saveExecution, "{SectionItem.shortText.empty}");
        sectionItem.setShortText(FOO);

        testValidationMethodExecution(saveExecution, "{SectionItem.fullText.empty}");
        sectionItem.setFullText(FOO);

        testValidationMethodExecution(saveExecution, "{SectionItem.section.empty}");

        // test with not saved section
        try {
            sectionItem.setSection(new Section(SectionType.TEXT));
            Assert.fail();
        } catch (IllegalArgumentException e) {
            // OK
        }

        // test with not SectionType.LIST type
        int index = 0;
        for (SectionType sectionType : SectionType.values()) {
            if (sectionType != SectionType.LIST) {
                Section section = createValidSection(sectionType, FOO + sectionType, FOO + index++);
                sectionService.save(section);

                try {
                    sectionItem.setSection(section);
                    Assert.fail();
                } catch (IllegalArgumentException e) {
                    // OK
                }
            }
        }

        Section section = createValidSection(SectionType.LIST, FOO, FOO);
        sectionService.save(section);
        sectionItem.setSection(section);
        sectionService.save(sectionItem);
    }

    @Test
    public void testSectionConstraints() {
        final Section section = new Section(SectionType.TEXT);

        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                sectionService.save(section);
            }
        };

        testValidationMethodExecution(saveExecution, "{Section.name.empty}");
        String fooName = FOO;
        section.setName(fooName);

        for (Pair<String, String> entityNameToMsgTemplate : ENTITY_NAME_TO_MESSAGE_TEMPLATE) {
            section.setEntityName(entityNameToMsgTemplate.getLeft());
            testValidationMethodExecution(saveExecution, entityNameToMsgTemplate.getRight());
        }
        String fooEntityName = FOO;
        section.setEntityName(fooEntityName);

        testValidationMethodExecution(saveExecution, "{Section.text.empty}");
        section.setText(FOO);

        testValidationMethodExecution(saveExecution);

        final Section section2 = createValidSection(SectionType.LIST, fooName, fooEntityName);
        saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                sectionService.save(section2);
            }
        };
        testValidationMethodExecution(saveExecution, "{Section.name.sameExists}");
        section2.setName(fooName + FOO);

        testValidationMethodExecution(saveExecution, "{Section.entityName.sameExists}");
        section2.setEntityName(fooEntityName + FOO);

        testValidationMethodExecution(saveExecution);
    }

    private Section createValidSection(SectionType type, String name, String entityName) {
        Section section = new Section(type);
        section.setName(name);
        section.setEntityName(entityName);

        section.setText(FOO);

        return section;
    }
}
