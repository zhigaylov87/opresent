package ru.opresent.core.service;

import javax.validation.ConstraintViolationException;

/**
 * User: artem
 * Date: 26.12.14
 * Time: 0:09
 */
public interface ValidationMethodExecution {

    public void execute() throws ConstraintViolationException;

}
