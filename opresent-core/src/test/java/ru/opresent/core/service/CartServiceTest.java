//package qwe.market.core.service;
//
//import com.google.common.collect.ImmutableSet;
//import org.junit.Assert;
//import org.junit.Test;
//import ru.opresent.core.service.api.CartService;
//
//import javax.annotation.Resource;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//import java.util.concurrent.*;
//
///**
// * TODO: Поправить код, чтобы тест проходил
// * User: artem
// * Date: 15.01.2017
// * Time: 3:55
// */
//public class CartServiceTest extends AbstractServiceTest {
//
//    private static final int CONCURRENT_ADD_PRODUCT_THREADS_COUNT = 10;
//
//    @Resource(name = "cartService")
//    private CartService cartService;
//
//    @Test
//    public void concurentAddProductWithoutNotSingleCart() {
//        List<Long> productIds = new ArrayList<>(CONCURRENT_ADD_PRODUCT_THREADS_COUNT);
//        for (int i = 0; i < CONCURRENT_ADD_PRODUCT_THREADS_COUNT; i++) {
//            long productId = createAndSaveFooProduct().getId();
//            productIds.add(productId);
//            System.out.println(i + " - product saved - " + productId);
//        }
//
//        final String trackingId = UUID.randomUUID().toString();
//        List<Callable<Long>> addProductTasks = new ArrayList<>(CONCURRENT_ADD_PRODUCT_THREADS_COUNT);
//
//        for (final long productId : productIds) {
//            addProductTasks.add(new Callable<Long>() {
//                @Override
//                public Long call() throws Exception {
//                    long cartId = cartService.addProduct(trackingId, productId);
//                    System.out.println("productId = " + productId + ", cartId = " + cartId);
//                    return cartId;
//                }
//            });
//        }
//
//
//        List<Long> cartIds = new ArrayList<>();
//        ExecutorService executor = Executors.newFixedThreadPool(CONCURRENT_ADD_PRODUCT_THREADS_COUNT);
//        try {
//            List<Future<Long>> futures = executor.invokeAll(addProductTasks);
//            for (Future<Long> future : futures) {
//                cartIds.add(future.get());
//            }
//            executor.shutdown();
//        } catch (InterruptedException | ExecutionException e) {
//            executor.shutdownNow();
//            throw new RuntimeException(e);
//        }
//
//        Assert.assertEquals("Not single cart", 1, ImmutableSet.copyOf(cartIds).size());
//    }
//}
