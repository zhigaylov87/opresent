package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.api.ProductService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;

/**
 * User: artem
 * Date: 27.12.14
 * Time: 6:02
 */
public class ProductServiceTest extends AbstractServiceTest {

    @Resource(name = "productService")
    private ProductService productService;

    @Test
    public void testConstraints() {
        Product product = new Product();
        ValidationMethodExecution saveExecution = createSaveExecution(product);

        testValidationMethodExecution(saveExecution, "{Product.type.empty}");
        product.setType(createAndSaveFooProductType());

        testValidationMethodExecution(saveExecution, "{Product.name.empty}");
        product.setName(FOO);

        testValidationMethodExecution(saveExecution, "{Product.status.empty}");
        product.setStatus(ProductStatus.AVAILABLE);

        testValidationMethodExecution(saveExecution, "{Product.preview.empty}");
        ImageUse validPreviewImageUse = ImageUse.PRODUCT_PREVIEW_IMAGE;
        for (ImageUse imageUse : ImageUse.values()) {
            if (imageUse != validPreviewImageUse) {
                product.setPreview(createAndSaveFooJpgImage(imageUse));
                testValidationMethodExecution(saveExecution, "ImageUseConstraint.invalid");
            }
        }
        product.setPreview(createAndSaveFooJpgImage(validPreviewImageUse));

        for (ProductStatus productStatus : ProductStatus.values()) {
            product.setStatus(productStatus);
            product.setPrice(null);

            if (productStatus == ProductStatus.EXAMPLE) {
                testValidationMethodExecution(saveExecution, "{Product.size.empty}");
            } else {
                testValidationMethodExecution(saveExecution, "{Product.price.empty}");
                product.setPrice(42);
                testValidationMethodExecution(saveExecution, "{Product.size.empty}");
            }
        }

        product.setSizeText(FOO);
        product.setVisible(true);
        testValidationMethodExecution(saveExecution, "{Product.categories.emptyForVisible}");

        product.addCategory(createAndSaveFooCategory());
        testValidationMethodExecution(saveExecution);

        product.setSizeText(null);
        product.setSize(createAndSaveFooSize());
        // Create new preview image, because it can not be duplicated
        product.setPreview(createAndSaveFooJpgImage(validPreviewImageUse));
        testValidationMethodExecution(saveExecution);

        ImageUse validViewImageUse = ImageUse.PRODUCT_VIEW_IMAGE;
        for (ImageUse imageUse : ImageUse.values()) {
            if (imageUse != validViewImageUse) {
                // Create new preview image, because it can not be duplicated
                product.setPreview(createAndSaveFooJpgImage(validPreviewImageUse));
                product.setViews(Arrays.asList(createAndSaveFooJpgImage(imageUse)));
                testValidationMethodExecution(saveExecution, "ImageUseConstraint.invalid");
            }
        }

        product.setPreview(createAndSaveFooJpgImage(validPreviewImageUse));
        product.setViews(Arrays.asList(createAndSaveFooJpgImage(validViewImageUse)));
        productService.save(product);
    }

    @Test
    public void testProductionTimeDefined() {
        for (ProductStatus productStatus : ProductStatus.values()) {
            Product product = createValidFooProduct();
            product.setStatus(productStatus);
            product.setProductionTime(FOO);

            ValidationMethodExecution saveExecution = createSaveExecution(product);
            testValidationMethodExecution(saveExecution);
        }
    }

    @Test
    public void testLabelNoteDefinedForWithoutLabel() {
        Product product = createValidFooProduct();
        product.setLabel(null);
        product.setLabelNote(FOO);

        testValidationMethodExecution(createSaveExecution(product), "Label note defined for product without label");
    }

    @Test
    public void testDuplicatedLivemasterId() {
        Product product = createValidFooProduct();
        String livemasterId = FOO;
        product.setLivemasterId(livemasterId);

        ValidationMethodExecution saveExecution = createSaveExecution(product);
        testValidationMethodExecution(saveExecution);
        testValidationMethodExecution(saveExecution, "Product with livemaster id = '" + livemasterId + "' already exists.");
    }

    private ValidationMethodExecution createSaveExecution(final Product product) {
        return new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                productService.save(product);
            }
        };
    }
}
