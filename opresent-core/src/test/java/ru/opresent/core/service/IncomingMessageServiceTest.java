package ru.opresent.core.service;

import org.junit.Test;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.message.incoming.IncomingMessageSubjectType;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.core.validation.EntitiesConstraints;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

/**
 * User: artem
 * Date: 11.01.15
 * Time: 19:00
 */
public class IncomingMessageServiceTest extends AbstractServiceTest {

    @Resource(name = "incomingMessageService")
    private IncomingMessageService incomingMessageService;

    @Test
    public void testConstraints() {
        final IncomingMessage message = new IncomingMessage();

        ValidationMethodExecution saveExecution = new ValidationMethodExecution() {
            @Override
            public void execute() throws ConstraintViolationException {
                incomingMessageService.save(message, "TODO_TEST_TRACKING_ID"); // TODO
            }
        };

        testValidationMethodExecution(saveExecution, "{IncomingMessage.subjectType.empty}");
        message.setSubjectType(IncomingMessageSubjectType.PRODUCT);

        testValidationMethodExecution(saveExecution, "{IncomingMessage.subjectId.empty}");

        long notExistsProductId = 42L;
        message.setSubjectId(notExistsProductId);
        testValidationMethodExecution(saveExecution, "Product with id = " + notExistsProductId + " not exists.");
        message.setSubjectId(createAndSaveFooProduct().getId());

        testValidationMethodExecution(saveExecution, "{IncomingMessage.customerName.empty}");
        message.setCustomerName(createFooString(EntitiesConstraints.INCOMING_MESSAGE_CUSTOMER_NAME_MAX_LENGTH + 1));

        testValidationMethodExecution(saveExecution, "{IncomingMessage.customerName.veryLong}");
        message.setCustomerName(FOO);

        testValidationMethodExecution(saveExecution, "{IncomingMessage.customerEmailOrPhone.empty}");
        message.setCustomerEmailOrPhone(createFooString(EntitiesConstraints.INCOMING_MESSAGE_CUSTOMER_EMAIL_OR_PHONE_MAX_LENGTH + 1));

        testValidationMethodExecution(saveExecution, "{IncomingMessage.customerEmailOrPhone.veryLong}");
        message.setCustomerEmailOrPhone(FOO);

        testValidationMethodExecution(saveExecution, "{IncomingMessage.messageText.empty}");
        message.setMessageText(createFooString(EntitiesConstraints.INCOMING_MESSAGE_MESSAGE_TEXT_MAX_LENGTH + 1));

        testValidationMethodExecution(saveExecution, "{IncomingMessage.messageText.veryLong}");
        message.setMessageText(FOO);

        testValidationMethodExecution(saveExecution);
    }

}
