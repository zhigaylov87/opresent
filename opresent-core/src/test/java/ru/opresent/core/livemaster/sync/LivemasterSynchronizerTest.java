package ru.opresent.core.livemaster.sync;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.test.AbstractSimpleTest;

import static org.junit.Assert.assertEquals;
import static ru.opresent.core.domain.ProductStatus.AVAILABLE;
import static ru.opresent.core.domain.ProductStatus.FOR_ORDER;
import static ru.opresent.core.livemaster.sync.LivemasterSyncProductResult.Type.*;

/**
 * User: artem
 * Date: 15.04.2017
 * Time: 21:02
 */
public class LivemasterSynchronizerTest extends AbstractSimpleTest {

    @Test
    public void syncProductWithStatusUpdatedResult() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(FOR_ORDER, 123)
                .existsProduct(AVAILABLE, 123)
                .resultType(PRODUCT_STATUS_UPDATED));
    }

    @Test
    public void syncProductWithProductStatusAndPriceUpdatedResult() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(FOR_ORDER, 456)
                .existsProduct(AVAILABLE, 123)
                .resultType(PRODUCT_STATUS_AND_PRICE_UPDATED));
    }

    @Test
    public void syncProductWithProductStatusAndProductTypePriceDiffDefinedAndPriceUpdatedResult() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(FOR_ORDER, 456)
                .existsProduct(AVAILABLE, 123)
                .resultType(PRODUCT_STATUS_AND_PRICE_UPDATED)
                .productTypePriceDiffDefined(true));
    }

    @Test
    public void syncProductWithProductPriceUpdatedResultAndForceSyncPriceAndNotEqualsPrice() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(AVAILABLE, 456)
                .existsProduct(AVAILABLE, 123)
                .resultType(PRODUCT_PRICE_UPDATED)
                .forceSyncPrice(true));
    }

    @Test
    public void syncProductWithProductPriceUpdatedResultAndForceSyncPriceAndNotEqualsPriceAndProductTypePriceDiffDefined() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(AVAILABLE, 456)
                .existsProduct(AVAILABLE, 123)
                .resultType(PRODUCT_PRICE_UPDATED)
                .forceSyncPrice(true)
                .productTypePriceDiffDefined(true));
    }

    @Test
    public void syncProductWithNothingDoneResultAndNoForceSyncPriceAndEqualsPrice() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(AVAILABLE, 456)
                .existsProduct(AVAILABLE, 456)
                .resultType(NOTHING_DONE));
    }

    @Test
    public void syncProductWithNothingDoneResultAndNoForceSyncPriceAndNotEqualsPrice() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(AVAILABLE, 456)
                .existsProduct(AVAILABLE, 123)
                .resultType(NOTHING_DONE));
    }

    @Test
    public void syncProductWithNothingDoneResultAndForceSyncPriceAndEqualsPrice() {
        syncProductWithResult(ProcessSyncProductTestCase.create()
                .syncProduct(AVAILABLE, 123)
                .existsProduct(AVAILABLE, 123)
                .resultType(NOTHING_DONE));
    }

    @Test
    public void syncProductWithNothingDoneForDeletedResult() {
        LivemasterSyncProduct syncProduct = new LivemasterSyncProduct(
                "livemasterId123", "url123", ProductStatus.AVAILABLE, 123);

        LivemasterSynchronizer synchronizer = new DefaultLivemasterSynchronizer();

        ProductService productService = Mockito.mock(ProductService.class);
        Mockito.when(productService.loadByLivemasterId("livemasterId123")).thenReturn(null);
        Mockito.when(productService.isDeletedWithLivemasterId("livemasterId123")).thenReturn(true);
        ReflectionTestUtils.setField(synchronizer, "productService", productService);

        LivemasterSyncProductResult result = synchronizer.processSyncProduct(syncProduct, new LivemasterSyncConfig());

        assertEquals(LivemasterSyncProductResult.Type.NOTHING_DONE_FOR_DELETED, result.getType());
    }

    // TODO
//    @Test
//    public void syncProductWithProductAddResult() {
//        LivemasterSyncProduct syncProduct = new LivemasterSyncProduct(
//                "livemasterId123", "url123", ProductStatus.AVAILABLE, 123);
//
//        LivemasterSynchronizer synchronizer = new DefaultLivemasterSynchronizer();
//
//        ProductService productService = Mockito.mock(ProductService.class);
//        Mockito.when(productService.loadByLivemasterId("livemasterId123")).thenReturn(null);
//        Mockito.when(productService.isDeletedWithLivemasterId("livemasterId123")).thenReturn(false);
//        ReflectionTestUtils.setField(synchronizer, "productService", productService);
//
//        Document productPage = new Document("url123");
//        LivemasterConnector connector = Mockito.mock(LivemasterConnector.class);
//        Mockito.when(connector.loadDocument("url123")).thenReturn(productPage);
//        ReflectionTestUtils.setField(synchronizer, "connector", connector);
//
//        LivemasterPageResolverFactory pageResolverFactory = Mockito.mock(LivemasterPageResolverFactory.class);
//        Mockito.when(pageResolverFactory.getProductPageResolver(productPage)).thenReturn(Mockito.mock(LivemasterProductPageResolver.class));
//        ReflectionTestUtils.setField(synchronizer, "pageResolverFactory", pageResolverFactory);
//
//        LivemasterSyncProductResult result = synchronizer.processSyncProduct(syncProduct);
//
//        assertEquals(LivemasterSyncProductResult.Type.PRODUCT_ADDED, result.getType());
//    }

    private void syncProductWithResult(ProcessSyncProductTestCase testCase) {

        LivemasterSyncProduct syncProduct = new LivemasterSyncProduct(
                "livemasterId123", "url123",
                testCase.syncProductStatus, testCase.syncProductPrice);

        LivemasterSynchronizer synchronizer = new DefaultLivemasterSynchronizer();

        ProductService productService = Mockito.mock(ProductService.class);
        Product product = createProduct(testCase.existsProductStatus, testCase.existsProductPrice);
        Mockito.when(productService.loadByLivemasterId("livemasterId123")).thenReturn(product);
        Mockito.when(productService.save(product)).thenReturn(product);
        ReflectionTestUtils.setField(synchronizer, "productService", productService);

        LivemasterSyncConfig config = new LivemasterSyncConfig();
        config.setForceSyncPrice(testCase.forceSyncPrice);
        if (testCase.productTypePriceDiffDefined) {
            config.getProductTypeIdToPriceDiff().put(product.getType().getId(), 123);
        }

        LivemasterSyncProductResult result = synchronizer.processSyncProduct(syncProduct, config);

        assertEquals(testCase.resultType, result.getType());
    }

    private Product createProduct(ProductStatus status, Integer price) {
        Product product = new Product();
        product.setStatus(status);
        product.setPrice(price);
        product.setType(new ProductType(123));
        return product;
    }

    private static class ProcessSyncProductTestCase {

        ProductStatus syncProductStatus;
        Integer syncProductPrice;

        ProductStatus existsProductStatus;
        Integer existsProductPrice;

        LivemasterSyncProductResult.Type resultType;
        boolean forceSyncPrice;
        boolean productTypePriceDiffDefined;

        static ProcessSyncProductTestCase create() {
            return new ProcessSyncProductTestCase();
        }

        ProcessSyncProductTestCase syncProduct(ProductStatus syncProductStatus, Integer syncProductPrice) {
            this.syncProductStatus = syncProductStatus;
            this.syncProductPrice = syncProductPrice;
            return this;
        }

        ProcessSyncProductTestCase existsProduct(ProductStatus existsProductStatus, Integer existsProductPrice) {
            this.existsProductStatus = existsProductStatus;
            this.existsProductPrice = existsProductPrice;
            return this;
        }

        ProcessSyncProductTestCase resultType(LivemasterSyncProductResult.Type resultType) {
            this.resultType = resultType;
            return this;
        }

        ProcessSyncProductTestCase forceSyncPrice(boolean forceSyncPrice) {
            this.forceSyncPrice = forceSyncPrice;
            return this;
        }

        ProcessSyncProductTestCase productTypePriceDiffDefined(boolean productTypePriceDiffDefined) {
            this.productTypePriceDiffDefined = productTypePriceDiffDefined;
            return this;
        }
    }
}
