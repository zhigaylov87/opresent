package ru.opresent.core.livemaster.page_resolver;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.test.AbstractSimpleTest;
import ru.opresent.test.ResourcesUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * User: artem
 * Date: 21.03.15
 * Time: 19:34
 */
public class LivemasterProductPageResolverTest extends AbstractSimpleTest {

    private static final String PRODUCT_PAGE_DIR = "/ru/opresent/core/livemaster/product_page/";

    @Test
    public void testResolver() {
        LivemasterPageResolverFactory resolverFactory = new LivemasterPageResolverFactory();

        for (ProductPageCase productPageCase : getProductPageCases()) {

            log("Testing product page: " + FormatUtils.quotes(productPageCase.name));

            Document page = productPageCase.page;

            ProductPageData expectedData = productPageCase.data;
            LivemasterProductPageResolver resolver = resolverFactory.getProductPageResolver(page);

            String expectedResolverVersion = productPageCase.name.substring(0, productPageCase.name.indexOf("_"));
            String actualResolverVersion = resolver.getVersion();
            assertEquals(expectedResolverVersion, actualResolverVersion);

            assertEquals(expectedData.productTypeAlias, resolver.getProductTypeAlias(page));
            assertEquals(expectedData.categoriesAliases, resolver.getCategoriesAliases(page));
            assertEquals(expectedData.name, resolver.getName(page));
            assertEquals(expectedData.description, resolver.getDescription(page));
            assertEquals(expectedData.status, resolver.getStatus(page));
            if (expectedData.status != ProductStatus.EXAMPLE) {
                assertEquals(expectedData.price, resolver.getPrice(page));
            }
            assertEquals(expectedData.imagesUrls, resolver.getImagesUrls(page));
            assertEquals(expectedData.sizeText, resolver.getSizeText(page));
            assertEquals(expectedData.productionTime, resolver.getProductionTime(page));
            assertEquals(expectedData.keywords, resolver.getKeywords(page));
        }

    }

    private List<ProductPageCase> getProductPageCases() {
        List<ProductPageCase> result = new ArrayList<>();

        for (org.springframework.core.io.Resource htmlResource : ResourcesUtils.getList(PRODUCT_PAGE_DIR + "*.html")) {
            ProductPageCase pageCase = new ProductPageCase();

            String htmlFileName = htmlResource.getFilename();
            pageCase.name = htmlFileName.substring(0, htmlFileName.lastIndexOf("."));

            pageCase.page = Jsoup.parse(ResourcesUtils.getString(htmlResource));

            String jsonFileName = pageCase.name + ".json";
            pageCase.data = ResourcesUtils.getObject(PRODUCT_PAGE_DIR + jsonFileName, ProductPageData.class);

            result.add(pageCase);
        }

        return result;
    }

//    @Test
//    public void createShortProductPage() {
//         createShortProductPage("v4_22350943");
//    }

    private void createShortProductPage(String pageName) {
        org.springframework.core.io.Resource pageResource = ResourcesUtils.get(PRODUCT_PAGE_DIR + pageName + ".html");
        String pageContent = ResourcesUtils.getString(pageResource);
        Document page = Jsoup.parse(pageContent);
        page.outputSettings().prettyPrint(false);
        page.select("script").remove();
        page.select("style").remove();
        try {
            FileUtils.write(new File(pageResource.getFile().getParent() + "/" + pageName + "_short.html"), page.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static class ProductPageData {
        public String productTypeAlias;
        public String name;
        public String description;
        public ProductStatus status;
        public int price;
        public List<String> categoriesAliases;
        public List<String> imagesUrls;
        public String sizeText;
        public String productionTime;
        public List<String> keywords;
    }

    private static class ProductPageCase {
        String name;
        Document page;
        ProductPageData data;
    }
}
