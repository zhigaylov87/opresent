package ru.opresent.core.component.crypto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.component.api.crypto.CryptoKeyProvider;
import ru.opresent.core.component.api.crypto.Cryptographer;
import ru.opresent.core.component.impl.crypto.CryptographerImpl;
import ru.opresent.test.AbstractSimpleTest;
import ru.opresent.test.ResourcesUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 15:19
 */
public class CryptographerTest extends AbstractSimpleTest {

    private Cryptographer cryptographer;

    @Before
    public void before() {
        cryptographer = new CryptographerImpl();

        CryptoKeyProvider provider = Mockito.mock(CryptoKeyProvider.class);
        Mockito.when(provider.getKey()).thenReturn(ResourcesUtils.getBytes("/ru/opresent/core/component/crypto/test_key.dat"));
        ReflectionTestUtils.setField(cryptographer, "cryptoKeyProvider", provider);

        ReflectionTestUtils.setField(cryptographer, "jsonMapper", new ObjectMapper());

        CurrentTimestampHelper helper = Mockito.mock(CurrentTimestampHelper.class);
        Mockito.when(helper.getTimestamp()).thenReturn(new Date());
        ReflectionTestUtils.setField(cryptographer, "currentTimestampHelper", helper);
    }

    @Test
    public void testString() {
        String initial = "This is foo text for testing";
        String encrypted = cryptographer.encrypt(initial);
        String decrypted = cryptographer.decrypt(encrypted, String.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testNullString() {
        String initial = null;
        String encrypted = cryptographer.encrypt(initial);
        String decrypted = cryptographer.decrypt(encrypted, String.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testInt() {
        int initial = Integer.MAX_VALUE;
        String encrypted = cryptographer.encrypt(initial);
        int decrypted = cryptographer.decrypt(encrypted, Integer.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testLong() {
        long initial = Long.MAX_VALUE;
        String encrypted = cryptographer.encrypt(initial);
        long decrypted = cryptographer.decrypt(encrypted, Long.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testDate() {
        Date initial = new Date();
        String encrypted = cryptographer.encrypt(initial);
        Date decrypted = cryptographer.decrypt(encrypted, Date.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testBigDecimal() {
        BigDecimal initial = new BigDecimal("123456.7890");
        String encrypted = cryptographer.encrypt(initial);
        BigDecimal decrypted = cryptographer.decrypt(encrypted, BigDecimal.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testObject1() {
        Data initial = new Data(42, 424242L, new Date(), new BigDecimal("122345.645454"), null);
        String encrypted = cryptographer.encrypt(initial);
        Data decrypted = cryptographer.decrypt(encrypted, Data.class);
        assertEquals(initial, decrypted);
    }

    @Test
    public void testObject2() {
        Data initial = new Data(42, null, new Date(), new BigDecimal("122345.645454"), Arrays.asList(
                new Data(123, 24L, null, null, Arrays.asList(new Data(), new Data())),
                new Data(null, null, new Date(), null, null),
                new Data(),
                new Data(123, null, null, null, null)
        ));
        String encrypted = cryptographer.encrypt(initial);
        Data decrypted = cryptographer.decrypt(encrypted, Data.class);
        assertEquals(initial, decrypted);
    }

    public static class Data {

        public Integer integer;
        public Long aLong;
        public Date date;
        public BigDecimal bigDecimal;
        public List<Data> internalDatas;

        public Data() {
        }

        public Data(Integer integer, Long aLong, Date date, BigDecimal bigDecimal, List<Data> internalDatas) {
            this.integer = integer;
            this.aLong = aLong;
            this.date = date;
            this.bigDecimal = bigDecimal;
            this.internalDatas = internalDatas;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Data)) return false;

            Data data = (Data) o;

            if (aLong != null ? !aLong.equals(data.aLong) : data.aLong != null) return false;
            if (bigDecimal != null ? !bigDecimal.equals(data.bigDecimal) : data.bigDecimal != null) return false;
            if (date != null ? !date.equals(data.date) : data.date != null) return false;
            if (integer != null ? !integer.equals(data.integer) : data.integer != null) return false;
            if (internalDatas != null ? !internalDatas.equals(data.internalDatas) : data.internalDatas != null)
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = integer != null ? integer.hashCode() : 0;
            result = 31 * result + (aLong != null ? aLong.hashCode() : 0);
            result = 31 * result + (date != null ? date.hashCode() : 0);
            result = 31 * result + (bigDecimal != null ? bigDecimal.hashCode() : 0);
            result = 31 * result + (internalDatas != null ? internalDatas.hashCode() : 0);
            return result;
        }
    }


}
