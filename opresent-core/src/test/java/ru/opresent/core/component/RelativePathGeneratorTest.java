package ru.opresent.core.component;

import org.junit.Assert;
import org.junit.Test;
import ru.opresent.core.component.api.RelativePathGenerator;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.AbstractServiceTest;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 15.07.14
 * Time: 21:05
 */
public class RelativePathGeneratorTest extends AbstractServiceTest {

    @Resource(name = "relativePathGenerator")
    private RelativePathGenerator pathGenerator;

    @Test
    public void testImagePathValid() {
        testImagePathValid(1L, "00/00/00/");
        testImagePathValid(42L, "00/00/00/");
        testImagePathValid(100L, "00/00/01/");
        testImagePathValid(1234L, "00/00/12/");
        testImagePathValid(12345678L, "12/34/56/");
        testImagePathValid(99999999L, "99/99/99/");
    }

    @Test
    public void testImagePathInvalid() {
        testImagePathInvalid(1L, "00/00/01/");
        testImagePathInvalid(42L, "00/10/00/");
        testImagePathInvalid(100L, "10/00/01/");
        testImagePathInvalid(1234L, "/");
        testImagePathInvalid(12345678L, "12/34/56/78/");
        testImagePathInvalid(99999999L, "99/99/99");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotSavedImage() {
        pathGenerator.generateRelativePath(new Image(Identifiable.NOT_SAVED_ID, FOO, ImageUse.PRODUCT_TYPE_IMAGE, HasCopyrightOption.INHERIT_FROM_IMAGE_USE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testImageIdGreaterThanMaxId() {
        pathGenerator.generateRelativePath(new Image(100000000, FOO, ImageUse.PRODUCT_TYPE_IMAGE, HasCopyrightOption.INHERIT_FROM_IMAGE_USE));
    }

    private void testImagePathValid(long imageId, String expectedPath) {
        Assert.assertEquals(expectedPath, getImagePath(imageId));
    }

    private void testImagePathInvalid(long imageId, String expectedPath) {
        Assert.assertNotEquals(expectedPath, getImagePath(imageId));
    }

    private String getImagePath(long imageId) {
        return pathGenerator.generateRelativePath(new Image(imageId, FOO, ImageUse.PRODUCT_TYPE_IMAGE, HasCopyrightOption.INHERIT_FROM_IMAGE_USE));
    }

}
