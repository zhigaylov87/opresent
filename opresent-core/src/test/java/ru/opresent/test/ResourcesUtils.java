package ru.opresent.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 05.07.14
 * Time: 23:57
 */
public final class ResourcesUtils {

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

    private ResourcesUtils() {
    }

    public static Resource get(String path) {
        return new PathMatchingResourcePatternResolver().getResource(path);
    }

    public static List<Resource> getList(String path) {
        try {
            return Arrays.asList(new PathMatchingResourcePatternResolver().getResources(path));
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    public static String getString(String path) {
        return getString(get(path));
    }

    public static String getString(Resource resource) {
        try {
            return FileUtils.readFileToString(resource.getFile(), StandardCharsets.UTF_8.name());
        } catch (Exception e) {
            throw new CoreException("While reading resource. Resource: " + resource, e);
        }
    }

    public static byte[] getBytes(String path) {
        return getBytes(get(path));
    }

    public static byte[] getBytes(Resource resource) {
        try {
            return FileUtils.readFileToByteArray(resource.getFile());
        } catch (Exception e) {
            throw new CoreException("While reading resource. Resource: " + resource, e);
        }
    }

    public static <T> T getObject(String path, Class<T> elementType) {
        try {
            return JSON_MAPPER.readValue(getString(path), elementType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Image getImage(String path, ImageUse imageUse, HasCopyrightOption hasCopyrightOption) {
        int lastDotIndex = path.lastIndexOf(".");
        if (lastDotIndex == ArrayUtils.INDEX_NOT_FOUND) {
            throw new IllegalArgumentException("Invalid image file path: '" + path + "'");
        }
        String format = path.substring(lastDotIndex + 1, path.length());
        Image image = new Image(format, imageUse, hasCopyrightOption);

        try {
            BufferedImage bufferedImage = ImageIO.read(get(path).getFile());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, format, baos);
            baos.flush();
            byte[] content = baos.toByteArray();
            baos.close();
            image.setContent(content);

            return image;
        } catch (IOException e) {
            throw new CoreException("While reading image file. File path: '" + path + "'", e);
        }
    }

}
