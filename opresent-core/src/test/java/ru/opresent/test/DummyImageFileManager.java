package ru.opresent.test;

import org.springframework.core.io.Resource;
import org.springframework.mobile.device.DeviceType;
import ru.opresent.core.component.SizeOption;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.component.api.image.ImageFileProcessMode;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUseOption;

import java.util.Collection;

/**
 * User: artem
 * Date: 13.03.2017
 * Time: 23:06
 */
public class DummyImageFileManager implements ImageFileManager {

    @Override
    public String createUrl(Image image) {
        return null;
    }

    @Override
    public String createUrl(Image image, ImageUseOption useOption) {
        return null;
    }

    @Override
    public Resource getImageFileResource(Image image, ImageUseOption useOption) {
        return null;
    }

    @Override
    public String createUrl(Image image, ImageUseOption useOption, DeviceType deviceType) {
        return null;
    }

    @Override
    public void updateImageFileName(Image image) {
    }

    @Override
    public void updateImagesFileNames(Collection<Image> images) {
    }

    @Override
    public void saveImageFile(Image image, ImageFileProcessMode processMode) {
    }

    @Override
    public void saveImageFile(Image image) {
    }

    @Override
    public void deleteImageFile(Image image) {
    }

    @Override
    public void deleteImagesFiles(Collection<Image> images) {
    }

    @Override
    public String deleteAllImagesFiles() {
        return null;
    }

    @Override
    public SizeOption getSizeOption(ImageUseOption useOption) {
        return null;
    }
}
