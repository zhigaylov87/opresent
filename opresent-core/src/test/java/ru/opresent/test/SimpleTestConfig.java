package ru.opresent.test;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 19:21
 */
@Configuration
@Profile("test")
public class SimpleTestConfig {
}
