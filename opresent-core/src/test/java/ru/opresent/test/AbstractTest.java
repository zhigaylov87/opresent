package ru.opresent.test;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 21.03.15
 * Time: 20:26
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@ActiveProfiles("test")
public abstract class AbstractTest extends AbstractJUnit4SpringContextTests {

    public static final String FOO = "foo";

    @Resource(name = "testDataHelper")
    private TestDataHelper testDataHelper;

    @Before
    public void before() {
        if (isUseDatabase()) {
            testDataHelper.deleteAllData();
        }
    }

    protected boolean isUseDatabase() {
        return true;
    }
}
