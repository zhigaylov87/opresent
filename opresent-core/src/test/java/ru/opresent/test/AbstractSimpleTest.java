package ru.opresent.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 19:21
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SimpleTestConfig.class})
@ActiveProfiles("test")
public abstract class AbstractSimpleTest extends AbstractJUnit4SpringContextTests {

    protected void log(String text) {
        System.out.println(text);
    }

}
