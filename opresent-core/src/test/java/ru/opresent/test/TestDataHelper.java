package ru.opresent.test;

import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * User: artem
 * Date: 27.12.14
 * Time: 3:37
 */
@Component
public class TestDataHelper {

    @Resource(name = "dataSource")
    private DataSource dataSource;

    public void deleteAllData() {
        try {
            ScriptUtils.executeSqlScript(dataSource.getConnection(), new ClassPathResource("/sql/h2/drop_all_objects.sql"));
            ScriptUtils.executeSqlScript(dataSource.getConnection(), new ClassPathResource("/sql/h2/schema.sql"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}