package ru.opresent.test;

import org.slf4j.Logger;
import org.slf4j.helpers.NOPLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.config.DefaultServerConfig;
import ru.opresent.core.config.ServerConfig;

/**
 * http://java.dzone.com/articles/why-i-spring-bean-aliasing
 *
 * User: artem
 * Date: 05.07.14
 * Time: 20:05
 */
@Configuration
@ImportResource("classpath:spring/test-context.xml")
@Profile("test")
public class TestConfig {

    @Bean(name = "testServerSettings")
    public ServerConfig serverSettings() {
        DefaultServerConfig serverSettings = new DefaultServerConfig();
        serverSettings.setHost("http://localhost:8080");

        return serverSettings;
    }

    @Bean(name = "log")
    public Logger log() {
        return NOPLogger.NOP_LOGGER;
//        return LoggerFactory.getLogger(TestConfig.class);
    }

    @Bean(name = "testDataSource")
    public EmbeddedDatabase dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("/sql/h2/schema.sql")
                .build();
    }

    @Bean(name = "testImageFileManager")
    public ImageFileManager imageFileManager() {
        return new DummyImageFileManager();
    }

}
