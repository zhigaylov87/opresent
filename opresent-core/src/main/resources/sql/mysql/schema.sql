CREATE TABLE product (
    id BIGINT NOT NULL AUTO_INCREMENT,
    livemaster_id VARCHAR(64) NULL,
    product_type_id BIGINT NOT NULL,
    name VARCHAR(128) NOT NULL,
    description VARCHAR(2048),
    price INTEGER NULL,
    visible BIT NOT NULL,
    preview_id BIGINT NOT NULL,
    size_id BIGINT NULL,
    size_text VARCHAR(64) NULL,
    status INTEGER NOT NULL,
    status_note VARCHAR(256),
    production_time VARCHAR(64),
    product_label_id BIGINT,
    product_label_note VARCHAR(256),
    modify_time TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_type (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL UNIQUE,
  entity_name VARCHAR(32) NOT NULL UNIQUE,
  image_id BIGINT NOT NULL,
  description VARCHAR(512),
  index_number INTEGER NOT NULL UNIQUE,
  PRIMARY KEY (id)
);

CREATE TABLE category (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    entity_name VARCHAR(32) NOT NULL UNIQUE,
    description VARCHAR(512) NOT NULL,
    level INTEGER NOT NULL,
    index_number INTEGER NOT NULL,
    visible BIT NOT NULL,
    parent_id BIGINT,
    PRIMARY KEY (id),
    UNIQUE (parent_id, name),
    UNIQUE (parent_id, index_number)
);

CREATE TABLE size (
    id BIGINT NOT NULL AUTO_INCREMENT,
    height INTEGER NOT NULL,
    width INTEGER NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (width, height)
);

CREATE TABLE image (
  id BIGINT NOT NULL AUTO_INCREMENT,
  format VARCHAR(5) NOT NULL,
  image_use INTEGER NOT NULL,
  content longblob NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE color (
  id BIGINT NOT NULL AUTO_INCREMENT,
  rgb INTEGER NOT NULL UNIQUE,
  PRIMARY KEY (id)
);

CREATE TABLE products2categories (
    product_id BIGINT NOT NULL,
    category_id BIGINT NOT NULL,
    UNIQUE (product_id, category_id)
);

CREATE TABLE products2colors (
    product_id BIGINT NOT NULL,
    color_id BIGINT NOT NULL,
    UNIQUE (product_id, color_id)
);

CREATE TABLE products2views (
    product_id BIGINT NOT NULL,
    image_id BIGINT NOT NULL,
    view_index INTEGER NOT NULL,
    PRIMARY KEY (product_id, view_index),
    UNIQUE (image_id)
);

CREATE TABLE product_status_info (
  status_code INTEGER NOT NULL,
  default_note VARCHAR(256) NOT NULL,
  PRIMARY KEY (status_code)
);

CREATE TABLE product_label (
  id BIGINT NOT NULL AUTO_INCREMENT,
  product_label_type_code INTEGER NOT NULL,
  image_id BIGINT NOT NULL,
  default_note VARCHAR(256) NOT NULL,
  with_discount BIT NOT NULL,
  discount_percent INTEGER,
  PRIMARY KEY (id)
);

CREATE TABLE products2keywords (
  product_id BIGINT NOT NULL,
  keyword VARCHAR(64) NOT NULL,
  PRIMARY KEY (product_id, keyword)
);

CREATE TABLE product_keyword_thesaurus (
  keyword VARCHAR(64) NOT NULL,
  PRIMARY KEY (keyword)
);

CREATE TABLE incoming_message (
  id BIGINT NOT NULL AUTO_INCREMENT,
  subject_id BIGINT NOT NULL,
  subject_type INTEGER NOT NULL,
  customer_name VARCHAR(64) NOT NULL,
  customer_email_or_phone VARCHAR(64) NOT NULL,
  message_text VARCHAR(1024) NOT NULL,
  readed BIT NOT NULL,
  creation_time TIMESTAMP NOT NULL,
  reading_time TIMESTAMP NULL,
  PRIMARY KEY (id)
);

CREATE TABLE livemaster_alias (
  entity_id BIGINT NOT NULL,
  type INTEGER NOT NULL,
  alias VARCHAR(64) NOT NULL,
  UNIQUE (type, alias)
);

-- Constraints

ALTER TABLE product
    ADD INDEX index_product_type_id (product_type_id),
    ADD CONSTRAINT index_product_type_id
    FOREIGN KEY (product_type_id)
    REFERENCES product_type (id);

ALTER TABLE product
    ADD INDEX index_product_status (status);

ALTER TABLE product
    ADD INDEX index_product_preview_id (preview_id),
    ADD CONSTRAINT index_product_preview_id
    FOREIGN KEY (preview_id)
    REFERENCES image (id);

ALTER TABLE product
    ADD INDEX index_product_size_id (size_id),
    ADD CONSTRAINT index_product_size_id
    FOREIGN KEY (size_id)
    REFERENCES size (id);

ALTER TABLE product
    ADD INDEX index_product_label_id (product_label_id),
    ADD CONSTRAINT index_product_label_id
    FOREIGN KEY (product_label_id)
    REFERENCES product_label (id);

ALTER TABLE category
    ADD INDEX index_category_parent_id (parent_id),
    ADD CONSTRAINT index_category_parent_id
    FOREIGN KEY (parent_id)
    REFERENCES category (id);

ALTER TABLE products2categories
    ADD INDEX index_products2categories_category_id (category_id),
    ADD CONSTRAINT index_products2categories_category_id
    FOREIGN KEY (category_id)
    REFERENCES category (id);

ALTER TABLE products2categories
    ADD INDEX index_products2categories_product_id (product_id),
    ADD CONSTRAINT index_products2categories_product_id
    FOREIGN KEY (product_id)
    REFERENCES product (id);

ALTER TABLE products2views
    ADD INDEX index_products2views_image_id (image_id),
    ADD CONSTRAINT index_products2views_image_id
    FOREIGN KEY (image_id)
    REFERENCES image (id);

ALTER TABLE products2views
    ADD INDEX index_products2views_product_id (product_id),
    ADD CONSTRAINT index_products2views_product_id
    FOREIGN KEY (product_id)
    REFERENCES product (id);

ALTER TABLE products2colors
    ADD INDEX index_products2colors_product_id (product_id),
    ADD CONSTRAINT index_products2colors_product_id
    FOREIGN KEY (product_id)
    REFERENCES product (id);

ALTER TABLE products2colors
    ADD INDEX index_products2colors_color_id (color_id),
    ADD CONSTRAINT index_products2colors_color_id
    FOREIGN KEY (color_id)
    REFERENCES color (id);

ALTER TABLE product_label
    ADD INDEX index_product_label_image_id (image_id),
    ADD CONSTRAINT index_product_label_image_id
    FOREIGN KEY (image_id)
    REFERENCES image (id);

ALTER TABLE product_type
    ADD INDEX index_product_type_image_id (image_id),
    ADD CONSTRAINT index_product_type_image_id
    FOREIGN KEY (image_id)
    REFERENCES image (id);

ALTER TABLE livemaster_alias
    ADD INDEX index_livemaster_alias_entity_id (entity_id);

ALTER TABLE products2keywords
    ADD INDEX index_products2keywords_product_id (product_id),
    ADD CONSTRAINT index_products2keywords_product_id
    FOREIGN KEY (product_id)
    REFERENCES product (id);

-- Sections (BEGIN)

CREATE TABLE section (
  id BIGINT NOT NULL AUTO_INCREMENT,
  type INTEGER NOT NULL,
  index_number INTEGER NOT NULL UNIQUE,
  name VARCHAR(64) NOT NULL UNIQUE,
  entity_name VARCHAR(32) NOT NULL UNIQUE,
  visible BIT NOT NULL,
  section_text TEXT NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE section_item (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  preview_id BIGINT NOT NULL,
  short_text VARCHAR(512) NOT NULL,
  full_text TEXT NOT NULL,
  visible BIT NOT NULL,
  section_id BIGINT NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE section_item
ADD INDEX index_section_item_section_id (section_id),
ADD CONSTRAINT index_section_item_section_id
FOREIGN KEY (section_id)
REFERENCES section (id);

ALTER TABLE section_item
ADD INDEX index_section_item_preview_id (preview_id),
ADD CONSTRAINT index_section_item_preview_id
FOREIGN KEY (preview_id)
REFERENCES image (id);

-- Sections (END)

-- Order (BEGIN)

CREATE TABLE order_settings (
  personal_order_text VARCHAR(2048) NOT NULL
);

CREATE TABLE delivery_way_info (
  delivery_way_code INTEGER NOT NULL,
  note VARCHAR(256) NOT NULL,
  PRIMARY KEY (delivery_way_code)
);

CREATE TABLE order_info (
  id BIGINT NOT NULL AUTO_INCREMENT,
  status INTEGER NOT NULL,
  customer_full_name VARCHAR(128) NOT NULL,
  customer_phone VARCHAR(64) NOT NULL,
  customer_email VARCHAR(64) NOT NULL,
  personal_order BIT NOT NULL,
  address_type INTEGER NULL,
  delivery_way INTEGER NULL,
  customer_city VARCHAR(64) NULL,
  customer_postcode VARCHAR(16) NULL,
  customer_address VARCHAR(512) NULL,
  customer_comment VARCHAR(1024) NULL,
  master_comment VARCHAR(1024) NULL,
  create_time TIMESTAMP NOT NULL,
  modify_time TIMESTAMP NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE order_item (
  id BIGINT NOT NULL AUTO_INCREMENT,
  order_id BIGINT NOT NULL,
  product_id BIGINT NOT NULL,
  fix_price INTEGER NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (order_id, product_id)
);

ALTER TABLE order_item
  ADD INDEX index_order_item_order_id (order_id),
  ADD CONSTRAINT index_order_item_order_id
  FOREIGN KEY (order_id)
  REFERENCES order_info (id);

ALTER TABLE order_item
  ADD INDEX index_order_item_product_id (product_id),
  ADD CONSTRAINT index_order_item_product_id
  FOREIGN KEY (product_id)
  REFERENCES product (id);

-- Order (END)

-- Cart (BEGIN)

CREATE TABLE cart (
  id BIGINT NOT NULL AUTO_INCREMENT,
  tracking_id VARCHAR(36) NOT NULL,
  order_id BIGINT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE cart
  ADD INDEX index_cart_order_id (order_id),
  ADD CONSTRAINT index_cart_order_id
  FOREIGN KEY (order_id)
  REFERENCES order_info (id);

CREATE TABLE cart_item (
  cart_id BIGINT NOT NULL,
  product_id BIGINT NOT NULL,
  PRIMARY KEY (cart_id, product_id)
);

ALTER TABLE cart_item
  ADD INDEX index_cart_item_cart_id (cart_id),
  ADD CONSTRAINT index_cart_item_cart_id
  FOREIGN KEY (cart_id)
  REFERENCES cart (id);

ALTER TABLE cart_item
  ADD INDEX index_cart_item_product_id (product_id),
  ADD CONSTRAINT index_cart_item_product_id
  FOREIGN KEY (product_id)
  REFERENCES product (id);

-- Cart (END)

CREATE TABLE notification (
  id BIGINT NOT NULL AUTO_INCREMENT,
  subject_type INTEGER NOT NULL,
  subject_id BIGINT NOT NULL,
  status INTEGER NOT NULL,
  status_time TIMESTAMP NOT NULL,
  PRIMARY KEY (id)
);

-- Banner (BEGIN)

CREATE TABLE banner_item (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(128) NOT NULL,
  normal_image_id BIGINT NOT NULL,
  mobile_image_id BIGINT NOT NULL,
  product_id BIGINT NULL,
  index_number INTEGER NOT NULL UNIQUE,
  visible BIT NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE banner_item
  ADD INDEX index_banner_item_normal_image_id (normal_image_id),
  ADD CONSTRAINT index_banner_item_normal_image_id
  FOREIGN KEY (normal_image_id)
  REFERENCES image (id);

ALTER TABLE banner_item
  ADD INDEX index_banner_item_mobile_image_id (mobile_image_id),
  ADD CONSTRAINT index_banner_item_mobile_image_id
  FOREIGN KEY (mobile_image_id)
  REFERENCES image (id);

ALTER TABLE banner_item
  ADD INDEX index_banner_item_product_id (product_id),
  ADD CONSTRAINT index_banner_item_product_id
  FOREIGN KEY (product_id)
  REFERENCES product (id);

-- Banner (END)

-- News (BEGIN)

CREATE TABLE news_item (
  id BIGINT NOT NULL AUTO_INCREMENT,
  news_date DATE NOT NULL,
  news_text VARCHAR(512) NOT NULL,
  index_number INTEGER NOT NULL UNIQUE,
  visible BIT NOT NULL,
  PRIMARY KEY (id)
);
-- News (END)