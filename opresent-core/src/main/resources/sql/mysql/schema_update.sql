-- -------- #1 ----------
ALTER TABLE category DROP COLUMN visible;


-- -------- #2 ----------
ALTER TABLE banner_item DROP FOREIGN KEY index_banner_item_product_id;
ALTER TABLE banner_item DROP COLUMN product_id;
ALTER TABLE banner_item ADD COLUMN url VARCHAR(128) NULL;


-- -------- #3 ----------
ALTER TABLE image ADD COLUMN has_copyright INTEGER NOT NULL;
UPDATE image SET has_copyright = 0;-- INHERIT_FROM_IMAGE_USE


-- -------- #4 ----------
CREATE TABLE tmp_id2index_number(
  id BIGINT NOT NULL,
  index_number INTEGER NOT NULL UNIQUE
);

INSERT INTO tmp_id2index_number(id, index_number)
  SELECT ordered_products.id, @rownum := @rownum + 1
  FROM (SELECT @rownum := -1) r,
    (SELECT id FROM product ORDER BY status ASC, modify_time DESC) ordered_products;

-- Change for turn off auto update with CURRENT_TIMESTAMP. Current state:
-- `modify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
ALTER TABLE product CHANGE modify_time modify_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE product ADD COLUMN index_number INTEGER NULL;
UPDATE product p SET index_number = (SELECT index_number FROM tmp_id2index_number tmp WHERE tmp.id = p.id);
ALTER TABLE product CHANGE index_number index_number INTEGER NOT NULL UNIQUE;

DROP TABLE tmp_id2index_number;


-- -------- #5 ----------
ALTER TABLE product CHANGE livemaster_id livemaster_id VARCHAR(64) NULL UNIQUE;

CREATE TABLE deleted_product (
    livemaster_id VARCHAR(64) NOT NULL UNIQUE
);


-- -------- #6 ----------
ALTER TABLE incoming_message CHANGE creation_time creation_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE order_info CHANGE create_time create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE order_info CHANGE modify_time modify_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE notification CHANGE status_time status_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;


-- -------- #7 ----------
ALTER TABLE delivery_way_info ADD COLUMN price VARCHAR(32) NOT NULL;
ALTER TABLE delivery_way_info ADD COLUMN address_type_code INTEGER NOT NULL;
ALTER TABLE delivery_way_info DROP PRIMARY KEY, ADD PRIMARY KEY(address_type_code, delivery_way_code);
ALTER TABLE delivery_way_info CHANGE note note VARCHAR(1024) NOT NULL;
UPDATE section SET type = 4 WHERE entity_name = 'delivery'; -- SectionType.DELIVERY(4)


-- -------- #8 ----------
ALTER TABLE cart ADD INDEX cart_tracking_id (tracking_id);


-- -------- #9 ----------
ALTER TABLE incoming_message ADD COLUMN tracking_id VARCHAR(36) NULL;
UPDATE incoming_message SET tracking_id = 'TODO: DEFINE TRACKING_ID BY LOGS';
ALTER TABLE incoming_message CHANGE tracking_id tracking_id VARCHAR(36) NOT NULL;


-- -------- #10 ----------
ALTER TABLE section_item ADD COLUMN index_number INTEGER NULL;
UPDATE section_item SET index_number = id - 1; -- WARNING!!! Hacked simple update for concrete data.
ALTER TABLE section_item CHANGE index_number index_number INTEGER NOT NULL UNIQUE;
ALTER TABLE section_item ADD UNIQUE (section_id, index_number);


-- -------- #11 ----------
ALTER TABLE category DROP COLUMN level;
ALTER TABLE category DROP INDEX index_category_parent_id;
ALTER TABLE category DROP FOREIGN KEY index_category_parent_id;
ALTER TABLE category DROP INDEX parent_id;
ALTER TABLE category DROP INDEX parent_id_2;
ALTER TABLE category DROP COLUMN parent_id;
ALTER TABLE category ADD UNIQUE INDEX name (name);
ALTER TABLE category ADD UNIQUE INDEX index_number (index_number);


-- -------- #12 ----------
-- !!! Check MANUALLY that all personal orders is test orders and then delete them
SELECT * FROM order_info WHERE personal_order = 1;
DELETE FROM order_info WHERE personal_order = 1;
-- Delete carts and cart items with defined order_id, drop cart.order_id
DELETE FROM cart_item WHERE cart_id IN (SELECT id FROM cart WHERE order_id IS NOT NULL);
DELETE FROM cart WHERE order_id IS NOT NULL;
ALTER TABLE cart DROP FOREIGN KEY index_cart_order_id;
ALTER TABLE cart DROP COLUMN order_id;
-- Add new cart table and fill it. Drop old cart and cart_item.
RENAME TABLE cart TO cart_old;
CREATE TABLE cart (
  tracking_id VARCHAR(36) NOT NULL,
  product_id BIGINT NOT NULL,
  PRIMARY KEY (tracking_id, product_id)
);
ALTER TABLE cart
  ADD INDEX index_cart_tracking_id (tracking_id),
  ADD CONSTRAINT index_cart_product_id FOREIGN KEY (product_id) REFERENCES product (id);

INSERT INTO cart(tracking_id, product_id)
  SELECT co.tracking_id, ci.product_id FROM cart_old co INNER JOIN cart_item ci ON ci.cart_id = co.id;

DROP TABLE cart_item;
DROP TABLE cart_old;


-- -------- #13 ----------
CREATE TABLE livemaster_sync_config (
  json VARCHAR(256) NOT NULL
);
UPDATE product SET price = price + 600
WHERE price IS NOT NULL
      AND
      product_type_id = (SELECT id FROM product_type WHERE entity_name = 'photoalbum');
