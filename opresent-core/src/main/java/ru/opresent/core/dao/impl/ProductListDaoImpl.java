package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.api.ProductListDao;
import ru.opresent.core.domain.*;
import ru.opresent.core.pagination.PageOrderBy;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 12:34
 */
@Component("productListDao")
public class ProductListDaoImpl implements ProductListDao {

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "productBaseMapper")
    private ProductBaseMapper productBaseMapper;

    @Override
    public ProductPage load(ProductFilter filter, PageSettings pageSettings, boolean withCategories) {

        Map<ProductStatus, Integer> totalCountForStatus = getTotalCountForStatus(filter);
        if (totalCountForStatus.isEmpty()) {
            // there is no products for any status
            return new ProductPage(pageSettings, new ArrayList<Product>(), 0, totalCountForStatus);
        }

        LinkedHashMap<Long, Product> productIdToProduct = loadPage(filter, pageSettings);

        if (withCategories && !productIdToProduct.isEmpty()) {
            addCategories(productIdToProduct);
        }

        ProductStatus filterStatus = filter.getStatus();
        int totalItemsCount = filterStatus == null ? CollectionUtils.sum(totalCountForStatus.values()) :
                totalCountForStatus.get(filterStatus) == null ? 0 : totalCountForStatus.get(filterStatus);

        return new ProductPage(pageSettings, new ArrayList<>(productIdToProduct.values()),
                totalItemsCount, totalCountForStatus);
    }

    private static final String TOTAL_COUNT_FOR_STATUS =
            "SELECT p.status, COUNT(*) AS status_count " +
            "FROM product p " +
            "WHERE p.id IN({0}) " +
            "GROUP BY p.status";

    private Map<ProductStatus, Integer> getTotalCountForStatus(ProductFilter filter) {
        DefaultProductFilter filterWithoutStatus = new DefaultProductFilter(filter);
        filterWithoutStatus.setStatus(null);
        FilterQuery filterQuery = createFilterQuery(filterWithoutStatus);

        return jdbcTemplate.query(
                MessageFormat.format(TOTAL_COUNT_FOR_STATUS, filterQuery.getQuery()),
                filterQuery.getParams(),
                new ResultSetExtractor<Map<ProductStatus, Integer>>() {
                    @Override
                    public Map<ProductStatus, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
                        Map<ProductStatus, Integer> map = new HashMap<>();
                        while (rs.next()) {
                            map.put(ProductStatus.valueOfCode(rs.getInt("status")), rs.getInt("status_count"));
                        }
                        return Collections.unmodifiableMap(map);
                    }
                });
    }

    private static final String PAGE =
            "SELECT " + ProductDaoImpl.LOAD_BASE_FIELDS +
                    ", (CASE status WHEN " + ProductStatus.AVAILABLE.getCode() + " THEN 0 ELSE 1 END) AS ordering_group " +
            "FROM product p " +
            "INNER JOIN image img " +
            "ON p.preview_id = img.id " +
            "WHERE p.id IN({0}) ORDER BY {1} LIMIT :offset, :page_size";

    private static final Map<PageOrderBy, String> PAGE_ORDER_BY = ImmutableMap.of(
            PageOrderBy.STANDART, "ordering_group ASC, p.index_number ASC",
            PageOrderBy.RANDOM, "RAND()");

    private LinkedHashMap<Long, Product> loadPage(ProductFilter filter, PageSettings pageSettings) {
        FilterQuery filterQuery = createFilterQuery(filter);
        String orderBy = Objects.requireNonNull(PAGE_ORDER_BY.get(pageSettings.getOrderBy()));
        final ProductBaseMapper mapper = productBaseMapper;

        return jdbcTemplate.query(
                MessageFormat.format(PAGE, filterQuery.getQuery(), orderBy),

                ImmutableMap.<String, Object>builder()
                        .putAll(filterQuery.getParams())
                        .put("offset", pageSettings.getOffset())
                        .put("page_size", pageSettings.getPageSize())
                        .build(),

                new ResultSetExtractor<LinkedHashMap<Long, Product>>() {
                    @Override
                    public LinkedHashMap<Long, Product> extractData(ResultSet rs) throws SQLException, DataAccessException {
                        LinkedHashMap<Long, Product> productIdToProduct = new LinkedHashMap<>();

                        int rowNum = 0;
                        while (rs.next()) {
                            Product product = mapper.mapRow(rs, rowNum++);
                            productIdToProduct.put(product.getId(), product);
                        }

                        return productIdToProduct;
                    }
                });
    }

    private static final String PRODUCT_ID_TO_CATEGORIES_IDS =
            "SELECT product_id, category_id FROM products2categories WHERE product_id IN(:products_ids)";

    private void addCategories(final Map<Long, Product> productIdToProduct) {
        final IdentifiableList<Category> categories = categoryService.getAll();

        jdbcTemplate.query(PRODUCT_ID_TO_CATEGORIES_IDS,
                ImmutableMap.of("products_ids", productIdToProduct.keySet()),
                new ResultSetExtractor<Object>() {
                    @Override
                    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                        while (rs.next()) {
                            Product product = productIdToProduct.get(rs.getLong("product_id"));
                            Category category = categories.getById(rs.getLong("category_id"));
                            product.addCategory(category);
                        }
                        return null;
                    }
                });
    }

    private static final String FILTER_BASE = "SELECT DISTINCT p.id FROM product p ";

    private static final String COLORS_FILTER =
            "INNER JOIN products2colors p2col ON p.id = p2col.product_id AND p2col.color_id IN(:colors_ids) ";

    private static final String CATEGORY_FILTER =
            "INNER JOIN products2categories p2cat ON p.id = p2cat.product_id AND p2cat.category_id = :category_id ";

    private FilterQuery createFilterQuery(ProductFilter filter) {
        FilterQueryBuilder builder = new FilterQueryBuilder(FILTER_BASE);

        Set<Long> excludeProductIds = filter.getExcludeProductIds();
        if (excludeProductIds != null) {
            if (excludeProductIds.size() == 1) {
                builder.addWhere("p.id <> :excludeProductId", "excludeProductId", excludeProductIds.iterator().next());
            } else {
                builder.addWhere("p.id NOT IN(:excludeProductIds)", "excludeProductIds", excludeProductIds);
            }
        }

        builder.addJoin(COLORS_FILTER, "colors_ids", filter.getColors())
                .addJoin(CATEGORY_FILTER, "category_id", filter.getCategory())
                .addWhere("p.product_type_id = :product_type_id", "product_type_id", filter.getProductType())
                .addWhereHasIgnoreCase("UPPER(name) LIKE :name", "name", filter.getName())
                .addWhereHasIgnoreCase("UPPER(description) LIKE :description", "description", filter.getDescription())
                .addWhere("visible = :visible", "visible", filter.getVisible())
                .addWhere("price >= :min_price", "min_price", filter.getMinPrice())
                .addWhere("price <= :max_price", "max_price", filter.getMaxPrice());

        Set<ProductStatus> statuses = filter.getStatuses();
        if (statuses != null) {
            if (statuses.size() == 1) {
                builder.addWhere("status = :status", "status", statuses.iterator().next());
            } else {
                builder.addWhere("status IN(:statuses)", "statuses", statuses);
            }
        }

        builder.addWhere("size_id = :size_id", "size_id", filter.getSize());

        return builder.build();
    }
}
