package ru.opresent.core.dao.api;

import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderItem;
import ru.opresent.core.domain.order.OrderSettings;
import ru.opresent.core.domain.order.OrderStatus;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: artem
 * Date: 14.02.15
 * Time: 15:44
 */
public interface OrderDao {

    /**
     * Save new order
     * @param order order for save
     * @return saved order id
     */
    long saveBase(Order order);

    void saveItems(long orderId, List<OrderItem> items);

    void updateBase(Order order);

    Order loadBase(long orderId);

    Map<Long, List<OrderItem>> loadItems(Set<Long> orderIds);

    void removeProductFromAllOrders(long productId);

    int getOrderStatusCount(OrderStatus status);

    void saveOrderSettings(OrderSettings orderSettings);

    OrderSettings loadOrderSettings();
}
