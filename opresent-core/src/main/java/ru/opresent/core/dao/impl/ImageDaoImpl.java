package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.ImageDao;
import ru.opresent.core.domain.image.FixedImageName;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:39
 */
@Component("imageDao")
public class ImageDaoImpl implements ImageDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "dataSource")
    private DataSource dataSource;

    private static final String SAVE =
            "INSERT INTO image(format, image_use, has_copyright, content) " +
            "VALUES(:format, :image_use, :has_copyright, :content)";

    @Override
    public Image save(Image image) {
        Map<String, Object> params = ImmutableMap.<String, Object>of(
                "format", image.getFormat(),
                "image_use", image.getImageUse().getCode(),
                "has_copyright", image.getHasCopyrightOption().getCode(),
                "content", image.getContent());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        image.setId(keyHolder.getKey().longValue());

        return image;
    }

    private static final String DELETE_ONE = "DELETE FROM image WHERE id = :id";

    @Override
    public void delete(long id) {
        int rows = jdbcTemplate.update(DELETE_ONE, ImmutableMap.of("id", id));
        if (rows != 1) {
            throw new CoreException("Image with id = " + id + " not deleted. Affected rows: " + rows + ".");
        }
    }

    private static final String DELETE_SEVERAL = "DELETE FROM image WHERE id IN(:ids)";

    @Override
    public void delete(Set<Long> ids) {
        int rows = jdbcTemplate.update(DELETE_SEVERAL, ImmutableMap.of("ids", ids));
        if (rows != ids.size()) {
            throw new CoreException(
                    MessageFormat.format("Images not deleted. Ids: {0}. Ids count: {1}. Affected rows: {2}.",
                            Arrays.toString(ids.toArray()), ids.size(), rows));
        }
    }

    private static final String TOTAL_COUNT = "SELECT COUNT(*) FROM image";
//    private static final String LOAD_ALL = "SELECT id, format, image_use, content FROM image ORDER BY id";

    private static final String LOAD_ALL =
            "SELECT id, format, image_use, has_copyright, content, " +
                "(SELECT CASE img.image_use " +
                    "WHEN " + ImageUse.SECTION_ITEM_PREVIEW_IMAGE.getCode() +
                    " THEN (SELECT name FROM section_item WHERE preview_id = img.id) " +

                    "WHEN " + ImageUse.PRODUCT_TYPE_IMAGE.getCode() +
                    " THEN (SELECT entity_name FROM product_type WHERE image_id = img.id) " +

                    "WHEN " + ImageUse.PRODUCT_PREVIEW_IMAGE.getCode() +
                    " THEN (SELECT name FROM product WHERE preview_id = img.id) " +

                    "WHEN " + ImageUse.PRODUCT_VIEW_IMAGE.getCode() +
                    " THEN (SELECT p.name FROM products2views p2v " +
                           "JOIN product p ON p2v.product_id = p.id " +
                           "WHERE p2v.image_id = img.id) " +

                    "ELSE null " +
                "END) AS image_name " +

            "FROM image img ORDER BY id";

    @Override
    public EntityIterator<Image> createEntityIterator() {
        return new BaseMySqlStreamingResultsEntityIterator<>(dataSource, TOTAL_COUNT, LOAD_ALL, new ImageMapper(true));
    }

    private static class ImageMapper implements RowMapper<Image> {

        private boolean withContent;

        private ImageMapper(boolean withContent) {
            this.withContent = withContent;
        }

        @Override
        public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
            Image image = new Image(
                    rs.getLong("id"),
                    rs.getString("format"),
                    ImageUse.valueOf(rs.getInt("image_use")),
                    HasCopyrightOption.valueOf(rs.getInt("has_copyright")));
            if (withContent) {
                image.setContent(rs.getBytes("content"));
            }
            String imageName = rs.getString("image_name");
            if (imageName != null) {
                image.setImageName(new FixedImageName(imageName));
            }
            return image;
        }
    }
}
