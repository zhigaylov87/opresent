package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.api.SectionDao;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 14:20
 */
@Component("sectionDao")
public class SectionDaoImpl implements SectionDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO section(type, index_number, name, entity_name, visible, section_text) " +
                    "VALUES(:type, :index_number, :name, :entity_name, :visible, :section_text)";

    @Override
    public long save(Section section) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("type", section.getType().getCode())
                .put("index_number", section.getIndex())
                .put("name", section.getName())
                .put("entity_name", section.getEntityName())
                .put("visible", section.isVisible())
                .put("section_text", section.getText())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE =
            "UPDATE section " +
            "SET name = :name, entity_name = :entity_name, visible = :visible, section_text = :section_text " +
            "WHERE id = :id";

    @Override
    public void update(Section section) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("name", section.getName())
                .put("entity_name", section.getEntityName())
                .put("visible", section.isVisible())
                .put("section_text", section.getText())
                .put("id", section.getId())
                .build();

        int rows = jdbcTemplate.update(UPDATE, params);

        if (rows != 1) {
            throw new CoreException("Section not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM section WHERE id = :id";

    @Override
    public void delete(long sectionId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", sectionId));
        if (rows != 1) {
            throw new CoreException("Section not deleted!");
        }
    }

    private static final String LOAD_BASE = "SELECT id, type, index_number, name, entity_name, visible, section_text FROM section";
    private static final String LOAD_LIST_ORDER_BY = " ORDER BY index_number ASC";

    private static final String LOAD_ALL = LOAD_BASE + LOAD_LIST_ORDER_BY;
    private static final String LOAD_ALL_VISIBLE =
            LOAD_BASE + " WHERE visible = " + DaoUtils.BOOLEAN_TRUE_NUMBER + LOAD_LIST_ORDER_BY;

    @Override
    public List<Section> loadList(boolean onlyVisible) {
        return jdbcTemplate.query(onlyVisible ? LOAD_ALL_VISIBLE : LOAD_ALL, new SectionMapper());
    }

    private static final String LOAD_BY_ID = LOAD_BASE + " WHERE id = :id";

    @Override
    public Section load(long id) {
        return CollectionUtils.getSingle(jdbcTemplate.query(
                LOAD_BY_ID, new MapSqlParameterSource(ImmutableMap.of("id", id)), new SectionMapper()));
    }

    private static final String LOAD_BY_NAME = LOAD_BASE + " WHERE name = :name";

    @Override
    public Section findTheSameName(String name) {
        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(LOAD_BY_NAME,
                ImmutableMap.of("name", name), new SectionMapper()));
    }

    private static final String LOAD_BY_ENTITY_NAME = LOAD_BASE + " WHERE entity_name = :entity_name";

    @Override
    public Section findTheSameEntityName(String entityName) {
        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(LOAD_BY_ENTITY_NAME,
                ImmutableMap.of("entity_name", entityName), new SectionMapper()));
    }

    private static class SectionMapper implements RowMapper<Section> {

        @Override
        public Section mapRow(ResultSet rs, int rowNum) throws SQLException {
            SectionType type = SectionType.valueOfCode(rs.getInt("type"));
            Section section = new Section(rs.getLong("id"), type);
            section.setIndex(rs.getInt("index_number"));
            section.setName(rs.getString("name"));
            section.setEntityName(rs.getString("entity_name"));
            section.setVisible(rs.getBoolean("visible"));
            section.setText(rs.getString("section_text"));
            return section;
        }
    }
}
