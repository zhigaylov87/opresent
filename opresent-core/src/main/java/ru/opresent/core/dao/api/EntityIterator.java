package ru.opresent.core.dao.api;

/**
 * Iterator must be closed after using, typical usage:
 *
 * <pre>
 *     try (EntityIterator entityIterator = createEntityIterator()) {
 *
 *     }
 * </pre>
 *
 * User: artem
 * Date: 16.07.14
 * Time: 0:23
 */
public interface EntityIterator<E> extends AutoCloseable {

    boolean hasNext();

    E next();

    int getTotalCount();

    /**
     * Override for non exception signature
     */
    void close();

}
