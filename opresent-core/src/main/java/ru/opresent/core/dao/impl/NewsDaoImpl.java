package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.NewsDao;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.util.NullableImmutableMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:14
 */
@Component("newsDao")
public class NewsDaoImpl implements NewsDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO news_item(news_date, news_text, index_number, visible) " +
                    "VALUES(:news_date, :news_text, :index_number, :visible)";

    @Override
    public long save(NewsItem item) {
        Map<String, Object> params = NullableImmutableMap.<String, Object>builder()
                .put("news_date", item.getDate())
                .put("news_text", item.getText())
                .put("index_number", item.getIndex())
                .put("visible", item.isVisible())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int rows = jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        if (rows != 1) {
            throw new CoreException("News item not saved!");
        }
        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE = "UPDATE news_item " +
            "SET news_date = :news_date, news_text = :news_text, visible = :visible " +
            "WHERE id = :id";

    @Override
    public void update(NewsItem item) {
        int rows = jdbcTemplate.update(UPDATE, NullableImmutableMap.<String, Object>builder()
                .put("news_date", item.getDate())
                .put("news_text", item.getText())
                .put("visible", item.isVisible())
                .put("id", item.getId())
                .build());

        if (rows != 1) {
            throw new CoreException("News item not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM news_item WHERE id = :id";

    @Override
    public void delete(long itemId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", itemId));
        if (rows != 1) {
            throw new CoreException("News item not deleted!");
        }
    }

    private static final String LOAD_ALL =
            "SELECT id, news_date, news_text, index_number, visible FROM news_item ORDER BY index_number";

    @Override
    public List<NewsItem> loadAll() {
        return jdbcTemplate.query(LOAD_ALL, new RowMapper<NewsItem>() {
            @Override
            public NewsItem mapRow(ResultSet rs, int rowNum) throws SQLException {
                NewsItem item = new NewsItem(rs.getLong("id"));
                item.setDate(rs.getDate("news_date"));
                item.setText(rs.getString("news_text"));
                item.setIndex(rs.getInt("index_number"));
                item.setVisible(rs.getBoolean("visible"));
                return item;
            }
        });
    }
}
