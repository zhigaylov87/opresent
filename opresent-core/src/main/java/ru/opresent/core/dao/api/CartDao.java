package ru.opresent.core.dao.api;

import java.util.Set;

/**
 * User: artem
 * Date: 30.08.15
 * Time: 4:57
 */
public interface CartDao {

    /**
     * Add product must be idempotent - if product already in cart its OK
     * @param trackingId trackingId
     * @param productId productId
     * @return {@code true} if product added or {@code false} if product already in cart
     */
    boolean addProduct(String trackingId, long productId);

    Set<Long> getProductsIds(String trackingId);

    void removeProduct(String trackingId, long productId);

    void clearCart(String trackingId);

    void removeProductFromAllCarts(long productId);

}
