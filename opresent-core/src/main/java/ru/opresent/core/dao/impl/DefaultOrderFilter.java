package ru.opresent.core.dao.impl;

import ru.opresent.core.domain.order.OrderFilter;
import ru.opresent.core.domain.order.OrderStatus;
import ru.opresent.core.util.FormatUtils;

import java.io.Serializable;

/**
 * User: artem
 * Date: 04.03.15
 * Time: 3:05
 */
public class DefaultOrderFilter implements OrderFilter, Serializable {

    private Long orderId;
    private OrderStatus status;
    private String customerFullName;

    public DefaultOrderFilter() {
    }

    public DefaultOrderFilter(OrderFilter filter) {
        this.orderId = filter.getOrderId();
        this.status = filter.getStatus();
        this.customerFullName = filter.getCustomerFullName();
    }

    @Override
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    @Override
    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    @Override
    public String toString() {
        return "DefaultOrderFilter {" +
                "orderId: " + orderId +
                ", status: " + status +
                ", customerFullName: " + FormatUtils.quotes(customerFullName) +
                "}";
    }
}
