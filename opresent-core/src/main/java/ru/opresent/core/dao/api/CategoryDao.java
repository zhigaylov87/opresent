package ru.opresent.core.dao.api;

import ru.opresent.core.domain.Category;

import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 23:37
 */
public interface CategoryDao {

    /**
     * @param category category
     * @return saved category id
     */
    long save(Category category);

    void update(Category category);

    void delete(long categoryId);

    List<Category> findAll();

    Map<Long, Integer> getCategoryIdToProductsCount();

    boolean isUsedForProduct(long categoryId);

    boolean isExistsWithId(long categoryId);
}