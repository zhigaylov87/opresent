package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.QueryParamsCreator;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.ProductDao;
import ru.opresent.core.dao.api.ProductKeywordDao;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.Color;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.NullableImmutableMap;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 23:07
 */
@Component("productDao")
public class ProductDaoImpl implements ProductDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "dataSource")
    private DataSource dataSource;

    @Resource(name = "productBaseMapper")
    private ProductBaseMapper productBaseMapper;

    @Resource(name = "productKeywordDao")
    private ProductKeywordDao productKeywordDao;

    public static final String LOAD_BASE_FIELDS =
            "p.id, livemaster_id, product_type_id, name, description, visible, price, " +
            "status, status_note, production_time, product_label_id, product_label_note, " +
            "size_id, size_text, " +
            "preview_id, img.format AS preview_format, img.image_use AS preview_use, img.has_copyright AS preview_has_copyright, " +
            "modify_time, index_number ";

    private static final String LOAD_BASE_PATTERN =
            "SELECT " + LOAD_BASE_FIELDS +
                    "FROM product p " +
                    "INNER JOIN image img " +
                    "ON p.preview_id = img.id ";

    private static final String LOAD_BASE_ONE = LOAD_BASE_PATTERN + "WHERE p.id = :id";

    @Override
    public Product loadBase(long productId) {
        return CollectionUtils.getSingleOrNull(jdbcTemplate
                .query(LOAD_BASE_ONE, ImmutableMap.of("id", productId), productBaseMapper));
    }

    private static final String LOAD_ID_BY_LIVEMASTER_ID = "SELECT id FROM product WHERE livemaster_id = :livemaster_id";

    @Override
    public Long loadProductIdByLivemasterId(String livemasterId) {
        List<Long> ids = jdbcTemplate
                .queryForList(LOAD_ID_BY_LIVEMASTER_ID, ImmutableMap.of("livemaster_id", livemasterId), Long.class);
        return CollectionUtils.getSingleOrNull(ids);
    }

    private static final String LOAD_BASE_SEVERAL = LOAD_BASE_PATTERN + "WHERE p.id IN(:ids)";

    @Override
    public Set<Product> loadBase(Set<Long> productsIds) {
        List<Product> products = jdbcTemplate.query(
                LOAD_BASE_SEVERAL, ImmutableMap.of("ids", productsIds), productBaseMapper);
        return new HashSet<>(products);
    }

    private static final String LOAD_VIEWS =
            "SELECT p2v.image_id, img.format, img.image_use, img.has_copyright FROM products2views p2v " +
            "INNER JOIN image img ON img.id = p2v.image_id " +
            "WHERE p2v.product_id = :product_id ORDER BY p2v.view_index ASC";

    @Override
    public List<Image> loadViews(long productId) {
        return jdbcTemplate.query(LOAD_VIEWS,
                ImmutableMap.of("product_id", productId),
                new RowMapper<Image>() {
                    @Override
                    public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new Image(
                                rs.getLong("image_id"),
                                rs.getString("format"),
                                ImageUse.valueOf(rs.getInt("image_use")),
                                HasCopyrightOption.valueOf(rs.getInt("has_copyright")));
                    }
                });
    }

    private static final String LOAD_COLORS_IDS =
            "SELECT c.id FROM color c " +
                    "INNER JOIN products2colors p2c " +
                    "ON p2c.color_id = c.id AND p2c.product_id = :product_id";

    @Override
    public Set<Long> loadColorsIds(long productId) {
        Map<String, ?> params = ImmutableMap.of("product_id", productId);

        List<Long> colorsIds = jdbcTemplate
                .queryForList(LOAD_COLORS_IDS, new MapSqlParameterSource(params), Long.class);

        return new HashSet<>(colorsIds);
    }

    private static final String LOAD_CATEGORIES_IDS =
            "SELECT category_id FROM products2categories WHERE product_id = :product_id";

    @Override
    public Set<Long> loadCategoriesIds(long productId) {
        return new HashSet<>(jdbcTemplate.queryForList(
                LOAD_CATEGORIES_IDS, ImmutableMap.of("product_id", productId), Long.class));
    }

    private static final String SAVE_BASE =
            "INSERT INTO product(livemaster_id, product_type_id, name, description, visible, price, " +
                                "status, status_note, production_time, product_label_id, product_label_note, " +
                                "size_id, size_text, preview_id, modify_time, index_number) " +
            "VALUES(:livemaster_id, :product_type_id, :name, :description, :visible, :price, " +
                    ":status, :status_note, :production_time, :product_label_id, :product_label_note, " +
                    ":size_id, :size_text, :preview_id, :modify_time, :index_number)";

    @Override
    public long saveBase(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate
                .update(SAVE_BASE, new MapSqlParameterSource(createProductBaseParams(product, true)), keyHolder);
        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE_BASE =
            "UPDATE product SET livemaster_id = :livemaster_id, product_type_id = :product_type_id, name = :name, description = :description, " +
                    "visible = :visible, price = :price, status = :status, status_note = :status_note, " +
                    "production_time = :production_time, product_label_id = :product_label_id, " +
                    "product_label_note = :product_label_note, size_id = :size_id, size_text = :size_text, " +
                    "preview_id = :preview_id, modify_time = :modify_time WHERE id = :id";

    @Override
    public void updateBase(Product product) {
        Map<String, Object> params = new HashMap<>(createProductBaseParams(product, false));
        params.put("id", product.getId());

        int rows = jdbcTemplate.update(UPDATE_BASE, params);
        if (rows != 1) {
            throw new CoreException("Product not updated!");
        }
    }

    /**
     * @param product product
     * @param save {@code true} if params for product save
     * @return immutable map of product base params(without id)
     */
    private Map<String, Object> createProductBaseParams(Product product, boolean save) {
        NullableImmutableMap.NullableBuilder<String, Object> builder = NullableImmutableMap.<String, Object>builder()
                .put("livemaster_id", product.getLivemasterId())
                .put("product_type_id", product.getType().getId())
                .put("name", product.getName())
                .put("description", product.getDescription())
                .put("visible", product.isVisible())
                .put("price", product.getPrice())
                .put("status", product.getStatus().getCode())
                .put("status_note", product.getStatusNote())
                .put("production_time", product.getProductionTime())
                .put("product_label_id", product.getLabel() == null ? null : product.getLabel().getId())
                .put("product_label_note", product.getLabelNote())
                .put("size_id", product.getSize() == null ? null : product.getSize().getId())
                .put("size_text", product.getSizeText())
                .put("preview_id", product.getPreview().getId())
                .put("modify_time", new Timestamp(product.getModifyTime().getTime()));

        if (save) {
            builder.put("index_number", product.getIndex());
        }
        return builder.build();
    }

    private static final String SAVE_VIEW_LINK =
            "INSERT INTO products2views(product_id, image_id, view_index) " +
                    "VALUES(:product_id, :image_id, :view_index)";

    @Override
    public void saveViewsLinks(final long productId, List<Image> views) {
        DaoUtils.batchUpdate(jdbcTemplate, SAVE_VIEW_LINK, views, new QueryParamsCreator<Image>() {
            @Override
            public Map<String, ?> createParams(Image view, int viewIndex) {
                return ImmutableMap.of(
                        "product_id", productId,
                        "image_id", view.getId(),
                        "view_index", viewIndex);
            }
        });
    }

    private static final String DELETE_VIEWS_LINKS = "DELETE FROM products2views WHERE product_id = :product_id";

    @Override
    public void deleteViewsLinks(long productId) {
        jdbcTemplate.update(DELETE_VIEWS_LINKS, ImmutableMap.of("product_id", productId));
    }

    private static final String SAVE_COLOR_LINK =
            "INSERT INTO products2colors(product_id, color_id) VALUES(:product_id, :color_id)";

    @Override
    public void saveColorsLinks(final long productId, Set<Color> colors) {
        DaoUtils.batchUpdate(jdbcTemplate, SAVE_COLOR_LINK, new ArrayList<>(colors), new QueryParamsCreator<Color>() {
            @Override
            public Map<String, ?> createParams(Color color, int index) {
                return ImmutableMap.of("product_id", productId, "color_id", color.getId());
            }
        });
    }

    private static final String DELETE_COLORS_LINKS = "DELETE FROM products2colors WHERE product_id = :product_id";

    @Override
    public void deleteColorsLinks(long productId) {
        jdbcTemplate.update(DELETE_COLORS_LINKS, ImmutableMap.of("product_id", productId));
    }

    private static final String SAVE_CATEGORY_LINK =
            "INSERT INTO products2categories(product_id, category_id) VALUES(:product_id, :category_id)";

    @Override
    public void saveCategoriesLinks(final long productId, Set<Category> categories) {
        DaoUtils.batchUpdate(jdbcTemplate, SAVE_CATEGORY_LINK, new ArrayList<>(categories), new QueryParamsCreator<Category>() {
            @Override
            public Map<String, ?> createParams(Category category, int index) {
                return ImmutableMap.of("product_id", productId, "category_id", category.getId());
            }
        });
    }

    private static final String DELETE_CATEGORIES_LINKS = "DELETE FROM products2categories WHERE product_id = :product_id";

    public void deleteCategoriesLinks(long productId) {
        jdbcTemplate.update(DELETE_CATEGORIES_LINKS, ImmutableMap.of("product_id", productId));
    }

    private static final String DELETE_PRODUCT_BASE = "DELETE FROM product WHERE id = :id;";

    @Override
    public void deleteBase(long productId) {
        int rows = jdbcTemplate.update(DELETE_PRODUCT_BASE, ImmutableMap.of("id", productId));
        if (rows != 1) {
            throw new CoreException("Product with id = " + productId + " not deleted.");
        }
    }

    private static final String SAVE_DELETED_PRODUCT_LIVEMASTER_ID =
            "INSERT INTO deleted_product(livemaster_id) VALUES(:livemaster_id)";

    @Override
    public void saveDeletedProductLivemasterId(String livemasterId) {
        jdbcTemplate.update(SAVE_DELETED_PRODUCT_LIVEMASTER_ID,
                ImmutableMap.of("livemaster_id", livemasterId));
    }

    private static final String EXISTS_WITH_ID = "SELECT EXISTS(SELECT 1 FROM product WHERE id = :id)";

    @Override
    public boolean isExistsWithId(long productId) {
        return jdbcTemplate.queryForObject(EXISTS_WITH_ID, ImmutableMap.of("id", productId), Boolean.class);
    }

    private static final String FIND_MIN_INDEX_EXCLUDE_STATUS =
            "SELECT MIN(index_number) FROM product WHERE status <> :exclude_status";

    @Override
    public Integer findMinIndexExcludeStatus(ProductStatus excludeStatus) {
        return jdbcTemplate.queryForObject(FIND_MIN_INDEX_EXCLUDE_STATUS,
                ImmutableMap.of("exclude_status", excludeStatus.getCode()), Integer.class);
    }

    private static final String EXISTS_DELETED_WITH_LIVEMASTER_ID =
            "SELECT EXISTS(SELECT 1 FROM deleted_product WHERE livemaster_id = :livemaster_id)";

    @Override
    public boolean isDeletedWithLivemasterId(String livemasterId) {
        return jdbcTemplate.queryForObject(EXISTS_DELETED_WITH_LIVEMASTER_ID,
                ImmutableMap.of("livemaster_id", livemasterId), Boolean.class);
    }

    private static final String TOTAL_COUNT_VISIBLE = "SELECT COUNT(*) FROM product WHERE visible = " + DaoUtils.BOOLEAN_TRUE_NUMBER;
    private static final String LOAD_ALL_VISIBLE = LOAD_BASE_PATTERN + " WHERE visible = " + DaoUtils.BOOLEAN_TRUE_NUMBER;

    @Override
    public EntityIterator<Product> createEntityIterator() {
        return new BaseMySqlStreamingResultsEntityIterator<>(dataSource, TOTAL_COUNT_VISIBLE, LOAD_ALL_VISIBLE, new RowMapper<Product>() {
            @Override
            public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                Product product = productBaseMapper.mapRow(rs, rowNum);
                product.setKeywords(productKeywordDao.loadProductKeywords(product.getId()));
                return product;
            }
        });
    }
}