package ru.opresent.core.dao.api;

import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.pagination.IncomingMessagePage;
import ru.opresent.core.pagination.PageSettings;

/**
 * User: artem
 * Date: 23.11.14
 * Time: 0:54
 */
public interface IncomingMessageDao {

    long save(IncomingMessage message, String trackingId);

    IncomingMessage load(long messageId);

    int getNotReadedCount();

    void markReaded(long messageId);

    IncomingMessagePage load(PageSettings pageSettings);
}
