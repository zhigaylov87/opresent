package ru.opresent.core.dao.api;

import ru.opresent.core.domain.ProductType;

import java.util.List;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:31
 */
public interface ProductTypeDao {

    /**
     * @param productType product type
     * @return saved product type id
     */
    long save(ProductType productType);

    void update(ProductType productType);

    void delete(long productTypeId);

    List<ProductType> findAll();

    ProductType findTheSameName(String name);

    ProductType findTheSameEntityName(String entityName);

    boolean isUsedForProduct(long productTypeId);

    boolean isExistsWithId(long productTypeId);
}
