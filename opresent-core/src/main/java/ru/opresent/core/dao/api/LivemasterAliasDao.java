package ru.opresent.core.dao.api;

import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;

import java.util.Set;

/**
 * User: artem
 * Date: 30.11.14
 * Time: 22:09
 */
public interface LivemasterAliasDao {

    void save(LivemasterAliases aliases);

    void delete(long entityId, LivemasterAliasType type);

    LivemasterAliases load(long entityId, LivemasterAliasType type);

    Set<LivemasterAliases> load(LivemasterAliasType type);

    /**
     *
     * @param type type
     * @param alias alias
     * @return entity with defined type and alias or {@code null} if the same not exists
     */
    Long findEntityIdByAlias(LivemasterAliasType type, String alias);
}
