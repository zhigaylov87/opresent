package ru.opresent.core.dao.impl;

import org.apache.commons.lang3.StringUtils;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.Codeable;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.filter.StringFieldConstraint;
import ru.opresent.core.util.CollectionUtils;

import java.util.*;

import static ru.opresent.core.util.CollectionUtils.getIds;
import static ru.opresent.core.util.CollectionUtils.isNotEmpty;

/**
 * User: artem
 * Date: 04.03.15
 * Time: 1:51
 */
public class FilterQueryBuilder {

    private StringBuilder query = new StringBuilder();

    private Map<String, Object> params = new HashMap<>();

    private boolean whereConditionAdded = false;

    public FilterQueryBuilder() {
    }

    public FilterQueryBuilder(String base) {
        query.append(base);
    }

    public FilterQueryBuilder addJoin(String joinCondition, String paramName, Identifiable paramValue) {
        if (paramValue != null) {
            query.append(joinCondition);
            params.put(paramName, paramValue.getId());
        }
        return this;
    }

    public FilterQueryBuilder addJoin(String joinCondition, String paramName, Collection<? extends Identifiable> paramValue) {
        if (isNotEmpty(paramValue)) {
            query.append(joinCondition);
            params.put(paramName, getIds(paramValue));
        }
        return this;
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Number paramValue) {
        return paramValue == null ? this : addWhereCondition(whereCondition, paramName, paramValue);
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Boolean paramValue) {
        return paramValue == null ? this : addWhereCondition(whereCondition, paramName, paramValue);
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Identifiable paramValue) {
        return paramValue == null ? this : addWhereCondition(whereCondition, paramName, paramValue.getId());
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Codeable paramValue) {
        return paramValue == null ? this : addWhereCondition(whereCondition, paramName, paramValue.getCode());
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Set<? extends Codeable> paramValue) {
        return paramValue == null ? this : addWhereCondition(whereCondition, paramName, CollectionUtils.getCodes(paramValue));
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Date paramValue) {
        return addWhere(whereCondition, paramName, paramValue, false);
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Date paramValue, boolean useMillis) {
        return paramValue == null ? this :
                addWhereCondition(whereCondition, paramName, useMillis ? paramValue.getTime() : paramValue);
    }

    public FilterQueryBuilder addWhere(String whereCondition, String paramName, Collection<? extends Number> paramValue) {
        return paramValue == null ? this :
                addWhereCondition(whereCondition, paramName, paramValue);
    }

    public FilterQueryBuilder addWhereHasIgnoreCase(String whereCondition, String paramName, String paramValue) {
        return StringUtils.isEmpty(paramValue) ? this :
                addWhereCondition(whereCondition, paramName, "%" + paramValue.toUpperCase() + "%");
    }

    public FilterQueryBuilder addWhere(String fieldName, StringFieldConstraint constraint) {
        if (constraint == null || constraint.getCondition() == null) {
            return this;
        }
        StringBuilder whereCondition = new StringBuilder();
        String paramValue = constraint.getValue();
        if (constraint.isIgnoreCase()) {
            whereCondition.append("UPPER(").append(fieldName).append(")");
            paramValue = paramValue.toUpperCase();
        } else {
            whereCondition.append(fieldName);
        }

        switch (constraint.getCondition()) {
            case CONTAINS:
                whereCondition.append(" LIKE ");
                paramValue = "%" + paramValue + "%";
                break;

            case NOT_CONTAINS:
                whereCondition.append(" NOT LIKE ");
                paramValue = "%" + paramValue + "%";
                break;

            case STARTS_WITH:
                whereCondition.append(" LIKE ");
                paramValue = paramValue + "%";
                break;

            case ENDS_WITH:
                whereCondition.append(" LIKE ");
                paramValue = "%" + paramValue;
                break;

            case EQUALS:
                whereCondition.append(" = ");
                break;

            case NOT_EQUALS:
                whereCondition.append(" <> ");
                break;

            default:
                throw new CoreException("Unknown string field condition: " + constraint.getCondition());
        }
        whereCondition.append(":").append(fieldName);

        return addWhereCondition(whereCondition.toString(), fieldName, paramValue);
    }

    private FilterQueryBuilder addWhereCondition(String whereCondition, String paramName, Object paramValue) {
        query.append(whereConditionAdded ? "AND " : "WHERE ").append(whereCondition).append(" ");
        whereConditionAdded = true;
        params.put(paramName, Objects.requireNonNull(paramValue));
        return this;
    }

    public FilterQuery build() {
        return new FilterQuery(query.toString(), params);
    }
}