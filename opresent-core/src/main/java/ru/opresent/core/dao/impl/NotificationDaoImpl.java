package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.NotificationDao;
import ru.opresent.core.notification.Notification;
import ru.opresent.core.notification.NotificationStatus;
import ru.opresent.core.notification.NotificationSubjectType;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * User: artem
 * Date: 28.04.15
 * Time: 2:09
 */
@Component("notificationDao")
public class NotificationDaoImpl implements NotificationDao {

    private static final String SAVE =
            "INSERT INTO notification(subject_type, subject_id, status, status_time) " +
                    "VALUES(:subject_type, :subject_id, :status, :status_time)";

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "dataSource")
    private DataSource dataSource;

    @Override
    public long save(Notification notification) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("subject_type", notification.getSubjectType().getCode())
                .put("subject_id", notification.getSubjectId())
                .put("status", notification.getStatus().getCode())
                .put("status_time", notification.getStatusTime())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE =
            "UPDATE notification SET status = :status, status_time = :status_time WHERE id = :id";

    @Override
    public void update(Notification notification) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("status", notification.getStatus().getCode())
                .put("status_time", notification.getStatusTime())
                .put("id", notification.getId())
                .build();

        int rows = jdbcTemplate.update(UPDATE, params);
        if (rows != 1) {
            throw new CoreException("Notification not updated!");
        }
    }

    private static final String NEED_TO_SEND_WHERE =
            " WHERE status IN(" + NotificationStatus.NEW.getCode() + ", " + NotificationStatus.SEND_FAILED.getCode() + ")";

    private static final String NEED_TO_SEND_TOTAL_COUNT = "SELECT COUNT(*) FROM notification" + NEED_TO_SEND_WHERE;

    private static final String NEED_TO_SEND_LOAD =
            "SELECT id, subject_type, subject_id, status, status_time FROM notification" +
                    NEED_TO_SEND_WHERE + " ORDER BY id";

    @Override
    public EntityIterator<Notification> loadNeedToSendNotifications() {
        return new BaseMySqlStreamingResultsEntityIterator<>(dataSource, NEED_TO_SEND_TOTAL_COUNT, NEED_TO_SEND_LOAD,

                new RowMapper<Notification>() {
                    @Override
                    public Notification mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Notification notification = new Notification();

                        notification.setId(rs.getLong("id"));
                        notification.setSubjectType(NotificationSubjectType.valueOf(rs.getInt("subject_type")));
                        notification.setSubjectId(rs.getLong("subject_id"));
                        notification.setStatus(NotificationStatus.valueOf(rs.getInt("status")));
                        notification.setStatusTime(rs.getTimestamp("status_time"));

                        return notification;
                    }
        });
    }
}
