package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.CategoryDao;
import ru.opresent.core.domain.Category;
import ru.opresent.core.util.NullableImmutableMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 23:37
 */
@Component("categoryDao")
public class CategoryDaoImpl implements CategoryDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO category(name, entity_name, description, index_number) " +
            "VALUES(:name, :entity_name, :description, :index_number)";

    @Override
    public long save(Category category) {
        Map<String, Object> params = NullableImmutableMap.<String, Object>builder()
                .put("name", category.getName())
                .put("entity_name", category.getEntityName())
                .put("description", category.getDescription())
                .put("index_number", category.getIndex())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int rows = jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        if (rows != 1) {
            throw new CoreException("Category not saved!");
        }
        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE =
            "UPDATE category " +
                    "SET name = :name, entity_name = :entity_name, description = :description " +
                    "WHERE id = :id";

    @Override
    public void update(Category category) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("name", category.getName())
                .put("entity_name", category.getEntityName())
                .put("description", category.getDescription())
                .put("id", category.getId())
                .build();

        int rows = jdbcTemplate.update(UPDATE, params);
        if (rows != 1) {
            throw new CoreException("Category not updated [id: " + category.getId() + "]");
        }
    }

    private static final String FIND_ALL =
            "SELECT id, name, entity_name, description, index_number FROM category ORDER BY index_number;";

    @Override
    public List<Category> findAll() {
        return jdbcTemplate.query(FIND_ALL, new RowMapper<Category>() {
            @Override
            public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                Category category = new Category(rs.getLong("id"));
                category.setName(rs.getString("name"));
                category.setEntityName(rs.getString("entity_name"));
                category.setDescription(rs.getString("description"));
                category.setIndex(rs.getInt("index_number"));
                return category;
            }
        });
    }

    private static final String CATEGORY_ID_TO_PRODUCTS_COUNT =
            "SELECT category_id, COUNT(*) AS products_count FROM products2categories GROUP BY category_id;";

    @Override
    public Map<Long, Integer> getCategoryIdToProductsCount() {
        return jdbcTemplate.query(CATEGORY_ID_TO_PRODUCTS_COUNT, new MapSqlParameterSource(),
                new ResultSetExtractor<Map<Long, Integer>>() {
                    @Override
                    public Map<Long, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
                        Map<Long, Integer> idToCount = new HashMap<>();
                        while (rs.next()) {
                            idToCount.put(rs.getLong("category_id"), rs.getInt("products_count"));
                        }
                        return idToCount;
                    }
                });
    }

    private static final String USED_FOR_PRODUCT =
            "SELECT EXISTS(SELECT 1 FROM products2categories WHERE category_id = :category_id)";

    @Override
    public boolean isUsedForProduct(long categoryId) {
        return jdbcTemplate.queryForObject(USED_FOR_PRODUCT, ImmutableMap.of("category_id", categoryId), Boolean.class);
    }

    private static final String DELETE = "DELETE FROM category WHERE id = :id";

    @Override
    public void delete(long categoryId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", categoryId));
        if (rows != 1) {
            throw new CoreException("Category not deleted!");
        }
    }

    private static final String EXISTS_WITH_ID = "SELECT EXISTS(SELECT 1 FROM category WHERE id = :id)";

    @Override
    public boolean isExistsWithId(long categoryId) {
        return jdbcTemplate.queryForObject(EXISTS_WITH_ID, ImmutableMap.of("id", categoryId), Boolean.class);
    }
}
