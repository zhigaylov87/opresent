package ru.opresent.core.dao;

import java.util.Map;

/**
 * User: artem
 * Date: 24.08.14
 * Time: 18:17
 */
public interface QueryParamsCreator<T> {

    public Map<String, ?> createParams(T forObject, int index);

}
