package ru.opresent.core.dao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.opresent.core.CoreException;

import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 30.01.16
 * Time: 2:30
 */
public final class DaoUtils {

    public static final int BOOLEAN_FALSE_NUMBER = 0;
    public static final int BOOLEAN_TRUE_NUMBER = 1;

    private DaoUtils() {
    }

    public static <T> int[] batchUpdate(NamedParameterJdbcTemplate jdbcTemplate, String sql,
                                    List<T> batchObjects, QueryParamsCreator<T> paramsCreator) {
        int count = batchObjects.size();
        SqlParameterSource[] params = new SqlParameterSource[count];

        for (int i = 0; i < count; i++) {
            params[i] = new MapSqlParameterSource(paramsCreator.createParams(batchObjects.get(i), i));
        }

        return jdbcTemplate.batchUpdate(sql, params);
    }

    public static void checkSingleRowUpdated(int... rows) {
        for(int row : rows) {
            if (row != 1) {
                throw new CoreException("Not a single row updated. Rows: " + Arrays.toString(rows));
            }
        }
    }

}
