package ru.opresent.core.dao.impl;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * User: artem
 * Date: 04.03.15
 * Time: 1:25
 */
public class FilterQuery {

    private String query;

    private Map<String, Object> params;

    public FilterQuery(String query, Map<String, Object> params) {
        this.query = Objects.requireNonNull(query);
        this.params = Collections.unmodifiableMap(params);
    }

    public String getQuery() {
        return query;
    }

    public Map<String, Object> getParams() {
        return params;
    }
}
