package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.api.IncomingMessageDao;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.message.incoming.IncomingMessageSubjectType;
import ru.opresent.core.pagination.IncomingMessagePage;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 23.11.14
 * Time: 0:55
 */
@Component("incomingMessageDao")
public class IncomingMessageDaoImpl implements IncomingMessageDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO incoming_message(" +
                    "subject_id, subject_type, customer_name, customer_email_or_phone, message_text, readed, " +
                    "creation_time, tracking_id) " +
                    "VALUES(:subject_id, :subject_type, :customer_name, :customer_email_or_phone, :message_text, :readed, " +
                    ":creation_time, :tracking_id)";

    @Override
    public long save(IncomingMessage message, String trackingId) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("subject_id", message.getSubjectId())
                .put("subject_type", message.getSubjectType().getCode())
                .put("customer_name", message.getCustomerName())
                .put("customer_email_or_phone", message.getCustomerEmailOrPhone())
                .put("message_text", message.getMessageText())
                .put("readed", message.isReaded())
                .put("creation_time", message.getCreationTime())
                .put("tracking_id", trackingId)
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);
        return keyHolder.getKey().longValue();
    }

    private static final String ALL_FIELDS =
            "id, subject_id, subject_type, customer_name, customer_email_or_phone, message_text, readed, creation_time, reading_time";

    private static final String LOAD_BY_ID = "SELECT " + ALL_FIELDS + " FROM incoming_message WHERE id = :id";

    @Override
    public IncomingMessage load(long messageId) {
        return CollectionUtils.getSingle(
                jdbcTemplate.query(LOAD_BY_ID, ImmutableMap.of("id", messageId), new IncomingMessageMapper()));
    }

    private static final String NOT_READED_COUNT =
            "SELECT COUNT(*) FROM incoming_message WHERE readed = " + DaoUtils.BOOLEAN_FALSE_NUMBER;

    @Override
    public int getNotReadedCount() {
        return jdbcTemplate.queryForObject(NOT_READED_COUNT, EmptySqlParameterSource.INSTANCE, Integer.class);
    }

    private static final String MARK_READED =
            "UPDATE incoming_message SET readed = " + DaoUtils.BOOLEAN_TRUE_NUMBER + " WHERE id = :id";

    @Override
    public void markReaded(long messageId) {
        int rows = jdbcTemplate.update(MARK_READED, ImmutableMap.of("id", messageId));
        if (rows == 0) {
            throw new CoreException("Incoming message with id = " + messageId + " not marked as readed");
        }
    }

    @Override
    public IncomingMessagePage load(PageSettings pageSettings) {
        return new IncomingMessagePage(pageSettings, loadPage(pageSettings), getTotalCount(), getNotReadedCount());
    }

    private static final String TOTAL_COUNT = "SELECT COUNT(*) FROM incoming_message";

    private int getTotalCount() {
        return jdbcTemplate.queryForObject(TOTAL_COUNT, EmptySqlParameterSource.INSTANCE, Integer.class);
    }

    private static final String PAGE =
            "SELECT " + ALL_FIELDS +
            " FROM incoming_message ORDER BY readed ASC, creation_time DESC LIMIT :offset, :page_size";

    private List<IncomingMessage> loadPage(PageSettings pageSettings) {
        return jdbcTemplate.query(PAGE,
                ImmutableMap.of(
                        "offset", pageSettings.getOffset(),
                        "page_size", pageSettings.getPageSize()), new IncomingMessageMapper());
    }

    private static class IncomingMessageMapper implements RowMapper<IncomingMessage> {

        @Override
        public IncomingMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
            IncomingMessage m = new IncomingMessage();

            m.setId(rs.getLong("id"));
            m.setSubjectId(rs.getLong("subject_id"));
            m.setSubjectType(IncomingMessageSubjectType.valueOfCode(rs.getInt("subject_type")));
            m.setCustomerName(rs.getString("customer_name"));
            m.setCustomerEmailOrPhone(rs.getString("customer_email_or_phone"));
            m.setMessageText(rs.getString("message_text"));
            m.setReaded(rs.getBoolean("readed"));
            m.setCreationTime(rs.getTimestamp("creation_time"));
            m.setReadingTime(rs.getTimestamp("reading_time"));

            return m;
        }
    }
}
