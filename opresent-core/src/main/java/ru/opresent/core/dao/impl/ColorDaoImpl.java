package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.ColorDao;
import ru.opresent.core.domain.Color;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:21
 */
@Component("colorDao")
public class ColorDaoImpl implements ColorDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE = "INSERT INTO color(rgb) VALUES(:rgb)";

    @Override
    public long save(Color color) {
        Map<String, Integer> params = ImmutableMap.of("rgb", color.getRgb());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE = "UPDATE color SET rgb = :rgb WHERE id = :id";

    @Override
    public void update(Color color) {
        Map<String, Object> params = ImmutableMap.<String, Object>of("rgb", color.getRgb(), "id", color.getId());

        int rows = jdbcTemplate.update(UPDATE, params);
        if (rows != 1) {
            throw new CoreException("Color not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM color WHERE id = :id";

    @Override
    public void delete(long colorId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", colorId));
        if (rows != 1) {
            throw new CoreException("Color not deleted!");
        }
    }

    private static final String FIND_ALL = "SELECT id, rgb FROM color";

    @Override
    public List<Color> findAll() {
        return jdbcTemplate.query(FIND_ALL, new ColorMapper());
    }

    private static final String FIND_THE_SAME = "SELECT id, rgb FROM color WHERE rgb = :rgb";

    @Override
    public Color findTheSame(int rgb) {
        Map<String, Integer> params = ImmutableMap.of("rgb", rgb);
        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(FIND_THE_SAME, params, new ColorMapper()));
    }

    private static final String USED_FOR_PRODUCT = "SELECT COUNT(*) FROM products2colors WHERE color_id = :color_id";

    @Override
    public boolean isUsedForProduct(long colorId) {
        int count = jdbcTemplate.queryForObject(USED_FOR_PRODUCT,
                ImmutableMap.of("color_id", colorId), Integer.class);

        return count != 0;
    }

    private static class ColorMapper implements RowMapper<Color> {
        @Override
        public Color mapRow(ResultSet rs, int rowNum) throws SQLException {
            Color color = new Color(rs.getLong("id"));
            color.setRgb(rs.getInt("rgb"));
            return color;
        }
    }
}
