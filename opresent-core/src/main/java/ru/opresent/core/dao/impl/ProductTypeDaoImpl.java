package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.ProductTypeDao;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.domain.image.ProductTypeImageName;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.NullableImmutableMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:31
 */
@Component("productTypeDao")
public class ProductTypeDaoImpl implements ProductTypeDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO product_type(name, entity_name, image_id, description, index_number) " +
            "VALUES(:name, :entity_name, :image_id, :description, :index_number)";

    @Override
    public long save(ProductType productType) {
        Map<String, Object> params = NullableImmutableMap.<String, Object>builder()
                .put("name", productType.getName())
                .put("entity_name", productType.getEntityName())
                .put("image_id", productType.getImage().getId())
                .put("description", productType.getDescription())
                .put("index_number", productType.getIndex())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int rows = jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        if (rows != 1) {
            throw new CoreException("Product type not saved!");
        }
        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE = "UPDATE product_type " +
                    "SET name = :name, entity_name = :entity_name, image_id = :image_id, description = :description " +
                    "WHERE id = :id";

    @Override
    public void update(ProductType productType) {
        int rows = jdbcTemplate.update(UPDATE, NullableImmutableMap.<String, Object>builder()
                .put("name", productType.getName())
                .put("entity_name", productType.getEntityName())
                .put("image_id", productType.getImage().getId())
                .put("description", productType.getDescription())
                .put("id", productType.getId())
                .build());

        if (rows != 1) {
            throw new CoreException("Product type not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM product_type WHERE id = :id";

    @Override
    public void delete(long productTypeId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", productTypeId));
        if (rows != 1) {
            throw new CoreException("Product type not deleted!");
        }
    }

    private static final String LOAD_ALL_FIELDS =
            "SELECT p_type.id AS p_type_id, name, entity_name, description, index_number, " +
                    "img.id AS img_id, img.format AS img_format, img.image_use AS img_use, img.has_copyright AS img_has_copyright " +
                    "FROM product_type p_type " +
                    "INNER JOIN image img ON p_type.image_id = img.id";

    private static final String FIND_ALL = LOAD_ALL_FIELDS + " ORDER BY index_number";

    @Override
    public List<ProductType> findAll() {
        return jdbcTemplate.query(FIND_ALL, new ProductTypeMapper());
    }

    private static final String FIND_THE_SAME_NAME = LOAD_ALL_FIELDS + " WHERE name = :name";

    @Override
    public ProductType findTheSameName(String name) {
        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(
                FIND_THE_SAME_NAME, ImmutableMap.of("name", name), new ProductTypeMapper()));
    }

    private static final String FIND_THE_SAME_ENTITY_NAME = LOAD_ALL_FIELDS + " WHERE entity_name = :entity_name";

    @Override
    public ProductType findTheSameEntityName(String entityName) {
        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(
                FIND_THE_SAME_ENTITY_NAME, ImmutableMap.of("entity_name", entityName), new ProductTypeMapper()));
    }

    private static final String USED_FOR_PRODUCT = "SELECT EXISTS(SELECT 1 FROM product WHERE product_type_id = :product_type_id)";

    @Override
    public boolean isUsedForProduct(long productTypeId) {
        return jdbcTemplate.queryForObject(USED_FOR_PRODUCT, ImmutableMap.of("product_type_id", productTypeId), Boolean.class);
    }

    private static final String EXISTS_WITH_ID = "SELECT EXISTS(SELECT 1 FROM product_type WHERE id = :id)";

    @Override
    public boolean isExistsWithId(long productTypeId) {
        return jdbcTemplate.queryForObject(
                EXISTS_WITH_ID, ImmutableMap.of("id", productTypeId), Boolean.class);
    }

    private static class ProductTypeMapper implements RowMapper<ProductType> {
        @Override
        public ProductType mapRow(ResultSet rs, int rowNum) throws SQLException {
            ProductType productType = new ProductType(rs.getLong("p_type_id"));
            productType.setName(rs.getString("name"));
            productType.setEntityName(rs.getString("entity_name"));
            productType.setDescription(rs.getString("description"));
            productType.setIndex(rs.getInt("index_number"));

            Image image = new Image(rs.getLong("img_id"),
                    rs.getString("img_format"),
                    ImageUse.valueOf(rs.getInt("img_use")),
                    HasCopyrightOption.valueOf(rs.getInt("img_has_copyright")));
            image.setImageName(new ProductTypeImageName(productType));
            productType.setImage(image);

            return productType;
        }
    }
}
