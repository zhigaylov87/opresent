package ru.opresent.core.dao.api;

import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductStatusInfo;

import java.util.Map;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 14:54
 */
public interface ProductStatusDao {

    void save(ProductStatusInfo info);

    Map<ProductStatus, ProductStatusInfo> loadAll();

}
