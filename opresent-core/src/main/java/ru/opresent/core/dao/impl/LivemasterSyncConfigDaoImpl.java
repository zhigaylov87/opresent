package ru.opresent.core.dao.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.api.LivemasterSyncConfigDao;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * User: artem
 * Date: 09.07.2017
 * Time: 0:55
 */
@Component("livemasterSyncConfigDao")
public class LivemasterSyncConfigDaoImpl implements LivemasterSyncConfigDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "jsonMapper")
    private ObjectMapper jsonMapper;

    private static final String DELETE = "DELETE FROM livemaster_sync_config";

    private static final String INSERT = "INSERT INTO livemaster_sync_config(json) VALUES(:json)";

    @Override
    public void save(LivemasterSyncConfig config) {
        String json;
        try {
            json = jsonMapper.writeValueAsString(config);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        jdbcTemplate.update(DELETE, EmptySqlParameterSource.INSTANCE);
        jdbcTemplate.update(INSERT, ImmutableMap.of("json", json));
    }

    private static final String SELECT = "SELECT json FROM livemaster_sync_config";

    @Override
    public LivemasterSyncConfig load() {
        List<LivemasterSyncConfig> configList = jdbcTemplate.query(SELECT, new RowMapper<LivemasterSyncConfig>() {
            @Override
            public LivemasterSyncConfig mapRow(ResultSet rs, int rowNum) throws SQLException {
                String json = rs.getString("json");
                try {
                    return jsonMapper.readValue(json, LivemasterSyncConfig.class);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        return configList.isEmpty() ? null : configList.get(0);
    }
}
