package ru.opresent.core.dao.api;

import ru.opresent.core.notification.Notification;
import ru.opresent.core.notification.NotificationStatus;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:31
 */
public interface NotificationDao {

    long save(Notification notification);

    /**
     * Update {@link Notification#getStatus()} and {@link Notification#getStatusTime()} for notification
     * @param notification notification
     */
    void update(Notification notification);

    /**
     * @return iterator for notifications in statuses {@link NotificationStatus#NEW} and {@link NotificationStatus#SEND_FAILED}
     */
    EntityIterator<Notification> loadNeedToSendNotifications();

}
