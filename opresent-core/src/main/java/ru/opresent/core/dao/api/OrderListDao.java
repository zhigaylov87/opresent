package ru.opresent.core.dao.api;

import ru.opresent.core.domain.order.OrderFilter;
import ru.opresent.core.pagination.OrderPage;
import ru.opresent.core.pagination.PageSettings;

/**
 * User: artem
 * Date: 01.03.15
 * Time: 23:43
 */
public interface OrderListDao {

    OrderPage load(OrderFilter filter, PageSettings pageSettings);

}
