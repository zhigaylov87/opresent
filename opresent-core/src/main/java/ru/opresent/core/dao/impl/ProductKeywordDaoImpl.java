package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.QueryParamsCreator;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.ProductKeywordDao;
import ru.opresent.core.domain.ProductKeyword;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * User: artem
 * Date: 18.12.14
 * Time: 17:25
 */
@Component("productKeywordDao")
public class ProductKeywordDaoImpl implements ProductKeywordDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "dataSource")
    private DataSource dataSource;

    private static final String ADD_TO_THESAURUS = "INSERT INTO product_keyword_thesaurus(keyword) VALUES(:keyword)";

    @Override
    public void addToThesaurus(ProductKeyword keyword) {
        jdbcTemplate.update(ADD_TO_THESAURUS, ImmutableMap.of("keyword", keyword.getKeyword()));
    }

    private static final String REMOVE_FROM_THESAURUS = "DELETE FROM product_keyword_thesaurus WHERE keyword = :keyword";

    @Override
    public void removeFromThesaurus(ProductKeyword keyword) {
        int rows = jdbcTemplate.update(
                REMOVE_FROM_THESAURUS, ImmutableMap.of("keyword", keyword.getKeyword()));
        if (rows == 0) {
            throw new CoreException("Product keyword not deleted: " + keyword);
        }
    }

    private static final String LOAD_THESAURUS = "SELECT keyword FROM product_keyword_thesaurus ORDER BY keyword ASC";

    @Override
    public List<ProductKeyword> loadThesaurus() {
        return jdbcTemplate.query(LOAD_THESAURUS, new ProductKeywordRowMapper());
    }

    private static final String LOAD_TOP =
            "SELECT k2c.keyword FROM (" +
                    "SELECT keyword, COUNT(keyword) AS keyword_count " +
                    "FROM products2keywords GROUP BY keyword " +
                    "ORDER BY keyword_count DESC LIMIT :top_count OFFSET 0) k2c";

    @Override
    public List<ProductKeyword> loadTop(int count) {
        return jdbcTemplate.query(LOAD_TOP, ImmutableMap.of("top_count", count), new ProductKeywordRowMapper());
    }

    private static final String SAVE_FOR_PRODUCT =
            "INSERT INTO products2keywords(product_id, keyword) VALUES(:product_id, :keyword)";

    @Override
    public void saveProductKeywords(final long productId, Set<ProductKeyword> keywords) {
        deleteProductKeywords(productId);

        if (!keywords.isEmpty()) {
            DaoUtils.batchUpdate(jdbcTemplate, SAVE_FOR_PRODUCT, new ArrayList<>(keywords), new QueryParamsCreator<ProductKeyword>() {
                @Override
                public Map<String, ?> createParams(ProductKeyword keyword, int index) {
                    return ImmutableMap.of(
                            "product_id", productId,
                            "keyword", keyword.getKeyword());
                }
            });
        }
    }

    private static final String LOAD_FOR_PRODUCT =
            "SELECT keyword FROM products2keywords WHERE product_id = :product_id";

    @Override
    public Set<ProductKeyword> loadProductKeywords(long productId) {
        return new HashSet<>(jdbcTemplate.query(LOAD_FOR_PRODUCT,
                ImmutableMap.of("product_id", productId), new ProductKeywordRowMapper()));
    }

    private static final String DELETE_FOR_PRODUCT = "DELETE FROM products2keywords WHERE product_id = :product_id";

    @Override
    public void deleteProductKeywords(long productId) {
        jdbcTemplate.update(DELETE_FOR_PRODUCT, ImmutableMap.of("product_id", productId));
    }

    private static final String LOAD_ALL_USED_KEYWORDS =
            "SELECT DISTINCT keyword FROM products2keywords p2k " +
            "INNER JOIN product p ON p2k.product_id = p.id " +
            "WHERE p.visible = " + DaoUtils.BOOLEAN_TRUE_NUMBER + " ORDER BY keyword ASC";

    private static final String TOTAL_COUNT_USED_KEYWORDS =
            "SELECT COUNT(*) FROM (" + LOAD_ALL_USED_KEYWORDS + ") used_keywords";

    @Override
    public EntityIterator<ProductKeyword> createUsedProductKeywordsIterator() {
        return new BaseMySqlStreamingResultsEntityIterator<>(dataSource,
                TOTAL_COUNT_USED_KEYWORDS, LOAD_ALL_USED_KEYWORDS, new ProductKeywordRowMapper());
    }

    private static class ProductKeywordRowMapper implements RowMapper<ProductKeyword> {
        @Override
        public ProductKeyword mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ProductKeyword(rs.getString("keyword"));
        }
    }
}