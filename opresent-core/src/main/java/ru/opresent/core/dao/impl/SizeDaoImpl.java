package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.SizeDao;
import ru.opresent.core.domain.Size;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:04
 */
@Component("sizeDao")
public class SizeDaoImpl implements SizeDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE = "INSERT INTO size(width, height) VALUES(:width, :height)";

    @Override
    public long save(Size size) {
        Map<String, Object> params = ImmutableMap.<String, Object>of(
                "width", size.getWidth(),
                "height", size.getHeight());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE = "UPDATE size SET width = :width, height = :height WHERE id = :id";

    @Override
    public void update(Size size) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("width", size.getWidth())
                .put("height", size.getHeight())
                .put("id", size.getId())
                .build();

        int rows = jdbcTemplate.update(UPDATE, params);
        if (rows != 1) {
            throw new CoreException("Size not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM size WHERE id = :id";

    @Override
    public void delete(long sizeId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", sizeId));
        if (rows != 1) {
            throw new CoreException("Size not deleted!");
        }
    }

    private static final String FIND_ALL = "SELECT id, width, height FROM size";

    @Override
    public List<Size> findAll() {
        return jdbcTemplate.query(FIND_ALL, new SizeMapper());
    }

    private static final String FIND_THE_SAME =
            "SELECT id, width, height FROM size WHERE width = :width AND height = :height";

    @Override
    public Size findTheSame(int width, int height) {
        Map<String, Object> params = ImmutableMap.<String, Object>of(
                "width", width,
                "height", height);

        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(FIND_THE_SAME, params, new SizeMapper()));
    }

    private static final String USED_FOR_PRODUCT = "SELECT COUNT(*) FROM product WHERE size_id = :size_id";

    @Override
    public boolean isUsedForProduct(long sizeId) {
        int count = jdbcTemplate.queryForObject(USED_FOR_PRODUCT,
                ImmutableMap.of("size_id", sizeId), Integer.class);

        return count != 0;
    }

    private static class SizeMapper implements RowMapper<Size> {
        @Override
        public Size mapRow(ResultSet rs, int rowNum) throws SQLException {
            Size size = new Size(rs.getLong("id"));
            size.setWidth(rs.getInt("width"));
            size.setHeight(rs.getInt("height"));
            return size;
        }
    }
}
