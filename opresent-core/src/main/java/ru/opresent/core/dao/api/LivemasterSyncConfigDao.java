package ru.opresent.core.dao.api;

import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;

/**
 * User: artem
 * Date: 09.07.2017
 * Time: 0:52
 */
public interface LivemasterSyncConfigDao {

    void save(LivemasterSyncConfig config);

    /**
     * @return config or {@code null}
     */
    LivemasterSyncConfig load();

}
