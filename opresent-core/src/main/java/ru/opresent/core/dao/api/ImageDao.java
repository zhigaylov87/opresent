package ru.opresent.core.dao.api;

import ru.opresent.core.domain.image.Image;

import java.util.Set;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:39
 */
public interface ImageDao extends EntityIteratorProvider<Image> {

    Image save(Image image);

    void delete(long id);

    void delete(Set<Long> ids);

}
