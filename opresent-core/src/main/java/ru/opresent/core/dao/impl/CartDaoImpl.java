package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.api.CartDao;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * User: artem
 * Date: 30.08.15
 * Time: 5:03
 */
@Component("cartDao")
public class CartDaoImpl implements CartDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String ADD_PRODUCT =
            "INSERT INTO cart (tracking_id, product_id) " +
            "SELECT :tracking_id, :product_id FROM DUAL " +
            "WHERE NOT EXISTS(SELECT 1 FROM cart WHERE tracking_id = :tracking_id AND product_id = :product_id)";

    @Override
    public boolean addProduct(String trackingId, long productId) {
        int rows = jdbcTemplate.update(ADD_PRODUCT,
                ImmutableMap.of("tracking_id", trackingId, "product_id", productId));
        return rows != 0;
    }

    private static final String GET_PRODUCTS_IDS = "SELECT product_id FROM cart WHERE tracking_id = :tracking_id";

    @Override
    public Set<Long> getProductsIds(String trackingId) {
        return new HashSet<>(jdbcTemplate.queryForList(GET_PRODUCTS_IDS,
                ImmutableMap.of("tracking_id", trackingId), Long.class));
    }

    private static final String REMOVE_PRODUCT = "DELETE FROM cart WHERE tracking_id = :tracking_id AND product_id = :product_id";

    @Override
    public void removeProduct(String trackingId, long productId) {
        jdbcTemplate.update(REMOVE_PRODUCT, ImmutableMap.of("tracking_id", trackingId, "product_id", productId));
    }

    private static final String CLEAR_CART = "DELETE FROM cart WHERE tracking_id = :tracking_id";

    @Override
    public void clearCart(String trackingId) {
        jdbcTemplate.update(CLEAR_CART, ImmutableMap.of("tracking_id", trackingId));
    }

    private static final String REMOVE_PRODUCT_FROM_ALL_CARTS = "DELETE FROM cart WHERE product_id = :product_id";

    @Override
    public void removeProductFromAllCarts(long productId) {
        jdbcTemplate.update(REMOVE_PRODUCT_FROM_ALL_CARTS, ImmutableMap.of("product_id", productId));
    }

}
