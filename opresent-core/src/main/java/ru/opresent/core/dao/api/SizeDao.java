package ru.opresent.core.dao.api;

import ru.opresent.core.domain.Size;

import java.util.List;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:03
 */
public interface SizeDao {

    /**
     * @param size size for save
     * @return saved size id
     */
    long save(Size size);

    void update(Size size);

    void delete(long sizeId);

    List<Size> findAll();

    /**
     * @param width width
     * @param height height
     * @return the same size or {@code null} if the same not exists
     */
    Size findTheSame(int width, int height);

    boolean isUsedForProduct(long sizeId);
}
