package ru.opresent.core.dao.api;

import ru.opresent.core.domain.ProductKeyword;

import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 18.12.14
 * Time: 17:11
 */
public interface ProductKeywordDao {

    void addToThesaurus(ProductKeyword keyword);

    void removeFromThesaurus(ProductKeyword keyword);

    /**
     * @return all products keywords sorted by {@link ProductKeyword#getKeyword()} ascending
     */
    List<ProductKeyword> loadThesaurus();

    List<ProductKeyword> loadTop(int count);

    void saveProductKeywords(long productId, Set<ProductKeyword> keywords);

    Set<ProductKeyword> loadProductKeywords(long productId);

    void deleteProductKeywords(long productId);

    /**
     * Create iterator over used(linked to visible product) and unique product keywords. Keywords sorted in ascending order.
     * @return products keyword iterator
     */
    EntityIterator<ProductKeyword> createUsedProductKeywordsIterator();
}
