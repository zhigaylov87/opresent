package ru.opresent.core.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.EntityIterator;

import javax.sql.DataSource;
import java.sql.*;
import java.util.NoSuchElementException;

/**
 * http://stackoverflow.com/questions/2447324/streaming-large-result-sets-with-mysql#answer-2448019
 *
 * User: artem
 * Date: 30.11.14
 * Time: 15:12
 */
public class BaseMySqlStreamingResultsEntityIterator<T> implements EntityIterator<T> {

    private RowMapper<T> rowMapper;

    private int totalCount;

    private Connection connection;
    private Statement allStatement;
    private ResultSet allResultSet;

    private boolean hasNext;
    private boolean cursorMoved;

    public BaseMySqlStreamingResultsEntityIterator(DataSource dataSource, String totalCountQuery,
                                                   String loadAllQuery, RowMapper<T> rowMapper) {
        this.rowMapper = rowMapper;
        try {
            connection = dataSource.getConnection();
            totalCount = loadTotalCount(totalCountQuery);

            // http://stackoverflow.com/questions/2447324/streaming-large-result-sets-with-mysql#answer-2448019
            allStatement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            allStatement.setFetchSize(Integer.MIN_VALUE);
            allResultSet = allStatement.executeQuery(loadAllQuery);
        } catch (SQLException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public boolean hasNext() {
        if (!cursorMoved) {
            moveCursor();
        }
        return hasNext;
    }

    @Override
    public T next() {
        if (!cursorMoved) {
            moveCursor();
        }
        if (!hasNext) {
            throw new NoSuchElementException();
        }

        cursorMoved = false;
        try {
            return rowMapper.mapRow(allResultSet, Integer.MIN_VALUE);
        } catch (SQLException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public int getTotalCount() {
        return totalCount;
    }

    @Override
    public void close() {
        JdbcUtils.closeResultSet(allResultSet);
        JdbcUtils.closeStatement(allStatement);
        JdbcUtils.closeConnection(connection);
    }

    private void moveCursor() {
        try {
            hasNext = allResultSet.next();
            cursorMoved = true;
        } catch (SQLException e) {
            throw new CoreException(e);
        }
    }

    private int loadTotalCount(String totalCountQuery) throws SQLException {
        try (
                PreparedStatement ps = connection.prepareStatement(totalCountQuery);
                ResultSet rs = ps.executeQuery()
        ) {
            rs.next();
            return rs.getInt(1);
        }
    }
}
