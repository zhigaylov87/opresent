package ru.opresent.core.dao.api;

import ru.opresent.core.domain.section.Section;

import java.util.List;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 14:15
 */
public interface SectionDao {

    /**
     * @param section section for save
     * @return saved section id
     */
    long save(Section section);

    void update(Section section);

    void delete(long sectionId);

    List<Section> loadList(boolean onlyVisible);

    Section load(long id);

    Section findTheSameName(String name);

    Section findTheSameEntityName(String entityName);
}