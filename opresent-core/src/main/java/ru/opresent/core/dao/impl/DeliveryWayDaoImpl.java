package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.DeliveryWayDao;
import ru.opresent.core.domain.order.AddressType;
import ru.opresent.core.domain.order.DeliveryWay;
import ru.opresent.core.domain.order.DeliveryWayInfo;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * User: artem
 * Date: 20.02.15
 * Time: 1:48
 */
@Component("deliveryWayDao")
public class DeliveryWayDaoImpl implements DeliveryWayDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String DELETE =
            "DELETE FROM delivery_way_info " +
            "WHERE address_type_code = :address_type_code AND delivery_way_code = :delivery_way_code";

    private static final String SAVE =
            "INSERT INTO delivery_way_info(address_type_code, delivery_way_code, price, note) " +
            "VALUES(:address_type_code, :delivery_way_code, :price, :note)";

    @Override
    public void save(DeliveryWayInfo info) {
        jdbcTemplate.update(DELETE, ImmutableMap.of(
                "address_type_code", info.getAddressType().getCode(),
                "delivery_way_code", info.getDeliveryWay().getCode()));

        int rows = jdbcTemplate.update(SAVE, ImmutableMap.of(
                "address_type_code", info.getAddressType().getCode(),
                "delivery_way_code", info.getDeliveryWay().getCode(),
                "price", info.getPrice(),
                "note", info.getNote()));

        if (rows != 1) {
            throw new CoreException("Delivery way info not saved!");
        }
    }

    private static final String LOAD_ALL = "SELECT address_type_code, delivery_way_code, price, note FROM delivery_way_info";

    @Override
    public List<DeliveryWayInfo> loadAll() {
        return jdbcTemplate.query(LOAD_ALL, EmptySqlParameterSource.INSTANCE, new RowMapper<DeliveryWayInfo>() {
            @Override
            public DeliveryWayInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
                DeliveryWayInfo info = new DeliveryWayInfo(
                        AddressType.valueOf(rs.getInt("address_type_code")),
                        DeliveryWay.valueOf(rs.getInt("delivery_way_code")));
                info.setPrice(rs.getString("price"));
                info.setNote(rs.getString("note"));
                return info;
            }
        });
    }
}
