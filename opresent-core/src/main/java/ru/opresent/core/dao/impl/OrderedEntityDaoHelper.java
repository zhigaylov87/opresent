package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.QueryParamsCreator;
import ru.opresent.core.domain.*;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * User: artem
 * Date: 10.05.15
 * Time: 2:03
 */
@Component("orderedEntityDaoHelper")
public class OrderedEntityDaoHelper {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final Map<Class<? extends OrderedEntity>, String> ENTITY_CLASS_TO_TABLE_NAME =
            ImmutableMap.<Class<? extends OrderedEntity>, String>builder()
                    .put(Product.class, "product")
                    .put(ProductType.class, "product_type")
                    .put(Category.class, "category")
                    .put(Section.class, "section")
                    .put(SectionItem.class, "section_item")
                    .put(BannerItem.class, "banner_item")
                    .put(NewsItem.class, "news_item")
                    .build();

    private static final Map<Class<? extends GroupedOrderedEntity>, String> ENTITY_CLASS_TO_GROUP_BY_FIELD_NAME =
            ImmutableMap.<Class<? extends GroupedOrderedEntity>, String>builder()
                    .put(SectionItem.class, "section_id")
                    .build();

    private static final String ALL_COUNT = "SELECT COUNT(*) FROM {0}{1}";

    public int getTotalCount(OrderedEntity entity) {
        return jdbcTemplate.queryForObject(formatWithWhere(ALL_COUNT, entity),
                createParams(entity), Integer.class);
    }

    private static final String FIND_ID_BY_INDEX = "SELECT id FROM {0} WHERE index_number = :index_number{1}";

    public long findIdByIndex(OrderedEntity entity, int index) {
        return jdbcTemplate.queryForObject(formatWithAnd(FIND_ID_BY_INDEX, entity),
                createParams(entity, ImmutableMap.of("index_number", index)), Long.class);
    }

    private static final String SET_NEW_INDEX = "UPDATE {0} SET index_number = :index_number WHERE id = :id{1}";

    public void exchangePlaces(final OrderedEntity entity1, int index1, long id2, int index2) {
        long id1 = entity1.getId();
        int[] rows = DaoUtils.batchUpdate(jdbcTemplate, formatWithAnd(SET_NEW_INDEX, entity1),
                Arrays.asList(
                        Pair.of(id1, OrderedEntity.NON_EXISTENT_INDEX),
                        Pair.of(id2, index2),
                        Pair.of(id1, index1)),
                new QueryParamsCreator<Pair<Long, Integer>>() {
                    @Override
                    public Map<String, ?> createParams(Pair<Long, Integer> pair, int index) {
                        return OrderedEntityDaoHelper.this.createParams(entity1, ImmutableMap.of("id", pair.getLeft(), "index_number", pair.getRight()));
                    }
                });

        DaoUtils.checkSingleRowUpdated(rows);
    }

    private static final String MOVE_UP_DOWN_AFTER_INDEX =
            "UPDATE '{0}' SET index_number = index_number {0} 1 WHERE index_number > :after_index'{1}' ORDER BY index_number {1}";

    private static final String MOVE_UP_AFTER_INDEX = MessageFormat.format(MOVE_UP_DOWN_AFTER_INDEX, "-", "ASC");
    private static final String MOVE_DOWN_AFTER_INDEX = MessageFormat.format(MOVE_UP_DOWN_AFTER_INDEX, "+", "DESC");

    public void moveUpAfterIndex(OrderedEntity entity, int afterIndex) {
        jdbcTemplate.update(formatWithAnd(MOVE_UP_AFTER_INDEX, entity),
                createParams(entity, ImmutableMap.of("after_index", afterIndex)));
    }

    public void moveDownAfterIndex(OrderedEntity entity, int afterIndex) {
        jdbcTemplate.update(formatWithAnd(MOVE_DOWN_AFTER_INDEX, entity),
                createParams(entity, ImmutableMap.of("after_index", afterIndex)));
    }

    private static final String NEGATE_INDEX =
            "UPDATE {0} SET index_number = - index_number WHERE id = :id{1}";

    private static final String FIND_MIN_INDEX = "SELECT MIN(index_number) FROM {0}{1}";
    private static final String FIND_MAX_INDEX = "SELECT MAX(index_number) FROM {0}{1}";

    public void reorder(final Map<? extends OrderedEntity, Integer> entityToIndex) {
        final IdentifiableList<? extends OrderedEntity> entities = new IdentifiableList<>(entityToIndex.keySet());
        List<Long> ids = new ArrayList<>(CollectionUtils.getIds(entityToIndex.keySet()));

        final OrderedEntity anyEntity = entityToIndex.keySet().iterator().next();
        boolean groupedEntities = anyEntity instanceof GroupedOrderedEntity;
        for (OrderedEntity entity : entities.getAll()) {
            if (!anyEntity.getClass().equals(entity.getClass())) {
                throw new IllegalArgumentException("Different entities classes: " + anyEntity.getClass() + " and " + entity.getClass());
            }
            if (groupedEntities) {
                GroupedOrderedEntity groupedAnyEntity = (GroupedOrderedEntity) anyEntity;
                GroupedOrderedEntity groupedEntity = (GroupedOrderedEntity) entity;
                if (groupedAnyEntity.getGroupByEntity().getId() != groupedEntity.getGroupByEntity().getId()) {
                    throw new IllegalArgumentException("Different entities group by entity: " + groupedAnyEntity + " and " + groupedEntity);
                }
            }
        }

        long idForStartIndex = findIdByIndex(anyEntity, OrderedEntity.START_INDEX);
        List<Long> idsForNegate;
        if (ids.contains(idForStartIndex)) {
            jdbcTemplate.update(formatWithAnd(SET_NEW_INDEX, anyEntity),
                    createParams(anyEntity, ImmutableMap.of("id", idForStartIndex, "index_number", Integer.MIN_VALUE)));

            idsForNegate = new ArrayList<>(ids);
            idsForNegate.remove(idForStartIndex);
        } else {
            idsForNegate = ids;
        }

        int[] rows = DaoUtils.batchUpdate(jdbcTemplate, formatWithAnd(NEGATE_INDEX, anyEntity), idsForNegate,
                new QueryParamsCreator<Long>() {
                    @Override
                    public Map<String, ?> createParams(Long id, int index) {
                        return OrderedEntityDaoHelper.this.createParams(anyEntity, ImmutableMap.of("id", id));
                    }
                });
        DaoUtils.checkSingleRowUpdated(rows);

        rows = DaoUtils.batchUpdate(jdbcTemplate, formatWithAnd(SET_NEW_INDEX, anyEntity), ids,
                new QueryParamsCreator<Long>() {
                    @Override
                    public Map<String, ?> createParams(Long id, int index) {
                        return OrderedEntityDaoHelper.this.createParams(anyEntity,
                                ImmutableMap.of("id", id, "index_number", entityToIndex.get(entities.getById(id))));
                    }
                });
        DaoUtils.checkSingleRowUpdated(rows);

        // check result indexes
        int minIndex = jdbcTemplate.queryForObject(formatWithWhere(FIND_MIN_INDEX, anyEntity),
                createParams(anyEntity), Integer.class);
        if (minIndex != OrderedEntity.START_INDEX) {
            throw new CoreException("minIndex != startIndex -> " + minIndex + " != " + OrderedEntity.START_INDEX);
        }

        int maxIndex = jdbcTemplate.queryForObject(formatWithWhere(FIND_MAX_INDEX, anyEntity),
                createParams(anyEntity), Integer.class);
        int count = getTotalCount(anyEntity);
        if (maxIndex != count - 1) {
            throw new CoreException("maxIndex != count - 1 -> " + maxIndex + " != " + OrderedEntity.START_INDEX + " -1");
        }
    }

    private String formatWithSqlKeyword(String query, String sqlKeyword, OrderedEntity entity) {
        String sqlKeywordCondition = StringUtils.EMPTY;

        if (entity instanceof GroupedOrderedEntity) {
            GroupedOrderedEntity groupedEntity = (GroupedOrderedEntity) entity;
            String groupByFieldName = ENTITY_CLASS_TO_GROUP_BY_FIELD_NAME.get(groupedEntity.getClass());
            sqlKeywordCondition = " " + sqlKeyword + " " + groupByFieldName + " = :" + groupByFieldName;
        }

        return MessageFormat.format(query, ENTITY_CLASS_TO_TABLE_NAME.get(entity.getClass()), sqlKeywordCondition);
    }

    private String formatWithWhere(String query, OrderedEntity entity) {
        return formatWithSqlKeyword(query, "WHERE", entity);
    }

    private String formatWithAnd(String query, OrderedEntity entity) {
        return formatWithSqlKeyword(query, "AND", entity);
    }

    private Map<String, ?> createParams(OrderedEntity entity) {
        return createParams(entity, null);
    }

    private Map<String, ?> createParams(OrderedEntity entity, Map<String, ?> queryParams) {
        Map<String, Object> params = new HashMap<>();
        if (queryParams != null) {
            params.putAll(queryParams);
        }
        if (!(entity instanceof GroupedOrderedEntity)) {
            return params;
        }

        GroupedOrderedEntity groupedEntity = (GroupedOrderedEntity) entity;
        params.put(ENTITY_CLASS_TO_GROUP_BY_FIELD_NAME.get(groupedEntity.getClass()), groupedEntity.getGroupByEntity().getId());
        return params;
    }
}