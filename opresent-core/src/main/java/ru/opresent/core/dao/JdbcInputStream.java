package ru.opresent.core.dao;

import org.springframework.jdbc.support.JdbcUtils;
import ru.opresent.core.CoreException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: artem
 * Date: 06.01.16
 * Time: 2:03
 */
public class JdbcInputStream extends InputStream {

    private Connection connection;
    private PreparedStatement ps;
    private ResultSet rs;
    private InputStream inputStream;

    public JdbcInputStream(Connection connection, PreparedStatement ps) {
        this.connection = connection;
        this.ps = ps;
        try {
            rs = ps.executeQuery();
            rs.next();
            inputStream = rs.getBlob(1).getBinaryStream();
        } catch (SQLException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public int read() throws IOException {
        return inputStream.read();
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
        JdbcUtils.closeResultSet(rs);
        JdbcUtils.closeStatement(ps);
        JdbcUtils.closeConnection(connection);
    }
}
