package ru.opresent.core.dao.api;

import ru.opresent.core.domain.order.DeliveryWayInfo;

import java.util.List;

/**
 * User: artem
 * Date: 20.02.15
 * Time: 1:48
 */
public interface DeliveryWayDao {

    void save(DeliveryWayInfo deliveryWayInfo);

    List<DeliveryWayInfo> loadAll();

}
