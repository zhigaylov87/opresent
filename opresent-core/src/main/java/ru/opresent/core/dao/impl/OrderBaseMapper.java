package ru.opresent.core.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.order.AddressType;
import ru.opresent.core.domain.order.DeliveryWay;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: artem
 * Date: 02.03.15
 * Time: 1:43
 */
@Component("orderBaseMapper")
public class OrderBaseMapper implements RowMapper<Order> {

    @Override
    public Order mapRow(ResultSet rs, int i) throws SQLException {
        Order order = new Order(rs.getLong("id"));

        order.setStatus(OrderStatus.valueOfCode(rs.getInt("status")));
        order.setCustomerFullName(rs.getString("customer_full_name"));
        order.setCustomerPhone(rs.getString("customer_phone"));
        order.setCustomerEmail(rs.getString("customer_email"));
        order.setPersonalOrder(rs.getBoolean("personal_order"));
        int addressTypeCode = rs.getInt("address_type");
        order.setAddressType(rs.wasNull() ? null : AddressType.valueOf(addressTypeCode));
        int deliveryWayCode = rs.getInt("delivery_way");
        order.setDeliveryWay(rs.wasNull() ? null : DeliveryWay.valueOf(deliveryWayCode));
        order.setCustomerCity(rs.getString("customer_city"));
        order.setCustomerPostcode(rs.getString("customer_postcode"));
        order.setCustomerAddress(rs.getString("customer_address"));
        order.setCustomerComment(rs.getString("customer_comment"));
        order.setMasterComment(rs.getString("master_comment"));
        order.setModifyTime(rs.getTimestamp("modify_time"));
        order.setCreateTime(rs.getTimestamp("create_time"));

        return order;
    }
}
