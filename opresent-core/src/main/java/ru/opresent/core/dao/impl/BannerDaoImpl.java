package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.BannerDao;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.NullableImmutableMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:14
 */
@Component("bannerDao")
public class BannerDaoImpl implements BannerDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO banner_item(name, normal_image_id, mobile_image_id, url, index_number, visible) " +
                    "VALUES(:name, :normal_image_id, :mobile_image_id, :url, :index_number, :visible)";

    @Override
    public long save(BannerItem item) {
        Map<String, Object> params = NullableImmutableMap.<String, Object>builder()
                .put("name", item.getName())
                .put("normal_image_id", item.getNormalImage().getId())
                .put("mobile_image_id", item.getMobileImage().getId())
                .put("url", item.getUrl())
                .put("index_number", item.getIndex())
                .put("visible", item.isVisible())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int rows = jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        if (rows != 1) {
            throw new CoreException("Banner item not saved!");
        }
        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE = "UPDATE banner_item " +
            "SET name = :name, normal_image_id = :normal_image_id, mobile_image_id = :mobile_image_id, " +
                "url = :url, visible = :visible " +
            "WHERE id = :id";

    @Override
    public void update(BannerItem item) {
        int rows = jdbcTemplate.update(UPDATE, NullableImmutableMap.<String, Object>builder()
                .put("name", item.getName())
                .put("normal_image_id", item.getNormalImage().getId())
                .put("mobile_image_id", item.getMobileImage().getId())
                .put("url", item.getUrl())
                .put("visible", item.isVisible())
                .put("id", item.getId())
                .build());

        if (rows != 1) {
            throw new CoreException("Banner item not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM banner_item WHERE id = :id";

    @Override
    public void delete(long itemId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", itemId));
        if (rows != 1) {
            throw new CoreException("Banner item not deleted!");
        }
    }

    private static final String LOAD_ALL =
            "SELECT item.id AS item_id, name, url, index_number, visible, " +

                    "normal_image.id AS normal_image_id, " +
                    "normal_image.format AS normal_image_format, " +
                    "normal_image.image_use AS normal_image_use, " +
                    "normal_image.has_copyright AS normal_has_copyright, " +

                    "mobile_image.id AS mobile_image_id, " +
                    "mobile_image.format AS mobile_image_format, " +
                    "mobile_image.image_use AS mobile_image_use, " +
                    "mobile_image.has_copyright AS mobile_has_copyright " +

                    "FROM banner_item item " +
                    "INNER JOIN image normal_image ON item.normal_image_id = normal_image.id " +
                    "INNER JOIN image mobile_image ON item.mobile_image_id = mobile_image.id " +
                    "ORDER BY index_number";

    @Override
    public List<BannerItem> loadAll() {

        return jdbcTemplate.query(LOAD_ALL, EmptySqlParameterSource.INSTANCE, new RowMapper<BannerItem>() {
            @Override
            public BannerItem mapRow(ResultSet rs, int rowNum) throws SQLException {
                BannerItem item = new BannerItem(rs.getLong("item_id"));
                item.setName(rs.getString("name"));
                item.setIndex(rs.getInt("index_number"));
                item.setVisible(rs.getBoolean("visible"));

                Image normalImage = new Image(
                        rs.getLong("normal_image_id"),
                        rs.getString("normal_image_format"),
                        ImageUse.valueOf(rs.getInt("normal_image_use")),
                        HasCopyrightOption.valueOf(rs.getInt("normal_has_copyright")));
                item.setNormalImage(normalImage);

                Image mobileImage = new Image(
                        rs.getLong("mobile_image_id"),
                        rs.getString("mobile_image_format"),
                        ImageUse.valueOf(rs.getInt("mobile_image_use")),
                        HasCopyrightOption.valueOf(rs.getInt("mobile_has_copyright")));
                item.setMobileImage(mobileImage);

                item.setUrl(rs.getString("url"));
                return item;
            }
        });
    }
}
