package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.QueryParamsCreator;
import ru.opresent.core.dao.api.LivemasterAliasDao;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * User: artem
 * Date: 30.11.14
 * Time: 22:16
 */
@Component("livemasterAliasDao")
public class LivemasterAliasDaoImpl implements LivemasterAliasDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO livemaster_alias(entity_id, type, alias) VALUES(:entity_id, :type, :alias)";

    @Override
    public void save(final LivemasterAliases aliases) {
        delete(aliases.getId(), aliases.getType());

        if (!aliases.getAliases().isEmpty()) {
            DaoUtils.batchUpdate(jdbcTemplate, SAVE, new ArrayList<>(aliases.getAliases()), new QueryParamsCreator<String>() {
                @Override
                public Map<String, ?> createParams(String alias, int index) {
                    return ImmutableMap.of(
                            "entity_id", aliases.getId(),
                            "type", aliases.getType().getCode(),
                            "alias", alias);
                }
            });
        }
    }

    private static final String DELETE = "DELETE FROM livemaster_alias WHERE entity_id = :entity_id AND type = :type";

    @Override
    public void delete(long entityId, LivemasterAliasType type) {
        jdbcTemplate.update(DELETE, ImmutableMap.of(
                "entity_id", entityId,
                "type", type.getCode()));
    }

    private static final String LOAD_BY_ID =
            "SELECT alias FROM livemaster_alias WHERE entity_id = :entity_id AND type = :type";

    @Override
    public LivemasterAliases load(long entityId, LivemasterAliasType type) {
        LivemasterAliases aliases = new LivemasterAliases(entityId, type);

        aliases.addAllAliases(jdbcTemplate.queryForList(LOAD_BY_ID,
                ImmutableMap.of("entity_id", entityId, "type", type.getCode()), String.class));

        return aliases;
    }

    private static final String LOAD_BY_TYPE =
            "SELECT entity_id, alias FROM livemaster_alias WHERE type = :type ORDER BY entity_id";

    @Override
    public Set<LivemasterAliases> load(final LivemasterAliasType type) {
        return jdbcTemplate.query(LOAD_BY_TYPE, ImmutableMap.of("type", type.getCode()), new ResultSetExtractor<Set<LivemasterAliases>>() {
            @Override
            public Set<LivemasterAliases> extractData(ResultSet rs) throws SQLException, DataAccessException {
                Set<LivemasterAliases> aliasesSet = new HashSet<>();

                LivemasterAliases aliases = null;
                while (rs.next()) {
                    long entityId = rs.getLong("entity_id");
                    String aliasValue = rs.getString("alias");

                    if (aliases == null || aliases.getId() != entityId) {
                        aliases = new LivemasterAliases(entityId, type);
                        aliasesSet.add(aliases);
                    }

                    aliases.addAlias(aliasValue);
                }

                return aliasesSet;
            }
        });
    }

    private static final String FIND_BY_ALIAS_AND_TYPE =
            "SELECT entity_id FROM livemaster_alias WHERE type = :type AND alias = :alias";

    @Override
    public Long findEntityIdByAlias(LivemasterAliasType type, String alias) {
        List<Long> ids = jdbcTemplate.queryForList(FIND_BY_ALIAS_AND_TYPE,
                ImmutableMap.of("type", type.getCode(), "alias", alias), Long.class);
        return CollectionUtils.getSingleOrNull(ids);
    }
}
