package ru.opresent.core.dao.api;

import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.Color;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.image.Image;

import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 23:07
 */
public interface ProductDao extends EntityIteratorProvider<Product> {

    /**
     * @param product product
     * @return saved product id
     */
    long saveBase(Product product);

    void saveViewsLinks(long productId, List<Image> views);

    void saveColorsLinks(long productId, Set<Color> colors);

    void saveCategoriesLinks(long productId, Set<Category> categories);

    /**
     * @param productId product id
     * @return product with defined id or {@code null} if product with defined id not exists
     */
    Product loadBase(long productId);

    /**
     *
     * @param livemasterId livemasterId
     * @return product id or {@code null} if product with defined livemaster id not exists
     */
    Long loadProductIdByLivemasterId(String livemasterId);

    Set<Product> loadBase(Set<Long> productsIds);

    List<Image> loadViews(long productId);

    Set<Long> loadColorsIds(long productId);

    Set<Long> loadCategoriesIds(long productId);

    void updateBase(Product product);

    void deleteBase(long productId);

    void saveDeletedProductLivemasterId(String livemasterId);

    void deleteViewsLinks(long productId);

    void deleteColorsLinks(long productId);

    void deleteCategoriesLinks(long productId);

    boolean isExistsWithId(long productId);

    Integer findMinIndexExcludeStatus(ProductStatus excludeStatus);

    boolean isDeletedWithLivemasterId(String livemasterId);

    /**
     * Create iterator by visible products with base info and keywords
     * @return product iterator
     * @see Product#isVisible()
     * @see Product#getKeywords()
     */
    EntityIterator<Product> createEntityIterator();
}
