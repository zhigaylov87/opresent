package ru.opresent.core.dao.api;

import ru.opresent.core.domain.Identifiable;

/**
 * User: artem
 * Date: 03.11.14
 * Time: 4:52
 */
public interface EntityIteratorProvider<E extends Identifiable> {

    EntityIterator<E> createEntityIterator();

}
