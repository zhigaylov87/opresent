package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.api.OrderDao;
import ru.opresent.core.dao.api.OrderListDao;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderFilter;
import ru.opresent.core.domain.order.OrderItem;
import ru.opresent.core.pagination.OrderPage;
import ru.opresent.core.pagination.PageSettings;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 01.03.15
 * Time: 23:44
 */
@Component("orderListDao")
public class OrderListDaoImpl implements OrderListDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "orderDao")
    private OrderDao orderDao;

    @Resource(name = "orderBaseMapper")
    private OrderBaseMapper orderBaseMapper;

    @Override
    public OrderPage load(OrderFilter filter, PageSettings pageSettings) {
        FilterQuery filterQuery = createFilterQuery(filter);

        int totalCount = getTotalCount(filterQuery);
        if (totalCount == 0) {
            return new OrderPage(pageSettings, new ArrayList<Order>(), totalCount);
        }

        LinkedHashMap<Long, Order> orderIdToOrder = loadPage(filterQuery, pageSettings);
        Map<Long, List<OrderItem>> orderItems = orderDao.loadItems(orderIdToOrder.keySet());

        List<Order> orders = new ArrayList<>(orderIdToOrder.values());
        for (Order order : orders) {
            order.setItems(orderItems.get(order.getId()));
        }

        return new OrderPage(pageSettings, orders, getTotalCount(filterQuery));
    }

    private static final String TOTAL_COUNT = "SELECT COUNT(*) FROM order_info {0}";

    private int getTotalCount(FilterQuery filterQuery) {
        return jdbcTemplate.queryForObject(MessageFormat.format(TOTAL_COUNT, filterQuery.getQuery()),
                filterQuery.getParams(), Integer.class);
    }

    private static final String PAGE =
            "SELECT " + OrderDaoImpl.LOAD_BASE_FIELDS +
            " FROM order_info {0} ORDER BY status ASC, modify_time DESC LIMIT :offset, :page_size";

    private LinkedHashMap<Long, Order> loadPage(FilterQuery filterQuery, PageSettings pageSettings) {
        final OrderBaseMapper mapper = orderBaseMapper;

        return jdbcTemplate.query(
                MessageFormat.format(PAGE, filterQuery.getQuery()),
                ImmutableMap.<String, Object>builder()
                        .putAll(filterQuery.getParams())
                        .put("offset", pageSettings.getOffset())
                        .put("page_size", pageSettings.getPageSize())
                        .build(),
                new ResultSetExtractor<LinkedHashMap<Long, Order>>() {
                    @Override
                    public LinkedHashMap<Long, Order> extractData(ResultSet rs) throws SQLException, DataAccessException {
                        LinkedHashMap<Long, Order> orderIdToOrder = new LinkedHashMap<>();

                        int rowNum = 0;
                        while (rs.next()) {
                            Order order = mapper.mapRow(rs, rowNum++);
                            orderIdToOrder.put(order.getId(), order);
                        }

                        return orderIdToOrder;
                    }
                }

        );
    }

    private FilterQuery createFilterQuery(OrderFilter filter) {
        return new FilterQueryBuilder()
                .addWhere("id = :id", "id", filter.getOrderId())
                .addWhere("status = :status", "status", filter.getStatus())
                .addWhereHasIgnoreCase("UPPER(customer_full_name) LIKE :customer_full_name", "customer_full_name", filter.getCustomerFullName())
                .build();
    }
}
