package ru.opresent.core.dao.api;

import ru.opresent.core.domain.banner.BannerItem;

import java.util.List;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:14
 */
public interface BannerDao {

    /**
     * @param item banner item
     * @return saved banner item id
     */
    long save(BannerItem item);

    void update(BannerItem item);

    void delete(long itemId);

    List<BannerItem> loadAll();
}
