package ru.opresent.core.dao.api;


import ru.opresent.core.domain.news.NewsItem;

import java.util.List;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:14
 */
public interface NewsDao {

    /**
     * @param item news item
     * @return saved news item id
     */
    long save(NewsItem item);

    void update(NewsItem item);

    void delete(long itemId);

    List<NewsItem> loadAll();
}
