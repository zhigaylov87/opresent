package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.ProductLabelDao;
import ru.opresent.core.domain.ProductLabel;
import ru.opresent.core.domain.ProductLabelType;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.NullableImmutableMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 16:18
 */
@Component("productLabelDao")
public class ProductLabelDaoImpl implements ProductLabelDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String SAVE =
            "INSERT INTO product_label(product_label_type_code, image_id, default_note, with_discount, discount_percent) " +
            "VALUES(:product_label_type_code, :image_id, :default_note, :with_discount, :discount_percent)";

    @Override
    public long save(ProductLabel label) {
        Map<String, Object> params = NullableImmutableMap.<String, Object>builder()
                .put("product_label_type_code", label.getType().getCode())
                .put("image_id", label.getImage().getId())
                .put("default_note", label.getDefaultNote())
                .put("with_discount", label.isWithDiscount())
                .put("discount_percent", label.isWithDiscount() ? label.getDiscountPercent() : null)
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE =
            "UPDATE product_label " +
            "SET product_label_type_code = :product_label_type_code, image_id = :image_id, " +
                "default_note = :default_note, with_discount = :with_discount, discount_percent = :discount_percent " +
            "WHERE id = :id";

    @Override
    public void update(ProductLabel label) {
        Map<String, Object> params = ImmutableMap.<String, Object>builder()
                .put("product_label_type_code", label.getType().getCode())
                .put("image_id", label.getImage().getId())
                .put("default_note", label.getDefaultNote())
                .put("with_discount", label.isWithDiscount())
                .put("discount_percent", label.isWithDiscount() ? label.getDiscountPercent() : null)
                .put("id", label.getId())
                .build();

        int rows = jdbcTemplate.update(UPDATE, params);
        if (rows != 1) {
            throw new CoreException("Product label not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM product_label WHERE id = :id";

    @Override
    public void delete(long productLabelId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", productLabelId));
        if (rows != 1) {
            throw new CoreException("Product label not deleted!");
        }
    }

    private static final String USED_FOR_PRODUCT =
            "SELECT EXISTS(SELECT 1 FROM product WHERE product_label_id = :product_label_id)";

    @Override
    public boolean isUsedForProduct(long productLabelId) {
        return jdbcTemplate.queryForObject(USED_FOR_PRODUCT, ImmutableMap.of("product_label_id", productLabelId), Boolean.class);
    }

    private static final String LOAD_ALL =
            "SELECT p_label.id AS label_id, product_label_type_code, " +
                    "img.id AS img_id, img.format AS img_format, img.image_use AS img_use, img.has_copyright AS img_has_copyright, " +
                    "default_note, with_discount, discount_percent " +
            "FROM product_label p_label " +
            "INNER JOIN image img ON p_label.image_id = img.id";

    @Override
    public List<ProductLabel> loadAll() {
        return jdbcTemplate.query(LOAD_ALL, new RowMapper<ProductLabel>() {
            @Override
            public ProductLabel mapRow(ResultSet rs, int rowNum) throws SQLException {
                ProductLabel label = new ProductLabel(rs.getLong("label_id"));
                label.setType(ProductLabelType.valueOfCode(rs.getInt("product_label_type_code")));

                Image image = new Image(rs.getLong("img_id"),
                        rs.getString("img_format"),
                        ImageUse.valueOf(rs.getInt("img_use")),
                        HasCopyrightOption.valueOf(rs.getInt("img_has_copyright")));
                label.setImage(image);

                label.setDefaultNote(rs.getString("default_note"));
                boolean withDiscount = rs.getBoolean("with_discount");
                label.setWithDiscount(withDiscount);
                label.setDiscountPercent(withDiscount ? rs.getInt("discount_percent") : null);

                return label;
            }
        });
    }
}