package ru.opresent.core.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.domain.image.ProductImageName;
import ru.opresent.core.service.api.ProductLabelService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.service.api.SizeService;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: artem
 * Date: 24.08.14
 * Time: 15:22
 * @see ru.opresent.core.dao.impl.ProductDaoImpl#LOAD_BASE_FIELDS
 */
@Component("productBaseMapper")
public class ProductBaseMapper implements RowMapper<Product> {

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "productLabelService")
    private ProductLabelService productLabelService;

    @Resource(name = "sizeService")
    private SizeService sizeService;

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product(rs.getLong("id"));
        product.setLivemasterId(rs.getString("livemaster_id"));
        product.setType(productTypeService.getAll().getById(rs.getLong("product_type_id")));
        product.setName(rs.getString("name"));
        product.setDescription(rs.getString("description"));
        product.setVisible(rs.getBoolean("visible"));
        int price = rs.getInt("price");
        if (!rs.wasNull()) {
            product.setPrice(price);
        }
        product.setStatus(ProductStatus.valueOfCode(rs.getInt("status")));
        product.setStatusNote(rs.getString("status_note"));
        product.setProductionTime(rs.getString("production_time"));

        Long productLabelId = rs.getLong("product_label_id");
        productLabelId = rs.wasNull() ? null : productLabelId;
        product.setLabel(productLabelId == null ? null : productLabelService.getAll().getById(productLabelId));

        product.setLabelNote(rs.getString("product_label_note"));

        long sizeId = rs.getLong("size_id");
        if (!rs.wasNull()) {
            product.setSize(sizeService.getAll().getById(sizeId));
        }
        product.setSizeText(rs.getString("size_text"));

        Image preview = new Image(
                rs.getLong("preview_id"),
                rs.getString("preview_format"),
                ImageUse.valueOf(rs.getInt("preview_use")),
                HasCopyrightOption.valueOf(rs.getInt("preview_has_copyright")));
        preview.setImageName(new ProductImageName(product));
        product.setPreview(preview);

        product.setModifyTime(rs.getTimestamp("modify_time"));
        product.setIndex(rs.getInt("index_number"));

        return product;
    }
}
