package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.QueryParamsCreator;
import ru.opresent.core.dao.api.OrderDao;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderItem;
import ru.opresent.core.domain.order.OrderSettings;
import ru.opresent.core.domain.order.OrderStatus;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.ListValueMap;
import ru.opresent.core.util.NullableImmutableMap;
import ru.opresent.core.util.SetValueMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: artem
 * Date: 14.02.15
 * Time: 15:45
 */
@Component("orderDao")
public class OrderDaoImpl implements OrderDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "orderBaseMapper")
    private OrderBaseMapper orderBaseMapper;

    @Resource(name = "productService")
    private ProductService productService;

    public static final String LOAD_BASE_FIELDS =
            "id, status, customer_full_name, customer_phone, customer_email, personal_order, address_type, delivery_way, " +
            "customer_city, customer_postcode, customer_address, customer_comment, master_comment, modify_time, create_time";

    private static final String SAVE_BASE =
            "INSERT INTO order_info(status, customer_full_name, customer_phone, customer_email, personal_order, address_type, delivery_way, " +
                    "customer_city, customer_postcode, customer_address, customer_comment, master_comment, modify_time, create_time) " +
            "VALUES(:status, :customer_full_name, :customer_phone, :customer_email, :personal_order, :address_type, :delivery_way, " +
                    ":customer_city, :customer_postcode, :customer_address, :customer_comment, :master_comment, :modify_time, :create_time)";

    @Override
    public long saveBase(Order order) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SAVE_BASE, new MapSqlParameterSource(createOrderBaseParams(order)), keyHolder);
        return keyHolder.getKey().longValue();
    }

    private static final String SAVE_ORDER_ITEM =
            "INSERT INTO order_item(order_id, product_id, fix_price) VALUES(:order_id, :product_id, :fix_price)";

    @Override
    public void saveItems(final long orderId, List<OrderItem> items) {
        DaoUtils.batchUpdate(jdbcTemplate, SAVE_ORDER_ITEM, items, new QueryParamsCreator<OrderItem>() {
            @Override
            public Map<String, ?> createParams(OrderItem item, int index) {
                return ImmutableMap.<String, Object>builder()
                        .put("order_id", orderId)
                        .put("product_id", item.getProduct().getId())
                        .put("fix_price", item.getFixPrice())
                        .build();
            }
        });
    }

    private static final String UPDATE_BASE =
            "UPDATE order_info " +
            "SET status = :status, customer_full_name = :customer_full_name, customer_phone = :customer_phone, " +
                    "customer_email = :customer_email, address_type = :address_type, delivery_way = :delivery_way, " +
                    "customer_city = :customer_city, customer_postcode = :customer_postcode, customer_address = :customer_address, " +
                    "customer_comment = :customer_comment, master_comment = :master_comment, modify_time = :modify_time " +
            "WHERE id = :id";

    @Override
    public void updateBase(Order order) {
        int rows = jdbcTemplate.update(UPDATE_BASE, new MapSqlParameterSource(createOrderBaseParams(order)));
        if (rows == 0) {
            throw new CoreException("Order with id = " + order.getId() + " not updated");
        }
    }

    private static final String LOAD_BASE = "SELECT " + LOAD_BASE_FIELDS + " FROM order_info WHERE id = :id";

    @Override
    public Order loadBase(long orderId) {
        return CollectionUtils.getSingle(jdbcTemplate
                .query(LOAD_BASE, ImmutableMap.of("id", orderId), orderBaseMapper));
    }

    private static final String LOAD_ITEMS =
            "SELECT oi.id, oi.order_id, oi.product_id, oi.fix_price " +
            "FROM order_item oi " +
            "INNER JOIN product p ON p.id = oi.product_id " +
            "WHERE order_id IN(:order_ids) " +
            "ORDER BY order_id, p.name ASC";

    @Override
    public Map<Long, List<OrderItem>> loadItems(Set<Long> orderIds) {
        final SetValueMap<Long, OrderItem> productIdToItems = new SetValueMap<>();
        final ListValueMap<Long, OrderItem> orderIdToItems = new ListValueMap<>();

         jdbcTemplate.query(LOAD_ITEMS, ImmutableMap.of("order_ids", orderIds),
                 new ResultSetExtractor<Void>() {
                     @Override
                     public Void extractData(ResultSet rs) throws SQLException, DataAccessException {
                         while (rs.next()) {
                             OrderItem item = new OrderItem(rs.getLong("id"));
                             item.setFixPrice(rs.getInt("fix_price"));

                             productIdToItems.add(rs.getLong("product_id"), item);
                             orderIdToItems.add(rs.getLong("order_id"), item);
                         }
                         return null;
                     }
                 });

        for (Product product : productService.loadBase(productIdToItems.keySet())) {
            for (OrderItem orderItem : productIdToItems.get(product.getId())) {
                orderItem.setProduct(product);
            }
        }

        return orderIdToItems;
    }

    private static final String REMOVE_PRODUCT_FROM_ALL_ORDERS = "DELETE FROM order_item WHERE product_id = :product_id";

    @Override
    public void removeProductFromAllOrders(long productId) {
        jdbcTemplate.update(REMOVE_PRODUCT_FROM_ALL_ORDERS, ImmutableMap.of("product_id", productId));
    }

    private static final String GET_ORDER_STATUS_COUNT = "SELECT COUNT(*) FROM order_info WHERE status = :status";

    @Override
    public int getOrderStatusCount(OrderStatus status) {
        return jdbcTemplate.queryForObject(GET_ORDER_STATUS_COUNT,
                ImmutableMap.of("status", status.getCode()), Integer.class);
    }

    private static final String EXISTS_ORDER_SETTINGS =
            "SELECT EXISTS(SELECT 1 FROM order_settings)";

    private static final String INSERT_ORDER_SETTINGS =
            "INSERT INTO order_settings(personal_order_text) VALUES(:personal_order_text)";

    private static final String UPDATE_ORDER_SETTINGS =
            "UPDATE order_settings SET personal_order_text = :personal_order_text";

    @Override
    public void saveOrderSettings(OrderSettings orderSettings) {
        boolean exists = jdbcTemplate.queryForObject(
                EXISTS_ORDER_SETTINGS, EmptySqlParameterSource.INSTANCE, Boolean.class);

        int rows = jdbcTemplate.update(exists ? UPDATE_ORDER_SETTINGS : INSERT_ORDER_SETTINGS,
                ImmutableMap.of("personal_order_text", orderSettings.getPersonalOrderText()));

        if (rows != 1) {
            throw new CoreException("Order setting not " + (exists ? "updated" : "inserted"));
        }
    }

    private static final String LOAD_ORDER_SETTINGS = "SELECT personal_order_text FROM order_settings";

    @Override
    public OrderSettings loadOrderSettings() {
        return CollectionUtils.getSingleOrNull(jdbcTemplate.query(
                LOAD_ORDER_SETTINGS, EmptySqlParameterSource.INSTANCE,
                new RowMapper<OrderSettings>() {
                    @Override
                    public OrderSettings mapRow(ResultSet rs, int rowNum) throws SQLException {
                        OrderSettings orderSettings = new OrderSettings();
                        orderSettings.setPersonalOrderText(rs.getString("personal_order_text"));
                        return orderSettings;
                    }
                }));
    }

    private Map<String, Object> createOrderBaseParams(Order order) {
        NullableImmutableMap.NullableBuilder<String, Object> builder = NullableImmutableMap.<String, Object>builder()
                .put("status", order.getStatus().getCode())
                .put("customer_full_name", order.getCustomerFullName())
                .put("customer_phone", order.getCustomerPhone())
                .put("customer_email", order.getCustomerEmail())
                .put("personal_order", order.isPersonalOrder())
                .put("address_type", order.getAddressType() == null ? null : order.getAddressType().getCode())
                .put("delivery_way", order.getDeliveryWay() == null ? null : order.getDeliveryWay().getCode())
                .put("customer_city", order.getCustomerCity())
                .put("customer_postcode", order.getCustomerPostcode())
                .put("customer_address", order.getCustomerAddress())
                .put("customer_comment", order.getCustomerComment())
                .put("master_comment", order.getMasterComment())
                .put("modify_time", order.getModifyTime());
        if (order.getId() == Identifiable.NOT_SAVED_ID) {
            builder.put("create_time", order.getCreateTime());
        } else {
            builder.put("id", order.getId());
        }
        return builder.build();
    }
}
