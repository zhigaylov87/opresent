package ru.opresent.core.dao.api;

import ru.opresent.core.domain.section.SectionItem;

import java.util.List;

/**
 * User: artem
 * Date: 21.06.14
 * Time: 21:53
 */
public interface SectionItemDao {

    /**
     * @param sectionItem section item for save
     * @return saved section item id
     */
    long save(SectionItem sectionItem);

    void update(SectionItem sectionItem);

    void delete(long sectionItemId);

    SectionItem load(long id);

    List<SectionItem> loadSectionItems(long sectionId, boolean onlyVisible);

}