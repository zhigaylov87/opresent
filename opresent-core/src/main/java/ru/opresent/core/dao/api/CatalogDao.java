package ru.opresent.core.dao.api;

import java.util.Map;
import java.util.Set;

/**
 * User: artem
 * Date: 30.10.14
 * Time: 2:18
 */
public interface CatalogDao {

    /**
     * @return used product types with categories [product type id -> categories ids].
     */
    Map<Long, Set<Long>> findUsedProductTypesIdsWithCategoriesIds();

}
