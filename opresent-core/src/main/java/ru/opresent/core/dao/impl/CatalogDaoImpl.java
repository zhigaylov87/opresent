package ru.opresent.core.dao.impl;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.api.CatalogDao;
import ru.opresent.core.util.SetValueMap;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

/**
 * User: artem
 * Date: 30.10.14
 * Time: 2:22
 */
@Component("catalogDao")
public class CatalogDaoImpl implements CatalogDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String USED_PRODUCT_TYPES_WITH_CATEGORIES =
            "SELECT DISTINCT pt.id AS product_type_id, p2c.category_id AS category_id FROM product_type pt" +
            " INNER JOIN product p ON p.product_type_id = pt.id AND p.visible = " + DaoUtils.BOOLEAN_TRUE_NUMBER +
            " INNER JOIN products2categories p2c ON p2c.product_id = p.id ";

    @Override
    public Map<Long, Set<Long>> findUsedProductTypesIdsWithCategoriesIds() {
        final SetValueMap<Long, Long> result = new SetValueMap<>();

        jdbcTemplate.query(USED_PRODUCT_TYPES_WITH_CATEGORIES, new ResultSetExtractor<Void>() {
            @Override
            public Void extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {
                    result.add(rs.getLong("product_type_id"), rs.getLong("category_id"));
                }
                return null;
            }
        });

        return result;
    }

}