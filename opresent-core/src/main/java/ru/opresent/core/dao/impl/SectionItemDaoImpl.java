package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.DaoUtils;
import ru.opresent.core.dao.api.SectionItemDao;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.domain.image.SectionItemImageName;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 21.06.14
 * Time: 21:55
 */
@Component("sectionItemDao")
public class SectionItemDaoImpl implements SectionItemDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    private static final String SAVE =
            "INSERT INTO section_item(name, preview_id, short_text, visible, full_text, section_id, index_number) " +
                    "VALUES(:name, :preview_id, :short_text, :visible, :full_text, :section_id, :index_number)";

    @Override
    public long save(SectionItem sectionItem) {
        Map<String, ?> params = ImmutableMap.<String, Object>builder()
                .put("name", sectionItem.getName())
                .put("preview_id", sectionItem.getPreview().getId())
                .put("short_text", sectionItem.getShortText())
                .put("visible", sectionItem.isVisible())
                .put("full_text", sectionItem.getFullText())
                .put("section_id", sectionItem.getSection().getId())
                .put("index_number", sectionItem.getIndex())
                .build();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SAVE, new MapSqlParameterSource(params), keyHolder);

        return keyHolder.getKey().longValue();
    }

    private static final String UPDATE =
            "UPDATE section_item " +
            "SET name = :name, preview_id = :preview_id, short_text = :short_text, visible = :visible, full_text = :full_text " +
            "WHERE id = :id";

    @Override
    public void update(SectionItem sectionItem) {
        Map<String, ?> params = ImmutableMap.<String, Object>builder()
                .put("name", sectionItem.getName())
                .put("preview_id", sectionItem.getPreview().getId())
                .put("short_text", sectionItem.getShortText())
                .put("visible", sectionItem.isVisible())
                .put("full_text", sectionItem.getFullText())
                .put("id", sectionItem.getId())
                .build();

        int rows = jdbcTemplate.update(UPDATE, params);

        if (rows != 1) {
            throw new CoreException("Section item not updated!");
        }
    }

    private static final String DELETE = "DELETE FROM section_item WHERE id = :id";

    @Override
    public void delete(long sectionItemId) {
        int rows = jdbcTemplate.update(DELETE, ImmutableMap.of("id", sectionItemId));
        if (rows != 1) {
            throw new CoreException("Section item not deleted!");
        }
    }

    private static final String LOAD_BASE =
            "SELECT si.id, si.name, si.preview_id, si.short_text, si.visible, si.full_text, si.section_id, si.index_number, " +
                    "img.format AS preview_format, img.image_use AS preview_use, img.has_copyright AS preview_has_copyright " +
            "FROM section_item si " +
            "INNER JOIN image img ON si.preview_id = img.id ";

    private static final String LOAD_BY_ID = LOAD_BASE + "WHERE si.id = :id";

    @Override
    public SectionItem load(long id) {
        return CollectionUtils.getSingleOrNull(
                jdbcTemplate.query(LOAD_BY_ID,
                        new MapSqlParameterSource(ImmutableMap.of("id", id)),
                        new SectionItemMapper()));
    }

    private static final String FIND_BY_SECTION_ID_COMMON =
            LOAD_BASE + "WHERE si.section_id = :section_id{0} ORDER BY index_number";
    private static final String FIND_BY_SECTION_ID = MessageFormat.format(FIND_BY_SECTION_ID_COMMON, "");
    private static final String FIND_VISIBLE_BY_SECTION_ID =
            MessageFormat.format(FIND_BY_SECTION_ID_COMMON, " AND si.visible = " + DaoUtils.BOOLEAN_TRUE_NUMBER);

    @Override
    public List<SectionItem> loadSectionItems(long sectionId, boolean onlyVisible) {
        return jdbcTemplate.query(
                onlyVisible ? FIND_VISIBLE_BY_SECTION_ID : FIND_BY_SECTION_ID,
                new MapSqlParameterSource(ImmutableMap.of("section_id", sectionId)),
                new SectionItemMapper());
    }

    private class SectionItemMapper implements RowMapper<SectionItem> {

        @Override
        public SectionItem mapRow(ResultSet rs, int rowNum) throws SQLException {
            SectionItem item = new SectionItem(rs.getLong("id"));
            item.setName(rs.getString("name"));

            Image preview = new Image(
                    rs.getLong("preview_id"),
                    rs.getString("preview_format"),
                    ImageUse.valueOf(rs.getInt("preview_use")),
                    HasCopyrightOption.valueOf(rs.getInt("preview_has_copyright")));
            preview.setImageName(new SectionItemImageName(item));
            item.setPreview(preview);

            item.setShortText(rs.getString("short_text"));
            item.setFullText(rs.getString("full_text"));
            item.setVisible(rs.getBoolean("visible"));
            item.setIndex(rs.getInt("index_number"));

            item.setSection(sectionService.getAll().getById(rs.getLong("section_id")));

            return item;
        }
    }
}
