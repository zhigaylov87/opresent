package ru.opresent.core.dao.api;

import ru.opresent.core.domain.ProductLabel;

import java.util.List;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 16:15
 */
public interface ProductLabelDao {

    /**
     *
     * @param label product label
     * @return saved product label id
     */
    long save(ProductLabel label);

    void update(ProductLabel label);

    void delete(long productLabelId);

    boolean isUsedForProduct(long productLabelId);

    List<ProductLabel> loadAll();

}
