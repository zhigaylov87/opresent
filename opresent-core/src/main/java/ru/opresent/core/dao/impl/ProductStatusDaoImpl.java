package ru.opresent.core.dao.impl;

import com.google.common.collect.ImmutableMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.ProductStatusDao;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductStatusInfo;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 15:00
 */
@Component("productStatusDao")
public class ProductStatusDaoImpl implements ProductStatusDao {

    @Resource(name = "jdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String DELETE = "DELETE FROM product_status_info WHERE status_code = :status_code";

    private static final String SAVE =
            "INSERT INTO product_status_info(default_note, status_code) VALUES(:default_note,  :status_code)";

    @Override
    public void save(ProductStatusInfo info) {
        jdbcTemplate.update(DELETE, ImmutableMap.of("status_code", info.getStatus().getCode()));

        int rows = jdbcTemplate.update(SAVE, ImmutableMap.of(
                "default_note", info.getDefaultNote(),
                "status_code", info.getStatus().getCode()));

        if (rows != 1) {
            throw new CoreException("Product status info not saved!");
        }
    }

    private static final String LOAD_ALL = "SELECT status_code, default_note FROM product_status_info";

    @Override
    public Map<ProductStatus, ProductStatusInfo> loadAll() {
        return jdbcTemplate.query(LOAD_ALL, EmptySqlParameterSource.INSTANCE, new ResultSetExtractor<Map<ProductStatus, ProductStatusInfo>>() {
            @Override
            public Map<ProductStatus, ProductStatusInfo> extractData(ResultSet rs) throws SQLException, DataAccessException {
                Map<ProductStatus, ProductStatusInfo> infos = new HashMap<>();

                while (rs.next()) {
                    ProductStatusInfo info = new ProductStatusInfo(ProductStatus.valueOfCode(rs.getInt("status_code")));
                    info.setDefaultNote(rs.getString("default_note"));

                    infos.put(info.getStatus(), info);
                }

                return infos;
            }
        });
    }
}
