package ru.opresent.core.dao.api;

import ru.opresent.core.domain.ProductFilter;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 12:33
 */
public interface ProductListDao {

    ProductPage load(ProductFilter filter, PageSettings pageSettings, boolean withCategories);

}
