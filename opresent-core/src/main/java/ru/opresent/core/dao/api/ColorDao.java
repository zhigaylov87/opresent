package ru.opresent.core.dao.api;

import ru.opresent.core.domain.Color;

import java.util.List;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:21
 */
public interface ColorDao {

    /**
     * @param color color for save
     * @return saved color id
     */
    long save(Color color);

    void update(Color color);

    void delete(long colorId);

    List<Color> findAll();

    Color findTheSame(int rgb);

    boolean isUsedForProduct(long colorId);

}
