package ru.opresent.core.config;

/**
 * User: artem
 * Date: 17.05.14
 * Time: 23:35
 */
public interface ServerConfig {

    /**
     * @return host like "http://localhost:8080" or "http://static.o-present.ru"
     */
    String getHost();

    /**
     *
     * @return host for static resources like "http://static.o-present.ru/resources"
     */
    String getStaticResourcesBaseUrl();

    String getSecretKeyPath();

    boolean isSendNotifications();

    String getVersion();
}
