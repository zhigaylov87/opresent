package ru.opresent.core.config;

/**
 * User: artem
 * Date: 17.05.14
 * Time: 23:37
 */
public class DefaultServerConfig implements ServerConfig {

    private String host;
    private String staticResourcesBaseUrl;
    private String secretKeyPath;
    private Boolean sendNotifications;
    private String version;


    @Override
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String getStaticResourcesBaseUrl() {
        return staticResourcesBaseUrl;
    }

    public void setStaticResourcesBaseUrl(String staticResourcesBaseUrl) {
        this.staticResourcesBaseUrl = staticResourcesBaseUrl;
    }

    public String getSecretKeyPath() {
        return secretKeyPath;
    }

    public void setSecretKeyPath(String secretKeyPath) {
        this.secretKeyPath = secretKeyPath;
    }

    /**
     * If option not defined by {@link #setSendNotifications(boolean)}, {@code false} will be return
     * @return {@code true} if need to send notification, {@code false} otherwise
     */
    @Override
    public boolean isSendNotifications() {
        return sendNotifications == null ? false : sendNotifications;
    }

    public void setSendNotifications(boolean sendNotifications) {
        this.sendNotifications = sendNotifications;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
