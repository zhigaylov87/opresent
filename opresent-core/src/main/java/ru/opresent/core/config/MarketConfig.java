package ru.opresent.core.config;

/**
 * User: artem
 * Date: 27.04.15
 * Time: 1:09
 */
public interface MarketConfig {

    /**
     * @return opresent phone like "+79161234567"
     */
    String getPhone();

    /**
     * @return formatted opresent phone like "+7(916)123-45-67"
     */
    String getFormattedPhone();

    /**
     * @return opresent email like "master@o-present.ru"
     */
    String getEmail();

    /**
     * @return opresent name like "O!present"
     */
    String getName();

    /**
     * @return www adress like "www.o-present.ru"
     */
    String getWww();

    /**
     * @return logo path like "image/logo.png"
     */
    String getLogo();

    /**
     * @return copyright like "&copy; 2013-2015 O!present"
     */
    String getCopyright();

    String getCity();

    String getInstagramUrl();

}
