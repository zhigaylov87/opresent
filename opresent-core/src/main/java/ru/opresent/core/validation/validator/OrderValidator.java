package ru.opresent.core.validation.validator;

import org.apache.commons.collections.CollectionUtils;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.validation.constraint.OrderConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 22.02.15
 * Time: 22:58
 */
public class OrderValidator implements ConstraintValidator<OrderConstraint, Order> {

    @Override
    public void initialize(OrderConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Order order, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (order.isPersonalOrder()) {
            if (CollectionUtils.isNotEmpty(order.getItems())) {
                context.buildConstraintViolationWithTemplate("{Order.items.notEmpty}").addConstraintViolation();
                return false;
            }
        } else {
            if (CollectionUtils.isEmpty(order.getItems())) {
                context.buildConstraintViolationWithTemplate("{Order.items.empty}").addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
