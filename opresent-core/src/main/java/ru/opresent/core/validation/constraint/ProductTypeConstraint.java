package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.ProductTypeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 4:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = ProductTypeValidator.class)
@Documented
public @interface ProductTypeConstraint {

    String message() default "ProductType.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
