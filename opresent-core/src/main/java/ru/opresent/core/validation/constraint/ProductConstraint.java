package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.ProductValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 4:49
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = ProductValidator.class)
@Documented
public @interface ProductConstraint {

    String message() default "Product.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
