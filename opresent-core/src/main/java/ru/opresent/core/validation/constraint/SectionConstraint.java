package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.SectionValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 5:03
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = SectionValidator.class)
@Documented
public @interface SectionConstraint {

    String message() default "Section.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
