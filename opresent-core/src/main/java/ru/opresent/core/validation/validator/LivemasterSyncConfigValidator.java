package ru.opresent.core.validation.validator;

import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.validation.constraint.LivemasterSyncConfigConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Map;

/**
 * User: artem
 * Date: 09.07.2017
 * Time: 0:33
 */
public class LivemasterSyncConfigValidator implements ConstraintValidator<LivemasterSyncConfigConstraint, LivemasterSyncConfig> {

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Override
    public void initialize(LivemasterSyncConfigConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(LivemasterSyncConfig config, ConstraintValidatorContext context) {
        Map<Long, Integer> productTypeIdToPriceDiff = config.getProductTypeIdToPriceDiff();
        if (productTypeIdToPriceDiff.isEmpty()) {
            return true;
        }

        for (Long productTypeId : productTypeIdToPriceDiff.keySet()) {
            if (productTypeId == null) {
                context.buildConstraintViolationWithTemplate("Product type id is null")
                        .addConstraintViolation();
                return false;
            }
            if (!productTypeService.getAll().containsById(productTypeId)) {
                context.buildConstraintViolationWithTemplate("Product type id not found: " + productTypeId)
                        .addConstraintViolation();
                return false;
            }
            Integer priceDiff = productTypeIdToPriceDiff.get(productTypeId);
            if (priceDiff == null) {
                context.buildConstraintViolationWithTemplate("Price diff is null for product type id: " + productTypeId)
                        .addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
