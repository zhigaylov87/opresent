package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.ProductLabelValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 04.01.15
 * Time: 5:46
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = ProductLabelValidator.class)
@Documented
public @interface ProductLabelConstraint {

    String message() default "ProductLabel.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
