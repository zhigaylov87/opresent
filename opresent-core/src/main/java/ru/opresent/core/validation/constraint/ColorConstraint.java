package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.ColorValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 04.01.15
 * Time: 0:40
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = ColorValidator.class)
@Documented
public @interface ColorConstraint {

    String message() default "Color.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
