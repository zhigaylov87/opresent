package ru.opresent.core.validation.validator;

import ru.opresent.core.dao.api.ProductDao;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.validation.constraint.ProductConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 4:50
 */
public class ProductValidator implements ConstraintValidator<ProductConstraint, Product> {

    @Resource(name = "productDao")
    private ProductDao productDao;

    @Override
    public void initialize(ProductConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Product product, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (product.getStatus() != null) {
            if (product.getStatus() != ProductStatus.EXAMPLE && product.getPrice() == null) {
                context.buildConstraintViolationWithTemplate("{Product.price.empty}").addConstraintViolation();
                return false;
            }
        }

        if (product.getSize() == null && isEmpty(product.getSizeText())) {
            context.buildConstraintViolationWithTemplate("{Product.size.empty}").addConstraintViolation();
            return false;
        }

        if (product.getLabel() == null && isNotEmpty(product.getLabelNote())) {
            context.buildConstraintViolationWithTemplate("Label note defined for product without label")
                    .addConstraintViolation();
            return false;
        }

        if (product.isVisible() && product.getCategories().isEmpty()) {
            context.buildConstraintViolationWithTemplate("{Product.categories.emptyForVisible}").addConstraintViolation();
            return false;
        }

        String livemasterId = product.getLivemasterId();
        if (!isEmpty(livemasterId)) {
            Long productId = productDao.loadProductIdByLivemasterId(livemasterId);
            if (productId != null && !productId.equals(product.getId())) {
                context.buildConstraintViolationWithTemplate("Product with livemaster id = '" + livemasterId + "' already exists.")
                        .addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
