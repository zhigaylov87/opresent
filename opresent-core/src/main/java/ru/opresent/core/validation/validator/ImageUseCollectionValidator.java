package ru.opresent.core.validation.validator;

import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.validation.constraint.ImageUseConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 5:29
 */
public class ImageUseCollectionValidator implements ConstraintValidator<ImageUseConstraint, Collection<Image>> {

    private ImageUse requiredImageUse;

    @Override
    public void initialize(ImageUseConstraint constraintAnnotation) {
        requiredImageUse = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Collection<Image> images, ConstraintValidatorContext context) {
        if (images != null) {
            for (Image image : images) {
                if (image.getImageUse() != requiredImageUse) {
                    return false;
                }
            }
        }

        return true;
    }
}
