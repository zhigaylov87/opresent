package ru.opresent.core.validation.validator;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import ru.opresent.core.dao.api.SectionDao;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.validation.constraint.SectionConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 5:03
 */
public class SectionValidator implements ConstraintValidator<SectionConstraint, Section> {

    @Resource(name = "sectionDao")
    private SectionDao dao;

    @Override
    public void initialize(SectionConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Section section, ConstraintValidatorContext context) {
        if (section.getName() != null) {
            Section theSameName = dao.findTheSameName(section.getName());
            if (theSameName != null && theSameName.getId() != section.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("name", section.getName())
                        .buildConstraintViolationWithTemplate("{Section.name.sameExists}")
                        .addConstraintViolation();

                return false;
            }
        }

        if (section.getEntityName() != null) {
            Section theSameEntityName = dao.findTheSameEntityName(section.getEntityName());
            if (theSameEntityName != null && theSameEntityName.getId() != section.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("entity_name", section.getEntityName())
                        .buildConstraintViolationWithTemplate("{Section.entityName.sameExists}")
                        .addConstraintViolation();

                return false;
            }
        }

        return true;
    }
}
