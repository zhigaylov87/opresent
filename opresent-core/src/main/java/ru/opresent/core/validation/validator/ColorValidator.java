package ru.opresent.core.validation.validator;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import ru.opresent.core.dao.api.ColorDao;
import ru.opresent.core.domain.Color;
import ru.opresent.core.validation.constraint.ColorConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 04.01.15
 * Time: 0:42
 */
public class ColorValidator implements ConstraintValidator<ColorConstraint, Color> {

    @Resource(name = "colorDao")
    private ColorDao colorDao;

    @Override
    public void initialize(ColorConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Color color, ConstraintValidatorContext context) {
        if (color.getRgb() != null) {
            Color theSame = colorDao.findTheSame(color.getRgb());

            if (theSame != null && theSame.getId() != color.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("colorDesc", color.getDescription())
                        .buildConstraintViolationWithTemplate("{Color.sameExists}")
                        .addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
