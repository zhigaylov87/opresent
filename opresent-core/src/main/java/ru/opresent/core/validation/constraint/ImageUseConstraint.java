package ru.opresent.core.validation.constraint;

import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.validation.validator.ImageUseCollectionValidator;
import ru.opresent.core.validation.validator.ImageUseValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 4:27
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = {ImageUseValidator.class, ImageUseCollectionValidator.class})
@Documented
public @interface ImageUseConstraint {

    String message() default "ImageUseConstraint.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    ImageUse value();
}
