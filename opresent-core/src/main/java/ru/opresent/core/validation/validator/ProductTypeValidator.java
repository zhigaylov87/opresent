package ru.opresent.core.validation.validator;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import ru.opresent.core.dao.api.ProductTypeDao;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.validation.constraint.ProductTypeConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 4:09
 */
public class ProductTypeValidator implements ConstraintValidator<ProductTypeConstraint, ProductType> {

    @Resource(name = "productTypeDao")
    private ProductTypeDao dao;

    @Override
    public void initialize(ProductTypeConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(ProductType productType, ConstraintValidatorContext context) {
        if (productType.getName() != null) {
            ProductType theSameName = dao.findTheSameName(productType.getName());
            if (theSameName != null && theSameName.getId() != productType.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("name", productType.getName())
                        .buildConstraintViolationWithTemplate("{ProductType.name.sameExists}")
                        .addConstraintViolation();
                return false;
            }
        }

        if (productType.getEntityName() != null) {
            ProductType theSameEntityName = dao.findTheSameEntityName(productType.getEntityName());
            if (theSameEntityName != null && theSameEntityName.getId() != productType.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("entity_name", productType.getEntityName())
                        .buildConstraintViolationWithTemplate("{ProductType.entityName.sameExists}")
                        .addConstraintViolation();

                return false;
            }
        }

        return true;
    }
}
