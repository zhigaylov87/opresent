package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.LivemasterAliasesValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 04.01.15
 * Time: 4:14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = LivemasterAliasesValidator.class)
@Documented
public @interface LivemasterAliasesConstraint {

    String message() default "LivemasterAliases.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
