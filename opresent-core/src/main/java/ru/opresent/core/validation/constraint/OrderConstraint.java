package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.OrderValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 22.02.15
 * Time: 22:58
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = OrderValidator.class)
@Documented
public @interface OrderConstraint {

    String message() default "Order.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
