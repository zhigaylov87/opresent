package ru.opresent.core.validation;

import org.springframework.stereotype.Component;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 7:10
 */
@Component("entitiesConstraints")
public class EntitiesConstraints {

    public static final int PRODUCT_LIVEMASTER_ID_MAX_LENGTH = 64;
    public static final int PRODUCT_NAME_MAX_LENGTH = 128;
    public static final int PRODUCT_DESCRIPTION_MAX_LENGTH = 2048;
    public static final int PRODUCT_SIZE_TEXT_MAX_LENGTH = 64;
    public static final int PRODUCT_STATUS_NOTE_MAX_LENGTH = 256;
    public static final int PRODUCT_PRODUCTION_TIME_MAX_LENGTH = 64;
    public static final int PRODUCT_LABEL_NOTE_MAX_LENGTH = 256;
    public static final int PRODUCT_PRICE_MIN_VALUE = 1;

    public static final int PRODUCT_TYPE_NAME_MAX_LENGTH = 64;
    public static final int PRODUCT_TYPE_DESCRIPTION_MAX_LENGTH = 512;

    public static final int CATEGORY_NAME_MAX_LENGTH = 64;
    public static final int CATEGORY_DESCRIPTION_MAX_LENGTH = 512;

    public static final int IMAGE_FORMAT_MAX_LENGTH = 512;

    public static final int PRODUCT_KEYWORD_MAX_LENGTH = 64;

    public static final int INCOMING_MESSAGE_CUSTOMER_NAME_MAX_LENGTH = 64;
    public static final int INCOMING_MESSAGE_CUSTOMER_EMAIL_OR_PHONE_MAX_LENGTH = 64;
    public static final int INCOMING_MESSAGE_MESSAGE_TEXT_MAX_LENGTH = 1024;

    public static final int LIVEMASTER_ALIAS_MIN_LENGTH = 1;
    public static final int LIVEMASTER_ALIAS_MAX_LENGTH = 64;

    public static final int SECTION_NAME_MAX_LENGTH = 64;

    public static final int SECTION_ITEM_NAME_MAX_LENGTH = 64;
    public static final int SECTION_ITEM_SHORT_TEXT_MAX_LENGTH = 512;

    public static final int ORDER_MASTER_COMMENT_MAX_LENGTH = 1024;

    public static final int ORDER_INFO_CUSTOMER_FULL_NAME_MAX_LENGTH = 128;
    public static final int ORDER_INFO_CUSTOMER_PHONE_MAX_LENGTH = 64;
    public static final int ORDER_INFO_CUSTOMER_EMAIL_MAX_LENGTH = 64;
    public static final int ORDER_INFO_CUSTOMER_CITY_MAX_LENGTH = 64;
    public static final int ORDER_INFO_CUSTOMER_POSTCODE_MAX_LENGTH = 16;
    public static final int ORDER_INFO_CUSTOMER_ADDRESS_MAX_LENGTH = 512;
    public static final int ORDER_INFO_CUSTOMER_COMMENT_MAX_LENGTH = 1024;

    public static final int ORDER_SETTING_PERSONAL_ORDER_TEXT_MAX_LENGTH = 2048;

    public static final int DELIVERY_WAY_PRICE_MAX_LENGTH = 32;
    public static final int DELIVERY_WAY_NOTE_MAX_LENGTH = 1024;

    /**
     * @see ru.opresent.core.domain.NamedEntity
     * @see ru.opresent.core.validation.constraint.EntityName
     */
    public static final int ENTITY_NAME_MIN_LENGTH = 1;
    /**
     * @see ru.opresent.core.domain.NamedEntity
     * @see ru.opresent.core.validation.constraint.EntityName
     */
    public static final int ENTITY_NAME_MAX_LENGTH = 32;
    /**
     * @see ru.opresent.core.domain.NamedEntity
     * @see ru.opresent.core.validation.constraint.EntityName
     */
    public static final String ENTITY_NAME_REGEXP = "[a-z0-9-]+";

    public static final int BANNER_ITEM_NAME_MAX_LENGTH = 128;

    public static final int BANNER_ITEM_URL_MAX_LENGTH = 128;

    public static final int NEWS_ITEM_TEXT_MAX_LENGTH = 512;

    public int getProductNameMaxLength() {
        return PRODUCT_NAME_MAX_LENGTH;
    }

    public int getProductDescriptionMaxLength() {
        return PRODUCT_DESCRIPTION_MAX_LENGTH;
    }

    public int getProductSizeTextMaxLength() {
        return PRODUCT_SIZE_TEXT_MAX_LENGTH;
    }

    public int getProductStatusNoteMaxLength() {
        return PRODUCT_STATUS_NOTE_MAX_LENGTH;
    }

    public int getProductProductionTimeMaxLength() {
        return PRODUCT_PRODUCTION_TIME_MAX_LENGTH;
    }

    public int getProductLabelNoteMaxLength() {
        return PRODUCT_LABEL_NOTE_MAX_LENGTH;
    }

    public int getProductTypeNameMaxLength() {
        return PRODUCT_TYPE_NAME_MAX_LENGTH;
    }

    public int getProductTypeDescriptionMaxLength() {
        return PRODUCT_TYPE_DESCRIPTION_MAX_LENGTH;
    }

    public int getCategoryNameMaxLength() {
        return CATEGORY_NAME_MAX_LENGTH;
    }

    public int getCategoryDescriptionMaxLength() {
        return CATEGORY_DESCRIPTION_MAX_LENGTH;
    }

    public int getSectionNameMaxLength() {
        return SECTION_NAME_MAX_LENGTH;
    }

    public int getSectionItemNameMaxLength() {
        return SECTION_ITEM_NAME_MAX_LENGTH;
    }

    public int getOrderMasterCommentMaxLength() {
        return ORDER_MASTER_COMMENT_MAX_LENGTH;
    }

    public int getSectionItemShortTextMaxLength() {
        return SECTION_ITEM_SHORT_TEXT_MAX_LENGTH;
    }

    public int getOrderInfoCustomerFullNameMaxLength() {
        return ORDER_INFO_CUSTOMER_FULL_NAME_MAX_LENGTH;
    }

    public int getOrderInfoCustomerPhoneMaxLength() {
        return ORDER_INFO_CUSTOMER_PHONE_MAX_LENGTH;
    }

    public int getOrderInfoCustomerEmailMaxLength() {
        return ORDER_INFO_CUSTOMER_EMAIL_MAX_LENGTH;
    }

    public int getOrderInfoCustomerCityMaxLength() {
        return ORDER_INFO_CUSTOMER_CITY_MAX_LENGTH;
    }

    public int getOrderInfoCustomerPostcodeMaxLength() {
        return ORDER_INFO_CUSTOMER_POSTCODE_MAX_LENGTH;
    }

    public int getOrderInfoCustomerAddressMaxLength() {
        return ORDER_INFO_CUSTOMER_ADDRESS_MAX_LENGTH;
    }

    public int getOrderInfoCustomerCommentMaxLength() {
        return ORDER_INFO_CUSTOMER_COMMENT_MAX_LENGTH;
    }

    public int getDeliveryWayNoteMaxLength() {
        return DELIVERY_WAY_NOTE_MAX_LENGTH;
    }

    public int getEntityNameMaxLength() {
        return ENTITY_NAME_MAX_LENGTH;
    }

    public String getEntityNameRegexp() {
        return ENTITY_NAME_REGEXP;
    }

    public int getBannerItemNameMaxLength() {
        return BANNER_ITEM_NAME_MAX_LENGTH;
    }
}
