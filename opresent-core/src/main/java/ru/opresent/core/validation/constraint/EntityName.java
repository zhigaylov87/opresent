package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.EntitiesConstraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 13.03.15
 * Time: 3:52
 */
@NotNull(message = "{EntityName.empty}")
@Size(
        min = EntitiesConstraints.ENTITY_NAME_MIN_LENGTH,
        max = EntitiesConstraints.ENTITY_NAME_MAX_LENGTH,
        message = "{EntityName.invalidLength}")
@Pattern(regexp = EntitiesConstraints.ENTITY_NAME_REGEXP,
        message = "{EntityName.invalidFormat}")
//@ReportAsSingleViolation
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Constraint(validatedBy = {})
@Documented
public @interface EntityName {

    String message() default "{EntityName.invalid}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}