package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.IncomingMessageValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 11.01.15
 * Time: 20:38
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = IncomingMessageValidator.class)
@Documented
public @interface IncomingMessageConstraint {

    String message() default "IncomingMessage.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
