package ru.opresent.core.validation.validator;

import ru.opresent.core.domain.ProductLabel;
import ru.opresent.core.validation.constraint.ProductLabelConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 04.01.15
 * Time: 5:46
 */
public class ProductLabelValidator implements ConstraintValidator<ProductLabelConstraint, ProductLabel> {

    @Override
    public void initialize(ProductLabelConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(ProductLabel label, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (label.isWithDiscount()) {
            if (label.getDiscountPercent() == null) {
                context.buildConstraintViolationWithTemplate("{ProductLabel.emptyDiscountPercentForWithDiscount}")
                    .addConstraintViolation();
                return false;
            }
        } else {
            if (label.getDiscountPercent() != null) {
                context.buildConstraintViolationWithTemplate("Discount percent defined for label without discount")
                    .addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
