package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.CategoryValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 4:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = CategoryValidator.class)
@Documented
public @interface CategoryConstraint {

    String message() default "Category.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
