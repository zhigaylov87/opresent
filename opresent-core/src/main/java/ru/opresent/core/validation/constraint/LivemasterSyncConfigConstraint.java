package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.LivemasterSyncConfigValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 09.07.2017
 * Time: 0:33
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = LivemasterSyncConfigValidator.class)
@Documented
public @interface LivemasterSyncConfigConstraint {

    String message() default "LivemasterSyncConfig.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
