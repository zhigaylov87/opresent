package ru.opresent.core.validation.validator;

import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.ProductDao;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.core.validation.constraint.IncomingMessageConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 11.01.15
 * Time: 20:38
 */
public class IncomingMessageValidator implements ConstraintValidator<IncomingMessageConstraint, IncomingMessage> {

    @Resource(name = "productDao")
    private ProductDao productDao;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Override
    public void initialize(IncomingMessageConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(IncomingMessage message, ConstraintValidatorContext context) {
        if (message.getSubjectId() != null && message.getSubjectType() != null) {
            switch (message.getSubjectType()) {
                case PRODUCT:
                    if (!productDao.isExistsWithId(message.getSubjectId())) {
                        context.buildConstraintViolationWithTemplate("Product with id = " + message.getSubjectId() + " not exists.")
                               .addConstraintViolation();
                        return false;
                    }
                    break;

                case SECTION:
                    IdentifiableList<Section> sections = sectionService.getAll();
                    if (!sections.containsById(message.getSubjectId())) {
                        context.buildConstraintViolationWithTemplate("Section with id = " + message.getSubjectId() + " not exists.")
                                .addConstraintViolation();
                        return false;
                    }

                    Section section = sections.getById(message.getSubjectId());
                    if (!section.isVisible()) {
                        context.buildConstraintViolationWithTemplate("Section with id = " + message.getSubjectId() + " not visible.")
                                .addConstraintViolation();
                        return false;
                    }
                    break;

                default:
                    throw new CoreException("Unknown incoming message subject type: " + message.getSubjectType());
            }
        }

        return true;
    }
}
