package ru.opresent.core.validation.validator;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import ru.opresent.core.domain.Category;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.util.IdentifiableNamedEntityList;
import ru.opresent.core.validation.constraint.CategoryConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 4:09
 */
public class CategoryValidator implements ConstraintValidator<CategoryConstraint, Category> {

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Override
    public void initialize(CategoryConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Category category, ConstraintValidatorContext context) {

        String entityName = category.getEntityName();
        if (entityName != null) {
            IdentifiableNamedEntityList<Category> categories = categoryService.getAll();

            if (categories.containsByEntityName(entityName) && categories.getByEntityName(entityName).getId() != category.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("entity_name", entityName)
                        .buildConstraintViolationWithTemplate("{Category.entityName.sameExists}")
                        .addConstraintViolation();

                return false;
            }
        }

        return true;
    }
}
