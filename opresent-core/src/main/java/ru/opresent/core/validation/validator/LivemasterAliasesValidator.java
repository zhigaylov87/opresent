package ru.opresent.core.validation.validator;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.CategoryDao;
import ru.opresent.core.dao.api.LivemasterAliasDao;
import ru.opresent.core.dao.api.ProductTypeDao;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.LivemasterAliasesConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 04.01.15
 * Time: 4:15
 */
public class LivemasterAliasesValidator implements ConstraintValidator<LivemasterAliasesConstraint, LivemasterAliases> {

    @Resource(name = "livemasterAliasDao")
    private LivemasterAliasDao livemasterAliasDao;

    @Resource(name = "categoryDao")
    private CategoryDao categoryDao;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "productTypeDao")
    private ProductTypeDao productTypeDao;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Override
    public void initialize(LivemasterAliasesConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(LivemasterAliases aliases, ConstraintValidatorContext context) {
        Helper helper;
        switch (aliases.getType()) {
            case CATEGORY:
                helper = new CategoryHelper();
                break;

            case PRODUCT_TYPE:
                helper = new ProductTypeHelper();
                break;

            default:
                throw new CoreException("Unknown livemaster alias type: " + aliases.getType());
        }

        helper.checkEntityExists(aliases.getId());

        for (String alias : aliases.getAliases()) {
            if (alias == null) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("{LivemasterAliases.empty}").addConstraintViolation();
                return false;
            }
            if (alias.length() < EntitiesConstraints.LIVEMASTER_ALIAS_MIN_LENGTH ||
                    alias.length() > EntitiesConstraints.LIVEMASTER_ALIAS_MAX_LENGTH) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("alias", alias)
                        .addExpressionVariable("min", EntitiesConstraints.LIVEMASTER_ALIAS_MIN_LENGTH)
                        .addExpressionVariable("max", EntitiesConstraints.LIVEMASTER_ALIAS_MAX_LENGTH)
                        .buildConstraintViolationWithTemplate("{LivemasterAliases.invalidLength}")
                        .addConstraintViolation();
                return false;
            }

            Long sameAliasEntityId = livemasterAliasDao.findEntityIdByAlias(aliases.getType(), alias);
            if (sameAliasEntityId != null && !sameAliasEntityId.equals(aliases.getId())) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("alias", alias)
                        .addExpressionVariable("entityName", helper.getEntityName(sameAliasEntityId))
                        .buildConstraintViolationWithTemplate(helper.getMessageTemplate())
                        .addConstraintViolation();
                return false;
            }
        }

        return true;
    }

    private interface Helper {

        void checkEntityExists(long entityId);

        String getEntityName(long entityId);

        String getMessageTemplate();
    }

    private class CategoryHelper implements Helper {
        @Override
        public void checkEntityExists(long entityId) {
            if (!categoryDao.isExistsWithId(entityId)) {
                throw new CoreException("Category with id = " + entityId + " not exists");
            }
        }

        @Override
        public String getEntityName(long entityId) {
            return categoryService.getAll().getById(entityId).getName();
        }

        @Override
        public String getMessageTemplate() {
            return "{LivemasterAliases.category.sameExists}";
        }
    }

    private class ProductTypeHelper implements Helper {
        @Override
        public void checkEntityExists(long entityId) {
            if (!productTypeDao.isExistsWithId(entityId)) {
                throw new CoreException("Product type with id = " + entityId + " not exists");
            }
        }

        @Override
        public String getEntityName(long entityId) {
            return productTypeService.getAll().getById(entityId).getName();
        }

        @Override
        public String getMessageTemplate() {
            return "{LivemasterAliases.productType.sameExists}";
        }
    }
}
