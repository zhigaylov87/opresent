package ru.opresent.core.validation.validator;

import ru.opresent.core.domain.order.AddressType;
import ru.opresent.core.domain.order.DeliveryWay;
import ru.opresent.core.domain.order.OrderInfo;
import ru.opresent.core.validation.constraint.OrderInfoConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * User: artem
 * Date: 22.02.15
 * Time: 22:58
 */
public class OrderInfoValidator implements ConstraintValidator<OrderInfoConstraint, OrderInfo> {

    @Override
    public void initialize(OrderInfoConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(OrderInfo orderInfo, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (orderInfo.isPersonalOrder() && isBlank(orderInfo.getCustomerComment())) {
            context.buildConstraintViolationWithTemplate("{OrderInfo.customerComment.empty}").addConstraintViolation();
            return false;
        }

        AddressType addressType = orderInfo.getAddressType();
        DeliveryWay deliveryWay = orderInfo.getDeliveryWay();

        if (addressType == null) {
            if (!orderInfo.isPersonalOrder()) {
                context.buildConstraintViolationWithTemplate("{OrderInfo.addressType.empty}").addConstraintViolation();
                return false;
            }
            if (deliveryWay != null) {
                context.buildConstraintViolationWithTemplate("{OrderInfo.deliveryWay.notEmpty}").addConstraintViolation();
                return false;
            }
            return isMustBeEmptyValid(context, orderInfo.getCustomerCity(), "{OrderInfo.customerCity.notEmpty}") &&
                    isMustBeEmptyValid(context, orderInfo.getCustomerPostcode(), "{OrderInfo.customerPostcode.notEmpty}") &&
                    isMustBeEmptyValid(context, orderInfo.getCustomerAddress(), "{OrderInfo.customerAddress.notEmpty}");
        } else {
            if (!isFillingValid(context, addressType.isCityRequired(), orderInfo.getCustomerCity(),
                    "{OrderInfo.customerCity.empty}", "{OrderInfo.customerCity.notEmpty}")) {
                return false;
            }
            if (deliveryWay == null) {
                context.buildConstraintViolationWithTemplate("{OrderInfo.deliveryWay.empty}").addConstraintViolation();
                return false;
            }
            if (!addressType.getDeliveryWays().contains(deliveryWay)) {
                context.buildConstraintViolationWithTemplate("{OrderInfo.deliveryWay.notSupportedForAddressType}")
                        .addConstraintViolation();
                return false;
            }
            return isFillingValid(context, deliveryWay.isPostcodeRequired(), orderInfo.getCustomerPostcode(),
                    "{OrderInfo.customerPostcode.empty}", "{OrderInfo.customerPostcode.notEmpty}")
                    &&
                    isFillingValid(context, deliveryWay.isAddressRequired(), orderInfo.getCustomerAddress(),
                            "{OrderInfo.customerAddress.empty}", "{OrderInfo.customerAddress.notEmpty}");
        }
    }

    private boolean isMustBeEmptyValid(ConstraintValidatorContext context, String value, String notEmptyMsg) {
        if (isNotBlank(value)) {
            context.buildConstraintViolationWithTemplate(notEmptyMsg).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isMustBeNotEmptyValid(ConstraintValidatorContext context, String value, String emptyMsg) {
        if (isBlank(value)) {
            context.buildConstraintViolationWithTemplate(emptyMsg).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isFillingValid(ConstraintValidatorContext context, boolean mustBeNotEmpty, String value,
                                   String emptyMsg, String notEmptyMsg) {
        return mustBeNotEmpty ?
                isMustBeNotEmptyValid(context, value, emptyMsg) :
                isMustBeEmptyValid(context, value, notEmptyMsg);
    }
}
