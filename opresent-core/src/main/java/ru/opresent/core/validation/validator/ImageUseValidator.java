package ru.opresent.core.validation.validator;

import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.validation.constraint.ImageUseConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 4:27
 */
public class ImageUseValidator implements ConstraintValidator<ImageUseConstraint, Image> {

    private ImageUse requiredImageUse;

    @Override
    public void initialize(ImageUseConstraint constraintAnnotation) {
        requiredImageUse = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Image image, ConstraintValidatorContext context) {
        return image == null || image.getImageUse() == requiredImageUse;
    }
}
