package ru.opresent.core.validation.validator;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import ru.opresent.core.dao.api.SizeDao;
import ru.opresent.core.domain.Size;
import ru.opresent.core.validation.constraint.SizeConstraint;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 5:49
 */
public class SizeValidator implements ConstraintValidator<SizeConstraint, Size> {

    @Resource(name = "sizeDao")
    private SizeDao sizeDao;

    @Override
    public void initialize(SizeConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Size size, ConstraintValidatorContext context) {
        if (size.getWidth() != null && size.getHeight() != null) {
            Size theSame = sizeDao.findTheSame(size.getWidth(), size.getHeight());
            if (theSame != null && theSame.getId() != size.getId()) {
                context.disableDefaultConstraintViolation();
                context.unwrap(HibernateConstraintValidatorContext.class)
                        .addExpressionVariable("sizeDesc", size.getDescription())
                        .buildConstraintViolationWithTemplate("{Size.sameExists}")
                        .addConstraintViolation();

                return false;
            }
        }

        return true;
    }
}
