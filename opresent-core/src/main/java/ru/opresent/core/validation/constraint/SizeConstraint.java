package ru.opresent.core.validation.constraint;

import ru.opresent.core.validation.validator.SizeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 05.01.15
 * Time: 5:48
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = SizeValidator.class)
@Documented
public @interface SizeConstraint {

    String message() default "Size.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
