package ru.opresent.core.search;

import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Products must be sorted by status, score.
 *
 * User: artem
 * Date: 06.06.15
 * Time: 19:49
 */
public class ProductSearchResult {

    public static final int MAX_PRODUCTS_COUNT = 50;

    private List<Product> products;

    private LinkedHashMap<ProductStatus, List<Product>> statusToProducts;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        statusToProducts = new LinkedHashMap<>();
        for (Product product : products) {
            ProductStatus status = product.getStatus();
            List<Product> statusProducts = statusToProducts.get(status);
            if (statusProducts == null) {
                statusProducts = new ArrayList<>();
                statusToProducts.put(status, statusProducts);
            }
            statusProducts.add(product);
        }
    }

    public List<ProductStatus> getProductStatuses() {
        return new ArrayList<>(statusToProducts.keySet());
    }

    public List<Product> getProductsByStatus(ProductStatus status) {
        return statusToProducts.get(status);
    }
}
