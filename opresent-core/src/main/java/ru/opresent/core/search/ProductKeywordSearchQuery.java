package ru.opresent.core.search;

import ru.opresent.core.util.FormatUtils;

/**
 * User: artem
 * Date: 13.06.15
 * Time: 5:22
 */
public class ProductKeywordSearchQuery {

    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "ProductKeywordSearchQuery{" +
                "query: " + FormatUtils.quotes(query) +
                '}';
    }
}
