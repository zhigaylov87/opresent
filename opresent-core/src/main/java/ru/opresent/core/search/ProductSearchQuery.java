package ru.opresent.core.search;

import ru.opresent.core.util.FormatUtils;

/**
 * User: artem
 * Date: 02.06.15
 * Time: 2:51
 */
public class ProductSearchQuery {

    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "ProductSearchQuery {" +
                "query: " + FormatUtils.quotes(query) +
                "}";
    }
}
