package ru.opresent.core.search;

import java.util.List;

/**
 * User: artem
 * Date: 13.06.15
 * Time: 5:23
 */
public class ProductKeywordSearchResult {

    public static final int MAX_PRODUCT_KEYWORDS_COUNT = 10;

//    private List<String> highlightedKeywords;
    private List<String> keywords;

    // TODO: Раскомментировать подсветку ключевых слов(highlightedKeywords) и использовать их в интерфейсе
//    public List<String> getHighlightedKeywords() {
//        return highlightedKeywords;
//    }
//
//    public void setHighlightedKeywords(List<String> highlightedKeywords) {
//        this.highlightedKeywords = highlightedKeywords;
//    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
}
