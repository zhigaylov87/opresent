package ru.opresent.core.service.impl;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import ru.opresent.core.dao.api.LivemasterSyncConfigDao;
import ru.opresent.core.domain.DefaultProductFilter;
import ru.opresent.core.domain.Product;
import ru.opresent.core.livemaster.sync.LivemasterSyncProcessHandler;
import ru.opresent.core.livemaster.sync.LivemasterSyncProduct;
import ru.opresent.core.livemaster.sync.LivemasterSyncProductResult;
import ru.opresent.core.livemaster.sync.LivemasterSynchronizer;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEvent;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventDataField;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventListener;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventType;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.pagination.SinglePageSettings;
import ru.opresent.core.service.api.LivemasterSyncService;
import ru.opresent.core.service.api.ProductService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 22.04.2017
 * Time: 20:07
 */
@Service("livemasterSyncService")
public class LivemasterSyncServiceImpl implements LivemasterSyncService {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "livemasterSynchronizer")
    private LivemasterSynchronizer synchronizer;

    @Resource(name = "livemasterSyncConfigDao")
    private LivemasterSyncConfigDao configDao;

    @Override
    public void saveConfig(@Valid LivemasterSyncConfig config) throws ConstraintViolationException {
        configDao.save(config);
    }

    @Override
    public LivemasterSyncConfig getConfig() {
        LivemasterSyncConfig config = configDao.load();
        return config != null ? config : new LivemasterSyncConfig();
    }

    @Override
    public void synchronize(LivemasterSyncProcessHandler processHandler) {
        LivemasterSyncEventListener eventListener = processHandler.getEventListener();
        // start
        eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.SYNC_START));

        if (isSyncStopped(processHandler)) {
            return;
        }

        // livemaster sync products determining
        eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.SYNC_PRODUCTS_DETERMINING_START));
        List<LivemasterSyncProduct> syncProducts;
        try {
            log.info("Sync products determining...");
            syncProducts = synchronizer.determineSyncProducts();
            log.info("Sync products determining complete. Count: " + syncProducts.size());
            eventListener.processEvent(LivemasterSyncEvent.of(
                    LivemasterSyncEventType.SYNC_PRODUCTS_DETERMINING_COMPLETE,
                    LivemasterSyncEventDataField.COUNT, syncProducts.size()));
        } catch (Exception e) {
            log.error("Sync products determining failed", e);
            eventListener.processEvent(LivemasterSyncEvent.of(
                    LivemasterSyncEventType.SYNC_PRODUCTS_DETERMINING_FAILED,
                    LivemasterSyncEventDataField.EXCEPTION, e));
            return;
        }

        if (isSyncStopped(processHandler)) {
            return;
        }

        // livemaster sync products processing
        eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.SYNC_PRODUCTS_PROCESSING_START));
        LivemasterSyncConfig syncConfig = getConfig();
        for (LivemasterSyncProduct syncProduct : syncProducts) {
            if (isSyncStopped(processHandler)) {
                return;
            }
            LivemasterSyncProductResult result;
            String logMsg = "Sync product: " + syncProduct;
            try {
                log.info(logMsg + "...");
                result = synchronizer.processSyncProduct(syncProduct, syncConfig);
                log.info(logMsg + " complete. Result: " + result);
            } catch (Exception e) {
                log.error(logMsg + " failed", e);
                result = LivemasterSyncProductResult.createErrorResult(e);
            }
            eventListener.processEvent(LivemasterSyncEvent.of(
                    LivemasterSyncEventType.SYNC_PRODUCT_PROCESSED,
                    LivemasterSyncEventDataField.SYNC_PRODUCT, syncProduct,
                    LivemasterSyncEventDataField.SYNC_PRODUCT_RESULT, result));
        }
        eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.SYNC_PRODUCTS_PROCESSING_COMPLETE));

        if (isSyncStopped(processHandler)) {
            return;
        }

        // not actual livemaster id products processing
        eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.NOT_ACTUAL_LIVEMASTER_IDS_PROCESSING_START));
        Set<String> actualLivemasterIds = new HashSet<>();
        for (LivemasterSyncProduct syncProduct : syncProducts) {
            actualLivemasterIds.add(syncProduct.getLivemasterProductId());
        }

        Set<String> notActualLivemasterIds = new HashSet<>();
        ProductPage allProducts = productService.load(new DefaultProductFilter(), new SinglePageSettings());
        for (Product product : allProducts.getItems()) {
            String livemasterId = product.getLivemasterId();
            if (livemasterId != null && !actualLivemasterIds.contains(livemasterId)) {
                notActualLivemasterIds.add(livemasterId);
            }
        }
        eventListener.processEvent(LivemasterSyncEvent.of(
                LivemasterSyncEventType.NOT_ACTUAL_LIVEMASTER_IDS_DETERMINED,
                LivemasterSyncEventDataField.COUNT, notActualLivemasterIds.size()));

        for (String livemasterId : notActualLivemasterIds) {
            if (isSyncStopped(processHandler)) {
                return;
            }
            log.info("Process not actual livemaster id '" + livemasterId + "'");
            Product product = synchronizer.processNotActualLivemasterId(livemasterId);
            log.info("Process not actual livemaster id '" + livemasterId + "'" + "complete. Product: " + product);
            eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.NOT_ACTUAL_LIVEMASTER_ID_PROCESSED,
                    LivemasterSyncEventDataField.LIVEMASTER_ID, livemasterId,
                    LivemasterSyncEventDataField.PRODUCT, product));
        }
        eventListener.processEvent(LivemasterSyncEvent.of(
                LivemasterSyncEventType.NOT_ACTUAL_LIVEMASTER_ID_PROCESSING_COMPLETE));

        eventListener.processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.SYNC_COMPLETE));
    }

    private boolean isSyncStopped(LivemasterSyncProcessHandler config) {
        if (config.isSyncStopped()) {
            log.info("Sync stopped");
            config.getEventListener().processEvent(LivemasterSyncEvent.of(LivemasterSyncEventType.SYNC_STOPPED));
            return true;
        }
        return false;
    }
}
