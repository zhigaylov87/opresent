package ru.opresent.core.service.api;

import ru.opresent.core.livemaster.sync.LivemasterSyncProcessHandler;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 22.04.2017
 * Time: 20:07
 */
public interface LivemasterSyncService {

    void saveConfig(@Valid LivemasterSyncConfig config) throws ConstraintViolationException;

    LivemasterSyncConfig getConfig();

    void synchronize(LivemasterSyncProcessHandler processHandler);

}
