package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.dao.api.ProductTypeDao;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ProductTypeImageName;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.util.IdentifiableNamedEntityList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:30
 */
@Transactional
@Service("productTypeService")
public class ProductTypeServiceImpl implements ProductTypeService {

    @Resource(name = "productTypeDao")
    private ProductTypeDao productTypeDao;

    @Resource(name = "orderedEntityServiceHelper")
    private OrderedEntityServiceHelper orderedEntityServiceHelper;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper orderedEntityDaoHelper;

    @Resource(name = "livemasterAliasService")
    private LivemasterAliasService livemasterAliasService;

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;


    @CacheEvict(value = {CacheName.PRODUCT_TYPES, CacheName.CATALOG}, allEntries = true)
    @Override
    public long save(@Valid ProductType productType) throws ConstraintViolationException {
        long productTypeId = productType.getId();
        boolean isNew = productTypeId == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            // add product type to the end
            productType.setIndex(orderedEntityDaoHelper.getTotalCount(productType));
            productTypeId = productTypeDao.save(productType);
        } else {
            productTypeDao.update(productType);
        }
        // update image file name
        Image image = productType.getImage();
        image.setImageName(new ProductTypeImageName(productType));
        imageFileManager.updateImageFileName(image);

        return productTypeId;
    }

    @CacheEvict(value = {CacheName.PRODUCT_TYPES, CacheName.CATALOG}, allEntries = true)
    @Override
    public long save(@Valid ProductType productType, LivemasterAliases aliases) throws ConstraintViolationException {
        if (productType.getId() != aliases.getId()) {
            throw new CoreException("Ids not equals. Product type id = " + productType.getId() +
                    ", livemaster aliases id: " + aliases.getId());
        }
        if (aliases.getType() != LivemasterAliasType.PRODUCT_TYPE) {
            throw new CoreException("Illegal alias type: " + aliases.getType());
        }

        boolean isNew = productType.getId() == Identifiable.NOT_SAVED_ID;
        long productTypeId = save(productType);
        if (isNew) {
            aliases = new LivemasterAliases(aliases);
            aliases.setId(productTypeId);
        }
        livemasterAliasService.save(aliases);

        return productTypeId;
    }

    @CacheEvict(value = {CacheName.PRODUCT_TYPES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void delete(ProductType productType) throws ServiceException {
        if (productType.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved product type!");
        }
        if (productTypeDao.isUsedForProduct(productType.getId())) {
            throw new ServiceException("ProductType.delete_fail_using_by_products", productType.getName());
        }

        // TODO Удалять картинку
        productTypeDao.delete(productType.getId());
        orderedEntityDaoHelper.moveUpAfterIndex(productType, productType.getIndex());

        livemasterAliasService.delete(productType.getId(), LivemasterAliasType.PRODUCT_TYPE);
    }

    @CacheEvict(value = {CacheName.PRODUCT_TYPES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void moveUp(ProductType productType) {
        orderedEntityServiceHelper.moveUpDown(productType, true);
    }

    @CacheEvict(value = {CacheName.PRODUCT_TYPES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void moveDown(ProductType productType) {
        orderedEntityServiceHelper.moveUpDown(productType, false);
    }

    @Cacheable(value = CacheName.PRODUCT_TYPES)
    @Transactional(readOnly = true)
    @Override
    public IdentifiableNamedEntityList<ProductType> getAll() {
        return new IdentifiableNamedEntityList<>(productTypeDao.findAll());
    }
}