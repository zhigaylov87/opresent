package ru.opresent.core.service.api;

import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Set;

/**
 * User: artem
 * Date: 16.12.14
 * Time: 15:49
 */
public interface LivemasterAliasService {

    LivemasterAliases save(@Valid LivemasterAliases aliases) throws ConstraintViolationException;

    void delete(long entityId, LivemasterAliasType type);

    LivemasterAliases load(long entityId, LivemasterAliasType type);

    Set<LivemasterAliases> load(LivemasterAliasType type);
}
