package ru.opresent.core.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.dao.api.CartDao;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.cart.Cart;
import ru.opresent.core.service.api.CartService;
import ru.opresent.core.service.api.ProductService;

import javax.annotation.Resource;
import java.util.*;

/**
 * User: artem
 * Date: 16.11.14
 * Time: 15:27
 */
@Transactional
@Service("cartService")
public class CartServiceImpl implements CartService {

    @Resource(name = "cartDao")
    private CartDao cartDao;

    @Resource(name = "productService")
    private ProductService productService;

    @Transactional(readOnly = true)
    @Override
    public Cart load(String trackingId) {

        Set<Long> productsIds = cartDao.getProductsIds(trackingId);
        if (productsIds.isEmpty()) {
            return new Cart(Collections.<Product>emptyList());
        }

        List<Product> products = new ArrayList<>(productService.loadBase(productsIds));
        Iterator<Product> it = products.iterator();
        while (it.hasNext()) {
            Product product = it.next();
            if (!product.isVisible() || product.getStatus() == ProductStatus.EXAMPLE) {
                it.remove();
            }
        }
        Collections.sort(products, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });

        return new Cart(products);
    }

    @Override
    public void addProduct(String trackingId, long productId) {
        cartDao.addProduct(trackingId, productId);
    }

    @Override
    public void removeProduct(String trackingId, long productId) {
        cartDao.removeProduct(trackingId, productId);
    }
}
