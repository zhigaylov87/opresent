package ru.opresent.core.service.impl.search;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.ProductDao;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductKeyword;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.search.ProductSearchQuery;
import ru.opresent.core.search.ProductSearchResult;
import ru.opresent.core.service.api.ProductService;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

/**
 * User: artem
 * Date: 27.05.15
 * Time: 23:52
 */
@Component("productSearchIndex")
class ProductSearchIndex extends AbstractSearchIndex {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "productDao")
    private ProductDao productDao;

    @Resource(name = "productService")
    private ProductService productService;

    public ProductSearchResult searchProducts(ProductSearchQuery searchQuery) {
        String logMsg = " of searching products for query " + searchQuery;
        try {
            log.info("Start" + logMsg);

            try (Directory directory = getDirectory();
                 IndexReader reader = DirectoryReader.open(directory);
                 Analyzer analyzer = createAnalyzer()) {

                    ProductSearchResult searchResult = createResult(searchQuery, reader, analyzer);

                    log.info(MessageFormat.format("End{0}. Products count: {1}",
                            logMsg, searchResult.getProducts().size()));

                    return searchResult;
            }
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    public void updateAllProductsIndex() {
        String logMsg = " of update all products search index";
        try {
            log.info("Start" + logMsg);

            String processLogMsg = "Processing" + logMsg;
            try (FSDirectory dir = getDirectory()) {
                log.info(processLogMsg + ". Start of clean directory: " + dir);
                FileUtils.cleanDirectory(dir.getDirectory().toFile());
                log.info(processLogMsg + ". End of clean directory.");

                try (Analyzer analyzer = createAnalyzer()) {
                    IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
                    iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
                    try (IndexWriter writer = new IndexWriter(dir, iwc)) {
                        try (EntityIterator<Product> productIterator = productDao.createEntityIterator()) {
                            while (productIterator.hasNext()) {
                                indexProduct(productIterator.next(), writer);
                            }
                        }
                    }
                }
            }

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    public void updateProductIndex(Product product) {
        String logMsg = " of update product search index. Product id: " + product.getId();
        try {
            log.info("Start" + logMsg);
            try (FSDirectory dir = getDirectory()) {
                try (Analyzer analyzer = createAnalyzer()) {
                    IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
                    iwc.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
                    try (IndexWriter writer = new IndexWriter(dir, iwc)) {
                        indexProduct(product, writer);
                    }
                }
            }
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    public void deleteProductIndex(Product product) {
        String logMsg = " of delete product search index. Product id: " + product.getId();
        try {
            log.info("Start" + logMsg);
            try (FSDirectory dir = getDirectory()) {
                try (Analyzer analyzer = createAnalyzer()) {
                    IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
                    iwc.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
                    try (IndexWriter writer = new IndexWriter(dir, iwc)) {
                        writer.deleteDocuments(ProductIndexField.ID.createTerm(product));
                    }
                }
            }
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    protected String getDirectoryName() {
        return "product_index";
    }

    private void indexProduct(Product product, IndexWriter writer) throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Index product: " + product);
        }
        Document doc = new Document();
        for (ProductIndexField productIndexField : ProductIndexField.values()) {
            for (Field field : productIndexField.createFields(product)) {
                doc.add(field);
            }
        }

        if (log.isDebugEnabled()) {
            for (IndexableField field : doc.getFields()) {
                log.debug("Field: " + field);
            }
        }

//        if (writer.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
//            writer.addDocument(doc);
//        } else {
            writer.updateDocument(ProductIndexField.ID.createTerm(product), doc);
//        }
    }

    private Query createQuery(ProductSearchQuery searchQuery, Analyzer analyzer) throws ParseException {
        final ProductIndexField[] productIndexFields = new ProductIndexField[]{ProductIndexField.KEYWORD, ProductIndexField.NAME, ProductIndexField.DESCRIPTION};

        String[] fields = new String[productIndexFields.length];
        Map<String, Float> boosts = new HashMap<>();
        for (int i = 0; i < productIndexFields.length; i++) {
            ProductIndexField productIndexField = productIndexFields[i];
            fields[i] = productIndexField.getName();
            boosts.put(productIndexField.getName(), productIndexField.getBoost());
        }

        MultiFieldQueryParser queryParser = new MultiFieldQueryParser(fields, analyzer, boosts);
        queryParser.setDefaultOperator(QueryParser.Operator.AND);
        return queryParser.parse(searchQuery.getQuery());
    }

    private ProductSearchResult createResult(ProductSearchQuery searchQuery, IndexReader reader,
            Analyzer analyzer) throws ParseException, IOException, InvalidTokenOffsetsException {

        Query query = createQuery(searchQuery, analyzer);
        if (log.isDebugEnabled()) {
            log.debug("Searching for: '" + query + "'");
        }
        IndexSearcher searcher = new IndexSearcher(reader);

        TopDocs topDocs = searcher.search(query, ProductSearchResult.MAX_PRODUCTS_COUNT);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;

        List<Long> productsIds = new ArrayList<>();
        for (ScoreDoc scoreDoc : scoreDocs) {
            if (log.isDebugEnabled()) {
                log.debug("Score doc: " + scoreDoc);
            }
            long productId = Long.parseLong(searcher.doc(scoreDoc.doc).get(ProductIndexField.ID.getName()));
            productsIds.add(productId);
        }
        if (log.isDebugEnabled()) {
            log.debug("Products ids: " + Arrays.toString(productsIds.toArray(new Long[productsIds.size()])));
        }

        List<Product> products = productsIds.isEmpty() ? new ArrayList<Product>() : getSortedProducts(productsIds);
        ProductSearchResult searchResult = new ProductSearchResult();
        searchResult.setProducts(products);

        return searchResult;
    }

    private List<Product> getSortedProducts(final List<Long> productsIds) {
        List<Product> products = new ArrayList<>(productService.loadBase(new HashSet<>(productsIds)));

        Collections.sort(products, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                ProductStatus status1 = p1.getStatus();
                ProductStatus status2 = p2.getStatus();
                return status1 == status2 ?
                        Integer.compare(productsIds.indexOf(p1.getId()), productsIds.indexOf(p2.getId())) :
                        status1.compareTo(status2);
            }
        });

        return products;
    }

    private enum ProductIndexField {

        ID {
            @Override
            public List<? extends Field> createFields(Product product) {
                return Arrays.asList(new LongField(getName(), product.getId(), LongField.TYPE_STORED));
            }

            @Override
            public Term createTerm(Product product) {
                return new Term(getName(), Long.toString(product.getId()));
            }
        },
        NAME {
            @Override
            public List<? extends Field> createFields(Product product) {
                return Arrays.asList(new TextField(getName(), product.getName(), Field.Store.YES));
            }

            @Override
            public float getBoost() {
                return 2.0F;
            }
        },
        DESCRIPTION {
            @Override
            public List<? extends Field> createFields(Product product) {
                return Arrays.asList(new TextField(getName(), product.getDescription(), Field.Store.YES));
            }

            @Override
            public float getBoost() {
                return 1.0F;
            }
        },
        KEYWORD {
            @Override
            public List<? extends Field> createFields(Product product) {
                List<TextField> fields = new ArrayList<>();
                for (ProductKeyword keyword : product.getKeywords()) {
                    fields.add(new TextField(getName(), keyword.getKeyword(), Field.Store.YES));
                }
                return fields;
            }

            @Override
            public float getBoost() {
                return 3.0F;
            }
        };

        public String getName() {
            return name();
        }

        public List<? extends Field> createFields(Product product) {
            throw new UnsupportedOperationException("Unsupported operation for " + this);
        }

        public Term createTerm(Product product) {
            throw new UnsupportedOperationException("Unsupported operation for " + this);
        }

        public float getBoost() {
            throw new UnsupportedOperationException("Unsupported operation for " + this);
        }

    }
}
