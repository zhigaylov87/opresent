package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.dao.api.SectionDao;
import ru.opresent.core.dao.api.SectionItemDao;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.SectionItemImageName;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.IdentifiableNamedEntityList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 20:45
 */
@Transactional
@Service("sectionService")
public class SectionServiceImpl implements SectionService {

    @Resource(name = "sectionDao")
    private SectionDao sectionDao;

    @Resource(name = "orderedEntityServiceHelper")
    private OrderedEntityServiceHelper orderedEntityServiceHelper;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper orderedEntityDaoHelper;

    @Resource(name = "sectionItemDao")
    private SectionItemDao sectionItemDao;

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;


    @CacheEvict(value = CacheName.SECTIONS, allEntries = true)
    @Override
    public long save(@Valid Section section) throws ConstraintViolationException {
        boolean isNew = section.getId() == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            // add section to the end
            section.setIndex(orderedEntityDaoHelper.getTotalCount(section));
            section.setId(sectionDao.save(section));
        } else {
            sectionDao.update(section);
        }
        return section.getId();
    }

    @CacheEvict(value = CacheName.SECTIONS, allEntries = true)
    @Override
    public void deleteSection(long sectionId) {
        if (sectionId == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved section!");
        }
        Section section = sectionDao.load(sectionId);
        if (section.getType() == SectionType.LIST) {
            for (SectionItem sectionItem : loadItems(sectionId)) {
                deleteSectionItem(sectionItem.getId());
            }
        }
        sectionDao.delete(sectionId);
        orderedEntityDaoHelper.moveUpAfterIndex(section, section.getIndex());
    }

    @Cacheable(value = CacheName.SECTIONS, key = "#root.methodName")
    @Transactional(readOnly = true)
    @Override
    public IdentifiableNamedEntityList<Section> getAll() {
        return loadList(false);
    }

    @Cacheable(value = CacheName.SECTIONS, key = "#root.methodName")
    @Transactional(readOnly = true)
    @Override
    public IdentifiableNamedEntityList<Section> getAllVisible() {
        return loadList(true);
    }

    @Transactional(readOnly = true)
    @Override
    public Section load(long id) {
        return sectionDao.load(id);
    }

    @CacheEvict(value = CacheName.SECTIONS, allEntries = true)
    @Override
    public void moveUp(Section section) {
        orderedEntityServiceHelper.moveUpDown(section, true);
    }

    @CacheEvict(value = CacheName.SECTIONS, allEntries = true)
    @Override
    public void moveDown(Section section) {
        orderedEntityServiceHelper.moveUpDown(section, false);
    }

    @Override
    public SectionItem save(@Valid SectionItem sectionItem) throws ConstraintViolationException {
        long sectionItemId = sectionItem.getId();
        boolean isNew = sectionItemId == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            // add section item to the end
            sectionItem.setIndex(orderedEntityDaoHelper.getTotalCount(sectionItem));
            sectionItemId = sectionItemDao.save(sectionItem);
        } else {
            sectionItemDao.update(sectionItem);
        }
        // update preview image file name
        Image preview = sectionItem.getPreview();
        preview.setImageName(new SectionItemImageName(sectionItem));
        imageFileManager.updateImageFileName(preview);
        return loadItem(sectionItemId);
    }

    @Override
    public List<SectionItem> loadItems(long sectionId) {
        return loadItems(sectionId, false);
    }

    @Override
    public List<SectionItem> loadVisibleItems(long sectionId) {
        return loadItems(sectionId, true);
    }

    @Override
    public void deleteSectionItem(long sectionItemId) throws ServiceException {
        if (sectionItemId == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved section item!");
        }
        SectionItem sectionItem = sectionItemDao.load(sectionItemId);

        sectionItemDao.delete(sectionItemId);
        orderedEntityDaoHelper.moveUpAfterIndex(sectionItem, sectionItem.getIndex());
        // TODO Удалять картинку превью через ImageService - SectionItem sectionItem = sectionItemDao.load(sectionItemId);
    }

    @Transactional(readOnly = true)
    @Override
    public SectionItem loadItem(long sectionItemId) {
        return sectionItemDao.load(sectionItemId);
    }

    @Override
    public void moveUp(SectionItem sectionItem) {
        orderedEntityServiceHelper.moveUpDown(sectionItem, true);
    }

    @Override
    public void moveDown(SectionItem sectionItem) {
        orderedEntityServiceHelper.moveUpDown(sectionItem, false);
    }

    private IdentifiableNamedEntityList<Section> loadList(boolean onlyVisible) {
        return new IdentifiableNamedEntityList<>(sectionDao.loadList(onlyVisible));
    }

    private List<SectionItem> loadItems(long sectionId, boolean onlyVisible) {
        return sectionItemDao.loadSectionItems(sectionId, onlyVisible);
    }
}
