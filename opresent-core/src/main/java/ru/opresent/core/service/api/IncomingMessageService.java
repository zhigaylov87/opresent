package ru.opresent.core.service.api;

import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.pagination.IncomingMessagePage;
import ru.opresent.core.pagination.PageSettings;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 22.11.14
 * Time: 16:18
 */
public interface IncomingMessageService {

    /**
     * @param message message
     * @param trackingId trackingId
     * @return saved message id
     * @throws ConstraintViolationException
     */
    long save(@Valid IncomingMessage message, String trackingId) throws ConstraintViolationException;

    IncomingMessage load(long messageId);

    IncomingMessagePage load(PageSettings pageSettings);

    int getNotReadedCount();

    void markReaded(long messageId);
}
