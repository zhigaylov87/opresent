package ru.opresent.core.service.api;

import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.order.Order;

/**
 * User: artem
 * Date: 28.04.15
 * Time: 1:55
 */
public interface NotificationService {

    /**
     * @param order order
     * @return saved notification id
     */
    long saveNewOrderNotification(Order order);

    /**
     * @param message message
     * @return saved notification id
     */
    long saveNewIncomingMessageNotification(IncomingMessage message);

}
