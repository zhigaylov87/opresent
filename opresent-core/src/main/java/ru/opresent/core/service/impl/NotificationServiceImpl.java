package ru.opresent.core.service.impl;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.config.ServerConfig;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.NotificationDao;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderStatus;
import ru.opresent.core.notification.MailNotifier;
import ru.opresent.core.notification.Notification;
import ru.opresent.core.notification.NotificationStatus;
import ru.opresent.core.notification.NotificationSubjectType;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.core.service.api.NotificationService;
import ru.opresent.core.service.api.OrderService;

import javax.annotation.Resource;
import java.util.EnumMap;
import java.util.Map;

/**
 * User: artem
 * Date: 28.04.15
 * Time: 1:56
 */
@Transactional
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    @Resource(name = "currentTimestampHelper")
    private CurrentTimestampHelper currentTimestampHelper;

    @Resource(name = "mailNotifier")
    private MailNotifier mailNotifier;

    @Resource(name = "notificationDao")
    private NotificationDao notificationDao;

    @Resource(name = "orderService")
    private OrderService orderService;

    @Resource(name = "incomingMessageService")
    private IncomingMessageService incomingMessageService;

    @Override
    public long saveNewOrderNotification(Order order) {
        if (order.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot save new order notification for not saved order!");
        }
        if (order.getStatus() != OrderStatus.NEW) {
            throw new IllegalArgumentException("Illegal order status: " + order.getStatus());
        }
        Notification notification = new Notification();
        notification.setSubjectType(NotificationSubjectType.NEW_ORDER);
        notification.setSubjectId(order.getId());
        notification.setStatus(NotificationStatus.NEW);
        notification.setStatusTime(currentTimestampHelper.getTimestamp());
        return notificationDao.save(notification);
    }

    @Override
    public long saveNewIncomingMessageNotification(IncomingMessage message) {
        if (message.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot save new incoming message notification for not saved message!");
        }
        Notification notification = new Notification();
        notification.setSubjectType(NotificationSubjectType.NEW_INCOMING_MESSAGE);
        notification.setSubjectId(message.getId());
        notification.setStatus(NotificationStatus.NEW);
        notification.setStatusTime(currentTimestampHelper.getTimestamp());
        return notificationDao.save(notification);
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED) // no transaction for all sending process
    @Scheduled(fixedDelay = 120_000) // 120 seconds
    @SuppressWarnings("unused")
    public void sendNotifications() {
        if (!serverConfig.isSendNotifications()) {
            log.warn("Sending notifications off");
            return;
        }

        String logMsg = " of send notifications";
        try {
            log.info("Start" + logMsg);

            NotificationStatistic statistic;
            try (EntityIterator<Notification> needToSend = notificationDao.loadNeedToSendNotifications()) {
                statistic = new NotificationStatistic(needToSend.getTotalCount());
                log.info("Total need to send notifications count: " + statistic.totalCount);
                while (needToSend.hasNext()) {
                    statistic.increment(sendNotification(needToSend.next()));
                }
            }

            log.info("End" + logMsg + ". " + statistic);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    /**
     * @param notification notification for sendind
     * @return new notification status
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW) // individual transaction for each notification
    public NotificationStatus sendNotification(Notification notification) { // public modifier for transactional annotation working only
        String logMsg = " of send notification: " + notification;
        try {
            log.info("Start" + logMsg);

            Exception sendFailedException = null;
            try {
                switch (notification.getSubjectType()) {
                    case NEW_ORDER:
                        mailNotifier.notifyNewOrderSaved(orderService.load(notification.getSubjectId()));
                        break;

                    case NEW_INCOMING_MESSAGE:
                        mailNotifier.notifyNewIncomingMessageSaved(incomingMessageService.load(notification.getSubjectId()));
                        break;

                    default:
                        throw new CoreException("Unknown notification subject type: " + notification.getSubjectType());
                }
            } catch (Exception e) {
                sendFailedException = e;
            }

            NotificationStatus resultStatus = sendFailedException == null ?
                    NotificationStatus.SENDED : NotificationStatus.SEND_FAILED;
            notification.setStatus(resultStatus);
            notification.setStatusTime(currentTimestampHelper.getTimestamp());
            notificationDao.update(notification);

            log.info("End" + logMsg + ". Result: " + resultStatus +
                    (sendFailedException == null ? "" : ". Error: " + ExceptionUtils.getStackTrace(sendFailedException)));
            return resultStatus;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    private static class NotificationStatistic {

        final int totalCount;
        final Map<NotificationStatus, Integer> statusToCount = new EnumMap<>(NotificationStatus.class);

        NotificationStatistic(int totalCount) {
            this.totalCount = totalCount;
        }

        void increment(NotificationStatus status) {
            Integer count = statusToCount.get(status);
            statusToCount.put(status, count == null ? 1 : ++count);
        }

        @Override
        public String toString() {
            StringBuilder s = new StringBuilder("Total: ").append(totalCount);
            for (NotificationStatus status : statusToCount.keySet()) {
                s.append(", ").append(status).append(": ").append(statusToCount.get(status));
            }
            return s.toString();
        }
    }
}
