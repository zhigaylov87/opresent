package ru.opresent.core.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.dao.api.IncomingMessageDao;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.pagination.IncomingMessagePage;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.core.service.api.NotificationService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 22.11.14
 * Time: 16:19
 */
@Transactional
@Service("incomingMessageService")
public class IncomingMessageServiceImpl implements IncomingMessageService {

    @Resource(name = "currentTimestampHelper")
    private CurrentTimestampHelper currentTimestampHelper;

    @Resource(name = "notificationService")
    private NotificationService notificationService;

    @Resource(name = "incomingMessageDao")
    private IncomingMessageDao dao;

    @Override
    public long save(@Valid IncomingMessage message, String trackingId) throws ConstraintViolationException {
        message.setReaded(false);
        message.setCreationTime(currentTimestampHelper.getTimestamp());
        long messageId = dao.save(message, trackingId);

        message = dao.load(messageId);
        notificationService.saveNewIncomingMessageNotification(message);
        return messageId;
    }

    @Transactional(readOnly = true)
    @Override
    public IncomingMessage load(long messageId) {
        return dao.load(messageId);
    }

    @Transactional(readOnly = true)
    @Override
    public IncomingMessagePage load(PageSettings pageSettings) {
        return dao.load(pageSettings);
    }

    @Transactional(readOnly = true)
    @Override
    public int getNotReadedCount() {
        return dao.getNotReadedCount();
    }

    @Override
    public void markReaded(long messageId) {
        dao.markReaded(messageId);
    }
}
