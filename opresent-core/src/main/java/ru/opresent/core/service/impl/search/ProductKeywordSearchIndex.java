package ru.opresent.core.service.impl.search;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.ProductKeywordDao;
import ru.opresent.core.domain.ProductKeyword;
import ru.opresent.core.search.ProductKeywordSearchQuery;
import ru.opresent.core.search.ProductKeywordSearchResult;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * User: artem
 * Date: 14.06.15
 * Time: 1:21
 */
@Component("productKeywordSearchIndex")
class ProductKeywordSearchIndex extends AbstractSearchIndex {

    private static final String KEYWORD_FIELD_NAME = "KEYWORD";

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "productKeywordDao")
    private ProductKeywordDao productKeywordDao;

    public ProductKeywordSearchResult searchKeywords(ProductKeywordSearchQuery searchQuery) {
        String logMsg = " of searching product keywords for query " + searchQuery;
        try {
            log.info("Start" + logMsg);

            try (Directory directory = getDirectory();
                 IndexReader reader = DirectoryReader.open(directory);
                 Analyzer analyzer = createAnalyzer()) {

                ProductKeywordSearchResult searchResult = createResult(searchQuery, reader, analyzer);

                log.info(MessageFormat.format("End{0}. Keywords count: {1}",
                        logMsg, searchResult.getKeywords().size()));

                return searchResult;
            }
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    public void updateAllKeywordsIndex() {
        String logMsg = " of index product keyswords for search";
        try {
            log.info("Start" + logMsg);

            String processLogMsg = "Processing" + logMsg;
            try (FSDirectory dir = getDirectory()) {
                log.info(processLogMsg + ". Start of clean directory: " + dir);
                FileUtils.cleanDirectory(dir.getDirectory().toFile());
                log.info(processLogMsg + ". End of clean directory.");

                try (Analyzer analyzer = createAnalyzer()) {
                    IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
                    iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
                    try (IndexWriter writer = new IndexWriter(dir, iwc);
                        EntityIterator<ProductKeyword> keywordIterator = productKeywordDao.createUsedProductKeywordsIterator()) {

                        while (keywordIterator.hasNext()) {
                            Field keywordField = new TextField(KEYWORD_FIELD_NAME, keywordIterator.next().getKeyword(), Field.Store.YES);
                            if (log.isDebugEnabled()) {
                                log.debug("Keyword field: " + keywordField);
                            }
                            Document doc = new Document();
                            doc.add(keywordField);
                            writer.addDocument(doc);
                        }
                    }
                }
            }

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    protected String getDirectoryName() {
        return "product_keyword_index";
    }

    private ProductKeywordSearchResult createResult(ProductKeywordSearchQuery searchQuery, IndexReader reader,
            Analyzer analyzer) throws ParseException, IOException, InvalidTokenOffsetsException {

        QueryParser queryParser = new QueryParser(KEYWORD_FIELD_NAME, analyzer);
        queryParser.setDefaultOperator(QueryParser.Operator.AND);
        Query query = queryParser.parse(searchQuery.getQuery());
        if (log.isDebugEnabled()) {
            log.debug("Searching for: '" + query + "'");
        }

        IndexSearcher searcher = new IndexSearcher(reader);
        TopDocs topDocs = searcher.search(query, ProductKeywordSearchResult.MAX_PRODUCT_KEYWORDS_COUNT);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;

        // TODO: Раскомментировать подсветку ключевых слов(highlightedKeywords) и использовать их в интерфейсе
//        Formatter formatter = createFormatter();
        List<String> keywords = new ArrayList<>();
//        List<String> highlightedKeywords = new ArrayList<>();
        for (ScoreDoc scoreDoc : scoreDocs) {
            if (log.isDebugEnabled()) {
                log.debug("Score doc: " + scoreDoc);
            }
            String keyword = searcher.doc(scoreDoc.doc).get(KEYWORD_FIELD_NAME);
//            String highlightedKeyword = new Highlighter(formatter, new QueryScorer(query))
//                    .getBestFragment(analyzer, KEYWORD_FIELD_NAME, keyword);
//            if (log.isDebugEnabled()) {
//                log.debug("Keyword: " + FormatUtils.quotes(keyword) +
//                        ", highlighted keyword: " + FormatUtils.quotes(highlightedKeyword));
//            }
            keywords.add(keyword);
//            highlightedKeywords.add(highlightedKeyword);
        }

        ProductKeywordSearchResult searchResult = new ProductKeywordSearchResult();
//        searchResult.setHighlightedKeywords(highlightedKeywords);
        searchResult.setKeywords(keywords);

        return searchResult;
    }
}
