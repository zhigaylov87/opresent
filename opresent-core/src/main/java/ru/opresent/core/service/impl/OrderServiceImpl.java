package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.dao.api.CartDao;
import ru.opresent.core.dao.api.OrderDao;
import ru.opresent.core.dao.api.OrderListDao;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.cart.Cart;
import ru.opresent.core.domain.order.*;
import ru.opresent.core.pagination.OrderPage;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.service.api.CartService;
import ru.opresent.core.service.api.NotificationService;
import ru.opresent.core.service.api.OrderService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * User: artem
 * Date: 02.02.15
 * Time: 0:28
 */
@Transactional
@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Resource(name = "orderDao")
    private OrderDao orderDao;

    @Resource(name = "cartDao")
    private CartDao cartDao;

    @Resource(name = "orderListDao")
    private OrderListDao orderListDao;

    @Resource(name = "currentTimestampHelper")
    private CurrentTimestampHelper currentTimestampHelper;

    @Resource(name = "notificationService")
    private NotificationService notificationService;

    @Resource(name = "cartService")
    private CartService cartService;

    @Override
    public Order save(@Valid OrderInfo orderInfo, String trackingId) throws ConstraintViolationException {
        Order order = new Order(orderInfo);
        order.setStatus(OrderStatus.NEW);
        Date currentTime = currentTimestampHelper.getTimestamp();
        order.setModifyTime(currentTime);
        order.setCreateTime(currentTime);
        long orderId = orderDao.saveBase(order);

        if (!orderInfo.isPersonalOrder()) {
            Cart cart = cartService.load(trackingId);
            if (cart.getProducts().isEmpty()) {
                throw new CoreException("Empty cart for tracking id: '" + trackingId + "'");
            }
            List<OrderItem> orderItems = new ArrayList<>();
            for (Product product : cart.getProducts()) {
                OrderItem orderItem = new OrderItem();
                orderItem.setProduct(product);
                // fix current price
                orderItem.setFixPrice(product.getPrice());
                orderItems.add(orderItem);
            }
            orderDao.saveItems(orderId, orderItems);
            cartDao.clearCart(trackingId);
        }

        order = load(orderId);
        notificationService.saveNewOrderNotification(order);
        return order;
    }

    @Override
    public void update(@Valid Order order) throws ConstraintViolationException {
        if (order.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot update not saved order!");
        }
        order.setModifyTime(currentTimestampHelper.getTimestamp());
        orderDao.updateBase(order);
    }

    @Transactional(readOnly = true)
    @Override
    public Order load(long orderId) {
            Order order = orderDao.loadBase(orderId);
            if (!order.isPersonalOrder()) {
                order.setItems(orderDao.loadItems(Collections.singleton(orderId)).get(orderId));
            }
            return order;
    }

    @Transactional(readOnly = true)
    @Override
    public OrderPage load(OrderFilter filter, PageSettings pageSettings) {
        return orderListDao.load(filter, pageSettings);
    }

    @Transactional(readOnly = true)
    @Override
    public int getOrderStatusCount(OrderStatus status) {
        return orderDao.getOrderStatusCount(status);
    }

    @CacheEvict(value = CacheName.ORDER_SETTINGS, allEntries = true)
    @Override
    public void saveOrderSettings(@Valid OrderSettings orderSettings) {
        orderDao.saveOrderSettings(orderSettings);
    }

    @Cacheable(value = CacheName.ORDER_SETTINGS)
    @Transactional(readOnly = true)
    @Override
    public OrderSettings getOrderSettings() {
        return orderDao.loadOrderSettings();
    }
}
