package ru.opresent.core.service.api;

import ru.opresent.core.domain.order.*;
import ru.opresent.core.pagination.OrderPage;
import ru.opresent.core.pagination.PageSettings;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 02.02.15
 * Time: 0:25
 */
public interface OrderService {

    Order save(@Valid OrderInfo orderInfo, String trackingId) throws ConstraintViolationException;

    void update(@Valid Order order) throws ConstraintViolationException;

    Order load(long orderId);

    OrderPage load(OrderFilter filter, PageSettings pageSettings);

    int getOrderStatusCount(OrderStatus status);

    void saveOrderSettings(@Valid OrderSettings orderSettings);

    OrderSettings getOrderSettings();
}
