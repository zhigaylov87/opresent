package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.CategoryDao;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.CategoryProductsCountStatistic;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.core.util.IdentifiableNamedEntityList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 23:33
 */
@Transactional
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Resource(name = "categoryDao")
    private CategoryDao categoryDao;

    @Resource(name = "orderedEntityServiceHelper")
    private OrderedEntityServiceHelper orderedEntityServiceHelper;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper orderedEntityDaoHelper;

    @Resource(name = "livemasterAliasService")
    private LivemasterAliasService livemasterAliasService;

    @CacheEvict(value = {CacheName.CATEGORIES, CacheName.CATALOG}, allEntries = true)
    @Override
    public long save(@Valid Category category) throws ConstraintViolationException {
        long categoryId = category.getId();
        boolean isNew = categoryId == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            category.setIndex(orderedEntityDaoHelper.getTotalCount(category));
            categoryId = categoryDao.save(category);
        } else {
            categoryDao.update(category);
        }
        return categoryId;
    }

    @CacheEvict(value = {CacheName.CATEGORIES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void save(@Valid Category category, LivemasterAliases aliases) throws ConstraintViolationException {
        if (category.getId() != aliases.getId()) {
            throw new CoreException("Ids not equals. Category id = " + category.getId() +
                    ", livemaster aliases id: " + aliases.getId());
        }
        if (aliases.getType() != LivemasterAliasType.CATEGORY) {
            throw new CoreException("Illegal alias type: " + aliases.getType());
        }

        boolean isNew = category.getId() == Identifiable.NOT_SAVED_ID;
        long categoryId = save(category);
        if (isNew) {
            aliases = new LivemasterAliases(aliases);
            aliases.setId(categoryId);
        }
        livemasterAliasService.save(aliases);
    }

    @CacheEvict(value = {CacheName.CATEGORIES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void delete(Category category) throws ServiceException {
        if (category.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved category!");
        }
        if (categoryDao.isUsedForProduct(category.getId())) {
            throw new ServiceException("Category.delete_fail_using_by_products", category.getName());
        }

        categoryDao.delete(category.getId());
        orderedEntityDaoHelper.moveUpAfterIndex(category, category.getIndex());

        livemasterAliasService.delete(category.getId(), LivemasterAliasType.CATEGORY);
    }

    @CacheEvict(value = {CacheName.CATEGORIES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void moveUp(Category category) {
        orderedEntityServiceHelper.moveUpDown(category, true);
    }

    @CacheEvict(value = {CacheName.CATEGORIES, CacheName.CATALOG}, allEntries = true)
    @Override
    public void moveDown(Category category) {
        orderedEntityServiceHelper.moveUpDown(category, false);
    }

    @Cacheable(value = CacheName.CATEGORIES)
    @Transactional(readOnly = true)
    public IdentifiableNamedEntityList<Category> getAll() {
        return new IdentifiableNamedEntityList<>(categoryDao.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public CategoryProductsCountStatistic loadCategoryProductsCountStatistic() {
        return new CategoryProductsCountStatistic(categoryDao.getCategoryIdToProductsCount());
    }
}
