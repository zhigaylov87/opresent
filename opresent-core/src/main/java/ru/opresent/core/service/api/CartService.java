package ru.opresent.core.service.api;

import ru.opresent.core.domain.cart.Cart;

/**
 * User: artem
 * Date: 16.11.14
 * Time: 15:26
 */
public interface CartService {

    Cart load(String trackingId);

    void addProduct(String trackingId, long productId);

    void removeProduct(String trackingId, long productId);

}
