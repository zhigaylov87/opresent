package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.ProductLabelDao;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.ProductLabel;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.ProductLabelService;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 16:52
 */
@Transactional
@Service("productLabelService")
public class ProductLabelServiceImpl implements ProductLabelService {

    @Resource(name = "productLabelDao")
    private ProductLabelDao dao;


    @CacheEvict(value = {CacheName.PRODUCT_LABELS}, allEntries = true)
    @Override
    public void save(@Valid ProductLabel label) throws ConstraintViolationException {
        boolean isNew = label.getId() == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            dao.save(label);
        } else {
            dao.update(label);
        }
    }

    @CacheEvict(value = {CacheName.PRODUCT_LABELS}, allEntries = true)
    @Override
    public void delete(ProductLabel label) throws ServiceException {
        if (label.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved product label!");
        }
        if (dao.isUsedForProduct(label.getId())) {
            throw new ServiceException("ProductLabel.delete_fail_using_by_products");
        }
        // TODO: Delete label image
        dao.delete(label.getId());
    }

    @Cacheable(value = CacheName.PRODUCT_LABELS)
    @Transactional(readOnly = true)
    @Override
    public IdentifiableList<ProductLabel> getAll() {
        return new IdentifiableList<>(dao.loadAll());
    }
}
