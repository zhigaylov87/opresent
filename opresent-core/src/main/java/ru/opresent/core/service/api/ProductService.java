package ru.opresent.core.service.api;

import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductFilter;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.service.ServiceException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Set;

/**
 * User: artem
 * Date: 14.03.14
 * Time: 2:26
 */
public interface ProductService {

    Product save(@Valid Product product) throws ConstraintViolationException;

    void delete(long productId) throws ServiceException;

    /**
     * @param productId product id
     * @return product with defined id or {@code null} if product with defined id not exists
     */
    Product load(long productId);

    /**
     * @param productId product id
     * @return product with defined id or {@code null} if product with defined id not exists
     */
    Product loadBaseIfExists(long productId);

    /**
     * @param livemasterId livemasterId
     * @return product or {@code null} if product with defined livemaster id no exists
     */
    Product loadByLivemasterId(String livemasterId);

    Set<Product> loadBase(Set<Long> productsIds);

    ProductPage load(ProductFilter filter, PageSettings pageSettings);

    ProductPage loadWithCategories(ProductFilter filter, PageSettings pageSettings);

    boolean isDeletedWithLivemasterId(String livemasterId);
}
