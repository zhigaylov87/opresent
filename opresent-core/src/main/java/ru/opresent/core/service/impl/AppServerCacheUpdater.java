package ru.opresent.core.service.impl;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.opresent.core.cache.CacheActionListener;
import ru.opresent.core.component.api.crypto.Cryptographer;
import ru.opresent.core.config.ServerConfig;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: artem
 * Date: 31.01.16
 * Time: 14:28
 */
@Service("appServerCacheUpdater")
public class AppServerCacheUpdater implements CacheActionListener {

    private final ExecutorService cacheActionListenerService = Executors.newSingleThreadExecutor();

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    @Resource(name = "restTemplate")
    private RestTemplate restTemplate;

    @Resource(name = "cryptographer")
    private Cryptographer cryptographer;

    @PreDestroy
    private void destroy() {
//        final long timeout = 3L;
//        final TimeUnit timeoutUnit = TimeUnit.MINUTES;
//        String logMsg = MessageFormat.format(
//                " of awaiting termination of cache action listener service[timeout: {0} {1}]", timeout, timeoutUnit.name());
        String logMsg = "Shutdown cache action listener service";
        try {
            log.info(logMsg + "...");
            cacheActionListenerService.shutdown();
//            cacheActionListenerService.awaitTermination(timeout, timeoutUnit);
            log.info(logMsg + " complete");
        } catch (Exception e) {
            log.error(logMsg + " failed", e);
        }
    }

    @Override
    public void cacheEvictPerformed(String cacheName, Object key) {
        cacheActionListenerService.execute(new CacheEvictCommand(cacheName));
    }

    @Override
    public void cacheClearPerformed(String cacheName) {
        cacheActionListenerService.execute(new CacheClearCommand(cacheName));
    }

    private class CacheEvictCommand implements Runnable {

        final String cacheName;

        private CacheEvictCommand(String cacheName) {
            this.cacheName = cacheName;
        }

        @Override
        public void run() {
            log.error("CacheEvictCommand not implemented, but evict for cache '" + cacheName + "' was performed");
        }
    }

    private class CacheClearCommand implements Runnable {

        final String cacheName;

        private CacheClearCommand(String cacheName) {
            this.cacheName = cacheName;
        }

        @Override
        public void run() {
            String logMsg = "Execute cache clear command [cacheName: '" + cacheName + "']";
            try {
                log.info(logMsg + "...");

                String encryptedCacheName = cryptographer.encrypt(cacheName);
                if (log.isDebugEnabled()) {
                    log.debug(logMsg + ". Encrypted cache name: '" + encryptedCacheName + "'");
                }
                restTemplate.postForEntity(serverConfig.getHost() + "/api/cache/clear", encryptedCacheName, Void.class);

                log.info(logMsg + " complete");
            } catch (Exception e) {
                log.error(logMsg + "failed", e);
            }
        }
    }
}
