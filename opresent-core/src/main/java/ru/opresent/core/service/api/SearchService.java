package ru.opresent.core.service.api;

import ru.opresent.core.search.ProductKeywordSearchQuery;
import ru.opresent.core.search.ProductKeywordSearchResult;
import ru.opresent.core.search.ProductSearchQuery;
import ru.opresent.core.search.ProductSearchResult;

/**
 * User: artem
 * Date: 06.02.16
 * Time: 21:14
 */
public interface SearchService {

    ProductSearchResult search(ProductSearchQuery searchQuery);

    ProductKeywordSearchResult search(ProductKeywordSearchQuery searchQuery);

    void updateWholeIndex();

    void updateIndexAfterProductSave(long productId);

    void updateIndexAfterProductDelete(long productId);
}
