package ru.opresent.core.service.api;


import ru.opresent.core.domain.catalog.Catalog;

/**
 * User: artem
 * Date: 28.10.14
 * Time: 21:55
 */
public interface CatalogService {

    Catalog getCatalog();

}
