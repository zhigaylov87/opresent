package ru.opresent.core.service.impl.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.store.FSDirectory;
import ru.opresent.core.util.CommonUtils;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * User: artem
 * Date: 14.06.15
 * Time: 1:25
 */
abstract class AbstractSearchIndex {


    private static final String HIGHLIGHT_PRE_TAG = "<b>";
    private static final String HIGHLIGHT_POST_TAG = "</b>";

    protected abstract String getDirectoryName();

    protected FSDirectory getDirectory() throws IOException {
        return FSDirectory.open(Paths.get(CommonUtils.getTmpDirPath() + "/" + getDirectoryName()));
    }

    protected Analyzer createAnalyzer() {
        return new RussianAnalyzer();
    }

    protected Formatter createFormatter() {
        return new SimpleHTMLFormatter(HIGHLIGHT_PRE_TAG, HIGHLIGHT_POST_TAG);
    }

}
