package ru.opresent.core.service.api;

import ru.opresent.core.domain.ProductLabel;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.util.IdentifiableList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 16:46
 */
public interface ProductLabelService {

    void save(@Valid ProductLabel label) throws ConstraintViolationException;

    void delete(ProductLabel label) throws ServiceException;

    IdentifiableList<ProductLabel> getAll();

}
