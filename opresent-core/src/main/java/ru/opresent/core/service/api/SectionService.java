package ru.opresent.core.service.api;

import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.util.IdentifiableNamedEntityList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 20:41
 */
public interface SectionService {

    /**
     * @param section section
     * @return saved section id
     * @throws ConstraintViolationException
     */
    long save(@Valid Section section) throws ConstraintViolationException;

    void deleteSection(long sectionId);

    IdentifiableNamedEntityList<Section> getAll();

    IdentifiableNamedEntityList<Section> getAllVisible();

    Section load(long id);

    void moveUp(Section section);

    void moveDown(Section section);

    SectionItem save(@Valid SectionItem sectionItem) throws ConstraintViolationException;

    void deleteSectionItem(long sectionItemId) throws ServiceException;

    List<SectionItem> loadItems(long sectionId);

    List<SectionItem> loadVisibleItems(long sectionId);

    SectionItem loadItem(long sectionItemId);

    void moveUp(SectionItem sectionItem);

    void moveDown(SectionItem sectionItem);
}
