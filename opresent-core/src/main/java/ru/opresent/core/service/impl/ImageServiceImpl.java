package ru.opresent.core.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.dao.api.ImageDao;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.service.api.ImageService;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:38
 */
@Transactional
@Service("imageService")
public class ImageServiceImpl implements ImageService {

    @Resource(name = "imageDao")
    private ImageDao imageDao;

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;

    @Override
    public Image save(@Valid Image image) {
        if (image.getId() != Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Image already saved: " + image);
        }
        if (image.getContent() == null) {
            throw new IllegalArgumentException("Image content is null!");
        }

        image = imageDao.save(image);
        imageFileManager.saveImageFile(image);
        // content in image object not need more
        image.setContent(null);
        return image;
    }
}
