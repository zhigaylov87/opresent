package ru.opresent.core.service.impl;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.OrderedEntity;
import ru.opresent.core.util.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 09.05.15
 * Time: 23:46
 */
@Component("orderedEntityServiceHelper")
public class OrderedEntityServiceHelper {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper dao;

    public boolean canMoveUp(OrderedEntity entity) {
        return entity.getIndex() != OrderedEntity.START_INDEX;
    }

    public boolean canMoveDown(OrderedEntity entity, List<? extends OrderedEntity> all) {
        return entity.getIndex() != all.size() - 1;
    }

    void moveUpDown(OrderedEntity entity, boolean isUp) {
        String logMsg = " of moving " + (isUp ? "up" : "down") + " ordered entity: " + entity;
        try {
            log.info("Start" + logMsg);
            int index = entity.getIndex();
            boolean canMove = isUp ?
                    index != OrderedEntity.START_INDEX :
                    index != dao.getTotalCount(entity) - 1;
            if (!canMove) {
                throw new CoreException(MessageFormat.format(
                        "Ordered entity can not be moved {0,choice,0#down|1#up}", isUp ? 1 : 0));
            }

            int exchangeIndex = isUp ? index - 1 : index + 1;
            long exchangeId = dao.findIdByIndex(entity, exchangeIndex);

            dao.exchangePlaces(entity, exchangeIndex, exchangeId, index);

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Transactional
    public void reorder(Map<? extends OrderedEntity, Integer> entityToIndex) {
        String logMsg = " of reorder ordered entities. EntityToIndex: " + CollectionUtils.getIdKeyMap(entityToIndex);
        try {
            log.info("Start" + logMsg);
            dao.reorder(entityToIndex);
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}
