package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.ProductKeywordDao;
import ru.opresent.core.domain.ProductKeyword;
import ru.opresent.core.service.api.ProductKeywordThesarusService;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 19.12.14
 * Time: 0:36
 */
@Transactional
@Service("productKeywordThesarusService")
public class ProductKeywordThesarusServiceImpl implements ProductKeywordThesarusService {

    @Resource(name = "productKeywordDao")
    private ProductKeywordDao dao;


    @CacheEvict(value = {CacheName.PRODUCT_KEYWORD_THESAURUS}, allEntries = true)
    @Override
    public void add(@Valid ProductKeyword keyword) {
        if (getAll().contains(keyword)) {
            throw new IllegalArgumentException("Keyword already in thesaurus: " + keyword);
        }
        dao.addToThesaurus(keyword);
    }

    @CacheEvict(value = {CacheName.PRODUCT_KEYWORD_THESAURUS}, allEntries = true)
    @Override
    public void remove(ProductKeyword keyword) {
        if (!getAll().contains(keyword)) {
            throw new IllegalArgumentException("Keyword not in thesaurus: " + keyword);
        }
        dao.removeFromThesaurus(keyword);
    }

    @Cacheable(value = CacheName.PRODUCT_KEYWORD_THESAURUS)
    @Transactional(readOnly = true)
    @Override
    public List<ProductKeyword> getAll() {
        return dao.loadThesaurus();
    }

    @Override
    public List<ProductKeyword> getTop(int count) {
        return dao.loadTop(count);
    }
}