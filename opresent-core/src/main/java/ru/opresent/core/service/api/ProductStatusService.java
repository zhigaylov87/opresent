package ru.opresent.core.service.api;

import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductStatusInfo;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Map;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 15:36
 */
public interface ProductStatusService {

    void save(@Valid ProductStatusInfo info) throws ConstraintViolationException;

    Map<ProductStatus, ProductStatusInfo> getAll();
}
