package ru.opresent.core.service.api;

import ru.opresent.core.domain.ProductType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.util.IdentifiableNamedEntityList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 14.03.14
 * Time: 2:19
 */
public interface ProductTypeService {

    /**
     *
     * @param productType product type
     * @return saved product type id
     * @throws ConstraintViolationException
     */
    long save(@Valid ProductType productType) throws ConstraintViolationException;

    long save(ProductType productType, LivemasterAliases aliases) throws ConstraintViolationException;

    void delete(ProductType productType) throws ServiceException;

    IdentifiableNamedEntityList<ProductType> getAll();

    void moveUp(ProductType productType);

    void moveDown(ProductType productType);

}
