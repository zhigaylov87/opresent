package ru.opresent.core.service.impl.search;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.springframework.web.client.RestTemplate;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.crypto.Cryptographer;
import ru.opresent.core.config.ServerConfig;
import ru.opresent.core.search.ProductKeywordSearchQuery;
import ru.opresent.core.search.ProductKeywordSearchResult;
import ru.opresent.core.search.ProductSearchQuery;
import ru.opresent.core.search.ProductSearchResult;
import ru.opresent.core.service.api.SearchService;

import javax.annotation.Resource;
import java.text.MessageFormat;

/**
 * User: artem
 * Date: 06.02.16
 * Time: 21:25
 */
public class CmsSearchService implements SearchService {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "jsonMapper")
    private ObjectMapper jsonMapper;

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    @Resource(name = "restTemplate")
    private RestTemplate restTemplate;

    @Resource(name = "cryptographer")
    private Cryptographer cryptographer;

    @Override
    public ProductSearchResult search(ProductSearchQuery searchQuery) {
        throw new UnsupportedOperationException("Search not supported");
    }

    @Override
    public ProductKeywordSearchResult search(ProductKeywordSearchQuery searchQuery) {
        throw new UnsupportedOperationException("Search not supported");
    }

    @Override
    public void updateWholeIndex() {
        updateIndex(new SearchIndexUpdateConfig(SearchIndexUpdateConfig.Mode.WHOLE, null));
    }

    @Override
    public void updateIndexAfterProductSave(long productId) {
        // TODO: Сейчас частичное обновление индекса работает некорректно
//        updateIndex(new SearchIndexUpdateConfig(SearchIndexUpdateConfig.Mode.PRODUCT_SAVE, productId));
    }

    @Override
    public void updateIndexAfterProductDelete(long productId) {
        // TODO: Сейчас частичное обновление индекса работает некорректно
//        updateIndex(new SearchIndexUpdateConfig(SearchIndexUpdateConfig.Mode.PRODUCT_DELETE, productId));
    }

    private void updateIndex(SearchIndexUpdateConfig updateConfig) {
        String logMsg = " of update search index [config: " + updateConfig + "]";
        try {
            log.info("Start" + logMsg);

            String encryptedUpdateConfig = cryptographer.encrypt(updateConfig);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0}. Encrypted config: ''{1}''", logMsg, encryptedUpdateConfig));
            }

            restTemplate.postForEntity(serverConfig.getHost() + "/api/search/index-update", encryptedUpdateConfig, Void.class);

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}
