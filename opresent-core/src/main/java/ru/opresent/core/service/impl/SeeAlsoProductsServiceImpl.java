package ru.opresent.core.service.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.domain.DefaultProductFilter;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.SeeAlsoProducts;
import ru.opresent.core.pagination.DefaultPageSettings;
import ru.opresent.core.pagination.PageOrderBy;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SeeAlsoProductsService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 30.07.16
 * Time: 13:49
 */
@Transactional(readOnly = true)
@Service("seeAlsoProductsService")
public class SeeAlsoProductsServiceImpl implements SeeAlsoProductsService {

    private static final int COUNT_FOR_PRODUCT = 10;

   // TODO: Брать данные из БД
    private static final Map<Long, Integer> SECTION_ITEM_ID_TO_COUNT = ImmutableMap.<Long, Integer>builder()
            .put(1L, 3)
            .build();

    @Resource(name = "productService")
    private ProductService productService;

    @Override
    public SeeAlsoProducts getForProduct(long productId) {
        return get(COUNT_FOR_PRODUCT, productId);
    }

    @Override
    public SeeAlsoProducts getForSectionItem(long sectionItemId) {
        Integer count = SECTION_ITEM_ID_TO_COUNT.get(sectionItemId);
        count = count == null ? 10 : count;
        return get(count, null);
    }

    private SeeAlsoProducts get(int count, Long excludeProductId) {
        List<Product> products = new ArrayList<>(count);

        for (ProductStatus productStatus : ProductStatus.values()) {
            if (products.size() >= count) {
                break;
            }
            DefaultProductFilter filter = new DefaultProductFilter();
            if (excludeProductId != null) {
                filter.setExcludeProductIds(ImmutableSet.of(excludeProductId));
            }
            filter.setVisible(true);
            filter.setStatus(productStatus);
            DefaultPageSettings pageSettings = new DefaultPageSettings(
                    PageSettings.FIRST_PAGE_NUMBER, count - products.size(), PageOrderBy.RANDOM);

            ProductPage productPage = productService.load(filter, pageSettings);
            products.addAll(productPage.getItems());
        }

        return new SeeAlsoProducts(products);
    }

}
