package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.CatalogDao;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.catalog.Catalog;
import ru.opresent.core.domain.catalog.DefaultCatalog;
import ru.opresent.core.service.api.CatalogService;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.core.util.SetValueMap;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;

/**
 * User: artem
 * Date: 29.10.14
 * Time: 0:59
 */
@Transactional
@Service("catalogService")
public class CatalogServiceImpl implements CatalogService {

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "catalogDao")
    private CatalogDao catalogDao;

    @Cacheable(value = CacheName.CATALOG)
    @Transactional(readOnly = true)
    @Override
    public Catalog getCatalog() {
        IdentifiableList<ProductType> productTypes = productTypeService.getAll();
        IdentifiableList<Category> categories = categoryService.getAll();

        SetValueMap<ProductType, Category> productTypeToCategories = new SetValueMap<>();
        for (Map.Entry<Long, Set<Long>> entry : catalogDao.findUsedProductTypesIdsWithCategoriesIds().entrySet()) {
            ProductType productType = productTypes.getById(entry.getKey());
            for (long categoryId : entry.getValue()) {
                productTypeToCategories.add(productType, categories.getById(categoryId));
            }
        }

        return new DefaultCatalog(productTypeToCategories);
    }
}
