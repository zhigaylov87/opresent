package ru.opresent.core.service.api;

import ru.opresent.core.domain.order.DeliveryWayInfo;
import ru.opresent.core.util.IdentifiableList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 20.02.15
 * Time: 2:04
 */
public interface DeliveryWayService {

    void save(@Valid DeliveryWayInfo deliveryWayInfo) throws ConstraintViolationException;

    IdentifiableList<DeliveryWayInfo> getAll();

}
