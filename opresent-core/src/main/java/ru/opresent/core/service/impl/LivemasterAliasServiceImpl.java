package ru.opresent.core.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.dao.api.LivemasterAliasDao;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.LivemasterAliasService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Set;

/**
 * User: artem
 * Date: 16.12.14
 * Time: 15:50
 */
@Transactional
@Service("livemasterAliasService")
public class LivemasterAliasServiceImpl implements LivemasterAliasService {

    @Resource(name = "livemasterAliasDao")
    private LivemasterAliasDao livemasterAliasDao;

    @Override
    public LivemasterAliases save(@Valid LivemasterAliases aliases) throws ConstraintViolationException {
        livemasterAliasDao.save(aliases);
        return livemasterAliasDao.load(aliases.getId(), aliases.getType());
    }

    @Override
    public void delete(long entityId, LivemasterAliasType type) {
        livemasterAliasDao.delete(entityId, type);
    }

    @Transactional(readOnly = true)
    @Override
    public LivemasterAliases load(long entityId, LivemasterAliasType type) {
        return livemasterAliasDao.load(entityId, type);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<LivemasterAliases> load(LivemasterAliasType type) {
        return livemasterAliasDao.load(type);
    }
}
