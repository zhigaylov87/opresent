package ru.opresent.core.service.impl.search;

import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.domain.Product;
import ru.opresent.core.search.ProductKeywordSearchQuery;
import ru.opresent.core.search.ProductKeywordSearchResult;
import ru.opresent.core.search.ProductSearchQuery;
import ru.opresent.core.search.ProductSearchResult;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SearchService;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 06.02.16
 * Time: 21:24
 */
@Transactional(readOnly = true)
public class AppSearchService implements SearchService {

    @Resource(name = "productSearchIndex")
    private ProductSearchIndex productSearchIndex;

    @Resource(name = "productKeywordSearchIndex")
    private ProductKeywordSearchIndex productKeywordSearchIndex;

    @Resource(name = "productService")
    private ProductService productService;

    @Override
    public ProductSearchResult search(ProductSearchQuery searchQuery) {
        return productSearchIndex.searchProducts(searchQuery);
    }

    @Override
    public ProductKeywordSearchResult search(ProductKeywordSearchQuery searchQuery) {
        return productKeywordSearchIndex.searchKeywords(searchQuery);
    }

    @Override
    public void updateWholeIndex() {
        productSearchIndex.updateAllProductsIndex();
        productKeywordSearchIndex.updateAllKeywordsIndex();
    }

    @Override
    public void updateIndexAfterProductSave(long productId) {
        Product product = productService.load(productId);
        if (product.isVisible()) {
            productSearchIndex.updateProductIndex(product);
        } else {
            productSearchIndex.deleteProductIndex(product);
        }
        productKeywordSearchIndex.updateAllKeywordsIndex();
    }

    @Override
    public void updateIndexAfterProductDelete(long productId) {
        productSearchIndex.deleteProductIndex(new Product(productId));
        productKeywordSearchIndex.updateAllKeywordsIndex();
    }
}
