package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.NewsDao;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.news.News;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.service.api.NewsService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:08
 */
@Transactional
@Service("newsService")
public class NewsServiceImpl implements NewsService {

    @Resource(name = "newsDao")
    private NewsDao newsDao;

    @Resource(name = "orderedEntityServiceHelper")
    private OrderedEntityServiceHelper orderedEntityServiceHelper;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper orderedEntityDaoHelper;


    @Cacheable(value = CacheName.NEWS)
    @Transactional(readOnly = true)
    @Override
    public News getNews() {
        return new News(newsDao.loadAll());
    }

    @CacheEvict(value = CacheName.NEWS, allEntries = true)
    @Override
    public long save(@Valid NewsItem item) throws ConstraintViolationException {
        long itemId = item.getId();
        boolean isNew = itemId == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            // add news item to the end
            item.setIndex(orderedEntityDaoHelper.getTotalCount(item));
            itemId = newsDao.save(item);
        } else {
            newsDao.update(item);
        }
        return itemId;
    }

    @CacheEvict(value = CacheName.NEWS, allEntries = true)
    @Override
    public void delete(NewsItem item) {
        if (item.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved news item!");
        }
        newsDao.delete(item.getId());
        orderedEntityDaoHelper.moveUpAfterIndex(item, item.getIndex());
    }

    @CacheEvict(value = CacheName.NEWS, allEntries = true)
    @Override
    public void moveUp(NewsItem item) {
        orderedEntityServiceHelper.moveUpDown(item, true);
    }

    @CacheEvict(value = CacheName.NEWS, allEntries = true)
    @Override
    public void moveDown(NewsItem item) {
        orderedEntityServiceHelper.moveUpDown(item, false);
    }
}
