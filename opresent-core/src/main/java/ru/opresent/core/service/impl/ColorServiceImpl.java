package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.ColorDao;
import ru.opresent.core.domain.Color;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.ColorService;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:20
 */
@Transactional
@Service("colorService")
public class ColorServiceImpl implements ColorService {

    @Resource(name = "colorDao")
    private ColorDao colorDao;


    @CacheEvict(value = CacheName.COLORS, allEntries = true)
    @Override
    public void save(@Valid Color color) throws ConstraintViolationException {
        boolean isNew = color.getId() == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            colorDao.save(color);
        } else {
            colorDao.update(color);
        }
    }

    @CacheEvict(value = CacheName.COLORS, allEntries = true)
    @Override
    public void delete(Color color) throws ServiceException {
        if (color.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved color!");
        }
        if (colorDao.isUsedForProduct(color.getId())) {
            throw new ServiceException("Color.delete_fail_using_by_products", color.getDescription());
        }
        colorDao.delete(color.getId());
    }

    @Cacheable(value = CacheName.COLORS)
    @Transactional(readOnly = true)
    @Override
    public IdentifiableList<Color> getAll() {
        return new IdentifiableList<>(colorDao.findAll());
    }

}