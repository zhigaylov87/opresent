package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.SizeDao;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.Size;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.SizeService;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 22:00
 */
@Transactional
@Service("sizeService")
public class SizeServiceImpl implements SizeService {

    @Resource(name = "sizeDao")
    private SizeDao sizeDao;


    @CacheEvict(value = CacheName.SIZES, allEntries = true)
    @Override
    public void save(@Valid Size size) throws ConstraintViolationException {
        boolean isNew = size.getId() == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            sizeDao.save(size);
        } else {
            sizeDao.update(size);
        }
    }

    @CacheEvict(value = CacheName.SIZES, allEntries = true)
    @Override
    public void delete(Size size) throws ServiceException {
        if (size.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Cannot delete not saved size!");
        }
        if (sizeDao.isUsedForProduct(size.getId())) {
            throw new ServiceException("Size.delete_fail_using_by_products", size.getDescription());
        }

        sizeDao.delete(size.getId());
    }

    @Cacheable(value = CacheName.SIZES)
    @Transactional(readOnly = true)
    @Override
    public IdentifiableList<Size> getAll() {
        return new IdentifiableList<>(sizeDao.findAll());
    }
}