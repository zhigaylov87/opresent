package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.DeliveryWayDao;
import ru.opresent.core.domain.order.AddressType;
import ru.opresent.core.domain.order.DeliveryWay;
import ru.opresent.core.domain.order.DeliveryWayInfo;
import ru.opresent.core.service.api.DeliveryWayService;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 20.02.15
 * Time: 2:04
 */
@Transactional
@Service("deliveryWayService")
public class DeliveryWayServiceImpl implements DeliveryWayService {

    @Resource(name = "deliveryWayDao")
    private DeliveryWayDao dao;


    @CacheEvict(value = CacheName.DELIVERY_WAY_INFO, allEntries = true)
    @Override
    public void save(@Valid DeliveryWayInfo info) throws ConstraintViolationException {
        dao.save(info);
    }

    @Cacheable(value = CacheName.DELIVERY_WAY_INFO)
    @Transactional(readOnly = true)
    @Override
    public IdentifiableList<DeliveryWayInfo> getAll() {
        List<DeliveryWayInfo> infos = dao.loadAll();
        for (AddressType addressType : AddressType.values()) {
            for (DeliveryWay deliveryWay : addressType.getDeliveryWays()) {
                DeliveryWayInfo info = new DeliveryWayInfo(addressType, deliveryWay);
                if (!infos.contains(info)) {
                    infos.add(info);
                }
            }
        }
        Collections.sort(infos);
        return new IdentifiableList<>(infos);
    }
}
