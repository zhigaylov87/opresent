package ru.opresent.core.service.api;


import ru.opresent.core.domain.news.News;
import ru.opresent.core.domain.news.NewsItem;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:04
 */
public interface NewsService {

    News getNews();

    long save(@Valid NewsItem item) throws ConstraintViolationException;

    void delete(NewsItem item);

    void moveUp(NewsItem item);

    void moveDown(NewsItem item);
}
