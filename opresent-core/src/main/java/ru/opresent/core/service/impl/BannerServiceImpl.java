package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.BannerDao;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.banner.Banner;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.service.api.BannerService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:08
 */
@Transactional
@Service("bannerService")
public class BannerServiceImpl implements BannerService {

    @Resource(name = "bannerDao")
    private BannerDao bannerDao;

    @Resource(name = "orderedEntityServiceHelper")
    private OrderedEntityServiceHelper orderedEntityServiceHelper;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper orderedEntityDaoHelper;


    @Cacheable(value = CacheName.BANNER)
    @Transactional(readOnly = true)
    @Override
    public Banner getBanner() {
        return new Banner(bannerDao.loadAll());
    }

    @CacheEvict(value = CacheName.BANNER, allEntries = true)
    @Override
    public long save(@Valid BannerItem item) throws ConstraintViolationException {
        // TODO: В валидаторе проверять, что visible у BannerItem'a и Product'a в нём(если он задан) должно соответствовать,
        // TODO: т.е. если BannerItem#isVisible() == true, то Product#isVisible() тоже должно быть true
        long itemId = item.getId();
        boolean isNew = itemId == Identifiable.NOT_SAVED_ID;
        if (isNew) {
            // add banner item to the end
            item.setIndex(orderedEntityDaoHelper.getTotalCount(item));
            itemId = bannerDao.save(item);
        } else {
            bannerDao.update(item);
        }
        return itemId;
    }

    @CacheEvict(value = CacheName.BANNER, allEntries = true)
    @Override
    public void delete(BannerItem item) {
            if (item.getId() == Identifiable.NOT_SAVED_ID) {
                throw new IllegalArgumentException("Cannot delete not saved banner item!");
            }
            // TODO Удалять картинки
            bannerDao.delete(item.getId());
            orderedEntityDaoHelper.moveUpAfterIndex(item, item.getIndex());
    }

    @CacheEvict(value = CacheName.BANNER, allEntries = true)
    @Override
    public void moveUp(BannerItem item) {
        orderedEntityServiceHelper.moveUpDown(item, true);
    }

    @CacheEvict(value = CacheName.BANNER, allEntries = true)
    @Override
    public void moveDown(BannerItem item) {
        orderedEntityServiceHelper.moveUpDown(item, false);
    }
}
