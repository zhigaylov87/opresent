package ru.opresent.core.service.api;

import ru.opresent.core.domain.banner.Banner;
import ru.opresent.core.domain.banner.BannerItem;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 08.05.15
 * Time: 0:04
 */
public interface BannerService {

    Banner getBanner();

    long save(@Valid BannerItem item) throws ConstraintViolationException;

    void delete(BannerItem item);

    void moveUp(BannerItem item);

    void moveDown(BannerItem item);
}
