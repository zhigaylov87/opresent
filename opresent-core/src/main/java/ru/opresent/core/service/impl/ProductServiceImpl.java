package ru.opresent.core.service.impl;

import com.google.common.collect.ImmutableSet;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.dao.api.*;
import ru.opresent.core.dao.impl.OrderedEntityDaoHelper;
import ru.opresent.core.domain.*;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageName;
import ru.opresent.core.domain.image.ProductImageName;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.ColorService;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.IdentifiableList;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * User: artem
 * Date: 25.04.14
 * Time: 23:05
 */
@Transactional
@Service("productService")
public class ProductServiceImpl implements ProductService {

    @Resource(name = "productDao")
    private ProductDao productDao;

    @Resource(name = "imageDao")
    private ImageDao imageDao;

    @Resource(name = "productListDao")
    private ProductListDao productListDao;

    @Resource(name = "productKeywordDao")
    private ProductKeywordDao productKeywordDao;

    @Resource(name = "orderedEntityDaoHelper")
    private OrderedEntityDaoHelper orderedEntityDaoHelper;

    @Resource(name = "cartDao")
    private CartDao cartDao;

    @Resource(name = "orderDao")
    private OrderDao orderDao;

    @Resource(name = "currentTimestampHelper")
    private CurrentTimestampHelper currentTimestampHelper;

    @Resource(name = "colorService")
    private ColorService colorService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;

    @CacheEvict(value = CacheName.CATALOG, allEntries = true)
    @Override
    public Product save(@Valid Product product) throws ConstraintViolationException {
        long productId = product.getId();
        boolean isNew = productId == Identifiable.NOT_SAVED_ID;

        product.setModifyTime(currentTimestampHelper.getTimestamp());

        if (isNew) {
            // base
            int productIndex;
            if (product.getStatus() == ProductStatus.AVAILABLE) {
                productIndex = OrderedEntity.START_INDEX;
            } else {
                Integer minIndex = productDao.findMinIndexExcludeStatus(ProductStatus.AVAILABLE);
                productIndex = minIndex == null ? OrderedEntity.START_INDEX : minIndex;
            }

            orderedEntityDaoHelper.moveDownAfterIndex(product, productIndex - 1);

            product.setIndex(productIndex);
            productId = productDao.saveBase(product);
        } else {
            // base
            Image oldPreview = productDao.loadBase(productId).getPreview();
            productDao.updateBase(product);
            if (!oldPreview.equals(product.getPreview())) {
                imageDao.delete(oldPreview.getId());
                imageFileManager.deleteImageFile(oldPreview);
            }
            // views
            Set<Image> unusedViews = new HashSet<>();
            unusedViews.addAll(productDao.loadViews(productId));
            unusedViews.removeAll(product.getViews());
            productDao.deleteViewsLinks(product.getId());
            if (!unusedViews.isEmpty()) {
                imageDao.delete(CollectionUtils.getIds(unusedViews));
                imageFileManager.deleteImagesFiles(unusedViews);
            }
            // colors
            productDao.deleteColorsLinks(product.getId());
            // categories
            productDao.deleteCategoriesLinks(product.getId());
        }

        // update preview image file name
        ImageName productImageName = new ProductImageName(product);
        Image preview = product.getPreview();
        preview.setImageName(productImageName);
        imageFileManager.updateImageFileName(preview);

        // views
        if (!product.getViews().isEmpty()) {
            productDao.saveViewsLinks(productId, product.getViews());
            // update views image file name
            for (Image view : product.getViews()) {
                view.setImageName(productImageName);
            }
            imageFileManager.updateImagesFileNames(product.getViews());
        }
        // colors
        if (!product.getColors().isEmpty()) {
            productDao.saveColorsLinks(productId, product.getColors());
        }
        // categories
        if (!product.getCategories().isEmpty()) {
            productDao.saveCategoriesLinks(productId, product.getCategories());
        }
        // keywords
        productKeywordDao.saveProductKeywords(productId, product.getKeywords());

        return load(productId);
    }

    @CacheEvict(value = CacheName.CATALOG, allEntries = true)
    @Override
    public void delete(long productId) throws ServiceException {
        if (productId == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Can not delete not saved product!");
        }
        Product product = load(productId);

        // views
        productDao.deleteViewsLinks(productId);
        if (!product.getViews().isEmpty()) {
            imageDao.delete(CollectionUtils.getIds(product.getViews()));
        }
        // colors
        productDao.deleteColorsLinks(productId);
        // categories
        productDao.deleteCategoriesLinks(productId);
        // keywords
        productKeywordDao.deleteProductKeywords(productId);
        // carts
        cartDao.removeProductFromAllCarts(productId);
        // orders
        orderDao.removeProductFromAllOrders(productId);
        // base
        productDao.deleteBase(productId);
        if (product.getLivemasterId() != null) {
            productDao.saveDeletedProductLivemasterId(product.getLivemasterId());
        }
        imageDao.delete(product.getPreview().getId()); // TODO: Удаление картинок из БД и файлов делать в ImageService

        // delete images files
        imageFileManager.deleteImageFile(product.getPreview());
        for (Image view : product.getViews()) {
            imageFileManager.deleteImageFile(view); // TODO: Удаление картинок из БД и файлов делать в ImageService
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Product load(long productId) {
        Product product = productDao.loadBase(productId);
        if (product != null) {
            product.setViews(productDao.loadViews(productId));
            ImageName productImageName = new ProductImageName(product);
            for (Image view : product.getViews()) {
                view.setImageName(productImageName);
            }

            product.setColors(colorService.getAll().getSetByIds(productDao.loadColorsIds(productId)));

            IdentifiableList<Category> categories = categoryService.getAll();
            for (long categoryId : productDao.loadCategoriesIds(productId)) {
                product.addCategory(categories.getById(categoryId));
            }

            product.setKeywords(productKeywordDao.loadProductKeywords(productId));
        }
        return product;
    }

    @Override
    public Product loadBaseIfExists(long productId) {
        return CollectionUtils.getSingleOrNull(productDao.loadBase(ImmutableSet.of(productId)));
    }

    @Transactional(readOnly = true)
    @Override
    public Product loadByLivemasterId(String livemasterId) {
        Long productId = productDao.loadProductIdByLivemasterId(livemasterId);
        return productId == null ? null : load(productId);
    }

    @Transactional(readOnly = true)
    @Override
    public Set<Product> loadBase(Set<Long> productsIds) {
        Set<Product> products = productDao.loadBase(productsIds);
        if (productsIds.size() != products.size()) {
            Set<Long> loadedProductIds = new HashSet<>();
            for (Product product : products) {
                loadedProductIds.add(product.getId());
            }

            Set<Long> notLoaded = new HashSet<>(productsIds);
            notLoaded.removeAll(loadedProductIds);

            throw new CoreException(MessageFormat.format("Result does not correspond to the request." +
                    "\nRequest ids: {0}.\nResult ids: {1}.\nNot loaded ids: {2}",
                    Arrays.toString(productsIds.toArray()),
                    Arrays.toString(loadedProductIds.toArray()),
                    Arrays.toString(notLoaded.toArray())));
        }

        return products;
    }

    @Transactional(readOnly = true)
    @Override
    public ProductPage load(ProductFilter filter, PageSettings pageSettings) {
        return loadProducts(filter, pageSettings, false);
    }

    @Transactional(readOnly = true)
    @Override
    public ProductPage loadWithCategories(ProductFilter filter, PageSettings pageSettings) {
        return loadProducts(filter, pageSettings, true);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isDeletedWithLivemasterId(String livemasterId) {
        return productDao.isDeletedWithLivemasterId(livemasterId);
    }

    private ProductPage loadProducts(ProductFilter filter, PageSettings pageSettings, boolean withCategories) {
        return productListDao.load(filter, pageSettings, withCategories);
    }
}
