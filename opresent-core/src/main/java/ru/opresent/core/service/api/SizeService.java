package ru.opresent.core.service.api;

import ru.opresent.core.domain.Size;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.util.IdentifiableList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 10.03.14
 * Time: 14:46
 */
public interface SizeService {

    void save(@Valid Size size) throws ConstraintViolationException;

    void delete(Size size) throws ServiceException;

    IdentifiableList<Size> getAll();

}
