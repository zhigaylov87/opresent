package ru.opresent.core.service.api;

import ru.opresent.core.domain.SeeAlsoProducts;

/**
 * User: artem
 * Date: 30.07.16
 * Time: 13:47
 */
public interface SeeAlsoProductsService {

    SeeAlsoProducts getForProduct(long productId);

    SeeAlsoProducts getForSectionItem(long sectionItemId);

}
