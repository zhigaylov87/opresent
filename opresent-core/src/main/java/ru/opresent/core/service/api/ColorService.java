package ru.opresent.core.service.api;

import ru.opresent.core.domain.Color;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.util.IdentifiableList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 14.03.14
 * Time: 2:13
 */
public interface ColorService {

    void save(@Valid Color color) throws ConstraintViolationException;

    void delete(Color color) throws ServiceException;

    IdentifiableList<Color> getAll();
}
