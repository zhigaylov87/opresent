package ru.opresent.core.service.impl;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.crypto.Cryptographer;
import ru.opresent.core.config.ServerConfig;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.ProductDao;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.catalog.Catalog;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.service.api.CatalogService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.core.util.CommonUtils;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.util.xml.IndentingXMLStreamWriter;

import javax.annotation.Resource;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Objects;

/**
 * TODO: write SitemapServiceTest at least for SitemapService.writeSitemap
 *
 * http://www.sitemaps.org/ru/protocol.html
 *
 * User: artem
 * Date: 06.05.16
 * Time: 0:41
 */
@Component("sitemapService")
public class SitemapService {

    public static final String SITEMAP_XML_PATH = CommonUtils.getTmpDirPath() + "/sitemap.xml";

    // TODO: replace this by normal solution
    private static final String UPDATE_PARAMS = "THIS IS MAGIC STRING FOR UPDATE PARAMS, REPLACE THIS BY NORMAL SOLUTION";

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "productDao")
    private ProductDao productDao;

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    @Resource(name = "restTemplate")
    private RestTemplate restTemplate;

    @Resource(name = "cryptographer")
    private Cryptographer cryptographer;

    @Resource(name = "catalogService")
    private CatalogService catalogService;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    public void invokeUpdateSitemap() {
        String logMsg = " of invoke update sitemap.xml";
        try {
            log.info("Start" + logMsg);

            String encryptedUpdateParams = cryptographer.encrypt(UPDATE_PARAMS);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0}. Encrypted params: ''{1}''", logMsg, encryptedUpdateParams));
            }

            restTemplate.postForEntity(serverConfig.getHost() + "/api/sitemap/update", encryptedUpdateParams, Void.class);

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    public void updateSitemap(String updateParams) {
        String logMsg = " of update sitemap.xml";
        try {
            log.info("Start" + logMsg);

            if (!UPDATE_PARAMS.equals(updateParams)) {
                throw new IllegalArgumentException("Invalid update params: " + FormatUtils.quotes(updateParams));
            }
            // TODO: Писать во временный файл, потом удалять существующий, а потом переименовывать временный
            writeSitemap(new FileOutputStream(SITEMAP_XML_PATH));

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    public void writeSitemap(OutputStream out) {
        try {
            XMLStreamWriter w = new IndentingXMLStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(out, "UTF-8"));

            w.writeStartDocument("UTF-8", "1.0");

            w.writeStartElement("urlset");
            w.writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

            writeUrl(w, new Url(appNavigationManager.getAbsolutWelcomeUrl()));

            // products
            try (EntityIterator<Product> productIterator = productDao.createEntityIterator()) {
                while (productIterator.hasNext()) {
                    Product product = productIterator.next();
                    writeUrl(w, new Url(
                            appNavigationManager.getAbsolutProductUrl(product),
                            Url.PRODUCT_STATUS_PRIORITY.get(product.getStatus())));
                }
            }

            // catalog
            Catalog catalog = catalogService.getCatalog();
            writeUrl(w, new Url(appNavigationManager.getAbsolutCatalogUrl()));
            for (ProductType productType : productTypeService.getAll().getAll()) {
                if (catalog.getProductTypes().contains(productType)) {
                    writeUrl(w, new Url(appNavigationManager.getAbsolutCatalogUrl(productType)));
                    for (Category category : catalog.getProductTypeCategories(productType)) {
                        writeUrl(w, new Url(appNavigationManager.getAbsolutCatalogUrl(productType, category)));
                    }
                }
            }

            // sections
            for (Section section : sectionService.getAllVisible().getAll()) {
                if (section.getType() == SectionType.CATALOG) {
                    continue;
                }
                writeUrl(w, new Url(appNavigationManager.getAbsoluteSectionUrl(section)));
                if (section.getType() == SectionType.LIST) {
                    for (SectionItem sectionItem : sectionService.loadVisibleItems(section.getId())) {
                        writeUrl(w, new Url(appNavigationManager.getAbsoluteSectionItemUrl(sectionItem)));
                    }
                }
            }

            w.writeEndElement(); // urlset

            w.writeEndDocument();
            w.flush();
            w.close();

        } catch (Exception e) {
            log.error("Error while sitemap creating", e);
            throw new CoreException(e);
        }

    }

    private void writeUrl(XMLStreamWriter w, Url url) throws XMLStreamException {
        w.writeStartElement("url");

        // loc
        w.writeStartElement("loc");
        w.writeCharacters(url.loc);
        w.writeEndElement();

        // priority
        if (url.priority != null) {
            w.writeStartElement("priority");
            w.writeCharacters(url.priority.toPlainString());
            w.writeEndElement();
        }

        w.writeEndElement();
    }

    private static class Url {

        static final BigDecimal MIN_PRIOTITY = new BigDecimal("0.0");
//        static final BigDecimal DEFAULT_PRIOTITY = new BigDecimal("0.5");
        static final BigDecimal MAX_PRIOTITY = new BigDecimal("1.0");

        static final Map<ProductStatus, BigDecimal> PRODUCT_STATUS_PRIORITY = ImmutableMap
                .of(ProductStatus.AVAILABLE, MAX_PRIOTITY,
                    ProductStatus.FOR_ORDER, new BigDecimal("0.8"),
                    ProductStatus.EXAMPLE, new BigDecimal("0.6"));

        final String loc;
        final BigDecimal priority;

        Url(String loc, BigDecimal priority) {
            this.loc = Objects.requireNonNull(loc);
            this.priority = priority;
            if (priority != null && (priority.compareTo(MIN_PRIOTITY) < 0 || priority.compareTo(MAX_PRIOTITY) > 0)) {
                throw new IllegalArgumentException("Invalid priority value: " + priority.toPlainString());
            }
        }

        Url(String loc) {
            this(loc, null);
        }
    }

}
