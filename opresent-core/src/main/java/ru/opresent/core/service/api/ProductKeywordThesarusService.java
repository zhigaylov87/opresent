package ru.opresent.core.service.api;

import ru.opresent.core.domain.ProductKeyword;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 19.12.14
 * Time: 0:35
 */
public interface ProductKeywordThesarusService {

    void add(@Valid ProductKeyword keyword);

    void remove(ProductKeyword keyword);

    /**
     * @return all products keywords sorted by {@link ProductKeyword#getKeyword()} ascending
     */
    List<ProductKeyword> getAll();

    List<ProductKeyword> getTop(int count);

}
