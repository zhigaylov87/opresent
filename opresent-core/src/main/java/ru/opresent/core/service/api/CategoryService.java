package ru.opresent.core.service.api;

import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.CategoryProductsCountStatistic;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.util.IdentifiableNamedEntityList;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 16.03.14
 * Time: 21:44
 */
public interface CategoryService {

    /**
     *
     * @param category category
     * @return saved category id
     * @throws ConstraintViolationException
     */
    long save(@Valid Category category) throws ConstraintViolationException;

    void save(Category category, LivemasterAliases aliases) throws ConstraintViolationException;

    void delete(Category category) throws ServiceException;

    void moveUp(Category category);

    void moveDown(Category category);

    IdentifiableNamedEntityList<Category> getAll();

    CategoryProductsCountStatistic loadCategoryProductsCountStatistic();
}
