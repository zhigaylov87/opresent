package ru.opresent.core.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.cache.CacheName;
import ru.opresent.core.dao.api.ProductStatusDao;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductStatusInfo;
import ru.opresent.core.service.api.ProductStatusService;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Map;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 15:42
 */
@Transactional
@Service("productStatusService")
public class ProductStatusServiceImpl implements ProductStatusService {

    @Resource(name = "productStatusDao")
    private ProductStatusDao dao;


    @CacheEvict(value = CacheName.PRODUCT_STATUS_INFO, allEntries = true)
    @Override
    public void save(@Valid ProductStatusInfo info) throws ConstraintViolationException {
        dao.save(info);
    }

    @Cacheable(value = CacheName.PRODUCT_STATUS_INFO)
    @Transactional(readOnly = true)
    @Override
    public Map<ProductStatus, ProductStatusInfo> getAll() {
        return dao.loadAll();
    }
}