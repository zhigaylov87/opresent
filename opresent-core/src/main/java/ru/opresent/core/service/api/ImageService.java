package ru.opresent.core.service.api;

import ru.opresent.core.domain.image.Image;

import javax.validation.Valid;

/**
 * User: artem
 * Date: 10.03.14
 * Time: 17:35
 */
public interface ImageService {

    /**
     * Save only new image in database and to file store
     * @param image new image for saving
     * @return saved image
     */
    Image save(@Valid Image image);

}
