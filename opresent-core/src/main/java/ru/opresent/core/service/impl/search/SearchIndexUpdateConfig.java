package ru.opresent.core.service.impl.search;

/**
 * User: artem
 * Date: 06.02.16
 * Time: 22:20
 */
public class SearchIndexUpdateConfig {

    private Mode mode;
    private Long productId;

    public SearchIndexUpdateConfig() {
    }

    public SearchIndexUpdateConfig(Mode mode, Long productId) {
        this.mode = mode;
        this.productId = productId;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "SearchIndexUpdateConfig {" +
                "mode: " + mode +
                ", productId: " + productId +
                '}';
    }

    public enum Mode {
        WHOLE,
        PRODUCT_SAVE,
        PRODUCT_DELETE
    }
}
