package ru.opresent.core.pagination;

/**
 * User: artem
 * Date: 13.12.15
 * Time: 23:05
 */
public class SinglePageSettings extends DefaultPageSettings {

    public SinglePageSettings() {
        super(PageSettings.FIRST_PAGE_NUMBER, Integer.MAX_VALUE);
    }

}