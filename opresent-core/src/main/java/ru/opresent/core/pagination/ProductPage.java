package ru.opresent.core.pagination;

import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 16.05.14
 * Time: 22:51
 */
public class ProductPage extends DefaultPage<Product> {

    public static final int DEFAULT_PAGE_SIZE = 20;

    private Map<ProductStatus, Integer> totalCountForStatus;

    public ProductPage(PageSettings settings, List<Product> items, int totalItemsCount,
                       Map<ProductStatus, Integer> totalCountForStatus) {
        super(settings, items, totalItemsCount);
        this.totalCountForStatus = totalCountForStatus;
    }

    public int getTotalCountForStatus(ProductStatus status) {
        Integer count = totalCountForStatus.get(status);
        return count == null ? 0 : count;
    }

    public int getTotalCountForAllStatuses() {
        return CollectionUtils.sum(totalCountForStatus.values());
    }
}
