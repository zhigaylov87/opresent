package ru.opresent.core.pagination;

import ru.opresent.core.domain.order.Order;

import java.util.List;

/**
 * User: artem
 * Date: 14.02.15
 * Time: 15:38
 */
public class OrderPage extends DefaultPage<Order> {

    public OrderPage(PageSettings settings, List<Order> items, int totalItemsCount) {
        super(settings, items, totalItemsCount);
    }
}
