package ru.opresent.core.pagination;

import ru.opresent.core.util.stringify.EntityStringifier;

import java.io.Serializable;
import java.util.Objects;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 14:54
 */
public class DefaultPageSettings implements PageSettings, Serializable {

    private int pageNumber;
    private int pageSize;
    private PageOrderBy orderBy;

    /**
     * @param pageNumber page number (starting from {@link #FIRST_PAGE_NUMBER})
     * @param pageSize page size
     * @param orderBy page ordering
     */
    public DefaultPageSettings(int pageNumber, int pageSize, PageOrderBy orderBy) {
        if (pageNumber < PageSettings.FIRST_PAGE_NUMBER) {
            throw new IllegalArgumentException("Invalid page number: " + pageNumber +
                    ". Page number must be greater or equal " + PageSettings.FIRST_PAGE_NUMBER + ".");
        }
        if (pageSize <= 0) {
            throw new IllegalArgumentException("Invalid page size: " + pageSize +
                    ". Page size must be greater than zero.");
        }
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.orderBy = Objects.requireNonNull(orderBy);
    }

    /**
     * Create page setting with {@link #getOrderBy()} equals to {@link PageOrderBy#STANDART}
     * @param pageNumber page number (starting from {@link #FIRST_PAGE_NUMBER})
     * @param pageSize page size
     */
    public DefaultPageSettings(int pageNumber, int pageSize) {
        this(pageNumber, pageSize, PageOrderBy.STANDART);
    }

    /**
     * @return page number (starting from {@link #FIRST_PAGE_NUMBER})
     */
    @Override
    public int getPageNumber() {
        return pageNumber;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public int getOffset() {
        return (pageNumber - FIRST_PAGE_NUMBER) * pageSize;
    }

    @Override
    public PageOrderBy getOrderBy() {
        return orderBy;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("number", pageNumber)
                .add("size", pageSize)
                .add("orderBy", orderBy);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}