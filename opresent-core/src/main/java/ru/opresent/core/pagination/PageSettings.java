package ru.opresent.core.pagination;


import ru.opresent.core.util.stringify.Stringifiable;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 12:27
 */
public interface PageSettings extends Stringifiable {

    /**
     * Page numbers starts with a one(1)
     */
    public static final int FIRST_PAGE_NUMBER = 1;

    int getPageNumber();


    int getPageSize();


    int getOffset();

    PageOrderBy getOrderBy();

}
