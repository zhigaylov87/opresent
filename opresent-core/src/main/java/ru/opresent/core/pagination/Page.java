package ru.opresent.core.pagination;

import java.util.List;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 12:27
 */
public interface Page<T> {

    public List<T> getItems();

    public int getTotalItemsCount();

    public PageSettings getSettings();

}
