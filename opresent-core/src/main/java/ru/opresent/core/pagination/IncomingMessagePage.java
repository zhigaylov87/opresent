package ru.opresent.core.pagination;

import ru.opresent.core.domain.message.incoming.IncomingMessage;

import java.util.List;

/**
 * User: artem
 * Date: 23.11.14
 * Time: 1:21
 */
public class IncomingMessagePage extends DefaultPage<IncomingMessage> {

    private int notReadedCount;

    public IncomingMessagePage(PageSettings settings, List<IncomingMessage> messages, int totalCount, int notReadedCount) {
        super(settings, messages, totalCount);
        this.notReadedCount = notReadedCount;
    }

    public int getNotReadedCount() {
        return notReadedCount;
    }
}
