package ru.opresent.core.pagination;

import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;

import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 14:44
 */
public class DefaultPage<T> implements Page<T>, Stringifiable, Serializable {

    private List<T> items;
    private int totalItemsCount;
    private PageSettings settings;

    public DefaultPage(PageSettings settings, List<T> items, int totalItemsCount) {
        this.settings = settings;
        this.items = items;
        this.totalItemsCount = totalItemsCount;
    }

    @Override
    public List<T> getItems() {
        return items;
    }

    @Override
    public int getTotalItemsCount() {
        return totalItemsCount;
    }

    @Override
    public PageSettings getSettings() {
        return settings;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("itemsCount", items.size())
                .add("totalItemsCount", totalItemsCount);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
