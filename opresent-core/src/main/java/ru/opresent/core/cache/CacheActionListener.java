package ru.opresent.core.cache;

/**
 * User: artem
 * Date: 30.01.16
 * Time: 18:04
 */
public interface CacheActionListener {

    public void cacheEvictPerformed(String cacheName, Object key);

    public void cacheClearPerformed(String cacheName);
}
