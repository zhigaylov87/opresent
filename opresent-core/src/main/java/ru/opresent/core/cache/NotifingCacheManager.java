package ru.opresent.core.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 30.01.16
 * Time: 18:11
 */
public class NotifingCacheManager extends ConcurrentMapCacheManager {

    private List<CacheActionListener> actionListeners = new ArrayList<>();

    @PostConstruct
    private void init() {
        setCacheNames(Arrays.asList(CacheName.ALL));
    }

    @Override
    public Cache getCache(String name) {
        Cache cache = super.getCache(name);
        return new NotifingCache(cache, actionListeners);
    }

    // setters

    public void setActionListeners(List<CacheActionListener> actionListeners) {
        this.actionListeners = actionListeners;
    }
}
