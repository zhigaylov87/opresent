// TODO
//package ru.opresent.core.cache;
//
//import org.slf4j.Logger;
//import org.springframework.stereotype.Component;
//import ru.opresent.core.CoreException;
//import ru.opresent.core.service.api.*;
//import ru.opresent.core.util.FormatUtils;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.Resource;
//
///**
// * User: artem
// * Date: 24.12.15
// * Time: 1:40
// */
//@Component("cacheInitializer")
//public class CacheInitializer {
//
//    @Resource(name = "log")
//    private Logger log;
//
//    @Resource(name = "categoryService")
//    private CategoryService categoryService;
//
//    @Resource(name = "productTypeService")
//    private ProductTypeService productTypeService;
//
//    @Resource(name = "sizeService")
//    private SizeService sizeService;
//
//    @Resource(name = "colorService")
//    private ColorService colorService;
//
//    @Resource(name = "sectionService")
//    private SectionService sectionService;
//
//    @Resource(name = "productLabelService")
//    private ProductLabelService productLabelService;
//
//    @Resource(name = "productKeywordThesarusService")
//    private ProductKeywordThesarusService productKeywordThesarusService;
//
//    @Resource(name = "productStatusService")
//    private ProductStatusService productStatusService;
//
//    @Resource(name = "orderService")
//    private OrderService orderService;
//
//    @Resource(name = "deliveryWayService")
//    private DeliveryWayService deliveryWayService;
//
//    @Resource(name = "catalogService")
//    private CatalogService catalogService;
//
//    @Resource(name = "bannerService")
//    private BannerService bannerService;
//
//    @Resource(name = "newsService")
//    private NewsService newsService;
//
//    @PostConstruct
//    public void init() {
//        log.info("Start of initializing cache");
//
//        for (String cacheName : CacheName.ALL) {
//            switch (cacheName) {
//                case CacheName.CATEGORIES:
//                    categoryService.getModel();
//                    break;
//
//                case CacheName.PRODUCT_TYPES:
//                    productTypeService.getAll();
//                    break;
//
//                case CacheName.SIZES:
//                    sizeService.getAll();
//                    break;
//
//                case CacheName.COLORS:
//                    colorService.getAll();
//                    break;
//
//                case CacheName.SECTIONS:
//                    sectionService.getAll();
//                    sectionService.getAllVisible();
//                    break;
//
//                case CacheName.PRODUCT_LABELS:
//                    productLabelService.getAll();
//                    break;
//
//                case CacheName.PRODUCT_KEYWORD_THESAURUS:
//                    productKeywordThesarusService.getAll();
//                    break;
//
//                case CacheName.PRODUCT_STATUS_INFO:
//                    productStatusService.getAll();
//                    break;
//
//                case CacheName.ORDER_SETTINGS:
//                    orderService.getOrderSettings();
//                    break;
//
//                case CacheName.DELIVERY_WAY_INFO:
//                    deliveryWayService.getAll();
//                    break;
//
//                case CacheName.CATALOG:
//                    catalogService.getCatalog();
//                    break;
//
//                case CacheName.BANNER:
//                    bannerService.getBanner();
//                    break;
//
//                case CacheName.NEWS:
//                    newsService.getNews();
//                    break;
//
//                case CacheName.CARTS:
//                    // do nothing
//                    break;
//
//                default:
//                    throw new CoreException("Unknown cache name: " + FormatUtils.quotes(cacheName));
//            }
//        }
//
//        log.info("End of initializing cache");
//    }
//}
