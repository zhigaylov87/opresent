package ru.opresent.core.cache;

import org.springframework.cache.Cache;

import java.util.List;
import java.util.Objects;

/**
 * User: artem
 * Date: 30.01.16
 * Time: 18:42
 */
class NotifingCache implements Cache {

    private Cache wrappedCache;
    private List<CacheActionListener> actionListeners;

    NotifingCache(Cache wrappedCache, List<CacheActionListener> actionListeners) {
        this.wrappedCache = Objects.requireNonNull(wrappedCache);
        this.actionListeners = Objects.requireNonNull(actionListeners);
    }

    @Override
    public String getName() {
        return wrappedCache.getName();
    }

    @Override
    public Object getNativeCache() {
        return wrappedCache.getNativeCache();
    }

    @Override
    public ValueWrapper get(Object key) {
        return wrappedCache.get(key);
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        return wrappedCache.get(key, type);
    }

    @Override
    public void put(Object key, Object value) {
        wrappedCache.put(key, value);
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        return wrappedCache.putIfAbsent(key, value);
    }

    @Override
    public void evict(Object key) {
        wrappedCache.evict(key);
        for (CacheActionListener listener : actionListeners) {
            listener.cacheEvictPerformed(getName(), key);
        }
    }

    @Override
    public void clear() {
        wrappedCache.clear();
        for (CacheActionListener listener : actionListeners) {
            listener.cacheClearPerformed(getName());
        }
    }
}
