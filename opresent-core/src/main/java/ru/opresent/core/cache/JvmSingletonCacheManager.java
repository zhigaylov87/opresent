package ru.opresent.core.cache;

import org.springframework.cache.concurrent.ConcurrentMapCacheManager;

/**
 * Singleton cache manager for current jvm.<br/>
 * This is static(no dynamic) cache manager supported caches with names from {@link CacheName#ALL}
 *
 * @see org.springframework.cache.concurrent.ConcurrentMapCacheManager
 * @see ru.opresent.core.cache.CacheName
 *
 * User: artem
 * Date: 14.09.14
 * Time: 15:07
 */
public class JvmSingletonCacheManager extends ConcurrentMapCacheManager {

    private static final JvmSingletonCacheManager INSTANCE = new JvmSingletonCacheManager();

    private JvmSingletonCacheManager() {
        super(CacheName.ALL);
    }

    public static JvmSingletonCacheManager getInstance() {
        return INSTANCE;
    }
}
