package ru.opresent.core.cache;

/**
 * User: artem
 * Date: 14.09.14
 * Time: 15:14
 */
public interface CacheName {

    String CATEGORIES = "CATEGORIES";
    String PRODUCT_TYPES = "PRODUCT_TYPES";
    String SIZES = "SIZES";
    String COLORS = "COLORS";
    String SECTIONS = "SECTIONS";
    String PRODUCT_LABELS = "PRODUCT_LABELS";
    String PRODUCT_KEYWORD_THESAURUS = "PRODUCT_KEYWORD_THESAURUS";
    String PRODUCT_STATUS_INFO = "PRODUCT_STATUS_INFO";
    String ORDER_SETTINGS = "ORDER_SETTINGS";
    String DELIVERY_WAY_INFO = "DELIVERY_WAY_INFO";

    String CATALOG = "CATALOG";

    String BANNER = "BANNER";
    String NEWS = "NEWS";

    String[] ALL = {
            CATEGORIES,
            PRODUCT_TYPES,
            SIZES,
            COLORS,
            SECTIONS,
            PRODUCT_LABELS,
            PRODUCT_KEYWORD_THESAURUS,
            PRODUCT_STATUS_INFO,
            ORDER_SETTINGS,
            DELIVERY_WAY_INFO,
            CATALOG,
            BANNER,
            NEWS};
}
