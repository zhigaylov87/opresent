package ru.opresent.core.filter;

/**
 * User: artem
 * Date: 06.01.16
 * Time: 22:24
 */
public enum StringFieldCondition {
    CONTAINS,
    NOT_CONTAINS,
    STARTS_WITH,
    ENDS_WITH,
    EQUALS,
    NOT_EQUALS
}
