package ru.opresent.core.filter;

import ru.opresent.core.util.FormatUtils;

import java.io.Serializable;

/**
 * User: artem
 * Date: 06.01.16
 * Time: 22:26
 */
public class StringFieldConstraint implements Serializable {

    private StringFieldCondition condition;
    private String value;
    private boolean ignoreCase = true;

    public StringFieldConstraint() {
    }

    public StringFieldConstraint(StringFieldCondition condition, String value) {
        this.condition = condition;
        this.value = value;
    }

    public StringFieldCondition getCondition() {
        return condition;
    }

    public void setCondition(StringFieldCondition condition) {
        this.condition = condition;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    @Override
    public String toString() {
        return "StringFieldConstraint {" +
                "condition: " + condition +
                ", value: " + FormatUtils.quotes(value) +
                ", ignoreCase: " + ignoreCase +
                '}';
    }
}
