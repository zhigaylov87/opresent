package ru.opresent.core.filter;

/**
 * User: artem
 * Date: 07.01.16
 * Time: 3:18
 */
public class ImmutableStringFieldConstraint extends StringFieldConstraint {

    public ImmutableStringFieldConstraint(StringFieldCondition condition, String value) {
        super(condition, value);
    }

    @Override
    public void setCondition(StringFieldCondition condition) {
        throw new UnsupportedOperationException("This is ImmutableStringFieldConstraint");
    }

    @Override
    public void setValue(String value) {
        throw new UnsupportedOperationException("This is ImmutableStringFieldConstraint");
    }

    @Override
    public void setIgnoreCase(boolean ignoreCase) {
        throw new UnsupportedOperationException("This is ImmutableStringFieldConstraint");
    }
}
