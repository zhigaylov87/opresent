package ru.opresent.core.notification.mail;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.Resource;
import ru.opresent.core.CoreException;
import ru.opresent.core.util.TmpFileSystemResource;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * User: artem
 * Date: 27.04.15
 * Time: 2:54
 */
public class Mail implements AutoCloseable {

    private String subject;
    private HtmlBuilder htmlBuilder = new HtmlBuilder();
    private String html;
    private Map<String, Resource> cidToResource = new HashMap<>();

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    HtmlBuilder getHtmlBuilder() {
        return htmlBuilder;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public void addResource(String cid, Resource resource) {
        if (cidToResource.containsKey(cid)) {
            throw new IllegalArgumentException("Resource with cid = '" + cid + " already exists: " + resource);
        }
        cidToResource.put(cid, resource);
    }

    /**
     * @return cid to resource
     */
    public Map<String, Resource> getResources() {
        return Collections.unmodifiableMap(cidToResource);
    }

    @Override
    public void close() {
        for (Resource resource : cidToResource.values()) {
            if (resource instanceof TmpFileSystemResource) {
                try {
                    FileUtils.forceDelete(resource.getFile());
                } catch (IOException e) {
                    throw new CoreException(e);
                }
            }
        }
    }
}
