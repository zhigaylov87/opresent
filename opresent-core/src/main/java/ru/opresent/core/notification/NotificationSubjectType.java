package ru.opresent.core.notification;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 28.04.15
 * Time: 1:30
 */
public enum NotificationSubjectType implements Codeable {

    NEW_ORDER(0),
    NEW_INCOMING_MESSAGE(1);

    private int code;

    private NotificationSubjectType(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static NotificationSubjectType valueOf(int code) {
        for (NotificationSubjectType type : values()) {
            if (type.code == code) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid notification subject type code: " + code);
    }
}
