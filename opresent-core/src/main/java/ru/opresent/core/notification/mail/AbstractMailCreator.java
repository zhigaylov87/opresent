package ru.opresent.core.notification.mail;

import org.slf4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.config.MarketConfig;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.core.util.LocaleUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 27.04.15
 * Time: 2:48
 */
@Component
public abstract class AbstractMailCreator<T> {

    @Resource(name = "notificationMessageSource")
    private MessageSource notificationMessageSource;

    @Resource(name = "marketConfig")
    protected MarketConfig marketConfig;

    @Resource(name = "appNavigationManager")
    protected AppNavigationManager appNavigationManager;

    @Resource(name = "log")
    protected Logger log;

    public Mail createMail(T data) {
        Mail mail = new Mail();
        try {
            mail.setSubject(createSubject(data));
            HtmlBuilder html = mail.getHtmlBuilder();

            html.docType()
                    .htmlStart()
                    .headStart().metaContentType().headEnd()
                    .bodyStart();

            html.tableStart(); // main table start

            createHeader(mail);
            createBody(data, mail);
            createFooter(html)

                    .tableEnd() // main table end
                    .bodyEnd()
                    .htmlEnd();

            mail.setHtml(html.toString());
            return mail;
        } catch (Exception e) {
            log.error("Error while creating mail for: " + data, e);
            mail.close();
            throw new CoreException(e);
        }
    }

    protected String getMessage(String code, Object... args) {
        return notificationMessageSource.getMessage(code, args, LocaleUtils.RUSSIAN);
    }

    protected abstract String createSubject(T data);

    protected abstract void createBody(T data, Mail mail);

    private void createHeader(Mail mail) {
        final String logoCid = "logo";
        mail.addResource(logoCid, new ClassPathResource(marketConfig.getLogo()));

        HtmlBuilder html = mail.getHtmlBuilder();
        html
                .trStart().tdStart().tableStart()

                .trStart()

                .tdStart(null, 400)
                .aStart(appNavigationManager.getAbsolutWelcomeUrl(), marketConfig.getWww()).img(logoCid, marketConfig.getWww()).aEnd()
                .tdEnd()

                .tdStart();
                createContacts(html)
                .tdEnd()

                .trEnd()

                .tableEnd().trEnd().trEnd();
    }

    private HtmlBuilder createFooter(HtmlBuilder html) {
        html.trStart().tdStart()
                .text(getMessage("respect")).space();
        createMarketWww(html).br().br();
        return createContacts(html).tdEnd().trEnd();
    }

    protected HtmlBuilder createMarketWww(HtmlBuilder html) {
        return html.aStart(appNavigationManager.getAbsolutWelcomeUrl(), marketConfig.getWww()).text(marketConfig.getWww()).aEnd();
    }

    private HtmlBuilder createContacts(HtmlBuilder html) {
        html
                .text(getMessage("market.phone")).space().aStart("tel:" + marketConfig.getPhone(), marketConfig.getPhone()).text(marketConfig.getFormattedPhone()).aEnd().br()
                .text(getMessage("market.email")).space().aStart("mailto:" + marketConfig.getEmail(), marketConfig.getEmail()).text(marketConfig.getEmail()).aEnd().br();
        return createMarketWww(html).br()
                .text(marketConfig.getCopyright());
    }

}
