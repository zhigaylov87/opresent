package ru.opresent.core.notification;

import ru.opresent.core.domain.Identifiable;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * User: artem
 * Date: 28.04.15
 * Time: 1:38
 */
public class Notification implements Identifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotNull(message = "{Notification.subjectType.empty}")
    private NotificationSubjectType subjectType;

    @NotNull(message = "{Notification.subjectId.empty}")
    private Long subjectId;

    @NotNull(message = "{Notification.status.empty}")
    private NotificationStatus status;

    @NotNull(message = "{Notification.statusTime.empty}")
    private Date statusTime;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public NotificationSubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(NotificationSubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

    public Date getStatusTime() {
        return statusTime;
    }

    public void setStatusTime(Date statusTime) {
        this.statusTime = statusTime;
    }

    @Override
    public String toString() {
        return "Notification {" +
                "id: " + id +
                ", subjectType: " + subjectType +
                ", subjectId: " + subjectId +
                ", status: " + status +
                ", statusTime: " + statusTime +
                "}";
    }
}
