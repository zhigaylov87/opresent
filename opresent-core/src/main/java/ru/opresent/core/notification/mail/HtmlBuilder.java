package ru.opresent.core.notification.mail;

import org.apache.commons.lang3.StringUtils;
import org.apache.taglibs.standard.functions.Functions;

/**
 * User: artem
 * Date: 25.04.15
 * Time: 5:06
 */
class HtmlBuilder {

    private final StringBuilder html = new StringBuilder(4096);


    public HtmlBuilder docType() {
        html.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
        return this;
    }

    public HtmlBuilder htmlStart() {
        html.append("<html>");
        return this;
    }

    public HtmlBuilder htmlEnd() {
        html.append("</html>");
        return this;
    }

    public HtmlBuilder headStart() {
        html.append("<head>");
        return this;
    }

    public HtmlBuilder headEnd() {
        html.append("</head>");
        return this;
    }

    public HtmlBuilder metaContentType() {
        html.append("<meta http-equiv=\"content-type\" content=\"html/html; charset=UTF-8\">");
        return this;
    }

    public HtmlBuilder bodyStart() {
        html.append("<body>");
        return this;
    }

    public HtmlBuilder bodyEnd() {
        html.append("</body>");
        return this;
    }

    public HtmlBuilder tableStart() {
        html.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:0; padding:0\">");
        return this;
    }

    public HtmlBuilder tableEnd() {
        html.append("</table>");
        return this;
    }

    public HtmlBuilder trStart() {
        html.append("<tr>");
        return this;
    }

    public HtmlBuilder trEnd() {
        html.append("</tr>");
        return this;
    }

    public HtmlBuilder td() {
        html.append("<td/>");
        return this;
    }

    public HtmlBuilder tdStart() {
        return tdStart(null, null);
    }

    public HtmlBuilder tdStart(Integer colspan, Integer width) {
        if (colspan != null && colspan < 0) {
            throw new IllegalArgumentException("Invalid td colspan: " + colspan);
        }
        if (width != null && width < 0) {
            throw new IllegalArgumentException("Invalid td width: " + colspan);
        }

        html.append("<td style=\"padding: 5px\"");
        if (colspan != null && colspan > 0) {
            html.append(" colspan=\"").append(colspan).append("\"");
        }
        if (width != null && width > 0) {
            html.append(" width=\"").append(width).append("px\"");
        }
        html.append(">");
        return this;
    }

    public HtmlBuilder tdEnd() {
        html.append("</td>");
        return this;
    }

    public HtmlBuilder br() {
        html.append("<br/>");
        return this;
    }

    public HtmlBuilder text(String text1, String... textN) {
        return wrap(StringUtils.EMPTY, StringUtils.EMPTY, text1, textN);
    }

    public HtmlBuilder aStart(String href, String title) {
        html.append("<a href=\"").append(href).append("\" title=\"").append(Functions.escapeXml(title))
                .append("\" target=\"_blank\">");
        return this;
    }

    public HtmlBuilder aEnd() {
        html.append("</a>");
        return this;
    }

    public HtmlBuilder img(String cid, String alt) {
        html.append("<img src=\"cid:").append(cid).append("\" alt=\"").append(Functions.escapeXml(alt)).append("\"/>");
        return this;
    }

    public HtmlBuilder pStart() {
        html.append("<p>");
        return this;
    }

    public HtmlBuilder pEnd() {
        html.append("</p>");
        return this;
    }

    public HtmlBuilder bold(String text1, String... textN) {
        return wrap("<b>", "</b>", text1, textN);
    }

    public HtmlBuilder space() {
        html.append(" ");
        return this;
    }

    @Override
    public String toString() {
        return html.toString();
    }

    private HtmlBuilder wrap(String begin, String end, String text1, String... textN) {
        html.append(begin);
        html.append(text1);
        for (String t : textN) {
            html.append(t);
        }
        html.append(end);
        return this;
    }
}
