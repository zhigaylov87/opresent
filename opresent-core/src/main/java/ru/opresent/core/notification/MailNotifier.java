package ru.opresent.core.notification;

import org.slf4j.Logger;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import ru.opresent.core.config.MarketConfig;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.notification.mail.Mail;
import ru.opresent.core.notification.mail.NewIncomingMessageMailCreator;
import ru.opresent.core.notification.mail.NewOrderMailCreator;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * User: artem
 * Date: 11.03.15
 * Time: 2:22
 */
@Component("mailNotifier")
public class MailNotifier {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "mailSender")
    private JavaMailSenderImpl mailSender;

    @Resource(name = "marketConfig")
    private MarketConfig marketConfig;

    @Resource(name = "newOrderMailCreator")
    private NewOrderMailCreator newOrderMailCreator;

    @Resource(name = "newIncomingMessageMailCreator")
    private NewIncomingMessageMailCreator newIncomingMessageMailCreator;

    public void notifyNewOrderSaved(Order order) throws MessagingException {
        try (Mail mail = newOrderMailCreator.createMail(order)) {
            sendMail(mail, order.getCustomerEmail(), true);
        }
    }

    public void notifyNewIncomingMessageSaved(IncomingMessage incomingMessage) throws MessagingException {
        try (Mail mail = newIncomingMessageMailCreator.createMail(incomingMessage)) {
            sendMail(mail, mailSender.getUsername(), false);
        }
    }

    private void sendMail(Mail mail, String to, boolean bcc) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");

        try {
            helper.setFrom(mailSender.getUsername(), marketConfig.getName());
        } catch (UnsupportedEncodingException e) {
            throw new MessagingException("set from failed", e);
        }
        helper.setTo(to);
        if (bcc) {
            helper.setBcc(mailSender.getUsername());
        }

        helper.setSubject(mail.getSubject());

        log.debug(mail.getHtml());
        helper.setText(mail.getHtml(), true);

        for (Map.Entry<String, org.springframework.core.io.Resource> entry : mail.getResources().entrySet()) {
            helper.addInline(entry.getKey(), entry.getValue());
        }

        mailSender.send(message);
    }
}
