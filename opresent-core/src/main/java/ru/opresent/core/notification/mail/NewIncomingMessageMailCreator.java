package ru.opresent.core.notification.mail;

import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.core.util.FormatUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 28.01.16
 * Time: 23:39
 */
@Component("newIncomingMessageMailCreator")
public class NewIncomingMessageMailCreator extends AbstractMailCreator<IncomingMessage> {

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    @Override
    protected String createSubject(IncomingMessage message) {
        return getMessage("new_incoming_message.subject", marketConfig.getName(), message.getId());
    }

    @Override
    protected void createBody(IncomingMessage message, Mail mail) {
        HtmlBuilder html = mail.getHtmlBuilder();

        html.tableStart();

        trMessageParam(html, getMessage("incoming_message.creation_time"), FormatUtils.formatDateTime(message.getCreationTime()));
        trMessageParam(html, getMessage("incoming_message.customer_name"), message.getCustomerName());
        trMessageParam(html, getMessage("incoming_message.customer_email_or_phone"), message.getCustomerEmailOrPhone());
        createSubjectTr(html, message);
        trMessageParam(html, getMessage("incoming_message.message_text"), message.getMessageText());

        html.tableEnd();
    }

    private HtmlBuilder trMessageParam(HtmlBuilder html, String paramName, String paramValue) {
        return html.trStart()
                .tdStart().text(paramName).tdEnd()
                .tdStart().text(paramValue).tdEnd()
                .trEnd();
    }

    private void createSubjectTr(HtmlBuilder html, IncomingMessage message) {
        String subjectUrl;
        switch (message.getSubjectType()) {
            case PRODUCT:
                Product product = productService.loadBaseIfExists(message.getSubjectId());
                subjectUrl = appNavigationManager.getAbsolutProductUrl(product);
                break;

            case SECTION:
                Section section = sectionService.getAll().getById(message.getSubjectId());
                subjectUrl = appNavigationManager.getAbsoluteSectionUrl(section);
                break;

            default:
                throw new CoreException("Unsupported subject type: " + message.getSubjectType());
        }

        html.trStart()
                .tdStart().text(getMessage("incoming_message.subject_url")).tdEnd()
                .tdStart().aStart(subjectUrl, subjectUrl).text(subjectUrl).aEnd()
                .trEnd();
    }
}
