package ru.opresent.core.notification.mail;

import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUseOption;
import ru.opresent.core.domain.order.DeliveryWay;
import ru.opresent.core.domain.order.DeliveryWayInfo;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderItem;
import ru.opresent.core.service.api.DeliveryWayService;
import ru.opresent.core.util.EnumHelper;
import ru.opresent.core.util.FormatUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 27.04.15
 * Time: 3:02
 */
@Component("newOrderMailCreator")
public class NewOrderMailCreator extends AbstractMailCreator<Order> {

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;

    @Resource(name = "enumHelper")
    private EnumHelper enumHelper;

    @Resource(name = "deliveryWayService")
    private DeliveryWayService deliveryWayService;

    @Override
    protected String createSubject(Order order) {
        return getMessage("new_order.subject", marketConfig.getName(), order.getId());
    }

    @Override
    protected void createBody(Order order, Mail mail) {
        HtmlBuilder html = mail.getHtmlBuilder();

        createHello(order, html);
        if (!order.isPersonalOrder()) {
            createOrderItems(order, mail);
        }
        createOrderParams(order, mail);
        createThanks(html);
    }

    private void createHello(Order order, HtmlBuilder html) {
        html
                .trStart().tdStart()
                .bold(getMessage("hello", order.getCustomerFullName())).br()
                .text(getMessage("new_order.greeting")).space();
        createMarketWww(html).tdEnd().trEnd();
    }

    private void createOrderItems(Order order, Mail mail) {
        HtmlBuilder html = mail.getHtmlBuilder();

        html.trStart().tdStart()
                .pStart().bold(getMessage("your_order")).pEnd()
                .tableStart();
        for (OrderItem orderItem : order.getItems()) {
            Product product = orderItem.getProduct();
            String productUrl = appNavigationManager.getAbsolutProductUrl(product);

            Image preview = product.getPreview();
            org.springframework.core.io.Resource previewFileResource = imageFileManager.getImageFileResource(
                    preview, ImageUseOption.PRODUCT_PREVIEW_IMAGE_IN_CART);
            String previewCid = previewFileResource.getFilename();
            mail.addResource(previewCid, previewFileResource);

            html
                    .trStart()

                    .tdStart()
                    .aStart(productUrl, product.getName()).img(previewCid, product.getName()).aEnd()
                    .tdEnd()

                    .tdStart()
                    .aStart(productUrl, product.getName()).text(product.getName()).aEnd().br()
                    .text(getMessage("order.item.product.status", enumHelper.getDescription(product.getStatus()))).br()
                    .text(getMessage("order.item.fixPrice", FormatUtils.formatAmount(orderItem.getFixPrice())))
                    .tdEnd()

                    .trEnd();
        }
        html
                .trStart()

                .td()

                .tdStart()
                .bold(getMessage("order.total_count_x_total_price", order.getItems().size(), FormatUtils.formatAmount(order.getTotalPrice())))
                .tdEnd()

                .trEnd()

                .tableEnd() // order items
                .tdEnd().trEnd();
    }

    private void createOrderParams(Order order, Mail mail) {
        HtmlBuilder html = mail.getHtmlBuilder();

        html.trStart().tdStart()
                .pStart().bold(getMessage("order_params")).pEnd()
                .tableStart();

        trOrderParam(html, getMessage("order.customer_full_name"), order.getCustomerFullName());
        trOrderParam(html, getMessage("order.customer_phone"), order.getCustomerPhone());
        trOrderParam(html, getMessage("order.customer_email"), order.getCustomerEmail());

        if (!order.isPersonalOrder()) {
            DeliveryWayInfo deliveryWayInfo = deliveryWayService.getAll().getById(
                    new DeliveryWayInfo(order.getAddressType(), order.getDeliveryWay()).getId());

            trOrderParam(html, getMessage("order.delivery_way"),
                    enumHelper.getDescription(order.getDeliveryWay()) + ": " + deliveryWayInfo.getPrice() + "<br/>" +
                            deliveryWayInfo.getNote());

            if (order.getDeliveryWay() != DeliveryWay.SELF_EXPORT) {
                trOrderParam(html, getMessage("order.customer_city"),
                        order.getCustomerCity() != null ? order.getCustomerCity() : enumHelper.getDescription(order.getAddressType()));
            }
            if (order.getCustomerPostcode() != null) {
                trOrderParam(html, getMessage("order.customer_postcode"), order.getCustomerPostcode());
            }
            if (order.getCustomerAddress() != null) {
                trOrderParam(html, getMessage("order.customer_address"), order.getCustomerAddress());
            }
        }

        if (order.getCustomerComment() != null) {
            trOrderParam(html, getMessage("order.customer_comment"), order.getCustomerComment());
        }

        html.tableEnd().tdEnd().trEnd();
    }

    private HtmlBuilder trOrderParam(HtmlBuilder html, String paramName, String paramValue) {
        return html.trStart()
                .tdStart().text(paramName).tdEnd()
                .tdStart().text(paramValue).tdEnd()
                .trEnd();
    }

    private void createThanks(HtmlBuilder html) {
        html.
                trStart().tdStart()
                .text(getMessage("new_order.thanks")).br()
                .text(getMessage("new_order.master_contact_your")).br()
                .tdEnd().trEnd();
    }
}
