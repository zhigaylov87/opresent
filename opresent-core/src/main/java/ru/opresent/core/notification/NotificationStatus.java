package ru.opresent.core.notification;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 28.04.15
 * Time: 1:34
 */
public enum NotificationStatus implements Codeable {

    NEW(0),
    SEND_FAILED(1),
    SENDED(2);

    private int code;

    NotificationStatus(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static NotificationStatus valueOf(int code) {
        for (NotificationStatus status : values()) {
            if (status.code == code) {
                return status;
            }
        }
        throw new IllegalArgumentException("Invalid notification status code: " + code);
    }
}
