package ru.opresent.core.domain.message.incoming;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 22.11.14
 * Time: 16:05
 */
public enum IncomingMessageSubjectType implements Codeable {

    PRODUCT(0),
    SECTION(1);

    private final int code;

    private IncomingMessageSubjectType(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static IncomingMessageSubjectType valueOfCode(int code) {
        for (IncomingMessageSubjectType type : values()) {
            if (type.code == code) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unknown incoming message subject type code: " + code);
    }
}
