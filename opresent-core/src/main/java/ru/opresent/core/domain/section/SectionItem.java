package ru.opresent.core.domain.section;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.GroupedOrderedEntity;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.ImageUseConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 21.06.14
 * Time: 14:38
 */
public class SectionItem implements Identifiable, GroupedOrderedEntity<Section>, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotBlank(message = "{SectionItem.name.empty}")
    @Size(max = EntitiesConstraints.SECTION_ITEM_NAME_MAX_LENGTH)
    private String name;

    @NotNull(message = "{SectionItem.preview.empty}")
    @ImageUseConstraint(ImageUse.SECTION_ITEM_PREVIEW_IMAGE)
    private Image preview;

    @NotBlank(message = "{SectionItem.shortText.empty}")
    @Size(max = EntitiesConstraints.SECTION_ITEM_SHORT_TEXT_MAX_LENGTH)
    private String shortText;

    @NotBlank(message = "{SectionItem.fullText.empty}")
    private String fullText;

    @NotNull(message = "{SectionItem.section.empty}")
    private Section section;

    private boolean visible;

    private int index;

    public SectionItem() {
    }

    public SectionItem(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getPreview() {
        return preview;
    }

    public void setPreview(Image preview) {
        this.preview = preview;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        if (section.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Not saved section defined for section item!");
        }
        if (section.getType() != SectionType.LIST) {
            throw new IllegalArgumentException("Section must be with type " + SectionType.LIST +
                    ", but current is " + section.getType());
        }
        this.section = section;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public Section getGroupByEntity() {
        return section;
    }

    @Override
    public String toString() {
        return new EntityStringifier(true)
                .add("id", id)
                .add("name", name)
                .add("preview", preview)
                .add("shortText", shortText)
                .add("fullText", fullText)
                .add("section", section)
                .add("visible", visible)
                .add("index", index)
                .toString();
    }
}
