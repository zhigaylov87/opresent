package ru.opresent.core.domain;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.CategoryConstraint;
import ru.opresent.core.validation.constraint.EntityName;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 16.03.14
 * Time: 20:04
 */
@CategoryConstraint
public class Category implements Identifiable, NamedEntity, OrderedEntity, Stringifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @EntityName
    private String entityName;

    @NotBlank(message = "{Category.name.empty}")
    @Size(max = EntitiesConstraints.CATEGORY_NAME_MAX_LENGTH)
    private String name;

    @NotBlank(message = "{Category.description.empty}")
    @Size(max = EntitiesConstraints.CATEGORY_DESCRIPTION_MAX_LENGTH)
    private String description;

    private int index;

    public Category() {
    }

    public Category(long id) {
        this.id = id;
    }

    public Category(Category category) {
        this.id = category.id;
        this.entityName = category.entityName;
        this.name = category.name;
        this.description = category.description;
        this.index = category.index;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;

        Category category = (Category) o;
        return ObjectUtils.equalsIds(this, category);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("id", id)
                .add("entityName", entityName)
                .add("name", name)
                .add("description", description)
                .add("index", index);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }

}
