package ru.opresent.core.domain.banner;

import ru.opresent.core.util.IdentifiableList;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: artem
 * Date: 07.05.15
 * Time: 23:28
 */
public class Banner implements Stringifiable, Serializable {

    private IdentifiableList<BannerItem> allItems;
    private IdentifiableList<BannerItem> allVisibleItems;

    public Banner(List<BannerItem> allItems) {
        this.allItems = new IdentifiableList<>(allItems);

        List<BannerItem> visibleItems = new ArrayList<>();
        for (BannerItem item : allItems) {
            if (item.isVisible()) {
                visibleItems.add(item);
            }
        }
        this.allVisibleItems = new IdentifiableList<>(visibleItems);
    }

    public IdentifiableList<BannerItem> getAllItems() {
        return allItems;
    }

    public IdentifiableList<BannerItem> getAllVisibleItems() {
        return allVisibleItems;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("allItems", allItems.getAll().size())
                .add("allVisibleItems", allVisibleItems.getAll().size());
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
