package ru.opresent.core.domain.cart;

import ru.opresent.core.domain.Product;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * User: artem
 * Date: 16.11.14
 * Time: 15:21
 */
public class Cart implements Stringifiable, Serializable {

    private List<Product> products;

    public Cart(List<Product> products) {
        this.products = Objects.requireNonNull(products);
    }

    public List<Product> getProducts() {
        return products;
    }

    public int getTotalPrice() {
        int total = 0;
        for (Product product : products) {
            total += product.getPrice();
        }
        return total;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("products", products);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
