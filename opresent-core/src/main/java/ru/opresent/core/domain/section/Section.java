package ru.opresent.core.domain.section;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.NamedEntity;
import ru.opresent.core.domain.OrderedEntity;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.EntityName;
import ru.opresent.core.validation.constraint.SectionConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 2:48
 */
@SectionConstraint
public class Section implements Identifiable, NamedEntity, OrderedEntity, Serializable {

    public static final String CATALOG_SECTION_ENTITY_NAME = "catalog";

    private long id = Identifiable.NOT_SAVED_ID;
    @NotNull
    private SectionType type;

    @EntityName
    private String entityName;

    @NotBlank(message = "{Section.name.empty}")
    @Size(max = EntitiesConstraints.SECTION_NAME_MAX_LENGTH)
    private String name;

    private boolean visible;

    private int index;

    @NotBlank(message = "{Section.text.empty}")
    private String text;

    public Section(SectionType type) {
        this.type = Objects.requireNonNull(type);
    }

    public Section(long id, SectionType type) {
        this.id = id;
        this.type = Objects.requireNonNull(type);
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SectionType getType() {
        return type;
    }

    @Override
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Section)) return false;

        Section section = (Section) o;
        return ObjectUtils.equalsIds(this, section);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return new EntityStringifier(true)
                .add("id", id)
                .add("type", type)
                .add("index", index)
                .add("name", name)
                .add("entityName", entityName)
                .add("visible", visible)
                .add("text", text)
                .toString();
    }
}
