package ru.opresent.core.domain.news;

import ru.opresent.core.util.IdentifiableList;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: artem
 * Date: 07.05.15
 * Time: 23:28
 */
public class News implements Stringifiable, Serializable {

    private IdentifiableList<NewsItem> allItems;
    private IdentifiableList<NewsItem> allVisibleItems;

    public News(List<NewsItem> allItems) {
        this.allItems = new IdentifiableList<>(allItems);

        List<NewsItem> visibleItems = new ArrayList<>();
        for (NewsItem item : allItems) {
            if (item.isVisible()) {
                visibleItems.add(item);
            }
        }
        this.allVisibleItems = new IdentifiableList<>(visibleItems);
    }

    public IdentifiableList<NewsItem> getAllItems() {
        return allItems;
    }

    public IdentifiableList<NewsItem> getAllVisibleItems() {
        return allVisibleItems;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("allItems", allItems.getAll().size())
                .add("allVisibleItems", allVisibleItems.getAll().size());
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
