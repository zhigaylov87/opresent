package ru.opresent.core.domain;

/**
 * User: artem
 * Date: 13.03.15
 * Time: 3:04
 */
public interface NamedEntity {

    public String getEntityName();

}
