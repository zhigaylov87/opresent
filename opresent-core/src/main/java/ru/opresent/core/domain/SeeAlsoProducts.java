package ru.opresent.core.domain;

import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;

import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 18.06.16
 * Time: 23:02
 */
public class SeeAlsoProducts implements Stringifiable {

    private List<Product> products;

    public SeeAlsoProducts(List<Product> products) {
        this.products = Collections.unmodifiableList(products);
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("products", products.size());
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
