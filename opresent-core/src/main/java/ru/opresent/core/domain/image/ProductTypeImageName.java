package ru.opresent.core.domain.image;

import ru.opresent.core.domain.ProductType;

/**
 * User: artem
 * Date: 12.10.15
 * Time: 19:35
 */
public class ProductTypeImageName implements ImageName {

    private ProductType productType;

    public ProductTypeImageName(ProductType productType) {
        this.productType = productType;
    }

    @Override
    public String getName() {
        return productType.getEntityName();
    }
}
