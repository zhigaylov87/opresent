package ru.opresent.core.domain.order;

import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.OrderConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * User: artem
 * Date: 01.02.15
 * Time: 23:00
 */
@OrderConstraint
public class Order extends OrderInfo implements Identifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    private List<OrderItem> items;

    @NotNull(message = "{Order.status.empty}")
    private OrderStatus status;

    @Size(max = EntitiesConstraints.ORDER_MASTER_COMMENT_MAX_LENGTH, message = "{Order.masterComment.veryLong}")
    private String masterComment;

    private Date modifyTime;

    private Date createTime;

    public Order(OrderInfo orderInfo) {
        super(orderInfo);
    }

    public Order(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getMasterComment() {
        return masterComment;
    }

    public void setMasterComment(String masterComment) {
        this.masterComment = masterComment;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getTotalPrice() {
        int total = 0;
        if (items != null) {
            for (OrderItem item : items) {
                total += item.getFixPrice();
            }
        }
        return total;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("id", id)
                .add("status", status)
                .addObjects("items", items)
                .add("masterComment", masterComment)
                .add("modifyTime", modifyTime)
                .add("createTime", createTime)
                .add("info", super.stringify(fully).toString());
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
