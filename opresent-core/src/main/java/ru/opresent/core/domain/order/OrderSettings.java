package ru.opresent.core.domain.order;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.validation.EntitiesConstraints;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 17.10.15
 * Time: 4:54
 */
public class OrderSettings implements Serializable {

    @NotBlank(message = "{OrderSettings.personalOrderText.empty}")
    @Size(max = EntitiesConstraints.ORDER_SETTING_PERSONAL_ORDER_TEXT_MAX_LENGTH)
    private String personalOrderText;

    public String getPersonalOrderText() {
        return personalOrderText;
    }

    public void setPersonalOrderText(String personalOrderText) {
        this.personalOrderText = personalOrderText;
    }

    @Override
    public String toString() {
        return "OrderSettings {" +
                "personalOrderText: " + FormatUtils.quotes(personalOrderText) +
                '}';
    }
}
