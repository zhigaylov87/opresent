package ru.opresent.core.domain.catalog;

import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.util.stringify.EntityStringifier;

import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 30.10.14
 * Time: 1:33
 */
public class DefaultCatalog implements Catalog, Serializable {

    private Map<ProductType, Set<Category>> productTypeToCategories;
    private Set<Category> allCategories;

    public DefaultCatalog(Map<ProductType, Set<Category>> productTypeToCategories) {
        this.productTypeToCategories = new HashMap<>();
        this.allCategories = new HashSet<>();

        for (Map.Entry<ProductType, Set<Category>> entry : productTypeToCategories.entrySet()) {
            ProductType productType = entry.getKey();
            Set<Category> categories = entry.getValue();
            this.productTypeToCategories.put(productType, Collections.unmodifiableSet(categories));
            this.allCategories.addAll(categories);
        }

        this.productTypeToCategories = Collections.unmodifiableMap(this.productTypeToCategories);
        this.allCategories = Collections.unmodifiableSet(this.allCategories);
    }

    @Override
    public Set<ProductType> getProductTypes() {
        return productTypeToCategories.keySet();
    }

    @Override
    public Set<Category> getProductTypeCategories(ProductType productType) {
        if (!productTypeToCategories.containsKey(productType)) {
            throw new IllegalArgumentException("Illegal product type: " + productType);
        }
        return productTypeToCategories.get(productType);
    }

    @Override
    public Set<Category> getAllCategories() {
        return allCategories;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("productTypes", getProductTypes().size())
                .add("allCategories", getAllCategories().size());
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
