package ru.opresent.core.domain;

import ru.opresent.core.util.stringify.EntityStringifier;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

/**
 * User: artem
 * Date: 08.01.14
 * Time: 15:36
 */
public class DefaultProductFilter implements ProductFilter, Serializable {

    private ProductType productType;
    private String name;
    private String description;
    private Boolean visible;
    private Integer minPrice;
    private Integer maxPrice;
    private Set<ProductStatus> statuses;
    private Size size;
    private Set<Color> colors;
    private Category category;
    private Set<Long> excludeProductIds;

    public DefaultProductFilter() {
    }

    public DefaultProductFilter(ProductFilter filter) {
        this.productType = filter.getProductType();
        this.name = filter.getName();
        this.description = filter.getDescription();
        this.visible = filter.getVisible();
        this.minPrice = filter.getMinPrice();
        this.maxPrice = filter.getMaxPrice();
        this.statuses = filter.getStatuses();
        this.size = filter.getSize();
        this.colors = filter.getColors();
        this.category = filter.getCategory();
        this.excludeProductIds = filter.getExcludeProductIds();
    }

    @Override
    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    @Override
    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    @Override
    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    @Override
    public ProductStatus getStatus() {
        return statuses == null ? null : statuses.iterator().next();
    }

    public void setStatus(ProductStatus status) {
        this.statuses = status == null ? null : Collections.singleton(status);
    }

    @Override
    public Set<ProductStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<ProductStatus> statuses) {
        if (statuses == null) {
            this.statuses = null;
        } else {
            if (statuses.isEmpty()) {
                throw new IllegalArgumentException("Statuses is empty!");
            }
            this.statuses = Collections.unmodifiableSet(statuses);
        }
    }

    @Override
    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public Set<Color> getColors() {
        return colors;
    }

    public void setColors(Set<Color> colors) {
        this.colors = colors;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public Set<Long> getExcludeProductIds() {
        return excludeProductIds;
    }

    public void setExcludeProductIds(Set<Long> excludeProductIds) {
        this.excludeProductIds = excludeProductIds;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("type", productType)
                .add("name", name)
                .add("description", description)
                .add("visible", visible)
                .add("minPrice", minPrice)
                .add("maxPrice", maxPrice)
                .addEnums("statuses", statuses)
                .add("size", size)
                .add("colors", colors)
                .add("category", category)
                .addObjects("excludeProductIds", excludeProductIds);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
