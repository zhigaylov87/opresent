package ru.opresent.core.domain;

import ru.opresent.core.util.FormatUtils;

/**
 * User: artem
 * Date: 20.04.14
 * Time: 0:45
 */
public enum ProductStatus implements Codeable, NamedEntity {

    AVAILABLE(0, "available"),
    FOR_ORDER(1, "for-order"),
    EXAMPLE(2, "example");

    private final int code;
    private final String entityName;

    private ProductStatus(int code, String entityName) {
        this.code = code;
        this.entityName = entityName;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getEntityName() {
        return entityName;
    }

    public static ProductStatus valueOfCode(int code) {
        for (ProductStatus status : values()) {
            if (status.code == code) {
                return status;
            }
        }

        throw new IllegalArgumentException("Unknown product status code: " + code);
    }

    public static ProductStatus valueOfEntityName(String entityName) {
        for (ProductStatus status : values()) {
            if (status.entityName.equals(entityName)) {
                return status;
            }
        }

        throw new IllegalArgumentException("Unknown product status entity name: " + FormatUtils.quotes(entityName));
    }

    public static boolean containsByEntityName(String entityName) {
        for (ProductStatus status : values()) {
            if (status.entityName.equals(entityName)) {
                return true;
            }
        }
        return false;
    }
}
