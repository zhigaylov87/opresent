package ru.opresent.core.domain.order;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 14.02.15
 * Time: 15:31
 */
public enum OrderStatus implements Codeable {

    NEW(0),
    PROCESSING(1),
    EXECUTED(2),
    REJECTED(3);

    private final int code;

    private OrderStatus(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static OrderStatus valueOfCode(int code) {
        for (OrderStatus status : values()) {
            if (status.code == code) {
                return status;
            }
        }

        throw new IllegalArgumentException("Unknown order status code: " + code);
    }
}
