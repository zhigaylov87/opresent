package ru.opresent.core.domain;

/**
 * User: artem
 * Date: 04.03.15
 * Time: 2:09
 */
public interface Codeable {

    public int getCode();

}
