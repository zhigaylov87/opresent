package ru.opresent.core.domain.order;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.OrderInfoConstraint;

import javax.validation.constraints.Size;

/**
 * User: artem
 * Date: 01.02.15
 * Time: 23:07
 */
@OrderInfoConstraint
public class OrderInfo implements Stringifiable {

    @NotBlank(message = "{OrderInfo.customerFullName.empty}")
    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_FULL_NAME_MAX_LENGTH, message = "{OrderInfo.customerFullName.veryLong}")
    private String customerFullName;

    @NotBlank(message = "{OrderInfo.customerPhone.empty}")
    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_PHONE_MAX_LENGTH, message = "{OrderInfo.customerPhone.veryLong}")
    private String customerPhone;

    @NotBlank(message = "{OrderInfo.customerEmail.empty}")
    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_EMAIL_MAX_LENGTH, message = "{OrderInfo.customerEmail.veryLong}")
    private String customerEmail;

    private boolean personalOrder;

    private AddressType addressType;

    private DeliveryWay deliveryWay;

    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_CITY_MAX_LENGTH, message = "{OrderInfo.customerCity.veryLong}")
    private String customerCity;

    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_POSTCODE_MAX_LENGTH, message = "{OrderInfo.customerPostcode.veryLong}")
    private String customerPostcode;

    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_ADDRESS_MAX_LENGTH, message = "{OrderInfo.customerAddress.veryLong}")
    private String customerAddress;

    @Size(max = EntitiesConstraints.ORDER_INFO_CUSTOMER_COMMENT_MAX_LENGTH, message = "{OrderInfo.customerComment.veryLong}")
    private String customerComment;

    public OrderInfo() {
    }

    public OrderInfo(OrderInfo other) {
        this.customerFullName = other.customerFullName;
        this.customerPhone = other.customerPhone;
        this.customerEmail = other.customerEmail;
        this.personalOrder = other.personalOrder;
        this.addressType = other.addressType;
        this.deliveryWay = other.deliveryWay;
        this.customerCity = other.customerCity;
        this.customerPostcode = other.customerPostcode;
        this.customerAddress = other.customerAddress;
        this.customerComment = other.customerComment;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public boolean isPersonalOrder() {
        return personalOrder;
    }

    public void setPersonalOrder(boolean personalOrder) {
        this.personalOrder = personalOrder;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public DeliveryWay getDeliveryWay() {
        return deliveryWay;
    }

    public void setDeliveryWay(DeliveryWay deliveryWay) {
        this.deliveryWay = deliveryWay;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerPostcode() {
        return customerPostcode;
    }

    public void setCustomerPostcode(String customerPostcode) {
        this.customerPostcode = customerPostcode;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerComment() {
        return customerComment;
    }

    public void setCustomerComment(String customerComment) {
        this.customerComment = customerComment;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("customerFullName", customerFullName)
                .add("customerPhone", customerPhone)
                .add("customerEmail", customerEmail)
                .add("personalOrder", personalOrder)
                .add("addressType", addressType)
                .add("deliveryWay", deliveryWay)
                .add("customerCity", customerCity)
                .add("customerPostcode", customerPostcode)
                .add("customerAddress", customerAddress)
                .add("customerComment", customerComment);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
