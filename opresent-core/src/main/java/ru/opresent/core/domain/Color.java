package ru.opresent.core.domain;

import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.validation.constraint.ColorConstraint;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * User: artem
 * Date: 09.03.14
 * Time: 18:19
 */
@ColorConstraint
public class Color implements Identifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;
    @NotNull(message = "{Color.rgb.empty}")
    private Integer rgb;

    public Color() {
    }

    public Color(long id) {
        this.id = id;
    }

    public Color(Color color) {
        this.id = color.id;
        this.rgb = color.getRgb();
    }

    @Override
    public long getId() {
        return id;
    }

    public Integer getRgb() {
        return rgb;
    }

    public void setRgb(Integer rgb) {
        this.rgb = rgb;
    }

    /**
     * @return hex without "<b>#</b>", for example: "<b>b8ffb0</b>"
     */
    public String getHex() {
        return rgb == null ? null : Integer.toHexString(rgb).substring(2);
    }

    /**
     * @param hex hex without "<b>#</b>", for example: "<b>b8ffb0</b>"
     */
    public void setHex(String hex) {
        rgb = java.awt.Color.decode("#" + hex).getRGB();
    }

    /**
     * @return color hex, for example: "<b>#b8ffb0</b>"
     */
    public String getDescription() {
        return "#" + getHex();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Color)) return false;

        Color color = (Color) o;
        return ObjectUtils.equalsIds(this, color);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Color{" +
                "id: " + id +
                ", rgb: " + rgb + " - " + getDescription() +
                "}";
    }
}
