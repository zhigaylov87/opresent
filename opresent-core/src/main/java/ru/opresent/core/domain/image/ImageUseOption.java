package ru.opresent.core.domain.image;

import ru.opresent.core.util.ListValueMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 01.11.14
 * Time: 20:15
 */
public enum ImageUseOption {

    ORIGINAL(0, null),

    SECTION_ITEM_PREVIEW_IN_SECTION_LIST(1, ImageUse.SECTION_ITEM_PREVIEW_IMAGE),

    PRODUCT_TYPE_IMAGE_IN_WELCOME(2, ImageUse.PRODUCT_TYPE_IMAGE),

    PRODUCT_LABEL_IMAGE_IN_PRODUCT_LIST(3, ImageUse.PRODUCT_LABEL_IMAGE),

    PRODUCT_PREVIEW_IMAGE_IN_PRODUCT_LIST(4, ImageUse.PRODUCT_PREVIEW_IMAGE),
    PRODUCT_PREVIEW_IMAGE_IN_CART(5, ImageUse.PRODUCT_PREVIEW_IMAGE),

    PRODUCT_VIEW_IMAGE_IN_GALLERIA_CURRENT(6, ImageUse.PRODUCT_VIEW_IMAGE),
    PRODUCT_VIEW_IMAGE_IN_GALLERIA_LIST(7, ImageUse.PRODUCT_VIEW_IMAGE);

    private static final Map<ImageUse, List<ImageUseOption>> IMAGE_TYPE_TO_OPTIONS = new HashMap<>();
    static {
        ListValueMap<ImageUse, ImageUseOption> imageTypeToOptions = new ListValueMap<>();
        for (ImageUseOption option : values()) {
            imageTypeToOptions.add(option.getImageUse(), option);
        }

        for (Map.Entry<ImageUse, List<ImageUseOption>> entry : imageTypeToOptions.entrySet()) {
            IMAGE_TYPE_TO_OPTIONS.put(entry.getKey(), Collections.unmodifiableList(entry.getValue()));
        }
    }

    private final int code;
    private final ImageUse imageUse;

    ImageUseOption(int code, ImageUse imageUse) {
        this.code = code;
        this.imageUse = imageUse;
    }

    public int getCode() {
        return code;
    }

    public ImageUse getImageUse() {
        return imageUse;
    }

    public static List<ImageUseOption> getImageUseOptionsForUseImage(ImageUse imageUse) {
        return IMAGE_TYPE_TO_OPTIONS.get(imageUse);
    }
}
