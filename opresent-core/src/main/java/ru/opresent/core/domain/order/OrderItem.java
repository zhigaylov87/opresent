package ru.opresent.core.domain.order;

import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.Product;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * User: artem
 * Date: 01.02.15
 * Time: 23:00
 */
public class OrderItem implements Identifiable, Stringifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotNull(message = "{OrderItem.product.empty}")
    private Product product;

    @Min(value = EntitiesConstraints.PRODUCT_PRICE_MIN_VALUE, message = "{OrderItem.fixPrice.verySmall}")
    private int fixPrice;

    public OrderItem() {
    }

    public OrderItem(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getFixPrice() {
        return fixPrice;
    }

    public void setFixPrice(int fixPrice) {
        this.fixPrice = fixPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderItem)) return false;

        OrderItem orderItem = (OrderItem) o;
        return ObjectUtils.equalsIds(this, orderItem);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("id", id)
                .add("product", product)
                .add("fixPrice", fixPrice);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
