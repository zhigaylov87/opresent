package ru.opresent.core.domain.image;

import ru.opresent.core.domain.section.SectionItem;

/**
 * User: artem
 * Date: 12.10.15
 * Time: 14:27
 */
public class SectionItemImageName implements ImageName {

    private SectionItem sectionItem;

    public SectionItemImageName(SectionItem sectionItem) {
        this.sectionItem = sectionItem;
    }

    @Override
    public String getName() {
        return sectionItem.getName();
    }
}
