package ru.opresent.core.domain.order;

import ru.opresent.core.domain.Codeable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 18.09.15
 * Time: 2:46
 */
public enum AddressType implements Codeable {

    MOSCOW_AND_REGION(0, false,
            DeliveryWay.SELF_EXPORT,
            DeliveryWay.COURIER_TO_METRO_STATION,
            DeliveryWay.COURIER_TO_ADDRESS_INSIDE_MKAD,
            DeliveryWay.COURIER_OUTSIDE_MKAD,
            DeliveryWay.RUSSIAN_POST),

    OTHER_CITY(1, true,
            DeliveryWay.RUSSIAN_POST,
            DeliveryWay.COURIER_EXPRESS_SERVICE);

    private final int code;
    private final boolean cityRequired;
    private final List<DeliveryWay> deliveryWays;

    AddressType(int code, boolean cityRequired, DeliveryWay... deliveryWays) {
        this.code = code;
        this.cityRequired = cityRequired;
        this.deliveryWays = Collections.unmodifiableList(Arrays.asList(deliveryWays));
    }

    @Override
    public int getCode() {
        return code;
    }

    public boolean isCityRequired() {
        return cityRequired;
    }

    public List<DeliveryWay> getDeliveryWays() {
        return deliveryWays;
    }

    public static AddressType valueOf(int code) {
        for (AddressType type : values()) {
            if (type.code == code) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown address type code: " + code);
    }
}
