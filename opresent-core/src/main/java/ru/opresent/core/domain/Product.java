package ru.opresent.core.domain;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.ImageUseConstraint;
import ru.opresent.core.validation.constraint.ProductConstraint;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 01.03.14
 * Time: 14:20
 */
@ProductConstraint
public class Product implements Identifiable, OrderedEntity, Stringifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_LIVEMASTER_ID_MAX_LENGTH, message = "{Product.livemasterId.veryLong}")
    private String livemasterId;

    @NotNull(message = "{Product.type.empty}")
    private ProductType type;

    @NotBlank(message = "{Product.name.empty}")
    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_NAME_MAX_LENGTH, message = "{Product.name.veryLong}")
    private String name;

    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_DESCRIPTION_MAX_LENGTH, message = "{Product.description.veryLong}")
    private String description;

    private boolean visible;

    @NotNull(message = "{Product.status.empty}")
    private ProductStatus status;

    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_STATUS_NOTE_MAX_LENGTH, message = "{Product.statusNote.veryLong}")
    private String statusNote;

    @Min(value = EntitiesConstraints.PRODUCT_PRICE_MIN_VALUE, message = "{Product.price.verySmall}")
    private Integer price;

    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_PRODUCTION_TIME_MAX_LENGTH, message = "{Product.productionTime.veryLong}")
    private String productionTime;

    private ProductLabel label;

    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_LABEL_NOTE_MAX_LENGTH, message = "{Product.labelNote.veryLong}")
    private String labelNote;

    private Size size;

    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_SIZE_TEXT_MAX_LENGTH, message = "{Product.sizeText.veryLong}")
    private String sizeText;

    @NotNull(message = "{Product.preview.empty}")
    @ImageUseConstraint(ImageUse.PRODUCT_PREVIEW_IMAGE)
    private Image preview;
    @ImageUseConstraint(ImageUse.PRODUCT_VIEW_IMAGE)
    private List<Image> views = new ArrayList<>();

    private Set<Color> colors = new HashSet<>();

    private Set<Category> categories = new HashSet<>();

    private Set<ProductKeyword> keywords = new HashSet<>();

    private Date modifyTime;

    private int index;

    public Product() {
    }

    public Product(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getLivemasterId() {
        return livemasterId;
    }

    public void setLivemasterId(String livemasterId) {
        this.livemasterId = livemasterId;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status;
    }

    /**
     * @return product status note or {@code null} if status note is not defined
     */
    public String getStatusNote() {
        return statusNote;
    }

    /**
     * @param statusNote product status note or {@code null}
     */
    public void setStatusNote(String statusNote) {
        this.statusNote = statusNote;
    }

    public String getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(String productionTime) {
        this.productionTime = productionTime;
    }

    /**
     * @return product label or {@code null} if product label not defined
     */
    public ProductLabel getLabel() {
        return label;
    }

    /**
     * @param label product label or {@code null}
     */
    public void setLabel(ProductLabel label) {
        this.label = label;
    }

    /**
     * @return product label note or {@code null} if product label note is not defined
     */
    public String getLabelNote() {
        return labelNote;
    }

    /**
     * @param labelNote product label note or {@code null}
     */
    public void setLabelNote(String labelNote) {
        this.labelNote = labelNote;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public String getSizeText() {
        return sizeText;
    }

    public void setSizeText(String sizeText) {
        this.sizeText = sizeText;
    }

    public Image getPreview() {
        return preview;
    }

    public void setPreview(Image preview) {
        this.preview = preview;
    }

    public List<Image> getViews() {
        return views;
    }

    public void setViews(List<Image> views) {
        this.views = views;
    }

    public Set<Color> getColors() {
        return colors;
    }

    public void setColors(Set<Color> colors) {
        this.colors = colors;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public void addCategory(Category category) {
        categories.add(category);
    }

    public Set<ProductKeyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<ProductKeyword> keywords) {
        this.keywords = keywords;
    }

    public void addKeyword(ProductKeyword keyword) {
        if (keywords.contains(keyword)) {
            throw new IllegalArgumentException("Product already has keyword: " + keyword);
        }
        keywords.add(keyword);
    }

    public void removeKeyword(ProductKeyword keyword) {
        if (!keywords.contains(keyword)) {
            throw new IllegalArgumentException("Product hasn't keyword: " + keyword);
        }
        keywords.remove(keyword);
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;

        Product product = (Product) o;
        return ObjectUtils.equalsIds(this, product);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("id", id)
                .add("livemasterId", livemasterId)
                .add("type", type)
                .add("name", name)
                .add("description", description)
                .add("visible", visible)
                .add("price", price)
                .add("status", status)
                .add("size", size)
                .add("preview", preview)
                .add("views", views)
                .add("colors", colors)
                .add("categories", categories)
                .addObjects("keywords", keywords)
                .add("modifyTime", modifyTime)
                .add("index", index);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
