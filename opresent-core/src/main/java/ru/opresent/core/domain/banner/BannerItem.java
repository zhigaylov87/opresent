package ru.opresent.core.domain.banner;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.OrderedEntity;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.ImageUseConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 07.05.15
 * Time: 23:29
 */
public class BannerItem implements Identifiable, OrderedEntity, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotBlank(message = "{BannerItem.name.empty}")
    @Size(max = EntitiesConstraints.BANNER_ITEM_NAME_MAX_LENGTH)
    private String name;

    @NotNull(message = "{BannerItem.normalImage.empty}")
    @ImageUseConstraint(ImageUse.BANNER_ITEM_NORMAL_IMAGE)
    private Image normalImage;

    @NotNull(message = "{BannerItem.mobileImage.empty}")
    @ImageUseConstraint(ImageUse.BANNER_ITEM_MOBILE_IMAGE)
    private Image mobileImage;

    /**
     * May be {@code null}
     */
    @Size(max = EntitiesConstraints.BANNER_ITEM_URL_MAX_LENGTH)
    private String url;

    private int index;

    private boolean visible;

    public BannerItem() {
    }

    public BannerItem(long id) {
        this.id = id;
    }

    public BannerItem(BannerItem item) {
        this.id = item.id;
        this.name = item.name;
        this.normalImage = item.normalImage;
        this.mobileImage = item.mobileImage;
        this.url = item.url;
        this.index = item.index;
        this.visible = item.visible;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getNormalImage() {
        return normalImage;
    }

    public void setNormalImage(Image normalImage) {
        this.normalImage = normalImage;
    }

    public Image getMobileImage() {
        return mobileImage;
    }

    public void setMobileImage(Image mobileImage) {
        this.mobileImage = mobileImage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BannerItem)) return false;

        BannerItem item = (BannerItem) o;
        return ObjectUtils.equalsIds(this, item);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "BannerItem {" +
                "id: " + id +
                ", name: " + FormatUtils.quotes(name) +
                ", normalImage: " + normalImage +
                ", mobileImage: " + mobileImage +
                ", url: " + FormatUtils.quotes(url) +
                ", index: " + index +
                ", visible: " + visible +
                '}';
    }
}
