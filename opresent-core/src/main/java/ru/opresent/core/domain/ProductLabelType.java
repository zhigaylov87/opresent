package ru.opresent.core.domain;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 15:59
 */
public enum ProductLabelType implements Codeable {

    ACTION(0),
    DISCOUNT(1),
    NEW(2);

    private final int code;

    private ProductLabelType(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static ProductLabelType valueOfCode(int code) {
        for (ProductLabelType type : values()) {
            if (type.code == code) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unknown product label type code: " + code);
    }
}
