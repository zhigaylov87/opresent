package ru.opresent.core.domain.message.incoming;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.IncomingMessageConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * User: artem
 * Date: 22.11.14
 * Time: 16:08
 */
@IncomingMessageConstraint
public class IncomingMessage implements Stringifiable, Identifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotNull(message = "{IncomingMessage.subjectType.empty}")
    private IncomingMessageSubjectType subjectType;

    @NotNull(message = "{IncomingMessage.subjectId.empty}")
    private Long subjectId;

    @NotBlank(message = "{IncomingMessage.customerName.empty}")
    @Size(max = EntitiesConstraints.INCOMING_MESSAGE_CUSTOMER_NAME_MAX_LENGTH, message = "{IncomingMessage.customerName.veryLong}")
    private String customerName;

    @NotBlank(message = "{IncomingMessage.customerEmailOrPhone.empty}")
    @Size(max = EntitiesConstraints.INCOMING_MESSAGE_CUSTOMER_EMAIL_OR_PHONE_MAX_LENGTH, message = "{IncomingMessage.customerEmailOrPhone.veryLong}")
    private String customerEmailOrPhone;

    @NotBlank(message = "{IncomingMessage.messageText.empty}")
    @Size(max = EntitiesConstraints.INCOMING_MESSAGE_MESSAGE_TEXT_MAX_LENGTH, message = "{IncomingMessage.messageText.veryLong}")
    private String messageText;

    private boolean readed = false;

    // TODO: Наверно здесь @NotNull должен быть
    private Date creationTime;

    private Date readingTime;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public IncomingMessageSubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(IncomingMessageSubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmailOrPhone() {
        return customerEmailOrPhone;
    }

    public void setCustomerEmailOrPhone(String customerEmailOrPhone) {
        this.customerEmailOrPhone = customerEmailOrPhone;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getReadingTime() {
        return readingTime;
    }

    public void setReadingTime(Date readingTime) {
        this.readingTime = readingTime;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("id", id)
                .add("subjectType", subjectType)
                .add("subjectId", subjectId)
                .add("customerName", customerName)
                .add("customerEmailOrPhone", customerEmailOrPhone)
                .add("messageText", messageText)
                .add("creationTime", creationTime)
                .add("readingTime", readingTime);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}