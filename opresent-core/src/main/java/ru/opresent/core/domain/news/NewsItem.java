package ru.opresent.core.domain.news;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.OrderedEntity;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.validation.EntitiesConstraints;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * User: artem
 * Date: 07.05.15
 * Time: 23:29
 */
public class NewsItem implements Identifiable, OrderedEntity, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotNull(message = "{NewsItem.date.empty}")
    private Date date;

    @NotBlank(message = "{NewsItem.text.empty}")
    @Size(max = EntitiesConstraints.NEWS_ITEM_TEXT_MAX_LENGTH)
    private String text;

    private int index;

    private boolean visible;

    public NewsItem() {
    }

    public NewsItem(long id) {
        this.id = id;
    }

    public NewsItem(NewsItem item) {
        this.id = item.id;
        this.date = item.date;
        this.text = item.text;
        this.index = item.index;
        this.visible = item.visible;
    }

    @Override
    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsItem)) return false;

        NewsItem item = (NewsItem) o;
        return ObjectUtils.equalsIds(this, item);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "NewsItem {" +
                "id: " + id +
                ", date: " + date +
                ", text: " + FormatUtils.quotes(text) +
                ", index: " + index +
                ", visible: " + visible +
                '}';
    }
}
