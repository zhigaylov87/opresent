package ru.opresent.core.domain;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.EntityName;
import ru.opresent.core.validation.constraint.ImageUseConstraint;
import ru.opresent.core.validation.constraint.ProductTypeConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 09.03.14
 * Time: 15:04
 */
@ProductTypeConstraint
public class ProductType implements Identifiable, NamedEntity, OrderedEntity, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;
    @EntityName
    private String entityName;

    @NotBlank(message = "{ProductType.name.empty}")
    @Size(max = EntitiesConstraints.PRODUCT_TYPE_NAME_MAX_LENGTH)
    private String name;

    @NotNull(message = "{ProductType.image.empty}")
    @ImageUseConstraint(ImageUse.PRODUCT_TYPE_IMAGE)
    private Image image;

    @Size(max = EntitiesConstraints.PRODUCT_TYPE_DESCRIPTION_MAX_LENGTH)
    private String description;

    private int index;

    public ProductType() {
    }

    public ProductType(long id) {
        this.id = id;
    }

    public ProductType(ProductType productType) {
        this.id = productType.id;
        this.entityName = productType.entityName;
        this.name = productType.name;
        this.image = productType.image;
        this.description = productType.description;
        this.index = productType.index;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductType)) return false;

        ProductType that = (ProductType) o;
        return ObjectUtils.equalsIds(this, that);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "ProductType {" +
                "id: " + id +
                ", index: " + index +
                ", entityName: " + FormatUtils.quotes(entityName) +
                ", name: " + FormatUtils.quotes(name) +
                "}";
    }
}
