package ru.opresent.core.domain.order;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 01.02.15
 * Time: 23:26
 */
public enum DeliveryWay implements Codeable {

    SELF_EXPORT(0, false, false),
    COURIER_TO_METRO_STATION(1, false, false),
    COURIER_TO_ADDRESS_INSIDE_MKAD(2, false, true),
    COURIER_OUTSIDE_MKAD(3, false, true),
    RUSSIAN_POST(4, true, true),
    COURIER_EXPRESS_SERVICE(5, true, true);

    private final int code;
    private final boolean postcodeRequired;
    private final boolean addressRequired;

    DeliveryWay(int code, boolean postcodeRequired, boolean addressRequired) {
        this.code = code;
        this.postcodeRequired = postcodeRequired;
        this.addressRequired = addressRequired;
    }

    @Override
    public int getCode() {
        return code;
    }

    public boolean isPostcodeRequired() {
        return postcodeRequired;
    }

    public boolean isAddressRequired() {
        return addressRequired;
    }

    public static DeliveryWay valueOf(int code) {
        for (DeliveryWay way : values()) {
            if (way.code == code) {
                return way;
            }
        }
        throw new IllegalArgumentException("Unknown delivery way code: " + code);
    }
}
