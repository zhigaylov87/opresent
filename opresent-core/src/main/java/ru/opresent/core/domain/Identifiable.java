package ru.opresent.core.domain;

/**
 * User: artem
 * Date: 01.03.14
 * Time: 14:21
 */
public interface Identifiable {

    public static final long NOT_SAVED_ID = -1;

    /**
     * @return entity id or {@link #NOT_SAVED_ID} if entity is not saved in database
     */
    public long getId();

}
