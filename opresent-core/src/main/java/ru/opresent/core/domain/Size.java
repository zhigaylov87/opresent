package ru.opresent.core.domain;

import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.validation.constraint.SizeConstraint;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * User: artem
 * Date: 01.03.14
 * Time: 15:11
 */
@SizeConstraint
public class Size implements Identifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;
    @NotNull(message = "{Size.width.empty}")
    private Integer width;
    @NotNull(message = "{Size.height.empty}")
    private Integer height;

    public Size() {
    }

    public Size(long id) {
        this.id = id;
    }

    public Size(Size size) {
        this.id = size.id;
        this.width = size.width;
        this.height = size.height;
    }

    @Override
    public long getId() {
        return id;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return width x height, for example: <b>15 x 10</b>
     */
    public String getDescription() {
        return width + " x " + height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Size)) return false;

        Size size = (Size) o;
        return ObjectUtils.equalsIds(this, size);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Size {" +
                "id: " + id +
                ", width: " + width +
                ", height: " + height +
                "}";
    }
}
