package ru.opresent.core.domain;

import java.io.Serializable;
import java.util.Map;

/**
 * User: artem
 * Date: 04.05.14
 * Time: 17:02
 */
public class CategoryProductsCountStatistic implements Serializable {

    private Map<Long, Integer> categoryIdToProductsCount;

    public CategoryProductsCountStatistic(Map<Long, Integer> categoryIdToProductsCount) {
        this.categoryIdToProductsCount = categoryIdToProductsCount;
    }

    public int getProductsCount(Category category) {
        Integer productsCount = categoryIdToProductsCount.get(category.getId());
        return productsCount == null ? 0 : productsCount;
    }
}