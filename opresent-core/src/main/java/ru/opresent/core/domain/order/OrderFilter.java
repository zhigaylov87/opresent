package ru.opresent.core.domain.order;

/**
 * User: artem
 * Date: 14.02.15
 * Time: 15:41
 */
public interface OrderFilter {

    /**
     * @return {@code null} for any id order or not {@code null} for concrete
     */
    public Long getOrderId();

    /**
     * @return {@code null} for any order status or not {@code null} for concrete
     */
    public OrderStatus getStatus();

    /**
     * @return {@code null} for any customer full name or not {@code null} for "has ignore case" condition
     */
    public String getCustomerFullName();

}
