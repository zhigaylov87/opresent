package ru.opresent.core.domain.section;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 2:14
 */
public enum SectionType implements Codeable {
    TEXT(0),
    LIST(1),
    CONTACTS(2),
    CATALOG(3),
    DELIVERY(4);

    private final int code;

    private SectionType(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static SectionType valueOfCode(int code) {
        for (SectionType type : values()) {
            if (type.code == code) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unknown section type code: " + code);
    }
}
