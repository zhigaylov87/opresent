package ru.opresent.core.domain;

/**
 * User: artem
 * Date: 09.05.15
 * Time: 21:55
 */
public interface OrderedEntity extends Identifiable {

    int START_INDEX = 0;
    int NON_EXISTENT_INDEX = -1;

    int getIndex();

}
