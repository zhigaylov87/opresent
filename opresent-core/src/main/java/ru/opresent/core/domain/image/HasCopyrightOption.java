package ru.opresent.core.domain.image;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 04.11.15
 * Time: 14:08
 */
public enum HasCopyrightOption implements Codeable {

    INHERIT_FROM_IMAGE_USE(0),
    NO(1);

    private final int code;

    HasCopyrightOption(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public static HasCopyrightOption valueOf(int code) {
        for (HasCopyrightOption option : values()) {
            if (option.code == code) {
                return option;
            }
        }

        throw new IllegalArgumentException("There is no HasCopyrightOption with code = " + code);
    }
}
