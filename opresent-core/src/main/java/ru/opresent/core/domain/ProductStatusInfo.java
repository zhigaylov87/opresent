package ru.opresent.core.domain;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.validation.EntitiesConstraints;

import java.io.Serializable;
import java.util.Objects;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 14:55
 */
public class ProductStatusInfo implements Identifiable, Comparable<ProductStatusInfo>, Serializable {

    private ProductStatus status;

    @NotBlank(message = "{ProductStatusInfo.defaultNote.empty}")
    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_STATUS_NOTE_MAX_LENGTH)
    private String defaultNote;

    public ProductStatusInfo(ProductStatus status) {
        this.status = Objects.requireNonNull(status);
    }

    public ProductStatusInfo(ProductStatusInfo info) {
        this.status = info.getStatus();
        this.defaultNote = info.getDefaultNote();
    }

    @Override
    public long getId() {
        return status.getCode();
    }

    public ProductStatus getStatus() {
        return status;
    }

    public String getDefaultNote() {
        return defaultNote;
    }

    public void setDefaultNote(String defaultNote) {
        this.defaultNote = defaultNote;
    }

    @Override
    public int compareTo(ProductStatusInfo other) {
        return status.compareTo(other.getStatus());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductStatusInfo)) return false;

        ProductStatusInfo that = (ProductStatusInfo) o;

        return status == that.status;
    }

    @Override
    public int hashCode() {
        return status.hashCode();
    }

    @Override
    public String toString() {
        return "ProductStatusInfo {" +
                "status: " + status +
                ", defaultNote: " + FormatUtils.quotes(defaultNote) +
                "}";
    }
}
