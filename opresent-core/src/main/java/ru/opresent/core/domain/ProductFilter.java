package ru.opresent.core.domain;

import ru.opresent.core.util.stringify.Stringifiable;

import java.util.Set;

/**
 * User: artem
 * Date: 06.01.14
 * Time: 14:06
 */
public interface ProductFilter extends Stringifiable {

    /**
     * @return {@code null} for all or concrete product type for filtering
     */
    public ProductType getProductType();

    public String getName();

    public String getDescription();

    /**
     * @return {@code null} for all or concrete value for filtering
     */
    public Boolean getVisible();

    /**
     * @return {@code null} or min price in rubles
     */
    public Integer getMinPrice();

    /**
     * @return {@code null} or max price in rubles
     */
    public Integer getMaxPrice();

    /**
     * @return {@code null} for all or concrete {@link ProductStatus} for filtering
     */
    public ProductStatus getStatus();

    /**
     * @return {@code null} for all or set of concrete {@link ProductStatus}es for filtering
     */
    public Set<ProductStatus> getStatuses();

    /**
     * @return {@code null} for all or concrete size for filtering
     */
    public Size getSize();

    /**
     * @return {@code null} or empty set for all or concrete colors for filtering
     */
    public Set<Color> getColors();

    /**
     * @return {@code null} for all or concrete category for filtering
     */
    public Category getCategory();

    /**
     * @return {@code null} for no exclusions or concrete product ids wich must be excluded
     */
    public Set<Long> getExcludeProductIds();

}
