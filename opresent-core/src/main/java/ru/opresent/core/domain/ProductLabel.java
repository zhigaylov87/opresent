package ru.opresent.core.domain;


import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.core.validation.constraint.ImageUseConstraint;
import ru.opresent.core.validation.constraint.ProductLabelConstraint;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * TODO: Выпилить метку к товару, т.к. по сути она нигде не используется и нигде не планируется её использовать.
 * Либо всё-таки сделать использование этой фичи.
 *
 * User: artem
 * Date: 25.05.14
 * Time: 15:59
 */
@ProductLabelConstraint
public class ProductLabel implements Identifiable, Serializable {

    public static final int MIN_DISCOUNT_PERCENT = 1;
    public static final int MAX_DISCOUNT_PERCENT = 100;

    private long id = Identifiable.NOT_SAVED_ID;

    @NotNull(message = "{ProductLabel.type.empty}")
    private ProductLabelType type;

    @NotNull(message = "{ProductLabel.image.empty}")
    @ImageUseConstraint(ImageUse.PRODUCT_LABEL_IMAGE)
    private Image image;

    @NotBlank(message = "{ProductLabel.defaultNote.empty}")
    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_LABEL_NOTE_MAX_LENGTH)
    private String defaultNote;

    private boolean withDiscount = false;

    @Min(value = MIN_DISCOUNT_PERCENT, message = "{ProductLabel.discountPercent.lessThanMin}")
    @Max(value = MAX_DISCOUNT_PERCENT, message = "{ProductLabel.discountPercent.greaterThanMax}")
    private Integer discountPercent;

    public ProductLabel() {
    }

    public ProductLabel(long id) {
        this.id = id;
    }

    public ProductLabel(ProductLabel label) {
        this.id = label.getId();
        this.type = label.getType();
        this.image = label.getImage();
        this.defaultNote = label.getDefaultNote();
        this.withDiscount = label.isWithDiscount();
        this.discountPercent = label.getDiscountPercent();
    }

    public long getId() {
        return id;
    }

    public ProductLabelType getType() {
        return type;
    }

    public void setType(ProductLabelType type) {
        this.type = type;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getDefaultNote() {
        return defaultNote;
    }

    public void setDefaultNote(String defaultNote) {
        this.defaultNote = defaultNote;
    }

    public boolean isWithDiscount() {
        return withDiscount;
    }

    public void setWithDiscount(boolean withDiscount) {
        this.withDiscount = withDiscount;
    }

    public Integer getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Integer discountPercent) {
        this.discountPercent = discountPercent;
    }

    @Override
    public String toString() {
        return "ProductLabel {" +
                "id: " + id +
                ", type: " + type +
                ", image: " + image +
                ", defaultNote: " + FormatUtils.quotes(defaultNote) +
                ", withDiscount: " + withDiscount +
                ", discountPercent: " + discountPercent +
                "}";
    }
}
