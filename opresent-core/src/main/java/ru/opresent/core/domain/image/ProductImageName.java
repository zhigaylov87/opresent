package ru.opresent.core.domain.image;

import ru.opresent.core.domain.Product;

/**
 * User: artem
 * Date: 11.10.15
 * Time: 23:39
 */
public class ProductImageName implements ImageName {

    private Product product;

    public ProductImageName(Product product) {
        this.product = product;
    }

    @Override
    public String getName() {
        return product.getName();
    }
}
