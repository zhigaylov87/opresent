package ru.opresent.core.domain.order;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.util.FormatUtils;
import ru.opresent.core.validation.EntitiesConstraints;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

/**
 * User: artem
 * Date: 19.02.15
 * Time: 2:45
 */
public class DeliveryWayInfo implements Identifiable, Comparable<DeliveryWayInfo>, Serializable {

    private AddressType addressType;
    private DeliveryWay deliveryWay;

    @NotBlank(message = "{DeliveryWayInfo.price.empty}")
    @Size(max = EntitiesConstraints.DELIVERY_WAY_PRICE_MAX_LENGTH)
    private String price;

    @NotBlank(message = "{DeliveryWayInfo.note.empty}")
    @Size(max = EntitiesConstraints.DELIVERY_WAY_NOTE_MAX_LENGTH)
    private String note;

    public DeliveryWayInfo(AddressType addressType, DeliveryWay deliveryWay) {
        this.addressType = Objects.requireNonNull(addressType);
        this.deliveryWay = Objects.requireNonNull(deliveryWay);
        if (!this.addressType.getDeliveryWays().contains(deliveryWay)) {
            throw new IllegalArgumentException("addressType: " + addressType + ", deliveryWay: " + deliveryWay);
        }
    }

    public DeliveryWayInfo(DeliveryWayInfo info) {
        this.addressType = info.addressType;
        this.deliveryWay = info.deliveryWay;
        this.price = info.price;
        this.note = info.note;
    }

    @Override
    public long getId() {
        return (addressType.getCode() + 1) *
                BigInteger.TEN.pow(Integer.toString(DeliveryWay.values().length).length()).longValue() +
                (deliveryWay.getCode() + 1);
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public DeliveryWay getDeliveryWay() {
        return deliveryWay;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int compareTo(DeliveryWayInfo other) {
        int result = addressType.compareTo(other.addressType);
        return result != 0 ? result : deliveryWay.compareTo(other.getDeliveryWay());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeliveryWayInfo)) return false;

        DeliveryWayInfo that = (DeliveryWayInfo) o;

        return addressType == that.addressType && deliveryWay == that.deliveryWay;
    }

    @Override
    public int hashCode() {
        int result = addressType.hashCode();
        result = 31 * result + deliveryWay.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DeliveryWayInfo {" +
                "addressType: " + addressType +
                ", deliveryWay: " + deliveryWay +
                ", price: " + FormatUtils.quotes(price) +
                ", note: " + FormatUtils.quotes(note) +
                "}";
    }
}
