package ru.opresent.core.domain;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;

import java.io.Serializable;
import java.util.Objects;

/**
 * Product keyword.<br/>
 *
 * Keyword string in constructor {@link #ProductKeyword(String)} will be processed:
 * <ul>
 * <li>trimmed by {@link String#trim()}</li>
 * <li>convers to lowercase by {@link String#toLowerCase()}</li>
 * <li>replace russian <b>ё</b> to russian <b>е</b></li>
 * </ul>
 *
 * User: artem
 * Date: 18.12.14
 * Time: 17:02
 */
public class ProductKeyword implements Stringifiable, Serializable, Comparable<ProductKeyword> {

    @NotBlank
    @javax.validation.constraints.Size(max = EntitiesConstraints.PRODUCT_KEYWORD_MAX_LENGTH)
    private String keyword;

    public ProductKeyword(String keyword) {
        this.keyword = Objects.requireNonNull(keyword)
                .trim()
                .toLowerCase()
                .replace('ё', 'е');

        if (this.keyword.isEmpty()) {
            throw new IllegalArgumentException("Keyword is empty: '" + keyword + "'");
        }
    }

    /**
     * @return trimmed and lowercase keyword
     */
    public String getKeyword() {
        return keyword;
    }

    @Override
    public int compareTo(ProductKeyword other) {
        return keyword.compareTo(other.getKeyword());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductKeyword)) return false;

        ProductKeyword that = (ProductKeyword) o;

        return keyword.equals(that.keyword);
    }

    @Override
    public int hashCode() {
        return keyword.hashCode();
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("keyword", keyword);
    }

    @Override
    public String toString() {
        return keyword;
    }
}
