package ru.opresent.core.domain.image;

import org.hibernate.validator.constraints.NotBlank;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.util.ObjectUtils;
import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.core.validation.EntitiesConstraints;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 01.03.14
 * Time: 17:19
 */
public class Image implements Stringifiable, Identifiable, Serializable {

    private long id = Identifiable.NOT_SAVED_ID;

    @NotBlank
    @Size(max = EntitiesConstraints.IMAGE_FORMAT_MAX_LENGTH)
    private String format;

    @NotNull
    private ImageUse imageUse;

    @NotNull
    private HasCopyrightOption hasCopyrightOption;

    private ImageName imageName;

    private byte[] content;

    public Image(String format, ImageUse imageUse, HasCopyrightOption hasCopyrightOption) {
        this(Identifiable.NOT_SAVED_ID, format, imageUse, hasCopyrightOption);
    }

    public Image(long id, String format, ImageUse imageUse, HasCopyrightOption hasCopyrightOption) {
        this.id = id;
        this.format = format;
        this.imageUse = imageUse;
        this.hasCopyrightOption = hasCopyrightOption;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFormat() {
        return format;
    }

    public ImageUse getImageUse() {
        return imageUse;
    }

    public HasCopyrightOption getHasCopyrightOption() {
        return hasCopyrightOption;
    }

    /**
     * May be {@code null} if image name not defined
     * @return image name entity
     */
    public ImageName getImageName() {
        return imageName;
    }

    public void setImageName(ImageName imageName) {
        this.imageName = imageName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Image)) return false;

        Image image = (Image) o;
        return ObjectUtils.equalsIds(this, image);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("id", id)
                .add("format", format)
                .add("imageUse", imageUse)
                .add("hasCopyrightOption", hasCopyrightOption)
                .add("imageName", imageName == null ? null : imageName.getName())
                .add("content(bytes)", content == null ? null : content.length);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
