package ru.opresent.core.domain.catalog;

import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.util.stringify.Stringifiable;

import java.io.Serializable;
import java.util.Set;

/**
 * User: artem
 * Date: 30.10.14
 * Time: 2:12
 */
public interface Catalog extends Stringifiable, Serializable {

    public Set<ProductType> getProductTypes();

    public Set<Category> getProductTypeCategories(ProductType productType);

    public Set<Category> getAllCategories();

}
