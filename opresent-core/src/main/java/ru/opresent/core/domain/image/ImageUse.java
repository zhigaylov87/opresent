package ru.opresent.core.domain.image;

import ru.opresent.core.domain.Codeable;

/**
 * User: artem
 * Date: 01.11.14
 * Time: 20:00
 */
public enum ImageUse implements Codeable {

    SECTION_ITEM_PREVIEW_IMAGE(0, false),
    PRODUCT_TYPE_IMAGE(1, false),
    PRODUCT_LABEL_IMAGE(2, false),
    PRODUCT_PREVIEW_IMAGE(3, true),
    PRODUCT_VIEW_IMAGE(4, true),
    RICH_TEXT_IMAGE(5, true),
    BANNER_ITEM_NORMAL_IMAGE(6, false),
    BANNER_ITEM_MOBILE_IMAGE(7, false);

    private final int code;
    private final boolean hasCopyright;

    ImageUse(int code, boolean hasCopyright) {
        this.code = code;
        this.hasCopyright = hasCopyright;
    }

    @Override
    public int getCode() {
        return code;
    }

    public boolean hasCopyright() {
        return hasCopyright;
    }

    public static ImageUse valueOf(int code) {
        for (ImageUse use : values()) {
            if (use.code == code) {
                return use;
            }
        }

        throw new IllegalArgumentException("There is no ImageUse with code = " + code);
    }
}
