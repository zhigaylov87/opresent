package ru.opresent.core.domain;

/**
 * User: artem
 * Date: 19.04.16
 * Time: 2:07
 */
public interface GroupedOrderedEntity<T extends Identifiable> extends OrderedEntity {

    T getGroupByEntity();

}
