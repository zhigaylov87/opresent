package ru.opresent.core.domain.image;

import java.io.Serializable;

/**
 * User: artem
 * Date: 10.10.15
 * Time: 1:53
 */
public interface ImageName extends Serializable {

    public String getName();

}
