package ru.opresent.core.domain.image;

/**
 * User: artem
 * Date: 12.10.15
 * Time: 13:36
 */
public class FixedImageName implements ImageName {

    private String name;

    public FixedImageName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
