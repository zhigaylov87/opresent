package ru.opresent.core.util.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * User: artem
 * Date: 01.05.15
 * Time: 0:22
 */
@Component("logFactory")
public class LogFactory {

    public static final Logger APP_LOG = LoggerFactory.getLogger("market-app");

    public static final Logger CMS_LOG = LoggerFactory.getLogger("market-cms");

    public Logger getAppLog() {
        return APP_LOG;
    }

    public Logger getCmsLog() {
        return CMS_LOG;
    }

}
