package ru.opresent.core.util;

import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.NamedEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * User: artem
 * Date: 14.03.15
 * Time: 6:37
 */
public class IdentifiableNamedEntityList<T extends Identifiable & NamedEntity> extends IdentifiableList<T> {

    private Map<String, T> entityNameToElement = new HashMap<>();

    public IdentifiableNamedEntityList(Collection<T> all) {
        super(all);
        for (T element : getAll()) {
            entityNameToElement.put(element.getEntityName(), element);
        }
    }

    /**
     * @param entityName entity name
     * @return element with defined entity name
     * @throws NullPointerException if entity name is {@code null)
     * @throws IllegalArgumentException if element with defined entity name not found in list
     */
    public T getByEntityName(String entityName) throws NullPointerException, IllegalArgumentException {
        Objects.requireNonNull(entityName, "Entity name is null!");
        T element = entityNameToElement.get(entityName);
        if (element == null) {
            throw new IllegalArgumentException("Element with entity name = '" + entityName + "' not found!");
        }
        return element;
    }

    public boolean containsByEntityName(String entityName) {
        return entityNameToElement.get(entityName) != null;
    }
}
