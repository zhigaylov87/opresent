package ru.opresent.core.util;

import java.util.Locale;

/**
 * User: artem
 * Date: 20.10.15
 * Time: 4:03
 */
public final class LocaleUtils {

    private LocaleUtils() {
    }

    public static final Locale RUSSIAN = new Locale("ru", "RU");
}
