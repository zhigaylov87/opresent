package ru.opresent.core.util;

import java.util.HashSet;
import java.util.Set;

/**
 * User: artem
 * Date: 15.03.15
 * Time: 14:01
 */
public class SetValueMap<K, V> extends CollectionValueMap<Set<V>, K, V> {

    @Override
    protected Set<V> newCollection() {
        return new HashSet<>();
    }
}
