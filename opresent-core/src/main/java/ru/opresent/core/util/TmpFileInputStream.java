package ru.opresent.core.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Temp file will be force deteted on input stream closing
 * @see java.io.InputStream#close()
 * @see org.apache.commons.io.FileUtils#forceDelete(java.io.File)
 *
 * User: artem
 * Date: 23.01.16
 * Time: 15:00
 */
public class TmpFileInputStream extends FileInputStream {

    private File tmpFile;

    public TmpFileInputStream(File tmpFile) throws FileNotFoundException {
        super(tmpFile);
        this.tmpFile = tmpFile;
    }

    @Override
    public void close() throws IOException {
        super.close();
        FileUtils.forceDelete(tmpFile);
    }
}
