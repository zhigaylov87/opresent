package ru.opresent.core.util.stringify;

/**
 * User: artem
 * Date: 27.03.16
 * Time: 22:44
 */
public interface Stringifiable {

    EntityStringifier stringify(boolean fully);

    String toString();

}
