package ru.opresent.core.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.opresent.core.config.ServerConfig;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.pagination.PageSettings;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 31.08.14
 * Time: 14:35
 */
@Component("appNavigationManager")
public class AppNavigationManager {

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    public String getWelcomeUrl() {
        return "/";
    }

    public String getCatalogUrl() {
        return "/" + Section.CATALOG_SECTION_ENTITY_NAME;
    }

    public String getCatalogUrl(ProductType productType) {
        return getCatalogUrl(productType, null);
    }

    public String getCatalogUrl(ProductType productType, Category category) {
        return getCatalogUrl(productType, category, null);
    }

    public String getCatalogUrl(ProductType productType, Category category, ProductStatus status) {
        return getCatalogUrl(productType, category, status, PageSettings.FIRST_PAGE_NUMBER);
    }

    public String getCatalogUrl(ProductType productType, Category category, ProductStatus status, int pageNumber) {
        StringBuilder url = new StringBuilder(getCatalogUrl());

        if (productType != null) {
            url.append("/").append(productType.getEntityName());
        }
        if (category != null) {
            url.append("/").append(category.getEntityName());
        }
        if (status != null) {
            url.append("/").append(status.getEntityName());
        }
        if (pageNumber != PageSettings.FIRST_PAGE_NUMBER) {
            url.append("/").append(pageNumber);
        }

        return url.toString();
    }

    public String getSectionUrl(Section section) {
        return "/" + section.getEntityName();
    }

    public String getSectionItemUrl(SectionItem sectionItem) {
        return getSectionUrl(sectionItem.getSection()) + "/" + UrlUtils.createNamedUrl(sectionItem.getId(),  sectionItem.getName());
    }

    public String getProductUrl(Product product) {
        return "/product/" + UrlUtils.createNamedUrl(product.getId(), product.getName());
    }

    public String getCartUrl() {
        return "/cart";
    }

    public String getCartRemoveUrl() {
        return "/cart/remove";
    }

    public String getNavCartAddUrl() {
        return "/navcart/add";
    }

    public String getNavCartRemoveUrl() {
        return "/navcart/remove";
    }

    public String getOrderUrl() {
        return "/order";
    }

    public String getOrderAddUrl() {
        return "/order/add";
    }

    public String getOrderAddPersonalUrl() {
        return "/order/add-personal";
    }

    public String getSearchUrl() {
        return "/search";
    }

    public String getSearchUrl(String query) {
        return getSearchUrl() + "/" + query;
//        try {
//            return getSearchUrl() + "/" + URLEncoder.encode(query, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            throw new CoreException(e);
//        }
    }

    public String getCatalogReferer(String referer) {
        if (StringUtils.isBlank(referer) || !referer.startsWith(getAbsolutCatalogUrl())) {
            return getCatalogUrl();
        }
        return referer.substring(serverConfig.getHost().length(), referer.length());
    }

    public String getIncomingMessageAddUrl() {
        return "/message/incoming/add";
    }

    // absolut urls

    public String getAbsolutWelcomeUrl() {
        return getAbsolutUrl(getWelcomeUrl());
    }

    public String getAbsolutCatalogUrl() {
        return getAbsolutUrl(getCatalogUrl());
    }

    public String getAbsolutCatalogUrl(ProductType productType) {
        return getAbsolutUrl(getCatalogUrl(productType, null, null));
    }

    public String getAbsolutCatalogUrl(ProductType productType, Category category) {
        return getAbsolutUrl(getCatalogUrl(productType, category, null, PageSettings.FIRST_PAGE_NUMBER));
    }

    public String getAbsoluteSectionUrl(Section section) {
        return getAbsolutUrl(getSectionUrl(section));
    }

    public String getAbsoluteSectionItemUrl(SectionItem sectionItem) {
        return getAbsolutUrl(getSectionItemUrl(sectionItem));
    }

    public String getAbsolutProductUrl(Product product) {
        return getAbsolutUrl(getProductUrl(product));
    }

    public String getAbsoluteSearchUrl(String query) {
        return getAbsolutUrl(getSearchUrl(query));
    }

    private String getAbsolutUrl(String relativeUrl) {
        return serverConfig.getHost() + relativeUrl;
    }
}
