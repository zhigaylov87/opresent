package ru.opresent.core.util.stringify;

import org.apache.commons.lang3.StringUtils;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.util.CollectionUtils;
import ru.opresent.core.util.FormatUtils;

import java.util.Collection;
import java.util.Date;

/**
 * User: artem
 * Date: 27.03.16
 * Time: 21:39
 */
public class EntityStringifier {

    private StringBuilder sb;
    private boolean full;
    private boolean firstField = true;

    public EntityStringifier() {
        this(false);
    }

    public EntityStringifier(boolean full) {
        this.sb = new StringBuilder("{");
        this.full = full;
    }

    public EntityStringifier add(String field, Number value) {
        return addFieldValue(field, value == null ? null : value.toString());
    }

    public EntityStringifier add(String field, String value) {
        return addFieldValue(field, FormatUtils.quotes(value));
    }

    public EntityStringifier add(String field, Boolean value) {
        return addFieldValue(field, value == null ? null : value.toString());
    }

    public EntityStringifier add(String field, Date value) {
        return addFieldValue(field, value == null ? null : FormatUtils.formatDateTime(value));
    }

    public EntityStringifier add(String field, Identifiable value) {
        return addFieldValue(field, value == null ? null : Long.toString(value.getId()));
    }

    public EntityStringifier addObjects(String field, Collection<?> value) {
        return addFieldValue(field, value == null ? null : toString(value));
    }

    public EntityStringifier add(String field, Collection<? extends Identifiable> value) {
        return addFieldValue(field, value == null ? null : toString(CollectionUtils.getIds(value)));
    }

    public <T extends Enum> EntityStringifier add(String field, T value) {
        return addFieldValue(field, value == null ? null : value.name());
    }

    public EntityStringifier addEnums(String field, Collection<? extends Enum> value) {
        return addFieldValue(field, toString(value));
    }

    private String toString(Collection collection) {
        if (collection == null) {
            return null;
        }
        if (collection.isEmpty()) {
            return full ? "<empty>" : null;
        }
        return StringUtils.join(collection, ";");
    }

    private EntityStringifier addFieldValue(String field, String value) {
        if (value == null && !full) {
            return this;
        }

        if (!firstField) {
            sb.append(", ");
        }
        firstField = false;
        sb.append(field).append(": ").append(value);

        return this;
    }

    @Override
    public String toString() {
        return sb.append("}").toString();
    }
}
