package ru.opresent.core.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * User: artem
 * Date: 28.02.15
 * Time: 3:24
 */
public class ConfiguredObjectMapper extends ObjectMapper {

    public ConfiguredObjectMapper() {
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
        setDateFormat(FormatUtils.getDateFormat());
    }
}
