package ru.opresent.core.util;


import ru.opresent.core.domain.Identifiable;

/**
 * User: artem
 * Date: 23.06.15
 * Time: 4:20
 */
public final class ObjectUtils {

    private ObjectUtils() {
    }

    public static boolean equalsIds(Identifiable a, Identifiable b) {
        return a.getId() != Identifiable.NOT_SAVED_ID &&
                b.getId() != Identifiable.NOT_SAVED_ID &&
                a.getId() == b.getId();
    }
}
