package ru.opresent.core.util;

import org.apache.commons.lang3.time.FastDateFormat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: artem
 * Date: 02.09.14
 * Time: 1:00
 */
public final class FormatUtils {

    private static final int AMOUNT_GROUP_LENGTH = 3;
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    private static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance(DATE_FORMAT_PATTERN);
    private static final FastDateFormat DATE_TIME_FORMAT = FastDateFormat.getInstance("dd.MM.yyyy HH:mm:ss,SSS");

    private FormatUtils() {
    }

    /**
     * Format amount by grouping by three numbers with space as separator.<br/>
     * For expample:<br/>
     * <ul>
     * <li>12</li>
     * <li>123</li>
     * <li>1 234</li>
     * <li>12 345</li>
     * <li>123 456</li>
     * <li>1 234 567</li>
     * </ul>
     * @param amount amount for formatting
     * @return formatted amount
     */
    public static String formatAmount(int amount) {
        return formatAmount(Integer.toString(amount));
    }

    public static String formatAmount(long amount) {
        return formatAmount(Long.toString(amount));
    }

    private static String formatAmount(String amountString) {
        String formattingAmount = "";

        int currentGroupLength = 0;
        for (int i = amountString.length() - 1; i >= 0; i--) {
            if (currentGroupLength < AMOUNT_GROUP_LENGTH) {
                formattingAmount = amountString.charAt(i) + formattingAmount;
                currentGroupLength++;
            } else {
                formattingAmount = amountString.charAt(i) + " " + formattingAmount;
                currentGroupLength = 1;
            }
        }

        return formattingAmount;
    }

    public static String formatDate(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static String formatDateTime(Date date) {
        return DATE_TIME_FORMAT.format(date);
    }

    public static String formatNullableDateTime(Date date) {
        return date == null ? null : formatDateTime(date);
    }

    public static DateFormat getDateFormat() {
        return new SimpleDateFormat(DATE_FORMAT_PATTERN);
    }

    /**
     * @param s string for quoting
     * @return {@code null} if string is {@code null} or single quoting string
     */
    public static String quotes(String s) {
        return s == null ? null : "'" + s + "'";
    }
}
