package ru.opresent.core.util;

import org.springframework.core.io.FileSystemResource;

import java.io.File;

/**
 * User: artem
 * Date: 24.01.16
 * Time: 19:58
 */
public class TmpFileSystemResource extends FileSystemResource {

    public TmpFileSystemResource(File file) {
        super(file);
    }

}
