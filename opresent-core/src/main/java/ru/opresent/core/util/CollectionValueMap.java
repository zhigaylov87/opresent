package ru.opresent.core.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * User: artem
 * Date: 15.03.15
 * Time: 7:32
 */
public abstract class CollectionValueMap<C extends Collection<V>, K, V> extends HashMap<K, C> {

    protected abstract C newCollection();

    public void add(K key, V value) {
        getCollection(key).add(value);
    }

    public void addAll(K key, C values) {
        getCollection(key).addAll(values);
    }

    @Override
    public C put(K key, C value) {
        throw new UnsupportedOperationException("Use add method");
    }

    @Override
    public void putAll(Map<? extends K, ? extends C> m) {
        throw new UnsupportedOperationException("Use add method");
    }

    private C getCollection(K key) {
        C collection = get(key);
        if (collection == null) {
            collection = newCollection();
            super.put(key, collection);
        }
        return collection;
    }
}
