package ru.opresent.core.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class unlike {@link com.google.common.collect.ImmutableMap} supports null values
 * and does not permit null keys like {@link com.google.common.collect.ImmutableMap}
 *
 * User: artem
 * Date: 15.09.14
 * Time: 0:54
 */
public class NullableImmutableMap {

    public static <K, V> Map<K, V> of(K key, V value) {
        return Collections.singletonMap(checkKeyNotNull(key), value);
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2) {
        NullableBuilder<K, V> builder = builder();
        return builder.put(key1, value1).put(key2, value2).build();
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2, K key3, V value3) {
        NullableBuilder<K, V> builder = builder();
        return builder.put(key1, value1).put(key2, value2).put(key3, value3).build();
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2, K key3, V value3, K key4, V value4) {
        NullableBuilder<K, V> builder = builder();
        return builder.put(key1, value1).put(key2, value2).put(key3, value3).put(key4, value4).build();
    }

    public static <K, V> NullableBuilder<K, V> builder() {
        return new NullableBuilder<>();
    }

    private static <T> T checkKeyNotNull(T object) {
        if (object == null) {
            throw new NullPointerException("Null keys not supported");
        }
        return object;
    }

    public static class NullableBuilder<K, V> {

        private Map<K, V> map = new HashMap<>();

        public NullableBuilder<K, V> put(K key, V value) {
            map.put(checkKeyNotNull(key), value);
            return this;
        }

        /**
         * @return immutable map
         */
        public Map<K, V> build() {
            return Collections.unmodifiableMap(map);
        }
    }

}
