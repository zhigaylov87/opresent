package ru.opresent.core.util;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

/**
 *
 * User: artem
 * Date: 07.06.14
 * Time: 23:03
 */
public class EnumHelper {

    private String folder;
    private Set<Class<? extends Enum>> enums;
    private Map<Class<? extends Enum>, ResourceBundle> enumClassToResourceBundle = new HashMap<>();

    @PostConstruct
    private void init() {
        for (Class<? extends Enum> enumClass : enums) {
            enumClassToResourceBundle.put(
                    enumClass,
                    ResourceBundle.getBundle(folder + "." + enumClass.getSimpleName(), LocaleUtils.RUSSIAN));
        }
    }

    public <T extends Enum> String getDescription(T enumElement) {
        return getDescription(enumElement, null);
    }

    public <T extends Enum> String getDescription(T enumElement, String suffix) {
        ResourceBundle resourceBundle = enumClassToResourceBundle.get(enumElement.getClass());
        if (resourceBundle == null) {
            throw new IllegalArgumentException("There is no resource bundle defined for: " + enumElement.getClass().getName());
        }
        return resourceBundle.getString(enumElement.name() + (suffix == null ? "" : suffix));
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public void setEnums(Set<Class<? extends Enum>> enums) {
        this.enums = enums;
    }

}
