package ru.opresent.core.util;

import org.apache.commons.lang3.tuple.Pair;
import ru.opresent.core.domain.Codeable;
import ru.opresent.core.domain.Identifiable;

import java.util.*;

/**
 * User: artem
 * Date: 16.09.14
 * Time: 0:43
 */
public class CollectionUtils {

    public static int sum(Collection<Integer> values) {
        int sum = 0;

        for (int value : values) {
            sum += value;
        }

        return sum;
    }

    public static Set<Long> getIds(Collection<? extends Identifiable> entities) {
        Set<Long> ids = new HashSet<>();
        for (Identifiable entity : entities) {
            ids.add(entity.getId());
        }
        return ids;
    }

    /**
     * @param entities identifiable entities
     * @return string of identifiers, separated by ";" or empty string if entities collection is {@code null} or empty
     */
    public static String getIdsString(Collection<? extends Identifiable> entities) {
        if (entities == null || entities.isEmpty()) {
            return "";
        }
        StringBuilder ids = new StringBuilder();
        for (Identifiable entity : entities) {
            if (ids.length() > 0) {
                ids.append(";");
            }
            ids.append(entity.getId());
        }
        return ids.toString();
    }

    public static <K extends Identifiable, V> Map<Long, V> getIdKeyMap(Map<K, V> map) {
        Map<Long, V> idKeyMap = new HashMap<>();

        for (Map.Entry<K, V> entry : map.entrySet()) {
            idKeyMap.put(entry.getKey().getId(), entry.getValue());
        }

        return idKeyMap;
    }

    public static Set<Integer> getCodes(Collection<? extends Codeable> entities) {
        Set<Integer> codes = new HashSet<>();
        for (Codeable codeable : entities) {
            codes.add(codeable.getCode());
        }
        return codes;
    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static <E> E getSingle(Collection<E> collection) throws IllegalArgumentException{
        switch (collection.size()) {
            case 0:
                throw new IllegalArgumentException("Collection is empty.");

            case 1:
                return collection.iterator().next();

            default:
                throw new IllegalArgumentException("Collection has more than one element.");
        }
    }

    public static <E> E getSingleOrNull(Collection<E> collection) throws IllegalArgumentException{
        switch (collection.size()) {
            case 0:
                return null;

            case 1:
                return collection.iterator().next();

            default:
                throw new IllegalArgumentException("Collection has more than one element.");
        }
    }

    public static void sort(List<? extends Identifiable> list, final List<Long> sortedIds) {
        Collections.sort(list, new Comparator<Identifiable>() {
            @Override
            public int compare(Identifiable o1, Identifiable o2) {
                return Integer.compare(sortedIds.indexOf(o1.getId()), sortedIds.indexOf(o2.getId()));
            }
        });
    }

    public static <K,V> List<Pair<K, V>> convert(Map<K, V> map) {
        List<Pair<K, V>> list = new ArrayList<>();

        for (Map.Entry<K, V> entry : map.entrySet()) {
            list.add(Pair.of(entry.getKey(), entry.getValue()));
        }

        return list;
    }
}
