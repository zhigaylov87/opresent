package ru.opresent.core.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * User: artem
 * Date: 08.10.15
 * Time: 23:34
 */
public final class UrlUtils {

    private UrlUtils() {
    }

    public static final String SLASH = "/";

    public static final char DIVIDER = '-';

    private static final String EMPTY = "";

    private static final char SPACE = ' ';

    private static final Set<Character> REPLACE_BY_DIVIDER = ImmutableSet.<Character>builder()
            .add(' ').add(DIVIDER)
            .build();

    private static final Set<Character> DIGITS = ImmutableSet.<Character>builder()
            .add('0').add('1').add('2').add('3').add('4').add('5').add('6').add('7').add('8').add('9')
            .build();

    private static final Set<Character> EN = ImmutableSet.<Character>builder()
            .add('a').add('b').add('c').add('d').add('e').add('f').add('g').add('h').add('i').add('j')
            .add('k').add('l').add('m').add('n').add('o').add('p').add('q').add('r').add('s').add('t')
            .add('u').add('v').add('w').add('x').add('y').add('z')
            .build();

    private static final Map<Character, String> RU_REPLACE = ImmutableMap.<Character, String>builder()

            .put('а', "a")    .put('б', "b")    .put('в', "v")    .put('г', "g")    .put('д', "d")
            .put('е', "e")    .put('ё', "e")    .put('ж', "zh")   .put('з', "z")    .put('и', "i")
            .put('й', "i")    .put('к', "k")    .put('л', "l")    .put('м', "m")    .put('н', "n")
            .put('о', "o")    .put('п', "p")    .put('р', "r")    .put('с', "s")    .put('т', "t")
            .put('у', "u")    .put('ф', "f")    .put('х', "h")    .put('ц', "c")    .put('ч', "ch")
            .put('ш', "sh")   .put('щ', "sh")   .put('ъ', EMPTY)  .put('ы', "y")    .put('ь', EMPTY)
            .put('э', "e")    .put('ю', "yu")   .put('я', "ya")

            .build();

    public static String createNamedUrl(long id, String name) {
        return Long.toString(id) + DIVIDER + createNamedUrl(name);
    }

    public static String createNamedUrl(String name) {
        Objects.requireNonNull(name, "Name is null");
        String preparedName = name.toLowerCase().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name is empty or contains only spaces: '" + name + "'");
        }

        boolean lastAddedIsDivider = false;
        StringBuilder target = new StringBuilder();
        for (int i = 0; i < preparedName.length(); i++) {
            char ch = preparedName.charAt(i);

            if (REPLACE_BY_DIVIDER.contains(ch) && !lastAddedIsDivider) {
                target.append(DIVIDER);
                lastAddedIsDivider = true;
            } else if (DIGITS.contains(ch) || EN.contains(ch)) {
                target.append(ch);
                lastAddedIsDivider = false;
            } else if (RU_REPLACE.containsKey(ch)) {
                target.append(RU_REPLACE.get(ch));
                lastAddedIsDivider = false;
            }
            // skip others chars
        }

        return target.toString();
    }

    /**
     * Supports id and entity name like "123-entity-name" and only id like "123"
     * @param namedUrl named url
     * @return entity id
     * @throws MalformedURLException if named url is invalid
     */
    public static long extractIdFromNamedUrl(String namedUrl) throws MalformedURLException {
        if (namedUrl == null || namedUrl.trim().isEmpty()) {
            throw new MalformedURLException("Invalid named url: " + FormatUtils.quotes(namedUrl));
        }
        int index = namedUrl.indexOf(DIVIDER);
        String idStr = index == StringUtils.INDEX_NOT_FOUND ?
                namedUrl : // only id like "123"
                namedUrl.substring(0, index); // id and entity name like "123-entity-name"
        try {
            return Long.parseLong(idStr);
        } catch (NumberFormatException e) {
            throw new MalformedURLException("Invalid named url: " + FormatUtils.quotes(namedUrl));
        }
    }
}
