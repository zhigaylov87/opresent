package ru.opresent.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * User: artem
 * Date: 15.03.15
 * Time: 7:50
 */
public class ListValueMap<K, V> extends CollectionValueMap<List<V>, K, V> {

    @Override
    protected List<V> newCollection() {
        return new ArrayList<>();
    }
}
