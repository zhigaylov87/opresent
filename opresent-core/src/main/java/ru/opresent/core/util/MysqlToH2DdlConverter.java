package ru.opresent.core.util;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.text.StrSubstitutor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: artem
 * Date: 12.03.2017
 * Time: 13:14
 */
public final class MysqlToH2DdlConverter {

    private MysqlToH2DdlConverter() {
    }

    /**
     * Convert Mysql ddl to H2 ddl
     * @param mysqlDdl mysqlDdl
     * @return h2 ddl
     */
    public static String convert(String mysqlDdl) {
        String h2Ddl = mysqlDdl;

        for (ReplaceCase replaceCase : ReplaceCase.values()) {
            Matcher matcher = replaceCase.mysqlPattern.matcher(mysqlDdl);
            while (matcher.find()) {
                String mysqlTarget = mysqlDdl.substring(matcher.start(), matcher.end());

                Map<String, String> paramNameToValue = new HashMap<>();
                for (String paramName : replaceCase.paramNames) {
                    paramNameToValue.put(paramName, matcher.group(paramName));
                }

                for (Map.Entry<String, String> paramNameToAlias : replaceCase.paramNameToAlias.entrySet()) {
                    String paramName = paramNameToAlias.getKey();
                    String paramAlias = paramNameToAlias.getValue();

                    if (!paramNameToValue.get(paramName).equals(matcher.group(paramAlias))) {
                        throw new RuntimeException(
                                "Not equals values for params '" + paramName + "' and '" + paramAlias + "' in:\n" + mysqlTarget);
                    }
                }

                String h2Replacement = new StrSubstitutor(paramNameToValue, "<", ">").replace(replaceCase.h2Format);

                h2Ddl = h2Ddl.replace(mysqlTarget, h2Replacement);
            }
        }

        return h2Ddl;
    }

    private enum ReplaceCase {

        ADD_INDEX_AND_FOREIGN_KEY(Pattern.compile(
                "ALTER\\s+TABLE\\s+(?<tableName>[a-zA-Z_\\d]+)\\s+" +
                "ADD\\s+INDEX\\s+(?<indexName>[a-zA-Z_\\d]+)\\s+\\((?<columnName>[a-zA-Z_\\d]+)\\),\\s+" +
                "ADD\\s+CONSTRAINT\\s+(?<indexName2>[a-zA-Z_\\d]+)\\s+" +
                "FOREIGN\\s+KEY\\s+\\((?<columnName2>[a-zA-Z_\\d]+)\\)\\s+" +
                "REFERENCES\\s+(?<refTableName>[a-zA-Z_\\d]+)\\s+\\((?<refColumnName>[a-zA-Z_\\d]+)\\);"),

                "CREATE INDEX <indexName> ON <tableName>(<columnName>);\n" +
                "ALTER TABLE <tableName> ADD FOREIGN KEY (<columnName>) REFERENCES <refTableName>(<refColumnName>);",

                Arrays.asList("tableName", "indexName", "columnName", "refTableName", "refColumnName"),

                ImmutableMap.of(
                        "indexName", "indexName2",
                        "columnName", "columnName2")
        ),


        ADD_INDEX(Pattern.compile(
                "ALTER\\s+TABLE\\s+(?<tableName>[a-zA-Z_\\d]+)\\s+" +
                        "ADD\\s+INDEX\\s+(?<indexName>[a-zA-Z_\\d]+)\\s+\\((?<columnName>[a-zA-Z_\\d]+)\\);"),

                "CREATE INDEX <indexName> ON <tableName>(<columnName>);",

                Arrays.asList("tableName", "indexName", "columnName"),

                ImmutableMap.<String, String>of()
        )

        ;

        final Pattern mysqlPattern;
        final String h2Format;
        final List<String> paramNames;
        final Map<String, String> paramNameToAlias;

        ReplaceCase(Pattern mysqlPattern, String h2Format, List<String> paramNames, Map<String, String> paramNameToAlias) {
            this.mysqlPattern = mysqlPattern;
            this.h2Format = h2Format;
            this.paramNames = paramNames;
            this.paramNameToAlias = paramNameToAlias;
        }
    }
}
