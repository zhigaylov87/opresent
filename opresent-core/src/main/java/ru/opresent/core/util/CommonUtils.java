package ru.opresent.core.util;

import java.util.Objects;

/**
 * User: artem
 * Date: 23.01.16
 * Time: 14:38
 */
public final class CommonUtils {

    private CommonUtils() {
    }

    private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";

    public static String getTmpDirPath() {
        return Objects.requireNonNull(System.getProperty(JAVA_IO_TMPDIR),
                "Property '" + JAVA_IO_TMPDIR + "' not defined.");
    }


}
