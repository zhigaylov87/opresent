package ru.opresent.core.util;

import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import ru.opresent.core.CoreException;

import java.util.Arrays;

/**
 * User: artem
 * Date: 29.11.14
 * Time: 3:11
 */
public class ReflectionUtils {

    public static boolean isInheritInterface(Class clazz, Class interfaceClass) {
        if (!interfaceClass.isInterface()) {
            throw new IllegalArgumentException("There is not interface class: " + interfaceClass);
        }

        do {
            if (Arrays.asList(clazz.getInterfaces()).contains(interfaceClass)) {
                return true;
            }
            clazz = clazz.getSuperclass();
        } while (clazz != null);

        return false;
    }

    @SuppressWarnings("unchecked")
    public static <T> T unwrapAopProxy(T potentialAopProxy) {
        if(AopUtils.isAopProxy(potentialAopProxy) && potentialAopProxy instanceof Advised) {
            T target;
            try {
                target = (T) ((Advised) potentialAopProxy).getTargetSource().getTarget();
            } catch (Exception e) {
                throw new CoreException("Error of unwrapping aop proxy: " + potentialAopProxy, e);
            }
            return target;
        }

        return potentialAopProxy;
    }

}
