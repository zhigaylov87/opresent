package ru.opresent.core.synchronization;


import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.component.api.image.ImageFileProcessMode;
import ru.opresent.core.dao.api.EntityIterator;
import ru.opresent.core.dao.api.EntityIteratorProvider;
import ru.opresent.core.domain.image.Image;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 11.07.14
 * Time: 1:56
 */
@Component("imageFileSynchronizer")
@Transactional(readOnly = true)
public class ImageFileSynchronizer {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;

    @Resource(name = "imageDao")
    private EntityIteratorProvider<Image> imageEntityIteratorProvider;

    public void synchronize(ImageSynchronizationListener listener) {
        try {
            doSynchronization(listener);
        } catch (Exception e) {
            log.error("Error of images files synchronization", e);
            listener.error(e);
            // not rethrow - listener must be handle error message
        }
    }

    private void doSynchronization(final ImageSynchronizationListener listener) throws Exception {
        log.info("Start of images files synchronization. Process mode: " + listener.getImageFileProcessMode());

        if (listener.getImageFileProcessMode() == ImageFileProcessMode.HARD) {
            log.info("Start of cleaning files dir");
            listener.cleaningStarted();
            String cleanedDir = imageFileManager.deleteAllImagesFiles();
            log.info("End of cleaning files dir. Cleaned dir: '" + cleanedDir + "'");
            listener.cleaningFinished();
        }

        final long startTime = System.currentTimeMillis();
        int processedCount = 0;
        final int totalCount;
        try (EntityIterator<Image> allIterator = imageEntityIteratorProvider.createEntityIterator()) {
            totalCount = allIterator.getTotalCount();
            log.info("Total images count: " + totalCount);
            listener.consumeTotalCount(totalCount);

            while (allIterator.hasNext()) {
                Image image = allIterator.next();

                log.info("Start of saving image file " + image);
                imageFileManager.saveImageFile(image, listener.getImageFileProcessMode());
                log.info("End of saving image file " + image);
                processedCount++;

                if (processedCount % 100 == 0 || processedCount == totalCount) {
                    log.info("Processed count: " + processedCount);
                    listener.consumeProcessedCount(processedCount);
                }
            }
        }

        long totalTime = System.currentTimeMillis() - startTime;
        log.info("End of images files synchronization. Processed images: " + processedCount + " / " + totalCount +
                ". Total time: " + totalTime + " ms -> " + (totalTime / 1000) + " seconds.");
        listener.complete();
    }
}