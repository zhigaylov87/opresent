package ru.opresent.core.synchronization;

import ru.opresent.core.component.api.image.ImageFileProcessMode;

import java.io.Serializable;

/**
 * User: artem
 * Date: 16.07.14
 * Time: 0:38
 */
public interface ImageSynchronizationListener extends Serializable {

    public void cleaningStarted();

    public void cleaningFinished();

    public void error(Exception e);

    public void consumeTotalCount(int totalCount);

    public void consumeProcessedCount(int processedCount);

    public void complete();

    public ImageFileProcessMode getImageFileProcessMode();
}
