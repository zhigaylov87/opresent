package ru.opresent.core.component.impl.crypto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.component.api.crypto.CryptoKeyProvider;
import ru.opresent.core.component.api.crypto.Cryptographer;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.util.Date;
import java.util.UUID;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 17:42
 */
@Component("cryptographer")
public class CryptographerImpl implements Cryptographer {

    @Resource(name = "cryptoKeyProvider")
    private CryptoKeyProvider cryptoKeyProvider;

    @Resource(name = "jsonMapper")
    private ObjectMapper jsonMapper;

    @Resource(name = "currentTimestampHelper")
    private CurrentTimestampHelper currentTimestampHelper;

    @Override
    public <T> String encrypt(T source) {
        CryptoDataHolder dataHolder = new CryptoDataHolder();
        // TODO: Брать время БД, а то с реальным серваком будут расхождения
        dataHolder.setTimestamp(currentTimestampHelper.getTimestamp());
        dataHolder.setRandomAddition(UUID.randomUUID().toString());

        String jsonDataHolder;
        try {
            dataHolder.setData(jsonMapper.writeValueAsString(source));
            jsonDataHolder = jsonMapper.writeValueAsString(dataHolder);
        } catch (JsonProcessingException e) {
            throw new CoreException(e);
        }
        return CryptoUtils.encrypt(getKey(), jsonDataHolder);
    }

    @Override
    public <T> T decrypt(String source, Class<T> valueType) {
        String decrypted = CryptoUtils.decrypt(getKey(), source);
        CryptoDataHolder dataHolder;
        try {
            dataHolder = jsonMapper.readValue(decrypted, CryptoDataHolder.class);
            // TODO: Проверять временную метку. Задать TTL(time to live) где-то в конфигах
            return jsonMapper.readValue(dataHolder.getData(), valueType);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public void encrypt(InputStream source, OutputStream target) {
        CryptoUtils.encrypt(getKey(), source, target);
    }

    @Override
    public void decrypt(InputStream source, OutputStream target) {
        CryptoUtils.decrypt(getKey(), source, target);
    }

    private Key getKey() {
        return CryptoUtils.readKey(cryptoKeyProvider.getKey());
    }

    private static class CryptoDataHolder {

        private Date timestamp;
        private String randomAddition;
        private String data;

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

        public String getRandomAddition() {
            return randomAddition;
        }

        public void setRandomAddition(String randomAddition) {
            this.randomAddition = randomAddition;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }
}
