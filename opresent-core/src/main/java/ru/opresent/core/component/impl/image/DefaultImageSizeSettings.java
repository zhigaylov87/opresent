package ru.opresent.core.component.impl.image;

import org.springframework.mobile.device.DeviceType;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.SizeOption;
import ru.opresent.core.component.api.image.ImageSizeSettings;
import ru.opresent.core.domain.Size;
import ru.opresent.core.domain.image.ImageUseOption;

import java.text.MessageFormat;
import java.util.Map;

/**
 * User: artem
 * Date: 02.11.14
 * Time: 1:13
 */
public class DefaultImageSizeSettings implements ImageSizeSettings {

    private Map<ImageUseOption, Map<DeviceType, SizeOption>> options;

    @Override
    public SizeOption getSizeOption(ImageUseOption useOption, DeviceType deviceType) {
        return useOption == ImageUseOption.ORIGINAL ? null : options.get(useOption).get(deviceType);
    }

    public void setOptions(Map<ImageUseOption, Map<DeviceType, SizeOption>> options) {
        for (ImageUseOption useOption : ImageUseOption.values()) {
            if (useOption == ImageUseOption.ORIGINAL) {
                continue;
            }
            Map<DeviceType, SizeOption> option = options.get(useOption);
            if (option == null || option.isEmpty()) {
                throw new CoreException("Size option for use option " + useOption.name() + " not defined!");
            }
            if (option.get(DeviceType.NORMAL) == null) {
                throw new CoreException("Size for option " + useOption.name() +
                        " and device type " + DeviceType.NORMAL + " not defined!");
            }
            for (Map.Entry<DeviceType, SizeOption> entry : option.entrySet()) {
                SizeOption sizeOption = entry.getValue();
                String invalidCause = checkSizeOption(sizeOption);
                if (invalidCause != null) {
                    throw new CoreException(MessageFormat.format(
                            "Invalid size option: {0} [imageUseOption: {1}, deviceType: {2}, sizeOption: {3}]",
                            invalidCause, useOption, entry.getKey(), sizeOption));
                }
            }
        }
        this.options = options;
    }

    /**
     * @param sizeOption size option
     * @return invalid cause or {@code null} for valid
     */
    private String checkSizeOption(SizeOption sizeOption) {
        Size size = sizeOption.getSize();
        if (size == null) {
            return "size is null";
        }
        SizeOption.Mode mode = sizeOption.getMode();
        if (mode == null) {
            return "mode is null";
        }
        switch (mode) {
            case NONE:
                return checkSize(size, true, true);

            case SQUARE:
                String msg = checkSize(size, true, true);
                if (msg != null) {
                    return msg;
                }
                return size.getWidth().equals(size.getHeight()) ? null : "width and height not equals";

            case FIT_TO_WIDTH:
                return checkSize(size, true, false);

            case FIT_TO_HEIGHT:
                return checkSize(size, false, true);

            default:
                throw new CoreException("Unknown size option mode: " + mode);
        }
    }

    private String checkSize(Size size, boolean widthRequired, boolean heightRequired) {
        boolean widthIsNull = size.getWidth() == null;
        boolean heightIsNull = size.getHeight() == null;

        if (widthRequired && widthIsNull) {
            return "width is null";
        }
        if (!widthRequired && !widthIsNull) {
            return "width is not null";
        }
        if (heightRequired && heightIsNull) {
            return "height is null";
        }
        if (!heightRequired && !heightIsNull) {
            return "height is not null";
        }

        return null;
    }
}
