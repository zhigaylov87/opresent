package ru.opresent.core.component.api.image;

import org.springframework.core.io.Resource;
import org.springframework.mobile.device.DeviceType;
import ru.opresent.core.component.SizeOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUseOption;

import java.util.Collection;

/**
 * User: artem
 * Date: 19.11.15
 * Time: 0:36
 */
public interface ImageFileManager extends ImageUrlCreator {

    public Resource getImageFileResource(Image image, ImageUseOption useOption);

    public String createUrl(Image image, ImageUseOption useOption, DeviceType deviceType);

    public void updateImageFileName(Image image);

    public void updateImagesFileNames(Collection<Image> images);

    public void saveImageFile(Image image, ImageFileProcessMode processMode);

    public void saveImageFile(Image image);

    public void deleteImageFile(Image image);

    public void deleteImagesFiles(Collection<Image> images);

    public String deleteAllImagesFiles();

    public SizeOption getSizeOption(ImageUseOption useOption);

}
