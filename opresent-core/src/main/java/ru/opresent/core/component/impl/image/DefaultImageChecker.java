package ru.opresent.core.component.impl.image;

import ru.opresent.core.component.api.image.ImageCheckException;
import ru.opresent.core.component.api.image.ImageChecker;
import ru.opresent.core.component.api.image.ImageConstraints;
import ru.opresent.core.domain.image.Image;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

/**
 * TODO: Сделать обычным валидатором
 *
 * User: artem
 * Date: 30.03.14
 * Time: 23:24
 */
public class DefaultImageChecker implements ImageChecker {

    private static final String VERY_BIG_MSG =
            "Image file is very big: {0} bytes. Max image file size: {1} bytes.";

    private static final String INVALID_FORMAT =
            "Image file format ''{0}'' not supported. Supported image file formats: {1}.";

    private ImageConstraints imageConstraints;

    @Override
    public void check(Image image) throws ImageCheckException {
        int size = image.getContent().length;
        int maxSize = imageConstraints.getMaxSize();
        if (size > maxSize) {
            throw new ImageCheckException(MessageFormat.format(VERY_BIG_MSG, size, maxSize));
        }

        String format = image.getFormat();
        List<String> supportedFormats = imageConstraints.getSupportedFormats();
        if (!supportedFormats.contains(format)) {
            throw new ImageCheckException(MessageFormat.format(
                    INVALID_FORMAT, format, Arrays.toString(supportedFormats.toArray())));
        }
    }

    @Override
    public void setImageConstraints(ImageConstraints imageConstraints) {
        this.imageConstraints = imageConstraints;
    }
}
