package ru.opresent.core.component;

import ru.opresent.core.domain.Size;

import java.io.Serializable;

/**
 * User: artem
 * Date: 30.07.15
 * Time: 2:27
 */
public class SizeOption implements Serializable {

    private Size size;
    private Mode mode = Mode.NONE;

    public SizeOption() {
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    @Override
    public String toString() {
        return "SizeOption {" +
                "size: " + size +
                ", mode: " + mode +
                '}';
    }

    public enum Mode {
        NONE,
        SQUARE,
        FIT_TO_WIDTH,
        FIT_TO_HEIGHT
    }
}
