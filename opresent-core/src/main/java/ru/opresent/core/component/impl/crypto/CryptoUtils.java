package ru.opresent.core.component.impl.crypto;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CharSequenceInputStream;
import ru.opresent.core.CoreException;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 19:10
 */
final class CryptoUtils {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    private static final String ALGORITHM = "DESede";

    private CryptoUtils() {
    }

    public static void main(String[] args) {
        String dir = args[0];
        String name = "server-key-" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".dat";
        createNewKey(dir + "/" + name);
    }

    private static void createNewKey(String filePath) {
        try {
            KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM);
            SecretKey key = kg.generateKey();
            try (FileOutputStream fos = new FileOutputStream(filePath)) {
                SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
                DESedeKeySpec keyspec = (DESedeKeySpec) skf.getKeySpec(key, DESedeKeySpec.class);
                fos.write(keyspec.getKey());
            }
        } catch (Exception e) {
            throw new CoreException(e);
        }
    }

    public static Key readKey(byte[] keyContent) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
            DESedeKeySpec keyspec = new DESedeKeySpec(keyContent);
            return skf.generateSecret(keyspec);
        } catch (Exception e) {
            throw new CoreException(e);
        }
    }

    public static void encrypt(Key key, InputStream source, OutputStream target) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            try (OutputStream encryptAndGzip = new CipherOutputStream(new GZIPOutputStream(target), cipher)) {
                IOUtils.copy(source, encryptAndGzip);
            }
        } catch (Exception e) {
            throw new CoreException(e);
        }
    }

    public static String encrypt(Key key, String source) {
        try (CharSequenceInputStream sourceStream = new CharSequenceInputStream(source, CHARSET);
             ByteArrayOutputStream targetStream = new ByteArrayOutputStream()) {

            encrypt(key, sourceStream, targetStream);
            return Base64.encodeBase64String(targetStream.toByteArray());
        } catch (Exception e) {
            throw new CoreException(e);
        }
    }

    public static void decrypt(Key key, InputStream source, OutputStream target) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key);
            try (InputStream decryptAndUnGzip = new CipherInputStream(new GZIPInputStream(source), cipher)) {
                IOUtils.copy(decryptAndUnGzip, target);
            }
        } catch (Exception e) {
            throw new CoreException(e);
        }
    }

    public static String decrypt(Key key, String source) {
        try (ByteArrayInputStream sourceStream = new ByteArrayInputStream(Base64.decodeBase64(source));
             ByteArrayOutputStream targetStream = new ByteArrayOutputStream()) {

            decrypt(key, sourceStream, targetStream);
            return new String(targetStream.toByteArray(), CHARSET);
        } catch (Exception e) {
            throw new CoreException(e);
        }
    }
}
