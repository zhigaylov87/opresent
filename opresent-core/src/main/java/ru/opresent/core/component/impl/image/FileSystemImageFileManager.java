package ru.opresent.core.component.impl.image;

import ru.opresent.core.component.api.image.ImageFileProcessor;

/**
 * User: artem
 * Date: 03.12.15
 * Time: 1:13
 */
public class FileSystemImageFileManager extends AbstractImageFileManager {

    @Override
    protected ImageFileProcessor createImageFileProcessor() {
        return new FileSystemImageFileProcessor();
    }
}
