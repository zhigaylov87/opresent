package ru.opresent.core.component.impl;

import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.CurrentTimestampHelper;

import java.util.Date;

/**
 * User: artem
 * Date: 04.05.14
 * Time: 15:51
 */
@Component("currentTimestampHelper")
public class NewDateCurrentTimestampHelper implements CurrentTimestampHelper {

    @Override
    public Date getTimestamp() {
        return new Date();
    }
}
