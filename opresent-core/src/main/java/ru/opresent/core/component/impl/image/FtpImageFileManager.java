package ru.opresent.core.component.impl.image;

import org.slf4j.Logger;
import ru.opresent.core.component.api.image.ImageFileProcessor;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 29.11.15
 * Time: 19:47
 */
public class FtpImageFileManager extends AbstractImageFileManager {

    @Resource(name = "log")
    private Logger log;

    private String ftpHostname;
    private int ftpPort;
    private String ftpUsername;
    private String ftpPassword;

    @Override
    protected ImageFileProcessor createImageFileProcessor() {
        return new FtpImageFileProcessor(ftpHostname, ftpPort, ftpUsername, ftpPassword, log);
    }

    // setters

    public void setFtpHostname(String ftpHostname) {
        this.ftpHostname = ftpHostname;
    }

    public void setFtpPort(int ftpPort) {
        this.ftpPort = ftpPort;
    }

    public void setFtpUsername(String ftpUsername) {
        this.ftpUsername = ftpUsername;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }
}
