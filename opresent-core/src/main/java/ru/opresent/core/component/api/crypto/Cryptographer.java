package ru.opresent.core.component.api.crypto;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 17:40
 */
public interface Cryptographer {

    <T> String encrypt(T source);

    <T> T decrypt(String source, Class<T> valueType);

    void encrypt(InputStream source, OutputStream target);

    void decrypt(InputStream source, OutputStream target);

}
