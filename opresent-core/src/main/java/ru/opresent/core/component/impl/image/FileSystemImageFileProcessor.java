package ru.opresent.core.component.impl.image;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.image.ImageFileProcessor;
import ru.opresent.core.util.UrlUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 01.12.15
 * Time: 1:31
 */
class FileSystemImageFileProcessor implements ImageFileProcessor {

    @Override
    public void saveImageFile(String dir, String name, File tempImageFile) {
        File targetImageFile = new File(dir + name);
        try {
            FileUtils.forceMkdir(targetImageFile.getParentFile());
            Files.move(tempImageFile.toPath(), targetImageFile.toPath());
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public Resource getImageFileContent(String dir, String name) {
        return new FileSystemResource(new File(dir + name));
    }

    @Override
    public void renameImageFile(String dir, String fromName, String toName) {
        try {
            Files.move(Paths.get(dir + fromName), Paths.get(dir + toName));
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public boolean isImageFileExists(String dir, String name) {
        return new File(dir + name).exists();
    }

    @Override
    public List<String> listImageFilesNamesById(String dir, final long imageId) {
        File[] files = new File(dir).listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().startsWith(Long.toString(imageId) + UrlUtils.DIVIDER);
            }
        });
        List<String> names = new ArrayList<>(files.length);
        for (File file : files) {
            names.add(file.getName());
        }
        return Collections.unmodifiableList(names);
    }

    @Override
    public void deleteImageFile(String dir, String name) {
        try {
            FileUtils.forceDelete(new File(dir + name));
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public void deleteAllImagesFiles(String dir) {
        try {
            FileUtils.cleanDirectory(new File(dir));
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public void close() {
        // do nothing
    }
}
