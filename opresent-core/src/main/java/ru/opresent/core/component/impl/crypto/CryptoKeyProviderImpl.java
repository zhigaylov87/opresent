package ru.opresent.core.component.impl.crypto;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.crypto.CryptoKeyProvider;
import ru.opresent.core.config.ServerConfig;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 19:04
 */
@Component("cryptoKeyProvider")
public class CryptoKeyProviderImpl implements CryptoKeyProvider {

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    @Override
    public byte[] getKey() {
        try {
            return FileUtils.readFileToByteArray(new File(serverConfig.getSecretKeyPath()));
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }
}
