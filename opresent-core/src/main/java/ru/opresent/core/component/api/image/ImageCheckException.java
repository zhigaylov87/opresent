package ru.opresent.core.component.api.image;

/**
 * User: artem
 * Date: 30.03.14
 * Time: 23:20
 */
public class ImageCheckException extends RuntimeException {

    public ImageCheckException(String message) {
        super(message);
    }
}
