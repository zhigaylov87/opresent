package ru.opresent.core.component.api.crypto;

/**
 * User: artem
 * Date: 17.09.16
 * Time: 19:03
 */
public interface CryptoKeyProvider {

    byte[] getKey();

}
