package ru.opresent.core.component.api.image;

/**
 * User: artem
 * Date: 29.12.14
 * Time: 1:42
 */
public enum ImageFileProcessMode {

    /**
     * Image files recreating and rewriting always
     */
    HARD,

    /**
     * Image files recreating and rewriting if only the same file not exists
     */
    LIGHT

}
