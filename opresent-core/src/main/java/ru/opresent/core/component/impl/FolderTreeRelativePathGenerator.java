package ru.opresent.core.component.impl;

import ru.opresent.core.component.api.RelativePathGenerator;
import ru.opresent.core.domain.Identifiable;

import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 14.07.14
 * Time: 3:29
 */
public class FolderTreeRelativePathGenerator implements RelativePathGenerator {

    private static final String SLASH = "/";

    private static final long MAX_ID = 99_999_999L; // eight nines

    private static final long LEVEL_FOLDER_LENGHT = 2L;

    private static final List<Long> LEVELS_DIVIDERS =
            Arrays.asList(1_000_000L /* one million*/, 10_000L /* ten thousands */, 100L /* one hundred */);

    @Override
    public String generateRelativePath(Identifiable entity) {
        if (entity.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Entity not saved in data base: " + entity);
        }

        if (entity.getId() > MAX_ID) {
            throw new IllegalArgumentException("Entity has id greater than available max id." +
                    " Max id: " + MAX_ID + ". Entity: " + entity);
        }

        StringBuilder relativePath = new StringBuilder();
        long currentId = entity.getId();
        for (long levelDivider : LEVELS_DIVIDERS) {
            long levelFolderNumber = currentId / levelDivider;
            currentId = currentId % levelDivider;

            relativePath.append(createLevelFolderName(levelFolderNumber));
            relativePath.append(SLASH);
        }

//        relativePath.append(resourceFile.getId()).append(".").append(resourceFile.getExtension());

        return relativePath.toString();
    }

    private String createLevelFolderName(long levelFolderNumber) {
        String folderName = Long.toString(levelFolderNumber);
        if (folderName.length() > LEVEL_FOLDER_LENGHT) {
            throw new IllegalArgumentException("Folder name has a greater name length than available. " +
                    "Available: " + LEVEL_FOLDER_LENGHT + " Level folder number: " + levelFolderNumber);
        }

        for (int i = folderName.length(); i < LEVEL_FOLDER_LENGHT; i++) {
            folderName = "0" + folderName;
        }

        return folderName;
    }
}
