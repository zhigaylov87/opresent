package ru.opresent.core.component.api;

import ru.opresent.core.domain.Identifiable;

/**
 * User: artem
 * Date: 30.03.14
 * Time: 20:03
 */
public interface RelativePathGenerator {

    String generateRelativePath(Identifiable entity);

}
