package ru.opresent.core.component.api;

import java.util.Date;

/**
 * User: artem
 * Date: 04.05.14
 * Time: 15:48
 */
public interface CurrentTimestampHelper {

    public Date getTimestamp();

}
