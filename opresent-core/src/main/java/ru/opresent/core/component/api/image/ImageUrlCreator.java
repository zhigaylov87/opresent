package ru.opresent.core.component.api.image;

import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUseOption;

/**
 * User: artem
 * Date: 16.09.14
 * Time: 23:20
 */
public interface ImageUrlCreator {

    public String createUrl(Image image);

    public String createUrl(Image image, ImageUseOption useOption);

}
