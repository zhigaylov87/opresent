package ru.opresent.core.component.impl.image;

import com.google.common.collect.ImmutableMap;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.resizers.configurations.AlphaInterpolation;
import net.coobird.thumbnailator.resizers.configurations.Antialiasing;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.mobile.device.DeviceType;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.SizeOption;
import ru.opresent.core.component.api.RelativePathGenerator;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.component.api.image.ImageFileProcessMode;
import ru.opresent.core.component.api.image.ImageFileProcessor;
import ru.opresent.core.component.api.image.ImageSizeSettings;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.Size;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageName;
import ru.opresent.core.domain.image.ImageUseOption;
import ru.opresent.core.util.UrlUtils;

import javax.annotation.PostConstruct;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;

/**
 * User: artem
 * Date: 19.11.15
 * Time: 0:53
 */
public abstract class AbstractImageFileManager implements ImageFileManager {

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.NORMAL;

    private static final Map<DeviceType, String> DEVICE_TYPE_TO_CODE = ImmutableMap.<DeviceType, String>builder()
            .put(DeviceType.NORMAL, "n")
            .put(DeviceType.MOBILE, "m")
            .build();

    private ImageSizeSettings imageSizeSettings;
    private RelativePathGenerator relativePathGenerator;
    private String basePath;
    private String baseUrl;

    private BufferedImage copyrightImage;

    @PostConstruct
    private void init() {
        try {
            copyrightImage = ImageIO.read(getClass().getResource("/image/image_copyright.jpg"));
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    protected abstract ImageFileProcessor createImageFileProcessor();

    @Override
    public String createUrl(Image image) {
        checkSaved(image);
        return getImageDirUrl(image) + createFileName(image, ImageUseOption.ORIGINAL, null);
    }

    @Override
    public String createUrl(Image image, ImageUseOption useOption) {
        checkSaved(image);
        checkImageUseOption(image, useOption);
        return getImageDirUrl(image) + createFileName(image, useOption, DEFAULT_DEVICE_TYPE);
    }

    @Override
    public String createUrl(Image image, ImageUseOption useOption, DeviceType deviceType) {
        checkSaved(image);
        checkImageUseOption(image, useOption);
        SizeOption sizeOption = getSizeOption(useOption, deviceType);
        return getImageDirUrl(image) + createFileName(image, useOption, sizeOption == null ? DEFAULT_DEVICE_TYPE : deviceType);
    }

    @Override
    public Resource getImageFileResource(Image image, ImageUseOption useOption) {
        checkSaved(image);
        checkImageUseOption(image, useOption);
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            return fileProcessor.getImageFileContent(getImageDirPath(image), createFileName(image, useOption, DEFAULT_DEVICE_TYPE));
        }
    }

    @Override
    public void updateImageFileName(Image image) {
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            updateImageFileName(fileProcessor, image);
        }
    }

    @Override
    public void updateImagesFileNames(Collection<Image> images) {
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            for (Image image : images) {
                updateImageFileName(fileProcessor, image);
            }
        }
    }

    @Override
    public void saveImageFile(Image image, ImageFileProcessMode processMode) {
        checkSaved(image);
        checkHasContent(image);
        String dir = getImageDirPath(image);

        boolean hasCopyright;
        switch (image.getHasCopyrightOption()) {
            case INHERIT_FROM_IMAGE_USE:
                hasCopyright = image.getImageUse().hasCopyright();
                break;
            case NO:
                hasCopyright = false;
                break;
            default:
                throw new IllegalArgumentException("Unknown image HasCopyrightOption: " + image.getHasCopyrightOption());
        }

        // original
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            String fileName = createFileName(image, ImageUseOption.ORIGINAL, null);
            BufferedImage originalImage = null;
            BufferedImage originalImageWithCopyright = null;
            // lazy
            if (processMode == ImageFileProcessMode.HARD || !fileProcessor.isImageFileExists(dir, fileName)) {
                originalImage = toBufferedImage(image);
                if (hasCopyright) {
                    originalImageWithCopyright = copy(originalImage);
                    addCopyrightImage(originalImageWithCopyright);
                }
                writeImage(fileProcessor, hasCopyright ? originalImageWithCopyright : originalImage, dir, fileName, image.getFormat());
            }

            // use options
            List<ImageUseOption> useOptions = ImageUseOption.getImageUseOptionsForUseImage(image.getImageUse());
            if (useOptions == null) {
                return;
            }

            for (ImageUseOption useOption : useOptions) {
                for (DeviceType deviceType : DeviceType.values()) {
                    SizeOption sizeOption = getSizeOption(useOption, deviceType);
                    if (sizeOption != null) {
                        fileName = createFileName(image, useOption, deviceType);
                        if (processMode == ImageFileProcessMode.HARD || !fileProcessor.isImageFileExists(dir, fileName)) {
                            // lazy
                            if (originalImage == null) {
                                originalImage = toBufferedImage(image);
                                if (hasCopyright) {
                                    originalImageWithCopyright = copy(originalImage);
                                    addCopyrightImage(originalImageWithCopyright);
                                }
                            }

                            BufferedImage resizedImage;
                            if (sizeOption.getMode() == SizeOption.Mode.SQUARE) {
                                resizedImage = squareImage(originalImage);
                                if (hasCopyright) {
                                    addCopyrightImage(resizedImage);
                                }
                                resizedImage = resizeImage(resizedImage, sizeOption);
                            } else {
                                resizedImage = resizeImage(hasCopyright ? originalImageWithCopyright : originalImage, sizeOption);
                            }

                            writeImage(fileProcessor, resizedImage, dir, fileName, image.getFormat());
                        }
                    }
                }
            }
        }
    }

    @Override
    public void saveImageFile(Image image) {
        saveImageFile(image, ImageFileProcessMode.HARD);
    }

    @Override
    public void deleteImageFile(Image image) {
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            deleteImageFile(fileProcessor, image);
        }
    }

    @Override
    public void deleteImagesFiles(Collection<Image> images) {
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            for (Image image : images) {
                deleteImageFile(fileProcessor, image);
            }
        }
    }

    @Override
    public String deleteAllImagesFiles() {
        try (ImageFileProcessor fileProcessor = createImageFileProcessor()) {
            fileProcessor.deleteAllImagesFiles(basePath);
        }
        return basePath;
    }

    @Override
    public SizeOption getSizeOption(ImageUseOption useOption) {
        return getSizeOption(useOption, DEFAULT_DEVICE_TYPE);
    }

    private void checkHasContent(Image image) {
        if (image.getContent() == null) {
            throw new IllegalArgumentException("Image content is null: " + image);
        }
        if (image.getContent().length == 0) {
            throw new IllegalArgumentException("Image content is empty: " + image);
        }
    }

    private void checkImageUseOption(Image image, ImageUseOption useOption) {
        if (image.getImageUse() != useOption.getImageUse()) {
            throw new IllegalArgumentException("Illegal image and use option combination. Image: " +
                    image + ". ImageUseOption: " + useOption);
        }
    }

    private SizeOption getSizeOption(ImageUseOption useOption, DeviceType deviceType) {
        return imageSizeSettings.getSizeOption(useOption, deviceType);
    }

    /**
     * Creates image file name by image parameters.<br/><br/>
     *
     * <p>1) Without image name</p>
     * <table border="1">
     *     <tr><td colspan="5">123-0.png</td></tr>
     *     <tr>
     *         <td>123</td>
     *         <td>-</td>
     *         <td>0</td>
     *         <td>.</td>
     *         <td>png</td>
     *     </tr>
     *     <tr>
     *         <td>{@link ru.opresent.core.domain.image.Image#getId()}</td>
     *         <td>-</td>
     *         <td>{@link ru.opresent.core.domain.image.ImageUseOption#getCode()}<br/>of<br/>{@link ru.opresent.core.domain.image.ImageUseOption#ORIGINAL}</td>
     *         <td>.</td>
     *         <td>{@link ru.opresent.core.domain.image.Image#getFormat()}</td>
     *     </tr>
     *
     *     <tr><td colspan="5">123-1n.png</td></tr>
     *     <tr>
     *         <td>123</td>
     *         <td>-</td>
     *         <td>1n</td>
     *         <td>.</td>
     *         <td>png</td>
     *     </tr>
     *     <tr>
     *         <td>{@link ru.opresent.core.domain.image.Image#getId()}</td>
     *         <td>-</td>
     *         <td>{@link ru.opresent.core.domain.image.ImageUseOption#getCode()}<br/>and<br/>
     *             "n" for {@link org.springframework.mobile.device.DeviceType#NORMAL}<br/>
     *             "t" for {@link org.springframework.mobile.device.DeviceType#TABLET}<br/>
     *             "m" for {@link org.springframework.mobile.device.DeviceType#MOBILE}<br/>
     *         </td>
     *         <td>.</td>
     *         <td>{@link ru.opresent.core.domain.image.Image#getFormat()}</td>
     *     </tr>
     * </table>
     *
     * <p>2) With image name</p>
     * <table border="1">
     *     <tr><td colspan="7">123-0-image-name.png</td></tr>
     *     <tr>
     *         <td>123</td>
     *         <td>-</td>
     *         <td>0</td>
     *         <td>-</td>
     *         <td>image-name</td>
     *         <td>.</td>
     *         <td>png</td>
     *     </tr>
     *     <tr>
     *         <td>{@link ru.opresent.core.domain.image.Image#getId()}</td>
     *         <td>-</td>
     *         <td>{@link ru.opresent.core.domain.image.ImageUseOption#getCode()}<br/>of<br/>{@link ru.opresent.core.domain.image.ImageUseOption#ORIGINAL}</td>
     *         <td>-</td>
     *         <td>{@link ru.opresent.core.domain.image.ImageName#getName()}<br/>of<br/>{@link ru.opresent.core.domain.image.Image#getImageName()}</td>
     *         <td>.</td>
     *         <td>{@link ru.opresent.core.domain.image.Image#getFormat()}</td>
     *     </tr>
     *
     *     <tr><td colspan="7">123-1n-image-name.png</td></tr>
     *     <tr>
     *         <td>123</td>
     *         <td>-</td>
     *         <td>1n</td>
     *         <td>-</td>
     *         <td>image-name</td>
     *         <td>.</td>
     *         <td>png</td>
     *     </tr>
     *     <tr>
     *         <td>{@link ru.opresent.core.domain.image.Image#getId()}</td>
     *         <td>-</td>
     *         <td>{@link ru.opresent.core.domain.image.ImageUseOption#getCode()}<br/>and<br/>
     *             "n" for {@link org.springframework.mobile.device.DeviceType#NORMAL}<br/>
     *             "t" for {@link org.springframework.mobile.device.DeviceType#TABLET}<br/>
     *             "m" for {@link org.springframework.mobile.device.DeviceType#MOBILE}<br/>
     *         </td>
     *         <td>-</td>
     *         <td>{@link ru.opresent.core.domain.image.ImageName#getName()}<br/>of<br/>{@link ru.opresent.core.domain.image.Image#getImageName()}</td>
     *         <td>.</td>
     *         <td>{@link ru.opresent.core.domain.image.Image#getFormat()}</td>
     *     </tr>
     * </table>
     *
     * @param image image
     * @param useOption image use option
     * @param deviceType device type or {@code null} for {@link ru.opresent.core.domain.image.ImageUseOption#ORIGINAL}
     * @return image file name
     */
    private String createFileName(Image image, ImageUseOption useOption, DeviceType deviceType) {
        StringBuilder fileName = new StringBuilder();
        fileName.append(image.getId()).append(UrlUtils.DIVIDER).append(useOption.getCode());

        if (useOption != ImageUseOption.ORIGINAL) {
            Objects.requireNonNull(deviceType);
            fileName.append(DEVICE_TYPE_TO_CODE.get(deviceType));
        }

        ImageName imageName = image.getImageName();
        if (imageName != null) {
            fileName.append(UrlUtils.DIVIDER).append(UrlUtils.createNamedUrl(imageName.getName()));
        }

        return fileName.append(".").append(image.getFormat()).toString();
    }

    private BufferedImage toBufferedImage(Image image) {
        InputStream in = new ByteArrayInputStream(image.getContent());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(in);
        } catch (IOException e) {
            throw new CoreException(e);
        }

        return bufferedImage;
    }

    private BufferedImage copy(BufferedImage src) {
        int width = src.getWidth();
        int height = src.getHeight();
        BufferedImage copy = new BufferedImage(width, height, src.getType());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                copy.setRGB(x, y, src.getRGB(x, y));
            }
        }

        return copy;
    }

    private void addCopyrightImage(BufferedImage src) {
        if (src.getWidth() < copyrightImage.getWidth() || src.getHeight() < copyrightImage.getHeight()) {
            throw new CoreException(MessageFormat.format("Can not to locate copyright on image. " +
                    "Image size: {0} x {1}, copyright image size: {2} x {3}",
                    src.getWidth(), src.getHeight(),
                    copyrightImage.getWidth(), copyrightImage.getHeight()));
        }

        int dx = src.getWidth() - copyrightImage.getWidth();
        int dy = src.getHeight() - copyrightImage.getHeight();

        for (int x = 0; x < copyrightImage.getWidth(); x++) {
            for (int y = 0; y < copyrightImage.getHeight(); y++) {
                src.setRGB(x + dx, y + dy, copyrightImage.getRGB(x, y));
            }
        }
    }

    private BufferedImage squareImage(BufferedImage src) {
        int srcWidth = src.getWidth();
        int srcHeight = src.getHeight();

        int resultSize = Math.min(srcWidth, srcHeight);
        int dx = resultSize == srcWidth ? 0 : (srcWidth - resultSize) / 2;
        int dy = resultSize == srcHeight ? 0 : (srcHeight - resultSize) / 2;

        BufferedImage result = new BufferedImage(resultSize, resultSize, src.getType());
        for (int x = 0; x < resultSize; x++) {
            for (int y = 0; y < resultSize; y++) {
                result.setRGB(x, y, src.getRGB(x + dx, y + dy));
            }
        }

        return result;
    }

    /**
     * @param src source image
     * @param sizeOption size option
     * @return resized image content
     */
    private BufferedImage resizeImage(BufferedImage src, SizeOption sizeOption) {
        Size size = sizeOption.getSize();
        SizeOption.Mode mode = sizeOption.getMode();

        int width;
        int height;
        switch (mode) {
            case NONE:
            case SQUARE:
                width = size.getWidth();
                height = size.getHeight();
                break;

            case FIT_TO_WIDTH:
                width = size.getWidth();
                height = Integer.MAX_VALUE;
                break;

            case FIT_TO_HEIGHT:
                width = Integer.MAX_VALUE;
                height = size.getHeight();
                break;

            default:
                throw new CoreException("Unknown size option mode: " + mode);
        }

        try {
            return Thumbnails.of(src)
                    .size(width, height)
                    .outputQuality(1.0d) // high quality
                    .alphaInterpolation(AlphaInterpolation.QUALITY)
                    .antialiasing(Antialiasing.ON)
                    .keepAspectRatio(true)
                    .asBufferedImage();
        } catch (IOException e) {
            throw new CoreException(e);
        }
//        return Scalr.resize(src, Scalr.Method.ULTRA_QUALITY, size.getWidth(), size.getHeight(), Scalr.OP_ANTIALIAS);
    }

    private void writeImage(ImageFileProcessor fileProcessor, BufferedImage img, String dir, String name, String format) {
        try {
            Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(format);
            ImageWriter writer = iter.next();
            ImageWriteParam iwp = writer.getDefaultWriteParam();
            iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwp.setCompressionQuality(1); // an integer between 0 and 1
            // 1 specifies minimum compression and maximum quality

            File tempImageFile = File.createTempFile(name, null);
            try (FileImageOutputStream fios = new FileImageOutputStream(tempImageFile)) {
                writer.setOutput(fios);
                IIOImage image = new IIOImage(img, null, null);
                writer.write(null, image, iwp);
                writer.dispose();
            }
            fileProcessor.saveImageFile(dir, name, tempImageFile);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    private void updateImageFileName(ImageFileProcessor fileProcessor, Image image) {
        checkSaved(image);
        String dir = getImageDirPath(image);

        List<String> filesNames = fileProcessor.listImageFilesNamesById(dir, image.getId());
        if (filesNames.isEmpty()) {
            throw new CoreException("Image files not found for image " + image);
        }

        String actualImageName = UrlUtils.createNamedUrl(image.getImageName().getName());
        for (String fileName : filesNames) {
            int dividerAfterIdIndex = fileName.indexOf(UrlUtils.DIVIDER);
            int dividerAfterUseOptionCodeWithDeviceTypeIndex = fileName.indexOf(UrlUtils.DIVIDER, dividerAfterIdIndex + 1);
            boolean fileWithoutImageName = dividerAfterUseOptionCodeWithDeviceTypeIndex == StringUtils.INDEX_NOT_FOUND;

            int dotIndex = fileName.indexOf(".");
            String fileImageName = fileWithoutImageName ? null :
                    fileName.substring(dividerAfterUseOptionCodeWithDeviceTypeIndex + 1, dotIndex);

            if (fileWithoutImageName || !actualImageName.equals(fileImageName)) {

                String idAndUseOptionCodeWithDeviceType = fileWithoutImageName ?
                        fileName.substring(0, dotIndex) :
                        fileName.substring(0, dividerAfterUseOptionCodeWithDeviceTypeIndex);
                String format = fileName.substring(dotIndex + 1);

                String newFileName = idAndUseOptionCodeWithDeviceType + UrlUtils.DIVIDER + actualImageName + "." + format;
                fileProcessor.renameImageFile(dir, fileName, newFileName);
            }
        }
    }

    private String getImageDirPath(Image image) {
        return basePath + UrlUtils.SLASH + relativePathGenerator.generateRelativePath(image);
    }

    private String getImageDirUrl(Image image) {
        return baseUrl + UrlUtils.SLASH + relativePathGenerator.generateRelativePath(image);
    }

    private void checkSaved(Image image) {
        if (image.getId() == Identifiable.NOT_SAVED_ID) {
            throw new IllegalArgumentException("Image not saved in database: " + image);
        }
    }

    private void deleteImageFile(ImageFileProcessor fileProcessor, Image image) {
        checkSaved(image);
        String dir = getImageDirPath(image);
        for (String fileName : fileProcessor.listImageFilesNamesById(dir, image.getId())) {
            fileProcessor.deleteImageFile(dir, fileName);
        }
    }

    // setters

    public void setImageSizeSettings(ImageSizeSettings imageSizeSettings) {
        this.imageSizeSettings = imageSizeSettings;
    }

    public void setRelativePathGenerator(RelativePathGenerator relativePathGenerator) {
        this.relativePathGenerator = relativePathGenerator;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
