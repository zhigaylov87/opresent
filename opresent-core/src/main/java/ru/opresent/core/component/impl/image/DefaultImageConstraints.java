package ru.opresent.core.component.impl.image;

import ru.opresent.core.component.api.image.ImageConstraints;

import java.util.List;

/**
 * User: artem
 * Date: 04.05.14
 * Time: 14:42
 */
public class DefaultImageConstraints implements ImageConstraints {

    private int maxSize;
    private List<String> supportedFormats;

    @Override
    public int getMaxSize() {
        return maxSize;
    }

    @Override
    public List<String> getSupportedFormats() {
        return supportedFormats;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public void setSupportedFormats(List<String> supportedFormats) {
        this.supportedFormats = supportedFormats;
    }
}
