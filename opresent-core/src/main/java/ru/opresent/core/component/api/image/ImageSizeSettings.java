package ru.opresent.core.component.api.image;

import org.springframework.mobile.device.DeviceType;
import ru.opresent.core.component.SizeOption;
import ru.opresent.core.domain.image.ImageUseOption;

/**
 * User: artem
 * Date: 02.11.14
 * Time: 1:09
 */
public interface ImageSizeSettings {

    public SizeOption getSizeOption(ImageUseOption useOption, DeviceType deviceType);

}
