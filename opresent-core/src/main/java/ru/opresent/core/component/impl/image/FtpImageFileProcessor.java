package ru.opresent.core.component.impl.image;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.slf4j.Logger;
import org.springframework.core.io.Resource;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.image.ImageFileProcessor;
import ru.opresent.core.util.CommonUtils;
import ru.opresent.core.util.TmpFileSystemResource;
import ru.opresent.core.util.UrlUtils;

import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * <b>This implementation is not thread safe</b>
 *
 * User: artem
 * Date: 01.12.15
 * Time: 1:31
 */
class FtpImageFileProcessor implements ImageFileProcessor {

    private static final String MSG_COMPLETE = "FTP operation complete in {0} ms - {1}";
    private static final String MSG_FAILED = "FTP operation failed with reply ''{0}'' - {1}";

    private static final String MSG_SAVE = "save image file [dir: ''{0}'', name: ''{1}'']";
    private static final String MSG_GET_CONTENT = "get image file content [dir: ''{0}'', name: ''{1}'']";
    private static final String MSG_RENAME = "rename image file [dir: ''{0}'', fromName: ''{1}'', toName: ''{2}'']";
    private static final String MSG_EXISTS = "exists image file [dir: ''{0}'', name: ''{1}'']";
    private static final String MSG_LIST_BY_ID = "list image files by id [dir: ''{0}'', imageId: {1}]";
    private static final String MSG_DELETE = "delete image file [dir: ''{0}'', name: ''{1}'']";
    private static final String MSG_MAKE_DIR = "make dir [dir: ''{0}'', subDir: ''{1}'', level: {2}]";
    private static final String MSG_CHANGE_DIR = "change dir [dir: ''{0}'', subDir: ''{1}'', level: {2}]";
    private static final String MSG_CHANGE_DIR_TO_ROOT = "change dir to root";

    private FTPClient ftpClient;
    private Logger log;

    public FtpImageFileProcessor(String hostname, int port, String username, String password, Logger log) {
        this.log = log;
        ftpClient = new FTPClient();
        try {
            long start = System.currentTimeMillis();
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Connecting to images ftp[hostname: ''{0}'', port: {1}]", hostname, port));
            }
            ftpClient.connect(hostname, port);
            ftpClient.login(username, password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Connecting to images ftp complete in {0} ms", System.currentTimeMillis() - start));
            }
        } catch (Exception e) {
            log.error("Error while connecting to images ftp", e);
            close();
            throw new CoreException(e);
        }
    }

    @Override
    public void saveImageFile(String dir, String name, File tempImageFile) {
        try (InputStream inputStream = new FileInputStream(tempImageFile)) {
            forceMakeDir(dir);
            long start = System.currentTimeMillis();
            boolean complete = ftpClient.storeFile(dir + name, inputStream);
            checkComplete(complete, start, MSG_SAVE, dir, name);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public Resource getImageFileContent(String dir, String name) {
        File file = createTmpFile(name);
        try {
            try (OutputStream outputStream = new FileOutputStream(file)) {
                long start = System.currentTimeMillis();
                boolean complete = ftpClient.retrieveFile(dir + name, outputStream);
                checkComplete(complete, start, MSG_GET_CONTENT, dir, name);
            }
            return new TmpFileSystemResource(file);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public void renameImageFile(String dir, String fromName, String toName) {
        try {
            long start = System.currentTimeMillis();
            boolean complete = ftpClient.rename(dir + fromName, dir + toName);
            checkComplete(complete, start, MSG_RENAME, dir, fromName, toName);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public boolean isImageFileExists(String dir, String name) {
        try {
            long start = System.currentTimeMillis();
            FTPFile[] ftpFiles = ftpClient.listFiles(dir + name);
            checkComplete(true, start, MSG_EXISTS, dir, name);
            return ftpFiles.length != 0;
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public List<String> listImageFilesNamesById(String dir, final long imageId) {
        try {
            long start = System.currentTimeMillis();
            FTPFile[] ftpFiles = ftpClient.listFiles(dir, new FTPFileFilter() {
                @Override
                public boolean accept(FTPFile file) {
                    return file.getName().startsWith(Long.toString(imageId) + UrlUtils.DIVIDER);
                }
            });
            checkComplete(true, start, MSG_LIST_BY_ID, dir, imageId);

            List<String> names = new ArrayList<>();
            for (FTPFile ftpFile : ftpFiles) {
                names.add(ftpFile.getName());
            }
            return Collections.unmodifiableList(names);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public void deleteImageFile(String dir, String name) {
        try {
            long start = System.currentTimeMillis();
            boolean complete = ftpClient.deleteFile(dir + name);
            checkComplete(complete, start, MSG_DELETE, dir, name);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    @Override
    public void deleteAllImagesFiles(String dir) {
//        throw new UnsupportedOperationException("TODO"); // TODO
    }

    @Override
    public void close() {
        try {
            if (ftpClient.isConnected()) {
                ftpClient.logout();
                ftpClient.disconnect();
            }
        } catch (Exception e) {
            log.warn("Error while closing images ftp connection", e);
        }
    }

    private File createTmpFile(String name) {
        return new File(CommonUtils.getTmpDirPath() + "/" + UUID.randomUUID().toString() + "_" + name);
    }

    private void checkComplete(boolean complete, long startMillis, String msgPattern, Object... msgParams) {
        if (complete) {
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format(MSG_COMPLETE,
                        System.currentTimeMillis() - startMillis, MessageFormat.format(msgPattern, msgParams)));
            }
        } else {
            String msg = MessageFormat.format(MSG_FAILED, ftpClient.getReplyString(), MessageFormat.format(msgPattern, msgParams));
            log.error(msg);
            throw new CoreException(msg);
        }
    }

    private void forceMakeDir(final String dir) throws IOException {
        boolean dirExists = ftpClient.changeWorkingDirectory(dir);
        if (dirExists) {
            return;
        }

        long start = System.currentTimeMillis();
        checkComplete(ftpClient.changeWorkingDirectory(UrlUtils.SLASH), start, MSG_CHANGE_DIR_TO_ROOT);

        String preparedDir = dir;
        if (preparedDir.startsWith(UrlUtils.SLASH)) {
            preparedDir = preparedDir.substring(1);
        }
        if (preparedDir.endsWith(UrlUtils.SLASH)) {
            preparedDir = preparedDir.substring(0, preparedDir.length() - 1);
        }
        int level = 0;
        for (String subDir : preparedDir.split(UrlUtils.SLASH)) {
            boolean subDirExists = ftpClient.changeWorkingDirectory(subDir);
            if (!subDirExists) {
                start = System.currentTimeMillis();
                checkComplete(ftpClient.makeDirectory(subDir), start, MSG_MAKE_DIR, dir, subDir, level);
                start = System.currentTimeMillis();
                checkComplete(ftpClient.changeWorkingDirectory(subDir), start, MSG_CHANGE_DIR, dir, subDir, level);
            }
            level++;
        }
    }
}
