package ru.opresent.core.component.api.image;

import ru.opresent.core.domain.image.Image;

/**
 * User: artem
 * Date: 30.03.14
 * Time: 23:13
 */
public interface ImageChecker {

    public void check(Image image) throws ImageCheckException;

    public void setImageConstraints(ImageConstraints imageConstraints);

}
