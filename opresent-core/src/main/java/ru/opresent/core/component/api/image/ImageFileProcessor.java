package ru.opresent.core.component.api.image;

import org.springframework.core.io.Resource;

import java.io.File;
import java.util.List;

/**
 * User: artem
 * Date: 01.12.15
 * Time: 1:30
 */
public interface ImageFileProcessor extends AutoCloseable {

    void saveImageFile(String dir, String name, File tempImageFile);

    Resource getImageFileContent(String dir, String name);

    void renameImageFile(String dir, String fromName, String toName);

    boolean isImageFileExists(String dir, String name);

    List<String> listImageFilesNamesById(String dir, long imageId);

    void deleteImageFile(String dir, String name);

    void deleteAllImagesFiles(String dir);

    @Override
    void close();
}
