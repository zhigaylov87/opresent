package ru.opresent.core.component.api.image;

import java.util.List;

/**
 * User: artem
 * Date: 30.03.14
 * Time: 23:02
 */
public interface ImageConstraints {

    /**
     * @return max size in bytes
     */
    public int getMaxSize();

    public List<String> getSupportedFormats();

}
