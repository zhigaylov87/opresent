package ru.opresent.core.livemaster.sync.event;

/**
 * User: artem
 * Date: 06.12.14
 * Time: 14:43
 */
public interface LivemasterSyncEventListener {

    void processEvent(LivemasterSyncEvent syncEvent);

}
