package ru.opresent.core.livemaster.page_resolver;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.ProductStatus;

import java.util.ArrayList;
import java.util.List;

import static ru.opresent.core.livemaster.page_resolver.ResolveUtils.*;

/**
 * User: artem
 * Date: 02.09.2017
 * Time: 16:28
 */
class LivemasterProductPageResolverV4 implements LivemasterProductPageResolver {

    static final String H1_ITEM_HEADER = "h1.item-header";

    @Override
    public String getVersion() {
        return "v4";
    }

    @Override
    public String getProductTypeAlias(Document productPage) {
        String alias = selectSingleElement(selectBlockContent(productPage, "Категории работ"),
                "a.master-categories__link.black")
                .text();
        return replaceNbsp(alias);
    }

    @Override
    public List<String> getCategoriesAliases(Document productPage) {
        List<String> aliases = new ArrayList<>();
        for (Element element : selectBlockContent(productPage, "Коллекции мастера")
                .select("a.master-categories__link.black")) {
            aliases.add(replaceNbsp(element.text()));
        }
        return aliases;
    }

    @Override
    public String getName(Document productPage) {
        return selectSingleElement(productPage, H1_ITEM_HEADER).text();
    }

    @Override
    public String getDescription(Document productPage) {
        // .text() skip all html elements - only text in result
        return selectBlockContent(productPage, "Описание").text();
    }

    @Override
    public ProductStatus getStatus(Document productPage) {
        Element container = selectSingleElement(productPage, H1_ITEM_HEADER).parent();
        String statusText = selectSingleElement(container, "ul.item-info li:first-child").text();
        statusText = replaceNbsp(statusText).trim();

        if ("Готовая работа".equals(statusText)) {
            return ProductStatus.AVAILABLE;
        }
        if (statusText.startsWith("Работа на заказ")) {
            return ProductStatus.FOR_ORDER;
        }
        if ("Работа представлена для примера".equals(statusText)) {
            return ProductStatus.EXAMPLE;
        }

        throw new CoreException("Can not to resolve status. Status text: '" + statusText + "'");
    }

    @Override
    public int getPrice(Document productPage) {
        Element container = selectSingleElement(productPage, H1_ITEM_HEADER).parent();
        String priceStr = selectSingleElement(container, "div.price span.price").ownText();
        return resolvePrice(priceStr);
    }

    @Override
    public List<String> getImagesUrls(Document productPage) {
        List<String> urls = new ArrayList<>();
        for (Element element : productPage.select("div.photo-switcher__main a.photo-switcher__largephoto")) {
            urls.add(element.attr("href"));
        }
        return urls;
    }

    @Override
    public String getSizeText(Document productPage) {
        Element container = selectSingleElement(productPage, H1_ITEM_HEADER).parent();
        String sizeText = selectSingleElement(container, "ul.item-info li:last-child").text();
        if (!sizeText.startsWith("Размер:")) {
            throw new CoreException("Can not to resolve size. Size text: '" + sizeText + "'");
        }
        sizeText = sizeText.substring(sizeText.indexOf(":") + 1);
        return StringUtils.trimToNull(replaceNbsp(sizeText));
    }

    @Override
    public String getProductionTime(Document productPage) {
        Element container = selectSingleElement(productPage, H1_ITEM_HEADER).parent();
        for (Element element : container.select("ul.item-info li")) {
            String text = element.text();
            if (text.startsWith("Срок изготовления:")) {
                text = text.substring(text.indexOf(":") + 1);
                return StringUtils.trimToNull(replaceNbsp(text));
            }
        }
        return null;
    }

    @Override
    public List<String> getKeywords(Document productPage) {
        Element container = selectBlockContent(productPage, "Ключевые слова");
        List<String> keywords = new ArrayList<>();
        Elements keywordsElements = container.select("li.tag-list__item a");
        for (Element keywordElement : keywordsElements) {
            keywords.add(keywordElement.text());
        }
        return keywords;
    }

    private Element selectBlockContent(Document productPage, String blockTitle) {
        Element targetBlockHeading = null;
        for (Element blockHeading : productPage.select("div.block--inner div.block__heading")) {
            if (blockHeading.text().trim().equals(blockTitle)) {
                if (targetBlockHeading != null) {
                    throw new CoreException("More than one block with title '" + blockTitle + "'");
                }
                targetBlockHeading = blockHeading;
            }
        }
        if (targetBlockHeading == null) {
            throw new CoreException("Block with title '" + blockTitle + "' not found");
        }
        return selectSingleElement(targetBlockHeading.parent(), "div.block__content");
    }
}
