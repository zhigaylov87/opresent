package ru.opresent.core.livemaster.sync;

import ru.opresent.core.domain.Product;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;

import java.util.List;

/**
 * User: artem
 * Date: 22.03.15
 * Time: 18:23
 */
public interface LivemasterSynchronizer {

    List<LivemasterSyncProduct> determineSyncProducts();

    LivemasterSyncProductResult processSyncProduct(LivemasterSyncProduct syncProduct, LivemasterSyncConfig config);

    Product processNotActualLivemasterId(String livemasterId);

}
