package ru.opresent.core.livemaster.sync;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.util.FormatUtils;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * User: artem
 * Date: 30.11.14
 * Time: 16:53
 */
@Component("livemasterConnector")
public class LivemasterConnector {

    private static final int CONNECTION_TIMEOUT_MILLIS = 30 * 1000;

    @Resource(name = "log")
    private Logger log;

    public Document loadDocument(String url) {
        // Чтобы не палиться перед сервером livemaster'a
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            throw new CoreException(e);
        }

        String logMsg = "Load document [url: " + FormatUtils.quotes(url) + "]";
        try {
            log.debug(logMsg + "...");
            Document document = createConnection(url).get();
            log.debug(logMsg + " complete");
            return document;
        } catch (IOException e) {
            log.error(logMsg + " failed", e);
            throw new CoreException(e);
        }
    }

    public byte[] loadImage(String imageUrl, String format) {
        try {
            BufferedImage img = ImageIO.read(new URL(imageUrl));
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(img, format, baos);
                baos.flush();
                return baos.toByteArray();
            }
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }

    private Connection createConnection(String url) {
        Connection connection = Jsoup.connect(url).timeout(CONNECTION_TIMEOUT_MILLIS);
        Connection.Request request = connection.request();

        request.data(HttpConnection.KeyVal.create(
                "Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"))

                .data(HttpConnection.KeyVal.create(
                "Accept-Encoding",
                "gzip, deflate"))

                .data(HttpConnection.KeyVal.create(
                "Accept-Language",
                "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"))

                .data(HttpConnection.KeyVal.create(
                "Connection",
                "keep-alive"))

                .data(HttpConnection.KeyVal.create(
                "blog_view_interface",
                "0"))

                .data(HttpConnection.KeyVal.create(
                        "Host",
                        LivemasterConstants.HOST))

//        request.data(HttpConnection.KeyVal.create(
//                "Cookie",
//                "__utma=15759285.313811372.1369512556.1395867166.1397309770.208; __utmz=15759285.1369512556.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _chartbeat2=22kpgmsoe00xm6j2.1369512560306.1397316167095.0001000110000001; wooTracker=lYHllAsn4WSy; __cfduid=dfa6601f62b99191e0bd1f08f53143aec1373718721974; _chartbeat_uuniq=3; _ga=GA1.2.313811372.1369512556; __utmb=15759285.226.10.1397309770; __utmc=15759285; _cb_ls=1; PHPSESSID=9vnlcrskd1jgigkbp4pics90a4"));

                .data(HttpConnection.KeyVal.create(
                "User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0"))

                .cookie("PerPageCount", "6"); // 6 * 20 = 120 products for page

        return connection;
    }

}
