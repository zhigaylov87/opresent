package ru.opresent.core.livemaster.page_resolver;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.util.HtmlUtils;
import ru.opresent.core.CoreException;

import java.text.MessageFormat;

/**
 * User: artem
 * Date: 11.09.16
 * Time: 14:40
 */
class ResolveUtils {

    private ResolveUtils() {
    }

    public static int resolvePrice(String priceStr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < priceStr.length(); i++) {
            char ch = priceStr.charAt(i);
            if (Character.isDigit(ch)) {
                sb.append(ch);
            }
        }
        return Integer.parseInt(sb.toString());
    }

    public static boolean hasSingleElement(Element source, String cssQuery) {
        return source.select(cssQuery).size() == 1;
    }

    public static Element selectSingleElement(Element source, String cssQuery) {
        Elements elements = source.select(cssQuery);
        if (elements.isEmpty()) {
            throw new IllegalArgumentException(MessageFormat.format(
                    "Elements is empty. Css query: ''{0}'', source:\n''{1}''", cssQuery, abbreviate(source.toString())));
        }
        if (elements.size() > 1) {
            throw new IllegalArgumentException(MessageFormat.format(
                    "More than one element. Css query: ''{0}'', source:\n''{1}''.", cssQuery, abbreviate(elements.toString())));
        }
        return elements.get(0);
    }

    public static Element selectSingleContainerContainsElement(Element source, String containerCssQuery, String elementCssQuery) {
        Element targetContainer = null;
        for (Element container : source.select(containerCssQuery)) {
            if (!container.select(elementCssQuery).isEmpty()) {
                if (targetContainer != null) {
                    throw new CoreException(MessageFormat.format(
                            "More than one container that contains element. " +
                                    "Container css query: ''{0}'', element css query: ''{1}'', source:\n''{2}''.",
                            containerCssQuery, elementCssQuery, abbreviate(source.toString())));
                }
                targetContainer = container;
            }
        }
        if (targetContainer == null) {
            throw new CoreException(MessageFormat.format(
                    "Element not found in any container. " +
                            "Container css query: ''{0}'', element css query: ''{1}'', source:\n''{2}''.",
                    containerCssQuery, elementCssQuery, abbreviate(source.toString())));
        }
        return targetContainer;
    }

    public static String replaceNbsp(String s) {
        return s.replace(HtmlUtils.htmlUnescape("&nbsp;"), " ");
    }

    private static String abbreviate(String s) {
        return StringUtils.abbreviate(s, 300);
    }
}
