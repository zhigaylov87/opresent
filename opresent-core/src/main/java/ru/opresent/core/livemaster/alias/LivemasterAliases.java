package ru.opresent.core.livemaster.alias;

import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.validation.constraint.LivemasterAliasesConstraint;

import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 30.11.14
 * Time: 22:03
 */
@LivemasterAliasesConstraint
public class LivemasterAliases implements Identifiable, Serializable {

    private long entityId = Identifiable.NOT_SAVED_ID;
    private LivemasterAliasType type;
    private Set<String> aliases = new HashSet<>();

    public LivemasterAliases(long entityId, LivemasterAliasType type) {
        this.entityId = entityId;
        this.type = Objects.requireNonNull(type);
    }

    public LivemasterAliases(LivemasterAliasType type) {
        this.type = Objects.requireNonNull(type);
    }

    public LivemasterAliases(LivemasterAliases other) {
        this.entityId = other.entityId;
        this.type = other.type;
        addAllAliases(other.getAliases());
    }

    @Override
    public long getId() {
        return entityId;
    }

    public void setId(long entityId) {
        this.entityId = entityId;
    }

    public LivemasterAliasType getType() {
        return type;
    }

    public void addAlias(String alias) {
        if (aliases.contains(alias)) {
            throw new IllegalArgumentException("Alias '" + alias + "' already exists!");
        }
        aliases.add(alias);
    }

    public void addAllAliases(Collection<String> aliases) {
        for (String alias : aliases) {
            addAlias(alias);
        }
    }

    public void removeAlias(String alias) {
        if (!aliases.contains(alias)) {
            throw new IllegalArgumentException("Alias '" + alias + "' not exists!");
        }
        aliases.remove(alias);
    }

    public Set<String> getAliases() {
        return Collections.unmodifiableSet(aliases);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LivemasterAliases)) return false;

        LivemasterAliases that = (LivemasterAliases) o;
        return entityId == that.entityId && type == that.type;
    }

    @Override
    public int hashCode() {
        int result = (int) (entityId ^ (entityId >>> 32));
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("LivemasterAliases {")
                .append("entityId: ").append(entityId)
                .append(", type: ").append(type)
                .append(", aliases: [");

        List<String> aliases = new ArrayList<>(this.aliases);
        Collections.sort(aliases);
        boolean first = true;
        for (String alias : aliases) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append("'").append(alias).append("'");
        }
        sb.append("]}");

        return sb.toString();
    }
}
