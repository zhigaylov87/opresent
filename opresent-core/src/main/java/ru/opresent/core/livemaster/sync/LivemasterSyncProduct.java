package ru.opresent.core.livemaster.sync;

import org.apache.commons.lang3.builder.ToStringBuilder;
import ru.opresent.core.domain.ProductStatus;

import java.util.Objects;

/**
 * User: artem
 * Date: 06.12.14
 * Time: 17:01
 */
public class LivemasterSyncProduct {

    private String livemasterProductId;
    private String productPageUrl;
    private ProductStatus productStatus;
    private Integer productPrice;

    public LivemasterSyncProduct(String livemasterProductId, String productPageUrl,
                                 ProductStatus productStatus, Integer productPrice) {
        this.livemasterProductId = Objects.requireNonNull(livemasterProductId);
        this.productPageUrl = Objects.requireNonNull(productPageUrl);
        this.productStatus = Objects.requireNonNull(productStatus);
        this.productPrice = productStatus != ProductStatus.EXAMPLE ? Objects.requireNonNull(productPrice) : productPrice;
    }

    public String getLivemasterProductId() {
        return livemasterProductId;
    }

    public String getProductPageUrl() {
        return productPageUrl;
    }

    public ProductStatus getProductStatus() {
        return productStatus;
    }

    public Integer getProductPrice() {
        return productPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LivemasterSyncProduct)) return false;

        LivemasterSyncProduct that = (LivemasterSyncProduct) o;

        return livemasterProductId.equals(that.livemasterProductId);
    }

    @Override
    public int hashCode() {
        return livemasterProductId.hashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("livemasterProductId", livemasterProductId).
                append("productPageUrl", productPageUrl).
                append("productStatus", productStatus).
                append("productPrice", productPrice).
                toString();
    }
}
