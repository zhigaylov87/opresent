package ru.opresent.core.livemaster.sync;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.*;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.livemaster.page_resolver.LivemasterPageResolverFactory;
import ru.opresent.core.livemaster.page_resolver.LivemasterProductListPageResolver;
import ru.opresent.core.livemaster.page_resolver.LivemasterProductPageResolver;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;
import ru.opresent.core.service.api.*;
import ru.opresent.core.util.FormatUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 *
 *
 * User: artem
 * Date: 06.12.14
 * Time: 22:54
 */
@Component("livemasterSynchronizer")
public class DefaultLivemasterSynchronizer implements LivemasterSynchronizer {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "imageService")
    private ImageService imageService;

    @Resource(name = "livemasterPageResolverFactory")
    private LivemasterPageResolverFactory pageResolverFactory;

    @Resource(name = "livemasterConnector")
    private LivemasterConnector connector;

    @Resource(name = "livemasterAliasService")
    private LivemasterAliasService livemasterAliasService;

    @Resource(name = "productKeywordThesarusService")
    private ProductKeywordThesarusService productKeywordThesarusService;

    @Override
    public List<LivemasterSyncProduct> determineSyncProducts() {
        List<LivemasterSyncProduct> syncProducts = new ArrayList<>();

        for (ProductStatus productStatus : ProductStatus.values()) {
            String firstProductListPageUrl =
                    LivemasterConstants.FIRST_PRODUCT_LIST_PAGE_URL +
                            LivemasterConstants.PRODUCT_STATUS_URL_CODES.get(productStatus);
            log.info("Getting products with status " + productStatus +
                    ". First product list page: " + FormatUtils.quotes(firstProductListPageUrl));

            Document firstProductListPage = connector.loadDocument(firstProductListPageUrl);
            LivemasterProductListPageResolver productListPageResolver = pageResolverFactory.getProductListPageResolver(firstProductListPage);
            log.info("Product list page resolver version: " + productListPageResolver.getVersion());
            List<String> paginationUrls = productListPageResolver.getPaginationUrls(firstProductListPage);
            log.info("Pagination urls: " + paginationUrls);

            List<LivemasterSyncProduct> pageSyncProducts = productListPageResolver.getSyncProducts(firstProductListPage, productStatus);
            log.info("First page products count: " + pageSyncProducts.size());
            syncProducts.addAll(pageSyncProducts);

            for (String url : paginationUrls) {
                String logMsg = "Get sync products [url: " + FormatUtils.quotes(url) + "]";
                log.info(logMsg + "...");
                pageSyncProducts = productListPageResolver.getSyncProducts(connector.loadDocument(url), productStatus);
                log.info(logMsg + " complete. Count: " + pageSyncProducts.size());
                syncProducts.addAll(pageSyncProducts);
            }
        }

        return syncProducts;
    }

    @Transactional
    @Override
    public LivemasterSyncProductResult processSyncProduct(LivemasterSyncProduct syncProduct, LivemasterSyncConfig config) {
        Product existsProduct = productService.loadByLivemasterId(syncProduct.getLivemasterProductId());

        if (existsProduct != null) {
            ProductStatus newStatus = syncProduct.getProductStatus();
            Integer newPrice = calcNewPrice(syncProduct.getProductPrice(), existsProduct.getType(), config.getProductTypeIdToPriceDiff());
            ProductStatus oldStatus = existsProduct.getStatus();
            Integer oldPrice = existsProduct.getPrice();
            if (newStatus != oldStatus) {
                existsProduct.setStatus(newStatus);
                boolean updatePrice = !Objects.equals(oldPrice, newPrice);
                if (updatePrice) {
                    existsProduct.setPrice(newPrice);
                }
                existsProduct = productService.save(existsProduct);
                return updatePrice ?
                        LivemasterSyncProductResult.createProductStatusAndPriceUpdatedResult(existsProduct, oldStatus, oldPrice) :
                        LivemasterSyncProductResult.createProductStatusUpdatedResult(existsProduct, oldStatus);
            } else if (config.isForceSyncPrice() && !Objects.equals(oldPrice, newPrice)) {
                existsProduct.setPrice(newPrice);
                existsProduct = productService.save(existsProduct);
                return LivemasterSyncProductResult.createProductPriceUpdatedResult(existsProduct, oldPrice);
            } else {
                return LivemasterSyncProductResult.createNothingDoneResult(existsProduct);
            }
        } else if (productService.isDeletedWithLivemasterId(syncProduct.getLivemasterProductId())){
            return LivemasterSyncProductResult.createNothingDoneForDeletedResult();
        } else {
            Document productPage = connector.loadDocument(syncProduct.getProductPageUrl());
            Pair<Product, List<LivemasterSyncProductWarning>> productAndWarnings;
            try {
                productAndWarnings = resolveProduct(productPage);
            } catch (Exception e) {
                storeNotResolvedProductPage(syncProduct, productPage);
                throw new CoreException(e);
            }

            Product newProduct = productAndWarnings.getLeft();
            newProduct.setLivemasterId(syncProduct.getLivemasterProductId());
            newProduct.setPrice(calcNewPrice(newProduct.getPrice(), newProduct.getType(), config.getProductTypeIdToPriceDiff()));
            if (log.isDebugEnabled()) {
                log.debug(newProduct.toString());
            }
            newProduct = productService.save(newProduct);
            return LivemasterSyncProductResult.createProductAddedResult(newProduct, productAndWarnings.getRight());
        }
    }

    private Integer calcNewPrice(Integer livemasterPrice, ProductType productType, Map<Long, Integer> productTypeToPriceDiff) {
        if (livemasterPrice == null) {
            return null;
        }
        if (!productTypeToPriceDiff.containsKey(productType.getId())) {
            return livemasterPrice;
        }
        int priceDiff = productTypeToPriceDiff.get(productType.getId());
        return livemasterPrice + priceDiff;
    }

    @Override
    public Product processNotActualLivemasterId(String livemasterId) {
        Product product = productService.loadByLivemasterId(livemasterId);
        product.setStatus(ProductStatus.EXAMPLE);
        product.setPrice(null);
        product.setLivemasterId(null);
        return productService.save(product);
    }

    private Pair<Product, List<LivemasterSyncProductWarning>> resolveProduct(Document productPage) {
        LivemasterProductPageResolver productPageResolver = pageResolverFactory.getProductPageResolver(productPage);
        log.info("Product page resolver version: " + productPageResolver.getVersion());
        Product product = new Product();
        List<LivemasterSyncProductWarning> warnings = new ArrayList<>();

        String productTypeAlias = productPageResolver.getProductTypeAlias(productPage);
        Long productTypeId = getEntityIdByAlias(livemasterAliasService.load(LivemasterAliasType.PRODUCT_TYPE), productTypeAlias);
        if (productTypeId == null) {
            throw new CoreException("Product type for alias '" + productTypeAlias + "' not found");
        }
        product.setType(productTypeService.getAll().getById(productTypeId));

        product.setName(productPageResolver.getName(productPage));
        product.setDescription(productPageResolver.getDescription(productPage));
        product.setVisible(false);
        product.setStatus(productPageResolver.getStatus(productPage));
        if (product.getStatus() != ProductStatus.EXAMPLE) {
            product.setPrice(productPageResolver.getPrice(productPage));
        }

        Set<Category> categories = new HashSet<>();
        Set<LivemasterAliases> categoryAliases = livemasterAliasService.load(LivemasterAliasType.CATEGORY);
        for (String categoryAlias : productPageResolver.getCategoriesAliases(productPage)) {
            Long categoryId = getEntityIdByAlias(categoryAliases, categoryAlias);
            if (categoryId != null) {
                categories.add(categoryService.getAll().getById(categoryId));
            }
        }
        product.setCategories(categories);

        ResolveImagesResult resolveImagesResult = resolveAndSaveImages(productPage, productPageResolver);
        product.setPreview(resolveImagesResult.preview);
        product.setViews(resolveImagesResult.views);
        warnings.addAll(resolveImagesResult.warnings);

        product.setSizeText(productPageResolver.getSizeText(productPage));
        product.setProductionTime(productPageResolver.getProductionTime(productPage));

        Set<ProductKeyword> keywords = new HashSet<>();
        for (String keyword : productPageResolver.getKeywords(productPage)) {
            keywords.add(new ProductKeyword(keyword));
        }
        product.setKeywords(keywords);
        for (ProductKeyword keyword : keywords) {
            if (!productKeywordThesarusService.getAll().contains(keyword)) {
                productKeywordThesarusService.add(keyword);
            }
        }

        return Pair.of(product, warnings);
    }

    /**
     * @param productPage productPage
     * @return saved preview and saved views
     */
    private ResolveImagesResult resolveAndSaveImages(Document productPage, LivemasterProductPageResolver productPageResolver) {
        ResolveImagesResult result = new ResolveImagesResult();
        boolean firstImage = true;
        for (String imageUrl : productPageResolver.getImagesUrls(productPage)) {
            int lastDotIndex = imageUrl.lastIndexOf(".");
            if (lastDotIndex == StringUtils.INDEX_NOT_FOUND) {
                throw new CoreException("There is no dot in image url '" + imageUrl + "'");
            }
            String format = imageUrl.substring(lastDotIndex + 1, imageUrl.length());
            byte[] content;
            try {
                content = connector.loadImage(imageUrl, format);
            } catch (Exception e) {
                log.warn("Error while reading image. Url: '" + imageUrl + "', format: '" + format + "'", e);
                result.warnings.add(LivemasterSyncProductWarning.createMissedImageWarning(imageUrl, e));
                continue;
            }

            if (firstImage) {
                Image image = new Image(format, ImageUse.PRODUCT_PREVIEW_IMAGE, HasCopyrightOption.INHERIT_FROM_IMAGE_USE);
                image.setContent(content);
                result.preview = imageService.save(image);
            }

            Image image = new Image(format, ImageUse.PRODUCT_VIEW_IMAGE, HasCopyrightOption.INHERIT_FROM_IMAGE_USE);
            image.setContent(content);
            result.views.add(imageService.save(image));

            firstImage = false;
        }

        if (result.preview == null) {
            throw new CoreException("Preview is null!");
        }
        if (result.views.isEmpty()) {
            throw new CoreException("Views is empty!");
        }

        return result;
    }

    private static class ResolveImagesResult {
        Image preview;
        List<Image> views = new ArrayList<>();
        List<LivemasterSyncProductWarning> warnings = new ArrayList<>();
    }

    private Long getEntityIdByAlias(Set<LivemasterAliases> entityAliases, String alias) {
        String lowerCaseAlias = alias.toLowerCase();
        String matchAlias = null;
        Long matchAliasId = null;

        for (LivemasterAliases aliases : entityAliases) {
            for (String comparingAlias : aliases.getAliases()) {
                if (lowerCaseAlias.contains(comparingAlias.toLowerCase())) {
                    if (matchAliasId != null) {
                        throw new CoreException("More than one matching for alias '" + alias + "' - '" +
                                matchAlias + "' and '" + comparingAlias + "'");
                    }
                    matchAlias = comparingAlias;
                    matchAliasId = aliases.getId();
                }
            }
        }
        return matchAliasId;
    }

    private void storeNotResolvedProductPage(LivemasterSyncProduct syncProduct, Document productPage) {
        try {
            File tmp = File.createTempFile(syncProduct.getLivemasterProductId() + "_" + System.currentTimeMillis(), null);
            FileUtils.write(tmp, productPage.toString(), StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            log.error("Error of store not resolved product page. Livemaster id: " + syncProduct.getLivemasterProductId(), e);
            // don't rethrow
        }
    }

}
