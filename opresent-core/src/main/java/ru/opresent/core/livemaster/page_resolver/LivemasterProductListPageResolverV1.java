package ru.opresent.core.livemaster.page_resolver;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.livemaster.sync.LivemasterConstants;
import ru.opresent.core.livemaster.sync.LivemasterSyncProduct;

import java.util.ArrayList;
import java.util.List;

import static ru.opresent.core.livemaster.page_resolver.ResolveUtils.resolvePrice;
import static ru.opresent.core.livemaster.page_resolver.ResolveUtils.selectSingleElement;

/**
 * User: artem
 * Date: 11.09.16
 * Time: 18:00
 */
class LivemasterProductListPageResolverV1 implements LivemasterProductListPageResolver {

    @Override
    public String getVersion() {
        return "v1";
    }

    @Override
    public List<String> getPaginationUrls(Document productListPage) {
        List<String> urls = new ArrayList<>();
        for (Element paginationLink : productListPage.select("table.pagebar td:not(.pagebar-nav) a")) {
            urls.add(LivemasterConstants.HOST_URL + "/" + paginationLink.attr("href"));
        }
        return urls;
    }

    @Override
    public List<LivemasterSyncProduct> getSyncProducts(Document productListPage, ProductStatus productStatus) {
        List<LivemasterSyncProduct> syncProducts = new ArrayList<>();

        Elements productItems = productListPage.select("div#b-middle div.grid-item div.b-item");
        for (Element productItem : productItems) {
            // For example: item123
            String productLivemasterItemId = productItem.attr("id");
            String productLivemasterId = productLivemasterItemId.substring("item".length());

            Element productLinkElement = selectSingleElement(productItem, "div.title a");
            // For example: /item/123-bla-bla-bla
            String productRelativeUrl = productLinkElement.attr("href");
            String productUrl = LivemasterConstants.HOST_URL + "/" + productRelativeUrl;

            Integer price = null;
            if (productStatus != ProductStatus.EXAMPLE) {
                String priceStr = selectSingleElement(productItem, "div.cost span.price").ownText();
                price = resolvePrice(priceStr);
            }

            syncProducts.add(new LivemasterSyncProduct(productLivemasterId, productUrl, productStatus, price));
        }

        return syncProducts;
    }
}
