package ru.opresent.core.livemaster.sync.event;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * User: artem
 * Date: 23.04.2017
 * Time: 0:57
 */
public enum LivemasterSyncEventType {

    SYNC_START,

    SYNC_PRODUCTS_DETERMINING_START,
    SYNC_PRODUCTS_DETERMINING_COMPLETE(LivemasterSyncEventDataField.COUNT),
    SYNC_PRODUCTS_DETERMINING_FAILED(LivemasterSyncEventDataField.EXCEPTION),

    SYNC_PRODUCTS_PROCESSING_START,
    SYNC_PRODUCT_PROCESSED(LivemasterSyncEventDataField.SYNC_PRODUCT, LivemasterSyncEventDataField.SYNC_PRODUCT_RESULT),
    SYNC_PRODUCTS_PROCESSING_COMPLETE,

    NOT_ACTUAL_LIVEMASTER_IDS_PROCESSING_START,
    NOT_ACTUAL_LIVEMASTER_IDS_DETERMINED(LivemasterSyncEventDataField.COUNT),
    NOT_ACTUAL_LIVEMASTER_ID_PROCESSED(LivemasterSyncEventDataField.LIVEMASTER_ID, LivemasterSyncEventDataField.PRODUCT),
    NOT_ACTUAL_LIVEMASTER_ID_PROCESSING_COMPLETE,

    SYNC_STOPPED,
    SYNC_COMPLETE;

    private Set<LivemasterSyncEventDataField> dataFields;

    LivemasterSyncEventType(LivemasterSyncEventDataField... dataFields) {
        this.dataFields = ImmutableSet.copyOf(dataFields);
    }

    public Set<LivemasterSyncEventDataField> getDataFields() {
        return dataFields;
    }
}
