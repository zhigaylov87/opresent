package ru.opresent.core.livemaster.alias;

/**
 * User: artem
 * Date: 30.11.14
 * Time: 22:03
 */
public enum LivemasterAliasType {

    PRODUCT_TYPE(0),
    CATEGORY(1);

    private int code;

    private LivemasterAliasType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static LivemasterAliasType valueOf(int code) {
        for (LivemasterAliasType type : values()) {
            if (type.code == code) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown livemaster alias type code: " + code);
    }
}
