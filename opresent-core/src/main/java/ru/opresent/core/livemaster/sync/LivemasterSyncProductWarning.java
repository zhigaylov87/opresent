package ru.opresent.core.livemaster.sync;

import org.apache.commons.lang3.StringUtils;
import ru.opresent.core.util.FormatUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * User: artem
 * Date: 21.03.15
 * Time: 23:46
 */
public class LivemasterSyncProductWarning implements Serializable {

    private Type type;
    private String missedImageUrl;
    private Exception exception;

    private LivemasterSyncProductWarning() {
    }

    public static LivemasterSyncProductWarning createMissedImageWarning(String missedImageUrl, Exception exception) {
        LivemasterSyncProductWarning warning = new LivemasterSyncProductWarning();

        warning.type = Type.MISSED_IMAGE;
        if (StringUtils.isEmpty(missedImageUrl)) {
            throw new IllegalArgumentException("Missed image url is empty: " + FormatUtils.quotes(missedImageUrl));
        }
        warning.missedImageUrl = missedImageUrl;
        warning.exception = Objects.requireNonNull(exception, "Missed image exception is null");

        return warning;
    }

    public Type getType() {
        return type;
    }

    public String getMissedImageUrl() {
        return missedImageUrl;
    }

    public Exception getException() {
        return exception;
    }

    public enum Type {
        MISSED_IMAGE
    }

}
