package ru.opresent.core.livemaster.sync.event;

import ru.opresent.core.domain.Product;
import ru.opresent.core.livemaster.sync.LivemasterSyncProduct;
import ru.opresent.core.livemaster.sync.LivemasterSyncProductResult;

/**
 * User: artem
 * Date: 23.04.2017
 * Time: 0:59
 */
public enum LivemasterSyncEventDataField {

    COUNT(Integer.class),

    EXCEPTION(Exception.class),

    SYNC_PRODUCT(LivemasterSyncProduct.class),

    SYNC_PRODUCT_RESULT(LivemasterSyncProductResult.class),

    LIVEMASTER_ID(String.class),

    PRODUCT(Product.class);

    private Class dataType;

    LivemasterSyncEventDataField(Class dataType) {
        this.dataType = dataType;
    }

    public Class getDataType() {
        return dataType;
    }
}
