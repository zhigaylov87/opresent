package ru.opresent.core.livemaster.sync.event;

import com.google.common.collect.ImmutableMap;

import java.util.Map;
import java.util.Objects;

/**
 * User: artem
 * Date: 23.04.2017
 * Time: 0:28
 */
public class LivemasterSyncEvent {

    private LivemasterSyncEventType type;
    private Map<LivemasterSyncEventDataField, Object> data;

    private LivemasterSyncEvent(LivemasterSyncEventType type, Map<LivemasterSyncEventDataField, Object> data) {
        this.type = Objects.requireNonNull(type);

        if (data.size() != type.getDataFields().size()) {
            throw new IllegalArgumentException("Illegal data size: " + data.size() +
                    ". Expected data size: " + type.getDataFields().size());
        }
        for (LivemasterSyncEventDataField field : type.getDataFields()) {
            Object fieldData = data.get(field);
            if (!field.getDataType().isAssignableFrom(fieldData.getClass())) {
                throw new IllegalArgumentException("Illegal data type for field " + field + ": " + field.getDataType() +
                        " is not assignable from " + fieldData.getClass());
            }
        }
        this.data = data;
    }

    public static LivemasterSyncEvent of(LivemasterSyncEventType type) {
        return new LivemasterSyncEvent(type, ImmutableMap.<LivemasterSyncEventDataField, Object>of());
    }

    public static LivemasterSyncEvent of(LivemasterSyncEventType type,
                                         LivemasterSyncEventDataField field1, Object fieldData1) {
        return new LivemasterSyncEvent(type, ImmutableMap.of(field1, fieldData1));
    }

    public static LivemasterSyncEvent of(LivemasterSyncEventType type,
                                         LivemasterSyncEventDataField field1, Object fieldData1,
                                         LivemasterSyncEventDataField field2, Object fieldData2) {
        return new LivemasterSyncEvent(type, ImmutableMap.of(field1, fieldData1, field2, fieldData2));
    }

    public LivemasterSyncEventType getType() {
        return type;
    }

    public Map<LivemasterSyncEventDataField, Object> getData() {
        return data;
    }

    @SuppressWarnings("unchecked")
    public <T> T getField(LivemasterSyncEventDataField dataField) {
        return (T) data.get(dataField);
    }
}
