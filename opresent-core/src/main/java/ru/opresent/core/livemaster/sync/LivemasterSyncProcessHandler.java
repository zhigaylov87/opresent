package ru.opresent.core.livemaster.sync;

import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventListener;

import java.io.Serializable;
import java.util.Objects;

/**
 * User: artem
 * Date: 15.04.2017
 * Time: 21:19
 */
public class LivemasterSyncProcessHandler implements Serializable {

    private LivemasterSyncEventListener eventListener;
    private boolean syncStopped = false;

    public LivemasterSyncProcessHandler(LivemasterSyncEventListener eventListener) {
        this.eventListener = Objects.requireNonNull(eventListener);
    }

    public LivemasterSyncEventListener getEventListener() {
        return eventListener;
    }

    public boolean isSyncStopped() {
        return syncStopped;
    }

    public void setSyncStopped(boolean syncStopped) {
        this.syncStopped = syncStopped;
    }
}
