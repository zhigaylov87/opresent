package ru.opresent.core.livemaster.page_resolver;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.ProductStatus;

import java.util.ArrayList;
import java.util.List;

import static ru.opresent.core.livemaster.page_resolver.ResolveUtils.*;

/**
 * User: artem
 * Date: 11.09.16
 * Time: 16:41
 */
class LivemasterProductPageResolverV3 implements LivemasterProductPageResolver {

    @Override
    public String getVersion() {
        return "v3";
    }

    @Override
    public String getProductTypeAlias(Document productPage) {
        String alias = selectSingleElement(productPage, "div.item-page-filter:first-child a.item-page-filter-link.black").text();
        return replaceNbsp(alias);
    }

    @Override
    public List<String> getCategoriesAliases(Document productPage) {
        List<String> aliases = new ArrayList<>();
        for (Element element : productPage.select("div.item-page-filter:last-child a.item-page-filter-link.black")) {
            aliases.add(replaceNbsp(element.text()));
        }
        return aliases;
    }

    @Override
    public String getName(Document productPage) {
        return selectSingleElement(productPage, "h1.item-page-item-name span[itemprop=name]").text();
    }

    @Override
    public String getDescription(Document productPage) {
        // .text() skip all html elements - only text in result
        return selectSingleElement(productPage, "div.item-page-desc-block--first").text();
    }

    @Override
    public ProductStatus getStatus(Document productPage) {
        String statusText = selectSingleElement(productPage, "ul.item-page-item-info-list li:first-child").text();
        statusText = replaceNbsp(statusText).trim();

        if ("Готовая работа".equals(statusText)) {
            return ProductStatus.AVAILABLE;
        }
        if (statusText.startsWith("Работа на заказ")) {
            return ProductStatus.FOR_ORDER;
        }
        if ("Работа представлена для примера".equals(statusText)) {
            return ProductStatus.EXAMPLE;
        }

        throw new CoreException("Can not to resolve status. Status text: '" + statusText + "'");
    }

    @Override
    public int getPrice(Document productPage) {
        String priceStr = selectSingleElement(productPage, "div.item-page-item-price span.price").ownText();
        return resolvePrice(priceStr);
    }

    @Override
    public List<String> getImagesUrls(Document productPage) {
        List<String> urls = new ArrayList<>();
        for (Element element : productPage.select("div.item-page-photo-slides a.js-start-slide")) {
            urls.add(element.attr("href"));
        }
        return urls;
    }

    @Override
    public String getSizeText(Document productPage) {
        String sizeText = selectSingleElement(productPage, "ul.item-page-item-info-list li:last-child").text();
        if (!sizeText.startsWith("Размер:")) {
            throw new CoreException("Can not to resolve size. Size text: '" + sizeText + "'");
        }
        sizeText = sizeText.substring(sizeText.indexOf(":") + 1);
        return StringUtils.trimToNull(replaceNbsp(sizeText));
    }

    @Override
    public String getProductionTime(Document productPage) {
        for (Element element : productPage.select("ul.item-page-item-info-list li")) {
            String text = element.text();
            if (text.startsWith("Срок изготовления:")) {
                text = text.substring(text.indexOf(":") + 1);
                return StringUtils.trimToNull(replaceNbsp(text));
            }
        }
        return null;
    }

    @Override
    public List<String> getKeywords(Document productPage) {
        List<String> keywords = new ArrayList<>();
        Elements keywordsElements = productPage.select("div.item-page-desc-block--tags p.item-page-desc-block-paragraph a");
        for (Element keywordElement : keywordsElements) {
            keywords.add(keywordElement.text());
        }
        return keywords;
    }

}
