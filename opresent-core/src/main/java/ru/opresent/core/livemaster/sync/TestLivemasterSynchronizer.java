package ru.opresent.core.livemaster.sync;

import org.springframework.stereotype.Component;
import ru.opresent.core.domain.DefaultProductFilter;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.pagination.SinglePageSettings;
import ru.opresent.core.service.api.ProductService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * TODO: create unit or integration test instead this component
 *
 * User: artem
 * Date: 22.04.2017
 * Time: 15:00
 */
@Component("testLivemasterSynchronizer")
public class TestLivemasterSynchronizer implements LivemasterSynchronizer {

    @Resource(name = "productService")
    private ProductService productService;

    @Override
    public List<LivemasterSyncProduct> determineSyncProducts() {
        List<LivemasterSyncProduct> syncProducts = new ArrayList<>();

        ProductPage productPage = productService.load(new DefaultProductFilter(), new SinglePageSettings());
        for (Product product : productPage.getItems()) {
            syncProducts.add(new LivemasterSyncProduct(
                    product.getLivemasterId(),
                    "url-for-" + product.getLivemasterId(),
                    product.getStatus(),
                    product.getPrice()));
        }
        int notActualLivemasterIdsCount = new Random().nextInt(syncProducts.size() / 5);
        // remove some products for notActualLivemasterIds
        for (int i = 0; i < notActualLivemasterIdsCount; i++) {
            syncProducts.remove(syncProducts.size() - 1);
        }

        return syncProducts;
    }

    private static int resultTypeIndex = 0;
    private static boolean withWarnings = false;

    @Override
    public LivemasterSyncProductResult processSyncProduct(LivemasterSyncProduct syncProduct, LivemasterSyncConfig config) {
        Product product = productService.loadByLivemasterId(syncProduct.getLivemasterProductId());

        int newStatusOrdinal = product.getStatus().ordinal() + 1;
        newStatusOrdinal = newStatusOrdinal >= ProductStatus.values().length - 1 ? 0 : newStatusOrdinal;
        ProductStatus newStatus = ProductStatus.values()[newStatusOrdinal];

        int newPrice = product.getPrice() == null ? 123 : syncProduct.getProductPrice() + 123;

        LivemasterSyncProductResult.Type[] resultTypes = LivemasterSyncProductResult.Type.values();
        resultTypeIndex = resultTypeIndex >= resultTypes.length - 1 ? 0 : resultTypeIndex + 1;
        LivemasterSyncProductResult.Type resultType = resultTypes[resultTypeIndex];

        switch (resultType) {
            case PRODUCT_ADDED:
                List<LivemasterSyncProductWarning> warnings = null;
                if (withWarnings) {
                    warnings = new ArrayList<>();
                    for (int i = 0; i < new Random().nextInt(5); i++) {
                        Exception exception;
                        try {
                            throw new RuntimeException("Missed image url " + i);
                        } catch (Exception e) {
                            exception = e;
                        }
                        LivemasterSyncProductWarning warning = LivemasterSyncProductWarning.createMissedImageWarning(
                                "missed_image_url_" + i, exception);
                        warnings.add(warning);
                    }

                    withWarnings = false;
                } else {
                    withWarnings = true;
                }

                return LivemasterSyncProductResult.createProductAddedResult(product, warnings);

            case PRODUCT_STATUS_UPDATED:
                return LivemasterSyncProductResult.createProductStatusUpdatedResult(product, newStatus);

            case PRODUCT_STATUS_AND_PRICE_UPDATED:
                return LivemasterSyncProductResult.createProductStatusAndPriceUpdatedResult(product, newStatus, newPrice);

            case PRODUCT_PRICE_UPDATED:
                return LivemasterSyncProductResult.createProductPriceUpdatedResult(product, newPrice);

            case NOTHING_DONE:
                return LivemasterSyncProductResult.createNothingDoneResult(product);

            case NOTHING_DONE_FOR_DELETED:
                return LivemasterSyncProductResult.createNothingDoneForDeletedResult();

            case ERROR:
                Exception error;
                try {
                    throw new RuntimeException("Process Sync Product Error!!!");
                } catch (Exception e) {
                    error = e;
                }
                return LivemasterSyncProductResult.createErrorResult(error);

            default:
                throw new RuntimeException("Unsupported result type: " + resultType);
        }
    }

    @Override
    public Product processNotActualLivemasterId(String livemasterId) {
        return productService.loadByLivemasterId(livemasterId);
    }

}
