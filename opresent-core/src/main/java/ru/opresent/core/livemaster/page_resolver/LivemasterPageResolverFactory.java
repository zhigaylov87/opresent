package ru.opresent.core.livemaster.page_resolver;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

/**
 * User: artem
 * Date: 11.09.16
 * Time: 16:45
 */
@Component("livemasterPageResolverFactory")
public class LivemasterPageResolverFactory {

    public LivemasterProductListPageResolver getProductListPageResolver(Document productListPage) {
        return new LivemasterProductListPageResolverV1();
    }

    public LivemasterProductPageResolver getProductPageResolver(Document productPage) {
        if (!productPage.select(LivemasterProductPageResolverV4.H1_ITEM_HEADER).isEmpty()) {
            return new LivemasterProductPageResolverV4();
        }
        return new LivemasterProductPageResolverV3();
    }

}
