package ru.opresent.core.livemaster.page_resolver;

import org.jsoup.nodes.Document;
import ru.opresent.core.domain.ProductStatus;

import java.util.List;

/**
 * User: artem
 * Date: 21.03.15
 * Time: 19:38
 */
public interface LivemasterProductPageResolver {

    String getVersion();

    String getProductTypeAlias(Document productPage);

    List<String> getCategoriesAliases(Document productPage);

    String getName(Document productPage);

    String getDescription(Document productPage);

    ProductStatus getStatus(Document productPage);

    int getPrice(Document productPage);

    List<String> getImagesUrls(Document productPage);

    String getSizeText(Document productPage);

    String getProductionTime(Document productPage);

    List<String> getKeywords(Document productPage);
}
