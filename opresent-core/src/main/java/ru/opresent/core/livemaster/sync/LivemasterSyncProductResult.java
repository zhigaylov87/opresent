package ru.opresent.core.livemaster.sync;

import org.apache.commons.collections.CollectionUtils;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.util.stringify.EntityStringifier;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * User: artem
 * Date: 06.12.14
 * Time: 17:09
 */
public class LivemasterSyncProductResult implements Serializable {

    private Type type;
    private List<LivemasterSyncProductWarning> warnings;
    private Product product;
    private ProductStatus oldStatus;
    private Integer oldPrice;
    private Exception error;

    private LivemasterSyncProductResult() {
    }

    /**
     * @param product added product
     * @param warnings warnings or {@code null} or empty list if no warnings
     * @return result object
     */
    public static LivemasterSyncProductResult createProductAddedResult(
            Product product, List<LivemasterSyncProductWarning> warnings) {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.PRODUCT_ADDED;
        result.product = Objects.requireNonNull(product);
        result.warnings = CollectionUtils.isEmpty(warnings) ? null : Collections.unmodifiableList(warnings);
        return result;
    }

    public static LivemasterSyncProductResult createProductStatusUpdatedResult(Product product, ProductStatus oldStatus) {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.PRODUCT_STATUS_UPDATED;
        result.product = Objects.requireNonNull(product);
        result.oldStatus = Objects.requireNonNull(oldStatus);
        if (oldStatus == product.getStatus()) {
            throw new IllegalArgumentException("Status not changed: " + product.getStatus());
        }
        return result;
    }

    public static LivemasterSyncProductResult createProductStatusAndPriceUpdatedResult(Product product, ProductStatus oldStatus, Integer oldPrice) {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.PRODUCT_STATUS_AND_PRICE_UPDATED;
        result.product = Objects.requireNonNull(product);
        result.oldStatus = Objects.requireNonNull(oldStatus);
        if (oldStatus == product.getStatus()) {
            throw new IllegalArgumentException("Status not changed: " + product.getStatus());
        }
        result.oldPrice = oldPrice;
        if (Objects.equals(oldPrice, product.getPrice())) {
            throw new IllegalArgumentException("Price not changed: " + product.getPrice());
        }
        return result;
    }

    public static LivemasterSyncProductResult createProductPriceUpdatedResult(Product product, Integer oldPrice) {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.PRODUCT_PRICE_UPDATED;
        result.product = Objects.requireNonNull(product);
        result.oldPrice = oldPrice;
        if (Objects.equals(oldPrice, product.getPrice())) {
            throw new IllegalArgumentException("Price not changed: " + product.getPrice());
        }
        return result;
    }

    public static LivemasterSyncProductResult createNothingDoneResult(Product product) {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.NOTHING_DONE;
        result.product = Objects.requireNonNull(product);
        return result;
    }

    public static LivemasterSyncProductResult createNothingDoneForDeletedResult() {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.NOTHING_DONE_FOR_DELETED;
        return result;
    }

    public static LivemasterSyncProductResult createErrorResult(Exception error) {
        LivemasterSyncProductResult result = new LivemasterSyncProductResult();
        result.type = Type.ERROR;
        result.error = Objects.requireNonNull(error);
        return result;
    }

    public Type getType() {
        return type;
    }

    /**
     * @return warnings or {@code null} if no warnings(empty list never returned)
     */
    public List<LivemasterSyncProductWarning> getWarnings() {
        return warnings;
    }

    /**
     * @return sync product or {@code null} if {@link #getType()} equals {@link Type#ERROR}
     */
    public Product getProduct() {
        return product;
    }

    public ProductStatus getOldStatus() {
        return oldStatus;
    }

    public Integer getOldPrice() {
        return oldPrice;
    }

    public Exception getError() {
        return error;
    }

    public enum Type {
        PRODUCT_ADDED,
        PRODUCT_STATUS_UPDATED,
        PRODUCT_STATUS_AND_PRICE_UPDATED,
        PRODUCT_PRICE_UPDATED,
        NOTHING_DONE,
        NOTHING_DONE_FOR_DELETED,
        ERROR
    }

    @Override
    public String toString() {
        return new EntityStringifier(false)
                .add("type", type)
                .add("product", product)
                .add("oldStatus", oldStatus)
                .add("oldPrice", oldPrice)
                .add("error", error == null ? null : error.toString())
                .toString();
    }
}
