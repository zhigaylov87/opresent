package ru.opresent.core.livemaster.sync.config;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import ru.opresent.core.validation.constraint.LivemasterSyncConfigConstraint;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * User: artem
 * Date: 09.07.2017
 * Time: 0:28
 */
@LivemasterSyncConfigConstraint
public class LivemasterSyncConfig implements Serializable {

    private boolean forceSyncPrice = false;
    private Map<Long, Integer> productTypeIdToPriceDiff = new HashMap<>();

    public boolean isForceSyncPrice() {
        return forceSyncPrice;
    }

    public void setForceSyncPrice(boolean forceSyncPrice) {
        this.forceSyncPrice = forceSyncPrice;
    }

    public Map<Long, Integer> getProductTypeIdToPriceDiff() {
        return productTypeIdToPriceDiff;
    }

    public void setProductTypeIdToPriceDiff(Map<Long, Integer> productTypeIdToPriceDiff) {
        this.productTypeIdToPriceDiff = productTypeIdToPriceDiff;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                append("forceSyncPrice", forceSyncPrice).
                append("productTypeIdToPriceDiff", productTypeIdToPriceDiff).
                toString();
    }
}
