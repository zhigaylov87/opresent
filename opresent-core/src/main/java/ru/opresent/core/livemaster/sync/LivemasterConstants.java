package ru.opresent.core.livemaster.sync;

import com.google.common.collect.ImmutableMap;
import ru.opresent.core.domain.ProductStatus;

import java.util.Map;

/**
 * User: artem
 * Date: 11.09.16
 * Time: 17:47
 */
public interface LivemasterConstants {

    String HOST = "www.livemaster.ru";

    String HOST_URL = "http://" + HOST;

    String FIRST_PRODUCT_LIST_PAGE_URL = LivemasterConstants.HOST_URL + "/tigrrra?v=";

    Map<ProductStatus, Integer> PRODUCT_STATUS_URL_CODES = ImmutableMap.of(
            ProductStatus.AVAILABLE, 1,
            ProductStatus.FOR_ORDER, 2,
            ProductStatus.EXAMPLE, 3
    );

}
