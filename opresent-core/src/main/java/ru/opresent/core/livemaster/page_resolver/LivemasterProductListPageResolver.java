package ru.opresent.core.livemaster.page_resolver;

import org.jsoup.nodes.Document;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.livemaster.sync.LivemasterSyncProduct;

import java.util.List;

/**
 * User: artem
 * Date: 11.09.16
 * Time: 17:37
 */
public interface LivemasterProductListPageResolver {

    String getVersion();

    List<String> getPaginationUrls(Document productListPage);

    List<LivemasterSyncProduct> getSyncProducts(Document productListPage, ProductStatus productStatus);

}
