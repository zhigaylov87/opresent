package ru.opresent.web.cms.bean.incoming_message;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.message.incoming.IncomingMessageSubjectType;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.web.cms.bean.MainMenuBean;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Objects;

/**
 * User: artem
 * Date: 23.11.14
 * Time: 1:54
 */
@Component("incomingMessageBean")
@Scope("view")
public class IncomingMessageBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.incoming_message.incoming_message";

    @Resource(name = "incomingMessageService")
    private transient IncomingMessageService incomingMessageService;

    @Resource(name = "productService")
    private transient ProductService productService;

    @Resource(name = "sectionService")
    private transient SectionService sectionService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "mainMenuBean")
    private transient MainMenuBean mainMenuBean;

    private IncomingMessage message;
    private Product subjectProduct;

    @PostConstruct
    private void init() {
        Long messageId = WebUtils.getLongRequestParameter("id");
        Objects.requireNonNull(messageId, "Message id must be not null!");

        message = incomingMessageService.load(messageId);

        if (message.getSubjectType() == IncomingMessageSubjectType.PRODUCT) {
            subjectProduct = productService.load(message.getSubjectId());
        }
    }

    public void markReaded() {
        if (message.isReaded()) {
            throw new IllegalStateException("Incoming message with id = " + message.getId() + " already readed!");
        }
        incomingMessageService.markReaded(message.getId());
        mainMenuBean.updateNotReadedIncomingMessagesCount();

        messageManager.addInfoMessage(BUNDLE_NAME, "mark_readed_complete");
    }

    // setters & getters

    public IncomingMessage getMessage() {
        return message;
    }

    public Product getSubjectProduct() {
        if (message.getSubjectType() != IncomingMessageSubjectType.PRODUCT) {
            throw new IllegalStateException("Illegal subject type: " + message.getSubjectType());
        }
        return subjectProduct;
    }

    public Section getSubjectSection() {
        if (message.getSubjectType() != IncomingMessageSubjectType.SECTION) {
            throw new IllegalStateException("Illegal subject type: " + message.getSubjectType());
        }
        return sectionService.getAll().getById(message.getSubjectId());
    }
}
