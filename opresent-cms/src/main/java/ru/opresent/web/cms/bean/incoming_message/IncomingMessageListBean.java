package ru.opresent.web.cms.bean.incoming_message;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.message.incoming.IncomingMessageSubjectType;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.pagination.DefaultPageSettings;
import ru.opresent.core.pagination.IncomingMessagePage;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SectionService;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 23.11.14
 * Time: 1:54
 */
@Component("incomingMessageListBean")
@Scope("view")
public class IncomingMessageListBean implements Serializable {

    @Resource(name = "incomingMessageService")
    private transient IncomingMessageService incomingMessageService;

    @Resource(name = "productService")
    private transient ProductService productService;

    @Resource(name = "sectionService")
    private transient SectionService sectionService;

    private IncomingMessageListModel listModel = new IncomingMessageListModel();
    private IncomingMessage selectedMessage;

    private Map<Long, Product> subjectId2Product;

    public String view() {
        return "/pages/incoming_message/incoming_message?faces-redirect=true&id=" + selectedMessage.getId();
    }

    public Product getSubjectProduct(long productId) {
        Product product = subjectId2Product.get(productId);
        if (product == null) {
            throw new CoreException("Subject product with id = " + productId + " not found!");
        }
        return product;
    }

    public Section getSubjectSection(long sectionId) {
        return sectionService.getAll().getById(sectionId);
    }

    // setters & getters

    public IncomingMessageListModel getListModel() {
        return listModel;
    }

    public IncomingMessage getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(IncomingMessage selectedMessage) {
        this.selectedMessage = selectedMessage;
    }


    // inner classes

    public class IncomingMessageListModel extends LazyDataModel<IncomingMessage> {

        private IncomingMessagePage page;

        @Override
        public List<IncomingMessage> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
            selectedMessage = null;
            int pageNumber = first / pageSize + PageSettings.FIRST_PAGE_NUMBER;

            page = incomingMessageService.load(new DefaultPageSettings(pageNumber, pageSize));
            Set<Long> subjectProductIds = new HashSet<>();
            for (IncomingMessage message : page.getItems()) {
                if (message.getSubjectType() == IncomingMessageSubjectType.PRODUCT) {
                    subjectProductIds.add(message.getSubjectId());
                }
            }
            if (!subjectProductIds.isEmpty()) {
                subjectId2Product = new HashMap<>();
                for (Product product : productService.loadBase(subjectProductIds)) {
                    subjectId2Product.put(product.getId(), product);
                }
            }

            setRowCount(page.getTotalItemsCount());
            return page.getItems();
        }

        @Override
        public List<IncomingMessage> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
            throw new UnsupportedOperationException("Multisort loading not implemented!");
        }

        @Override
        public IncomingMessage getRowData(String rowKey) {
            long messageId = Long.parseLong(rowKey);
            for (IncomingMessage message : page.getItems()) {
                if (messageId == message.getId()) {
                    return message;
                }
            }
            return null;
        }

        @Override
        public Object getRowKey(IncomingMessage message) {
            return message.getId();
        }

        public int getNotReadedCount() {
            return page.getNotReadedCount();
        }
    }
}
