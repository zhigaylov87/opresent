package ru.opresent.web.cms.bean.section;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.domain.section.SectionType;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.web.cms.component.rich_text.RichTextContainer;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 21:35
 */
@Component("sectionBean")
@Scope("view")
public class SectionBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.section.section";

    @Resource(name = "sectionService")
    private transient SectionService sectionService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private Mode mode;
    private SectionType sectionType;
    private Section section;
    private List<SectionItem> sectionItems;
    private SectionItem selectedSectionItem;

    @PostConstruct
    private void init() {
        Long sectionId = WebUtils.getLongRequestParameter("id");
        mode = sectionId == null ? Mode.ADD : Mode.EDIT;

        if (mode == Mode.EDIT) {
            section = sectionService.load(sectionId);
            sectionType = section.getType();
            if (sectionType == SectionType.LIST) {
                sectionItems = sectionService.loadItems(sectionId);
            }
        }
    }

    public void sectionTypeChanged() {
        if (isEditing()) {
            throw new IllegalStateException("Section type cannot be changed in editing mode!");
        }

        if (sectionType == null) {
            throw new IllegalStateException("Section type cannot be changed to null!");
        }

        Section previousSection = section;
        section = new Section(sectionType);

        if (previousSection != null) {
            section.setName(previousSection.getName());
            section.setEntityName(previousSection.getEntityName());
            section.setVisible(previousSection.isVisible());
        }
    }

    public String save() {
        long sectionId;
        try {
            sectionId = sectionService.save(section);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            return null;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", section.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "section?faces-redirect=true&id=" + sectionId;
    }

    public String addSectionItem() {
        return "section_item?faces-redirect=true&section_id=" + section.getId();
    }

    public String editSectionItem() {
        return "section_item?faces-redirect=true&id=" + selectedSectionItem.getId();
    }

    public String deleteSectionItem() {
        try {
            sectionService.deleteSectionItem(selectedSectionItem.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return null;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_item_success", selectedSectionItem.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "section?faces-redirect=true&id=" + section.getId();
    }

    public String moveUpSectionItem(SectionItem sectionItem) {
        sectionService.moveUp(sectionItem);
        return "section?faces-redirect=true&id=" + section.getId();
    }

    public String moveDownSectionItem(SectionItem sectionItem) {
        sectionService.moveDown(sectionItem);
        return "section?faces-redirect=true&id=" + section.getId();
    }

    // setters & getters (BEGIN)

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public SectionType getSectionType() {
        return sectionType;
    }

    public void setSectionType(SectionType sectionType) {
        this.sectionType = sectionType;
    }

    public List<SectionType> getAvailableTypes() {
        return Arrays.asList(SectionType.values());
    }

    public Section getSection() {
        return section;
    }

    public List<SectionItem> getSectionItems() {
        if (section.getType() != SectionType.LIST) {
            throw new IllegalStateException("Section items exists only for section type: " + SectionType.LIST +
                    ", but current is: " + section.getType());
        }
        return sectionItems;
    }

    public SectionItem getSelectedSectionItem() {
        return selectedSectionItem;
    }

    public void setSelectedSectionItem(SectionItem selectedSectionItem) {
        this.selectedSectionItem = selectedSectionItem;
    }

    public RichTextContainer getSectionTextContainer() {
        return new RichTextContainer() {
            @Override
            public String getText() {
                return section.getText();
            }

            @Override
            public void setText(String text) {
                section.setText(text);
            }
        };
    }

    // setters & getters (END)

    public enum Mode {
        ADD, EDIT
    }
}
