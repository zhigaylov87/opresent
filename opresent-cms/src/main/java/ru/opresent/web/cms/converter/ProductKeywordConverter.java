package ru.opresent.web.cms.converter;

import ru.opresent.core.domain.ProductKeyword;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * User: artem
 * Date: 19.12.14
 * Time: 5:18
 */
@FacesConverter(value = "ProductKeywordConverter")
public class ProductKeywordConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return new ProductKeyword(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((ProductKeyword) value).getKeyword();
    }
}
