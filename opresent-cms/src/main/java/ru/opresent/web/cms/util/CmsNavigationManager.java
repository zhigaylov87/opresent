package ru.opresent.web.cms.util;

import org.springframework.stereotype.Component;

/**
 * User: artem
 * Date: 08.03.15
 * Time: 3:47
 */
@Component("cmsNavigationManager")
public class CmsNavigationManager {

    private static final String BASE_URL = "/cms";

    public String getProductUrl(long productId) {
        return "/pages/product/product?id=" + productId;
    }

    public String getNotJsfProductUrl(long productId) {
        return BASE_URL + "/pages/product/product.xhtml?id=" + productId;
    }

    public String getNotJsfSectionUrl(long sectionId) {
        return BASE_URL + "/pages/section/section.xhtml?id=" + sectionId;
    }

    public String getNotJsfSectionItemUrl(long sectionItemId) {
        return BASE_URL + "/pages/section/section_item.xhtml?id=" + sectionItemId;
    }

}