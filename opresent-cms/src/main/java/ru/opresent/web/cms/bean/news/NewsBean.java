package ru.opresent.web.cms.bean.news;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.NewsService;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("newsBean")
@Scope("view")
public class NewsBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.news.news";

    @Resource(name = "newsService")
    private transient NewsService newsService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private NewsItem selectedNewsItem;

    public String add() {
        return "news_item?faces-redirect=true";
    }

    public String edit() {
        return "news_item?faces-redirect=true&id=" + selectedNewsItem.getId();
    }

    public void delete() {
        NewsItem newsItem = selectedNewsItem;

        try {
            newsService.delete(newsItem);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        selectedNewsItem = null;
        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success");
    }

    public void moveUp(NewsItem newsItem) {
        newsService.moveUp(newsItem);
    }

    public void moveDown(NewsItem newsItem) {
        newsService.moveDown(newsItem);
    }

    // getters & setters

    public List<NewsItem> getNewsItems() {
        return newsService.getNews().getAllItems().getAll();
    }

    public NewsItem getSelectedNewsItem() {
        return selectedNewsItem;
    }

    public void setSelectedNewsItem(NewsItem selectedNewsItem) {
        this.selectedNewsItem = selectedNewsItem;
    }
}

