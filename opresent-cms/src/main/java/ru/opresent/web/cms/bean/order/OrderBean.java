package ru.opresent.web.cms.bean.order;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.order.AddressType;
import ru.opresent.core.domain.order.DeliveryWay;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderStatus;
import ru.opresent.core.service.api.OrderService;
import ru.opresent.web.cms.bean.MainMenuBean;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 04.11.13
 * Time: 15:06
 */
@Component("orderBean")
@Scope("view")
public class OrderBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.order.order";

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "orderService")
    private transient OrderService orderService;

    @Resource(name = "mainMenuBean")
    private transient MainMenuBean mainMenuBean;

    private Order order;

    @PostConstruct
    private void init() {
        Long orderId = WebUtils.getLongRequestParameter("id");
        if (orderId == null) {
            throw new IllegalArgumentException("Parameter 'id' must be not null");
        }
        order = orderService.load(orderId);
    }

    public String save() {
        // clear not visible for user fields, that must be empty
        AddressType addressType = order.getAddressType();
        if (addressType != null && !addressType.isCityRequired()) {
            order.setCustomerCity(null);
        }
        DeliveryWay deliveryWay = order.getDeliveryWay();
        if (deliveryWay != null) {
            if (!deliveryWay.isPostcodeRequired()) {
                order.setCustomerPostcode(null);
            }
            if (!deliveryWay.isAddressRequired()) {
                order.setCustomerAddress(null);
            }
        }

        try {
            orderService.update(order);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            return null;
        }
        mainMenuBean.updateNewOrdersCount();

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", order.getId());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "order?faces-redirect=true&id=" + order.getId();
    }

    public void addressTypeChanged() {
        order.setDeliveryWay(null);
    }

    // setters & getters

    public Order getOrder() {
        return order;
    }

    public List<OrderStatus> getAvailableStatuses() {
        return Arrays.asList(OrderStatus.values());
    }

    public List<AddressType> getAvailableAddressTypes() {
        return Arrays.asList(AddressType.values());
    }

    // inner entities
}
