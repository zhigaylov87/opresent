package ru.opresent.web.cms.bean.misc;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.service.api.SearchService;
import ru.opresent.core.service.impl.SitemapService;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * User: artem
 * Date: 28.05.15
 * Time: 1:05
 */
@Component("miscBean")
@Scope("view")
public class MiscBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.misc.misc";

    @Resource(name = "searchService")
    private transient SearchService searchService;

    @Resource(name = "sitemapService")
    private transient SitemapService sitemapService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    public void updateWholeSearchIndex() {
        searchService.updateWholeIndex();
        messageManager.addInfoMessage(BUNDLE_NAME, "update_whole_search_index_success");
    }

    public void updateSitemap() {
        sitemapService.invokeUpdateSitemap();
        messageManager.addInfoMessage(BUNDLE_NAME, "update_sitemap_success");
    }
}
