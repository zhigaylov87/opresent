package ru.opresent.web.cms.bean.order;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.order.OrderSettings;
import ru.opresent.core.service.api.OrderService;
import ru.opresent.web.cms.component.rich_text.RichTextContainer;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;

/**
 * User: artem
 * Date: 18.10.15
 * Time: 15:10
 */
@Component("orderSettingsBean")
@Scope("view")
public class OrderSettingsBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.order.order_settings";

    @Resource(name = "orderService")
    private transient OrderService orderService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private OrderSettings orderSettings;

    @PostConstruct
    private void init() {
        orderSettings = orderService.getOrderSettings();
        if (orderSettings == null) {
            orderSettings = new OrderSettings();
        }
    }

    public void save() {
        try {
            orderService.saveOrderSettings(orderSettings);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "personal_order_text_save_success");
        orderSettings = orderService.getOrderSettings();
        RequestContext.getCurrentInstance().update(Arrays.asList("TODO", "messages")); // TODO
    }

    // setters & getters

    public RichTextContainer getPersonalOrderTextContainer() {
        return new RichTextContainer() {
            @Override
            public String getText() {
                return orderSettings.getPersonalOrderText();
            }

            @Override
            public void setText(String text) {
                orderSettings.setPersonalOrderText(text);
            }
        };
    }
}
