package ru.opresent.web.cms.bean.news;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.CurrentTimestampHelper;
import ru.opresent.core.domain.news.NewsItem;
import ru.opresent.core.service.api.NewsService;
import ru.opresent.web.cms.component.rich_text.RichTextContainer;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("newsItemBean")
@Scope("view")
public class NewsItemBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.news.news_item";

    @Resource(name = "newsService")
    private transient NewsService newsService;

    @Resource(name = "currentTimestampHelper")
    private transient CurrentTimestampHelper currentTimestampHelper;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;


    private Mode mode;
    private NewsItem newsItem;

    @PostConstruct
    private void init() {
        Long newsItemId = WebUtils.getLongRequestParameter("id");
        if (newsItemId == null) {
            mode = Mode.ADD;
            newsItem = new NewsItem();
            newsItem.setDate(currentTimestampHelper.getTimestamp());
        } else {
            mode = Mode.EDIT;
            newsItem = new NewsItem(newsService.getNews().getAllItems().getById(newsItemId));
        }
    }

    public String save() {
        long newsItemId;
        try {
            newsItemId = newsService.save(newsItem);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return null;
        }

        newsItem = newsService.getNews().getAllItems().getById(newsItemId);
        messageManager.addInfoMessage(BUNDLE_NAME, "save_success");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "news_item?faces-redirect=true&id=" + newsItem.getId();
    }

    // getters & setters

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public NewsItem getNewsItem() {
        return newsItem;
    }

    public RichTextContainer getNewsItemTextContainer() {
        return new RichTextContainer() {
            @Override
            public String getText() {
                return newsItem.getText();
            }

            @Override
            public void setText(String text) {
                newsItem.setText(text);
            }
        };
    }

    // inners classes

    private enum Mode {
        ADD, EDIT
    }
}

