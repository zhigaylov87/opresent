package ru.opresent.web.cms.converter;

import ru.opresent.core.util.FormatUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.util.Date;

/**
 * User: artem
 * Date: 03.01.16
 * Time: 21:04
 */
@FacesConverter(value = "dateTimeConverter")
public class DateTimeConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new UnsupportedOperationException("DateTimeConverter is not working in this direction!");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Date) {
            return FormatUtils.formatDateTime((Date) value);
        }

        throw new ConverterException("Invalid value: " + value +
                ", value class: " + (value == null ? null : value.getClass().getName()));
    }
}
