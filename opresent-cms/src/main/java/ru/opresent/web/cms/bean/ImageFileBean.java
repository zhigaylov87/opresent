package ru.opresent.web.cms.bean;

import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.image.ImageConstraints;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.api.ImageService;
import ru.opresent.web.cms.converter.UploadedFileToImageConverter;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: artem
 * Date: 31.03.14
 * Time: 0:32
 */
@Component("imageFileBean")
public class ImageFileBean {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.image_file";

    @Resource(name = "imageConstraints")
    private ImageConstraints imageConstraints;

    @Resource(name = "imageService")
    private ImageService imageService;

    @Resource(name = "uploadedFileToImageConverter")
    private UploadedFileToImageConverter uploadedFileToImageConverter;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    public String getAllowTypesPattern() {
        StringBuilder pattern = new StringBuilder("/(\\.|\\/)(");

        List<String> formats = imageConstraints.getSupportedFormats();
        for (int i = 0; i < formats.size(); i++) {
            if (i != 0) {
                pattern.append("|");
            }
            pattern.append(formats.get(i));
        }

        pattern.append(")$/");
        return pattern.toString();
    }

    public String getInvalidFileMessage() {
        StringBuilder formatsStr = new StringBuilder();

        List<String> formats = imageConstraints.getSupportedFormats();
        for (int i = 0; i < formats.size(); i++) {
            if (i != 0) {
                formatsStr.append(", ");
            }
            formatsStr.append(formats.get(i));
        }

        return messageManager.getMessage(BUNDLE_NAME, "invalid_file_client_msg", formatsStr.toString());
    }

    /**
     * @return size limit in bytes
     */
    public int getSizeLimit() {
        return imageConstraints.getMaxSize();
    }

    public String getInvalidSizeMessage() {
        int maxSize = imageConstraints.getMaxSize();
        return messageManager.getMessage(BUNDLE_NAME, "invalid_size_client_msg", maxSize / 1024, maxSize / 1024 / 1024);
    }

    public Image saveImage(UploadedFile uploadedFile, ImageUse imageUse) {
        return saveImage(uploadedFile, imageUse, HasCopyrightOption.INHERIT_FROM_IMAGE_USE);
    }

    public Image saveImage(UploadedFile uploadedFile, ImageUse imageUse, HasCopyrightOption hasCopyrightOption) {
        Image image = uploadedFileToImageConverter.convert(uploadedFile, imageUse, hasCopyrightOption);
        return imageService.save(image);
    }
}
