package ru.opresent.web.cms.bean.category;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.CategoryProductsCountStatistic;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 21:09
 */
@Component("categoryListBean")
@Scope("view")
public class CategoryListBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.category.category_list";

    @Resource(name = "categoryService")
    private transient CategoryService categoryService;

    @Resource(name = "livemasterAliasService")
    private transient LivemasterAliasService livemasterAliasService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private CategoryProductsCountStatistic statistic;
    private IdentifiableList<LivemasterAliases> categoryAliases;
    private Category selectedCategory;

    @PostConstruct
    private void init() {
        reload();
    }

    public String add() {
        return "category?faces-redirect=true";
    }

    public String edit() {
        return "category?faces-redirect=true&id=" + selectedCategory.getId();
    }

    public void delete() {
        Category category = selectedCategory;

        try {
            categoryService.delete(category);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", category.getName());
        reload();
    }

    public void moveUp(Category category) {
        moveUpDown(category, true);
    }

    public void moveDown(Category category) {
        moveUpDown(category, false);
    }

    public List<String> getLivemasterAliases(Category category) {
        if (!categoryAliases.containsById(category.getId())) {
            return null;
        }
        List<String> aliases = new ArrayList<>(categoryAliases.getById(category.getId()).getAliases());
        Collections.sort(aliases);
        return aliases;
    }

    private void reload() {
        categoryAliases = new IdentifiableList<>(livemasterAliasService.load(LivemasterAliasType.CATEGORY));
        statistic = categoryService.loadCategoryProductsCountStatistic();
    }

    private void moveUpDown(Category category, boolean isUp) {
        if (isUp) {
            categoryService.moveUp(category);
        } else {
            categoryService.moveDown(category);
        }
        reload();
    }

    // getters & setters

    public List<Category> getCategories() {
        return categoryService.getAll().getAll();
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public CategoryProductsCountStatistic getStatistic() {
        return statistic;
    }
}
