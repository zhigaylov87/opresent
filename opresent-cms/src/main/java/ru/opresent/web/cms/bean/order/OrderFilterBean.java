package ru.opresent.web.cms.bean.order;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.dao.impl.DefaultOrderFilter;
import ru.opresent.core.domain.order.OrderStatus;
import ru.opresent.web.cms.component.table.FilterActionListener;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 04.03.15
 * Time: 23:09
 */
@Component("orderFilterBean")
@Scope("view")
public class OrderFilterBean implements Serializable {

    private DefaultOrderFilter filter = new DefaultOrderFilter();

    private FilterActionListener filterActionListener;

    public void invokeFiltering() {
        filterActionListener.filteringInvoked();
    }

    public void invokeReset() {
        filter = new DefaultOrderFilter();
        filterActionListener.resetInvoked();
    }

    // setters & getters

    public void setFilterActionListener(FilterActionListener filterActionListener) {
        this.filterActionListener = filterActionListener;
    }

    public List<OrderStatus> getStatuses() {
        return Arrays.asList(OrderStatus.values());
    }

    public DefaultOrderFilter getFilter() {
        return filter;
    }

    public void setFilter(DefaultOrderFilter filter) {
        this.filter = filter;
    }
}
