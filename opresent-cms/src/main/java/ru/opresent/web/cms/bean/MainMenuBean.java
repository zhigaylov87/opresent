package ru.opresent.web.cms.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.order.OrderStatus;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.core.service.api.OrderService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;

/**
 * User: artem
 * Date: 16.01.16
 * Time: 3:28
 */
@Component("mainMenuBean")
@Scope("session")
public class MainMenuBean implements Serializable {

    private static final int UPDATE_COUNTS_INTERVAL_MS = 1 * 60 * 1000; // one minute

    @Resource(name = "incomingMessageService")
    private transient IncomingMessageService incomingMessageService;

    @Resource(name = "orderService")
    private transient OrderService orderService;

    private int notReadedIncomingMessagesCount;
    private long notReadedIncomingMessagesCountUpdateMs;

    private int newOrdersCount;
    private long newOrdersCountUpdateMs;

    @PostConstruct
    private void init() {
        updateNotReadedIncomingMessagesCount();
        updateNewOrdersCount();
    }

    public void updateNotReadedIncomingMessagesCount() {
        notReadedIncomingMessagesCount = incomingMessageService.getNotReadedCount();
        notReadedIncomingMessagesCountUpdateMs = System.currentTimeMillis();
    }

    public void updateNewOrdersCount() {
        newOrdersCount = orderService.getOrderStatusCount(OrderStatus.NEW);
        newOrdersCountUpdateMs = System.currentTimeMillis();
    }


    // getters

    public int getNotReadedIncomingMessagesCount() {
        if (System.currentTimeMillis() - notReadedIncomingMessagesCountUpdateMs >= UPDATE_COUNTS_INTERVAL_MS) {
            updateNotReadedIncomingMessagesCount();
        }
        return notReadedIncomingMessagesCount;
    }

    public int getNewOrdersCount() {
        if (System.currentTimeMillis() - newOrdersCountUpdateMs >= UPDATE_COUNTS_INTERVAL_MS) {
            updateNewOrdersCount();
        }
        return newOrdersCount;
    }
}
