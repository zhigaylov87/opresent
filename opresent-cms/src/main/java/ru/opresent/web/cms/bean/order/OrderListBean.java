package ru.opresent.web.cms.bean.order;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.pagination.Page;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.service.api.OrderService;
import ru.opresent.web.cms.component.table.PaginationDataModel;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * User: artem
 * Date: 20.02.15
 * Time: 2:15
 */
@Component("orderListBean")
@Scope("view")
public class OrderListBean implements PaginationDataModel.Handler<Order>, Serializable {

    @Resource(name = "orderService")
    private transient OrderService orderService;

    @Resource(name = "orderFilterBean")
    private transient OrderFilterBean orderFilterBean;

    private PaginationDataModel<Order> ordersModel = new PaginationDataModel<>(this);
    private Order selectedOrder;

    public String view() {
        return "/pages/order/order?faces-redirect=true&id=" + selectedOrder.getId();
    }

    @Override
    public String getDataTableId() {
        return ":tableForm:ordersTable";
    }

    @Override
    public Page<Order> loadPage(PageSettings pageSettings) {
        return orderService.load(orderFilterBean.getFilter(), pageSettings);
    }

    @Override
    public void deselect() {
        selectedOrder = null;
    }

    // setters & getters

    public PaginationDataModel<Order> getOrdersModel() {
        return ordersModel;
    }

    public Order getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    // inner entities
}
