package ru.opresent.web.cms.bean.notes;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductStatusInfo;
import ru.opresent.core.service.api.ProductStatusService;
import ru.opresent.core.util.EnumHelper;
import ru.opresent.web.cms.EditingList;
import ru.opresent.web.cms.component.dialog.EditDialog;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 19:25
 */
@Component("productStatusInfoListBean")
@Scope("view")
public class ProductStatusInfoListBean extends EditingList<ProductStatusInfo> implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.notes.product_status_info_list";

    @Resource(name = "productStatusService")
    private transient ProductStatusService productStatusService;

    @Resource(name = "enumHelper")
    private transient EnumHelper enumHelper;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private ProductStatusInfoEditDialog dialog = new ProductStatusInfoEditDialog();

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public List<ProductStatusInfo> loadList() {
        List<ProductStatusInfo> infos = new ArrayList<>(productStatusService.getAll().values());

        for (ProductStatus productStatus : ProductStatus.values()) {
            ProductStatusInfo info = new ProductStatusInfo(productStatus);
            if (!infos.contains(info)) {
                infos.add(info);
            }
        }

        Collections.sort(infos);
        return infos;
    }

    @Override
    public ProductStatusInfoEditDialog getDialog() {
        return dialog;
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Product status info can not be deleted!");
    }

    // inner classes

    public class ProductStatusInfoEditDialog extends EditDialog implements Serializable {

        private ProductStatusInfo statusInfo;
        private String title;

        @Override
        public void prepareToAdd() {
            throw new UnsupportedOperationException("Product status info can not be added!");
        }

        @Override
        public void prepareToEdit() {
            statusInfo = new ProductStatusInfo(getSelected());
            title = messageManager.getMessage(BUNDLE_NAME,
                    "edit_dialog_title", enumHelper.getDescription(statusInfo.getStatus()));
        }

        @Override
        public void save() {
            try {
                productStatusService.save(statusInfo);
            } catch (ConstraintViolationException e) {
                messageManager.addErrorMessage(e);
                FacesContext.getCurrentInstance().validationFailed();
                return;
            }

            messageManager.addInfoMessage(BUNDLE_NAME,
                    "save_success", enumHelper.getDescription(statusInfo.getStatus()));
            reloadList();
            RequestContext.getCurrentInstance().update(Arrays.asList("productStatusInfoListForm", "messages"));
        }

        @Override
        public String getTitle() {
            return title;
        }

        public ProductStatusInfo getStatusInfo() {
            return statusInfo;
        }
    }
}
