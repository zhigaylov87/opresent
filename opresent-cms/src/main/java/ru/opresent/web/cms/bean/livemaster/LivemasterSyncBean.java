package ru.opresent.web.cms.bean.livemaster;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.image.ImageUrlCreator;
import ru.opresent.core.domain.Product;
import ru.opresent.core.livemaster.sync.LivemasterSyncProcessHandler;
import ru.opresent.core.livemaster.sync.LivemasterSyncProduct;
import ru.opresent.core.livemaster.sync.LivemasterSyncProductResult;
import ru.opresent.core.livemaster.sync.LivemasterSyncProductWarning;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEvent;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventDataField;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventListener;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventType;
import ru.opresent.core.service.api.LivemasterSyncService;
import ru.opresent.core.util.EnumHelper;
import ru.opresent.web.cms.util.CmsNavigationManager;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 29.11.14
 * Time: 19:10
 */
@Component("livemasterSyncBean")
@Scope("view")
public class LivemasterSyncBean implements LivemasterSyncEventListener, Serializable {

    public static final String PUSH_CHANNEL_PATH = "/pages/livemaster/livemaster_sync";

    public static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.livemaster.livemaster_sync";

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "enumHelper")
    private transient EnumHelper enumHelper;

    @Resource(name = "cmsNavigationManager")
    private transient CmsNavigationManager cmsNavigationManager;

    @Resource(name = "imageFileManager")
    private transient ImageUrlCreator imageUrlCreator;

    @Resource(name = "livemasterSyncService")
    private transient LivemasterSyncService syncService;

    private ObjectMapper jsonMapper = new ObjectMapper();

    private boolean syncStarted = false;
    private LivemasterSyncProcessHandler processHandler;

    public void start() {
        syncStarted = true;
        processHandler = new LivemasterSyncProcessHandler(this);
        new Thread(new Runnable() {
            public void run() {
                syncService.synchronize(processHandler);
            }
        }).start();
    }

    public void stop() {
        syncStarted = false;
        processHandler.setSyncStopped(true);
    }

    @Override
    public void processEvent(LivemasterSyncEvent syncEvent) {
        switch (syncEvent.getType()) {
            case SYNC_STOPPED:
            case SYNC_COMPLETE:
                syncStarted = false;
        }
        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish(PUSH_CHANNEL_PATH, createCmsSyncEvent(syncEvent));
    }

    private CmsSyncEvent createCmsSyncEvent(LivemasterSyncEvent syncEvent) {
        LivemasterSyncEventType eventType = syncEvent.getType();


        switch (eventType) {
            case SYNC_START:
            case SYNC_PRODUCTS_DETERMINING_START:
                return new CmsSyncEvent(eventType);

            case SYNC_PRODUCTS_DETERMINING_COMPLETE:
                return new CmsSyncEvent(eventType, jsonMapper.createObjectNode().put(
                        "syncProductsCount",
                        syncEvent.<Integer>getField(LivemasterSyncEventDataField.COUNT)));

            case SYNC_PRODUCTS_DETERMINING_FAILED:
                return new CmsSyncEvent(eventType, jsonMapper.createObjectNode().put(
                        "errorStackTrace",
                        ExceptionUtils.getStackTrace(syncEvent.<Exception>getField(LivemasterSyncEventDataField.EXCEPTION))));

            case SYNC_PRODUCTS_PROCESSING_START:
                return new CmsSyncEvent(eventType);

            case SYNC_PRODUCT_PROCESSED:
                ObjectNode syncProductProcessedJson = jsonMapper.createObjectNode();

                fillJson(syncProductProcessedJson.putObject("syncProduct"),
                        syncEvent.<LivemasterSyncProduct>getField(LivemasterSyncEventDataField.SYNC_PRODUCT));

                fillJson(syncProductProcessedJson.putObject("syncProductResult"),
                        syncEvent.<LivemasterSyncProductResult>getField(LivemasterSyncEventDataField.SYNC_PRODUCT_RESULT));

                return new CmsSyncEvent(eventType, syncProductProcessedJson);

            case SYNC_PRODUCTS_PROCESSING_COMPLETE:
                return new CmsSyncEvent(eventType);

            case NOT_ACTUAL_LIVEMASTER_IDS_PROCESSING_START:
                return new CmsSyncEvent(eventType);

            case NOT_ACTUAL_LIVEMASTER_IDS_DETERMINED:
                return new CmsSyncEvent(eventType, jsonMapper.createObjectNode()
                        .put("notActualLivemasterIdsCount", syncEvent.<Integer>getField(LivemasterSyncEventDataField.COUNT)));

            case NOT_ACTUAL_LIVEMASTER_ID_PROCESSED:
                ObjectNode notActualLivemasterIdProcessedJson = jsonMapper.createObjectNode()
                        .put("livemasterId", syncEvent.<String>getField(LivemasterSyncEventDataField.LIVEMASTER_ID));

                Product product = syncEvent.getField(LivemasterSyncEventDataField.PRODUCT);
                notActualLivemasterIdProcessedJson.putObject("product")
                        .put("url", cmsNavigationManager.getNotJsfProductUrl(product.getId()))
                        .put("previewUrl", imageUrlCreator.createUrl(product.getPreview()))
                        .put("name", product.getName());

                return new CmsSyncEvent(eventType, notActualLivemasterIdProcessedJson);

            case NOT_ACTUAL_LIVEMASTER_ID_PROCESSING_COMPLETE:
                return new CmsSyncEvent(eventType);

            case SYNC_STOPPED:
            case SYNC_COMPLETE:
                return new CmsSyncEvent(eventType);

            default:
                throw new IllegalArgumentException("Unsupported event type: " + eventType);
        }

    }

    private void fillJson(ObjectNode syncProductJson, LivemasterSyncProduct syncProduct) {
        syncProductJson
                .put("livemasterId", syncProduct.getLivemasterProductId())
                .put("pageUrl", syncProduct.getProductPageUrl());
    }

    private void fillJson(ObjectNode syncProductResultJson, LivemasterSyncProductResult syncProductResult) {
        syncProductResultJson.put("type", syncProductResult.getType().name());

        Product product = syncProductResult.getProduct();
        if (product != null) {
            syncProductResultJson.putObject("product")
                    .put("url", cmsNavigationManager.getNotJsfProductUrl(product.getId()))
                    .put("previewUrl", imageUrlCreator.createUrl(product.getPreview()))
                    .put("name", product.getName());
        }

        syncProductResultJson.put("message", createSyncProductResultMessage(syncProductResult));

        List<LivemasterSyncProductWarning> warnings = syncProductResult.getWarnings();
        if (warnings != null) {
            ArrayNode warningsArray = syncProductResultJson.putArray("warnings");
            for (LivemasterSyncProductWarning warning : warnings) {
                warningsArray.addObject()
                        .put("missedImageUrlAbbrev", StringUtils.abbreviateMiddle(warning.getMissedImageUrl(), "...", 50))
                        .put("missedImageUrl", warning.getMissedImageUrl())
                        .put("errorStackTrace", ExceptionUtils.getStackTrace(warning.getException()));
            }
        }

        Exception error = syncProductResult.getError();
        if (error != null) {
            ObjectNode errorJson = syncProductResultJson.putObject("error");
            if (error instanceof ConstraintViolationException) {
                ArrayNode constraintViolations = errorJson.putArray("constraintViolations");
                for (ConstraintViolation violation : ((ConstraintViolationException) error).getConstraintViolations()) {
                    constraintViolations.add(violation.getMessage());
                }
            } else {
                errorJson.put("stackTrace", ExceptionUtils.getStackTrace(error));
            }
        }
    }

    private String createSyncProductResultMessage(LivemasterSyncProductResult syncProductResult) {
        switch (syncProductResult.getType()) {
            case PRODUCT_ADDED:
                return messageManager.getMessage(BUNDLE_NAME, "result_product_added");

            case PRODUCT_STATUS_UPDATED:
                return messageManager.getMessage(BUNDLE_NAME, "result_product_status_updated",
                        enumHelper.getDescription(syncProductResult.getOldStatus()),
                        enumHelper.getDescription(syncProductResult.getProduct().getStatus()));

            case PRODUCT_STATUS_AND_PRICE_UPDATED:
                return messageManager.getMessage(BUNDLE_NAME, "result_product_status_and_price_updated",
                        enumHelper.getDescription(syncProductResult.getOldStatus()),
                        enumHelper.getDescription(syncProductResult.getProduct().getStatus()),
                        syncProductResult.getOldPrice(),
                        syncProductResult.getProduct().getPrice());

            case PRODUCT_PRICE_UPDATED:
                return messageManager.getMessage(BUNDLE_NAME, "result_product_price_updated",
                        syncProductResult.getOldPrice(),
                        syncProductResult.getProduct().getPrice());

            case NOTHING_DONE:
                return messageManager.getMessage(BUNDLE_NAME, "result_nothing_done");

            case NOTHING_DONE_FOR_DELETED:
                return messageManager.getMessage(BUNDLE_NAME, "result_nothing_done_for_deleted");

            case ERROR:
                return messageManager.getMessage(BUNDLE_NAME, "result_error");

            default:
                throw new CoreException("Unknown sync product result type: " + syncProductResult.getType());
        }
    }

    // setters & getters

    public boolean isSyncStarted() {
        return syncStarted;
    }

    public String getPushChannelPath() {
        return PUSH_CHANNEL_PATH;
    }

}
