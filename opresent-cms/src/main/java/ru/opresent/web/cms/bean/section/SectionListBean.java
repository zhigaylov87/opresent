package ru.opresent.web.cms.bean.section;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.web.cms.Deleter;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 21:36
 */
@Component("sectionListBean")
@Scope("view")
public class SectionListBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.section.section_list";

    @Resource(name = "sectionService")
    private transient SectionService sectionService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private Section selectedSection;

    public String edit() {
        return "section?faces-redirect=true&id=" + selectedSection.getId();
    }

    public void moveUp(Section section) {
        sectionService.moveUp(section);
    }

    public void moveDown(Section section) {
        sectionService.moveDown(section);
    }

    // setters & getters

    public Deleter getDeleter() {
        return new Deleter() {
            @Override
            public void delete() {
                sectionService.deleteSection(selectedSection.getId());
                messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", selectedSection.getName());
                selectedSection = null;
            }
        };
    }

    public Section getSelectedSection() {
        return selectedSection;
    }

    public void setSelectedSection(Section selectedSection) {
        this.selectedSection = selectedSection;
    }

    public List<Section> getSections() {
        return sectionService.getAll().getAll();
    }
}
