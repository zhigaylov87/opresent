package ru.opresent.web.cms.bean.livemaster;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.livemaster.sync.config.LivemasterSyncConfig;
import ru.opresent.core.service.api.LivemasterSyncService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.web.cms.component.dialog.EditDialog;
import ru.opresent.web.cms.converter.IdentifiableConverter;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: artem
 * Date: 09.07.2017
 * Time: 5:11
 */
@Component("livemasterSyncConfigBean")
@Scope("view")
public class LivemasterSyncConfigBean implements Serializable {

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "livemasterSyncService")
    private transient LivemasterSyncService syncService;

    @Resource(name = "productTypeService")
    private transient ProductTypeService productTypeService;

    private LivemasterSyncConfigDialog dialog = new LivemasterSyncConfigDialog();

    // getters & setters

    public LivemasterSyncConfigDialog getDialog() {
        return dialog;
    }

    public List<ProductType> getProductTypes() {
        return productTypeService.getAll().getAll();
    }

    public IdentifiableConverter<ProductType> getProductTypeConverter() {
        return new IdentifiableConverter<>(getProductTypes(), true);
    }

    // inner classes

    public class LivemasterSyncConfigDialog extends EditDialog implements Serializable {

        private LivemasterSyncConfig config;
        private List<ProductTypePriceDiff> productTypePriceDiffList;

        @Override
        public void prepareToAdd() {
            throw new UnsupportedOperationException("Add not supported");
        }

        @Override
        public void prepareToEdit() {
            config = syncService.getConfig();

            productTypePriceDiffList = new ArrayList<>();
            for (Long productTypeId : config.getProductTypeIdToPriceDiff().keySet()) {
                productTypePriceDiffList.add(new ProductTypePriceDiff(
                        productTypeService.getAll().getById(productTypeId), config.getProductTypeIdToPriceDiff().get(productTypeId)));
            }
        }

        @Override
        public void save() {
            config.getProductTypeIdToPriceDiff().clear();
            for (ProductTypePriceDiff productTypePriceDiff : productTypePriceDiffList) {
                config.getProductTypeIdToPriceDiff().put(
                        productTypePriceDiff.getProductType() == null ? null : productTypePriceDiff.getProductType().getId(),
                        productTypePriceDiff.getPriceDiff());
            }

            try {
                syncService.saveConfig(config);
            } catch (ConstraintViolationException e) {
                messageManager.addErrorMessage(e);
                FacesContext.getCurrentInstance().validationFailed();
            }
        }

        @Override
        public String getTitle() {
            return "Edit Livemaster Sync Config";
        }

        public LivemasterSyncConfig getConfig() {
            return config;
        }

        public List<ProductTypePriceDiff> getProductTypePriceDiffList() {
            return productTypePriceDiffList;
        }

        public void addProductTypePriceDiff() {
            productTypePriceDiffList.add(new ProductTypePriceDiff(null, null));
        }
    }

    public static class ProductTypePriceDiff implements Serializable {

        private ProductType productType;
        private Integer priceDiff;

        public ProductTypePriceDiff(ProductType productType, Integer priceDiff) {
            this.productType = productType;
            this.priceDiff = priceDiff;
        }

        public ProductType getProductType() {
            return productType;
        }

        public void setProductType(ProductType productType) {
            this.productType = productType;
        }

        public Integer getPriceDiff() {
            return priceDiff;
        }

        public void setPriceDiff(Integer priceDiff) {
            this.priceDiff = priceDiff;
        }
    }
}
