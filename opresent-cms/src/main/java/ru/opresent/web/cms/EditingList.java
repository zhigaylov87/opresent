package ru.opresent.web.cms;

import ru.opresent.core.domain.Identifiable;
import ru.opresent.web.cms.component.dialog.EditDialog;

import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 24.10.13
 * Time: 0:18
 */
public abstract class EditingList<T extends Identifiable> implements Deleter, Serializable {

    private List<T> list;
    private T selected;

    protected void reloadList() {
        list = loadList();
        selected = null;
    }

    public abstract List<T> loadList();

    // getters & setters (begin)

    public abstract EditDialog getDialog();

    public List<T> getList() {
        return list;
    }

    public T getSelected() {
        return selected;
    }

    public void setSelected(T selected) {
        this.selected = selected;
    }

    // getters & setters (end)
}

