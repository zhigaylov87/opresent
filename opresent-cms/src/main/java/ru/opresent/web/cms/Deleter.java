package ru.opresent.web.cms;

/**
 * User: artem
 * Date: 18.01.14
 * Time: 17:06
 */
public interface Deleter {

    public void delete();

}
