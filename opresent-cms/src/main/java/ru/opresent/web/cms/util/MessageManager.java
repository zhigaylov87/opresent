package ru.opresent.web.cms.util;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.opresent.core.service.ServiceException;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 0:45
 */
@Component("messageManager")
public class MessageManager {

    private static final String MESSAGES_ID = "messages";
    private static final Locale LOCALE = new Locale("ru", "RU");

    @Resource(name = "serviceMessageSource")
    private MessageSource serviceMessageSource;

    public void addErrorMessage(ConstraintViolationException e) {
        for (ConstraintViolation constraintViolation : e.getConstraintViolations()) {
            addErrorMessage(constraintViolation.getMessage());
        }
    }

    public void addErrorMessage(ServiceException e) {
        addErrorMessage(serviceMessageSource.getMessage(e.getMessageSourceResolvable(), getLocale()));
    }

    public void addInfoMessage(String bundleName, String bundleKey, Object... params) {
        String message = getMessage(bundleName, bundleKey, params);
        addMessage(FacesMessage.SEVERITY_INFO, message);
    }

    public void addErrorMessage(String bundleName, String bundleKey, Object... params) {
        addErrorMessage(getMessage(bundleName, bundleKey, params));
    }

    public String getMessage(String bundleName, String bundleKey, Object... params) {
        String message = getResourceBundleString(bundleName, bundleKey);
        return MessageFormat.format(message, params);
    }

    private FacesMessage createMessage(FacesMessage.Severity severity, String message) {
        FacesMessage facesMessage = new FacesMessage(message);
        facesMessage.setSeverity(severity);
        return facesMessage;
    }

    private void addMessage(FacesMessage.Severity severity, String message) {
        FacesContext.getCurrentInstance().addMessage(MESSAGES_ID, createMessage(severity, message));
    }

    private void addErrorMessage(String message) {
        addMessage(FacesMessage.SEVERITY_ERROR, message);
    }

    private String getResourceBundleString(String bundleName, String bundleKey) {
        return ResourceBundle.getBundle(bundleName, getLocale()).getString(bundleKey);
    }

    private Locale getLocale() {
        return LOCALE;
    }

}
