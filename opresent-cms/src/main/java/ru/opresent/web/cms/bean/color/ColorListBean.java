package ru.opresent.web.cms.bean.color;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Color;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.ColorService;
import ru.opresent.web.cms.EditingList;
import ru.opresent.web.cms.component.dialog.EditDialog;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("colorListBean")
@Scope("view")
public class ColorListBean extends EditingList<Color> implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.color.color_list";

    @Resource(name = "colorService")
    private transient ColorService colorService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private ColorEditDialog dialog = new ColorEditDialog();

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public void delete() {
        Color deletingColor = getSelected();

        try {
            colorService.delete(deletingColor);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", deletingColor.getDescription());
        reloadList();
    }

    @Override
    public List<Color> loadList() {
        return colorService.getAll().getAll();
    }

    // getters & setters

    @Override
    public ColorEditDialog getDialog() {
        return dialog;
    }

    // inner classes

    public class ColorEditDialog extends EditDialog implements Serializable {

        private Color color;
        private String title;

        @Override
        public void prepareToAdd() {
            color = new Color();
            color.setRgb(java.awt.Color.WHITE.getRGB());
            title = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
        }

        @Override
        public void prepareToEdit() {
            color = new Color(getSelected());
            title = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
        }

        @Override
        public void save() {
            try {
                colorService.save(color);
            } catch (ConstraintViolationException e) {
                messageManager.addErrorMessage(e);
                FacesContext.getCurrentInstance().validationFailed();
                return;
            }

            messageManager.addInfoMessage(BUNDLE_NAME, "save_success", color.getDescription());
            reloadList();
            RequestContext.getCurrentInstance().update(Arrays.asList("listForm", "messages"));
        }

        @Override
        public String getTitle() {
            return title;
        }

        public Color getColor() {
            return color;
        }
    }
}

