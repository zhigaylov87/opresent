package ru.opresent.web.cms.bean.banner;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.BannerService;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("bannerBean")
@Scope("view")
public class BannerBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.banner.banner";

    @Resource(name = "bannerService")
    private transient BannerService bannerService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private BannerItem selectedBannerItem;

    public String add() {
        return "banner_item?faces-redirect=true";
    }

    public String edit() {
        return "banner_item?faces-redirect=true&id=" + selectedBannerItem.getId();
    }

    public void delete() {
        BannerItem bannerItem = selectedBannerItem;

        try {
            bannerService.delete(bannerItem);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        selectedBannerItem = null;
        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", bannerItem.getName());
    }

    public void moveUp(BannerItem bannerItem) {
        bannerService.moveUp(bannerItem);
    }

    public void moveDown(BannerItem bannerItem) {
        bannerService.moveDown(bannerItem);
    }

    // getters & setters

    public List<BannerItem> getBannerItems() {
        return bannerService.getBanner().getAllItems().getAll();
    }

    public BannerItem getSelectedBannerItem() {
        return selectedBannerItem;
    }

    public void setSelectedBannerItem(BannerItem selectedBannerItem) {
        this.selectedBannerItem = selectedBannerItem;
    }
}

