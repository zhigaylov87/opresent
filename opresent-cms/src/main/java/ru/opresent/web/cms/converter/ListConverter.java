package ru.opresent.web.cms.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: artem
 * Date: 18.05.14
 * Time: 18:47
 */
public abstract class ListConverter<T> implements Converter {

    public static final String DEFAULT_VALUES_DELIMITER = ";";

    @Override
    public final List<T> getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null) {
            return null;
        }

        List<T> elements = new ArrayList<>();
        for (String elementStr : value.split(DEFAULT_VALUES_DELIMITER)) {
            elements.add(getElementByString(elementStr));
        }
        return elements;
    }

    @Override
    public final String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }

        if (!(value instanceof List)) {
            throw new ConverterException("Value must be instace of " + List.class.getName() +
                    ". Value: " + value);
        }

        StringBuilder asString = new StringBuilder();
        for (Object elementObj : (List)value) {
            T element;
            try {
                element = (T) elementObj;
            } catch (ClassCastException e) {
                throw new ConverterException("List element has incorrect type", e);
            }

            asString.append(getStringByElement(element)).append(DEFAULT_VALUES_DELIMITER);
        }
        return asString.toString();
    }

    protected abstract String getStringByElement(T element);

    protected abstract T getElementByString(String s);
}
