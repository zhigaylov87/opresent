package ru.opresent.web.cms.util.exceptionhandler;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;
import ru.opresent.core.util.logging.LogFactory;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;

/**
 * Overriding only for logging to slf4j
 *
 * User: artem
 * Date: 14.11.13
 * Time: 0:30
 */
public class LoggingFullAjaxExceptionHandler extends FullAjaxExceptionHandler {

    public LoggingFullAjaxExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    @Override
    protected void logException(FacesContext context, Throwable exception, String location, String message, Object... parameters) {
        LogFactory.CMS_LOG.error(String.format(message, location), exception);
    }
}
