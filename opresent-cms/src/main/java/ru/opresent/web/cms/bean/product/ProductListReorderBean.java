package ru.opresent.web.cms.bean.product;

import org.primefaces.event.DragDropEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.image.ImageUrlCreator;
import ru.opresent.core.domain.DefaultProductFilter;
import ru.opresent.core.domain.OrderedEntity;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.image.ImageUseOption;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.pagination.SinglePageSettings;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.impl.OrderedEntityServiceHelper;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: artem
 * Date: 06.01.14
 * Time: 23:26
 */
@Component("productListReorderBean")
@Scope("view")
public class ProductListReorderBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.product.product_list_reorder";

    private static final int AVAILABLE_PRODUCTS_MODE_INDEX = 0;
    private static final int FOR_ORDER_AND_EXAMPLE_PRODUCTS_MODE_INDEX = 1;
    private static final List<Integer> MODES = Arrays.asList(
            AVAILABLE_PRODUCTS_MODE_INDEX, FOR_ORDER_AND_EXAMPLE_PRODUCTS_MODE_INDEX);

    private static final Pattern PRODUCT_ITEM_PANEL_ID_PATTERN =
            Pattern.compile("productItemsForm:productItemsGrid:(.*?):productItemPanel");

    private static final Pattern PRODUCT_ITEM_DIVIDER_ID_PATTERN =
            Pattern.compile("productItemsForm:productItemsGrid:(.*?):productItemDivider");

    @Resource(name = "productService")
    private transient ProductService productService;

    @Resource(name = "orderedEntityServiceHelper")
    private transient OrderedEntityServiceHelper orderedEntityServiceHelper;

    @Resource(name = "imageFileManager")
    private transient ImageUrlCreator imageUrlCreator;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private int modeIndex;
    private List<ProductItem> availableProductItems;
    private List<ProductItem> forOrderAndExampleProductItems;

    @PostConstruct
    private void init() {
        Integer modeParam = WebUtils.getIntegerRequestParameter("mode");
        modeIndex = modeParam == null ? AVAILABLE_PRODUCTS_MODE_INDEX : modeParam;
        if (!MODES.contains(modeIndex)) {
            throw new IllegalArgumentException("Illegal mode index: " + modeIndex);
        }

        DefaultProductFilter filter = new DefaultProductFilter();
        PageSettings pageSettings = new SinglePageSettings();

        filter.setStatuses(Collections.singleton(ProductStatus.AVAILABLE));
        ProductPage productPage = productService.load(filter, pageSettings);
        availableProductItems = createProductItems(productPage.getItems());

        filter.setStatuses(new HashSet<>(Arrays.asList(ProductStatus.FOR_ORDER, ProductStatus.EXAMPLE)));
        productPage = productService.load(filter, pageSettings);
        forOrderAndExampleProductItems = createProductItems(productPage.getItems());
    }

    public void onProductItemDrop(DragDropEvent event) {
        int productItemIndex = extractIndex(PRODUCT_ITEM_PANEL_ID_PATTERN, event.getDragId());
        int dividerIndex = extractIndex(PRODUCT_ITEM_DIVIDER_ID_PATTERN, event.getDropId());

        List<ProductItem> productItems = getProductItems();
        boolean lastDividerIndex = dividerIndex == productItems.size() - 1;

        ProductItem productItem = productItems.remove(productItemIndex);
        if (lastDividerIndex) {
            productItems.add(productItem);
        } else {
            productItems.add(productItemIndex < dividerIndex ? dividerIndex : dividerIndex + 1, productItem);
        }
    }

    public String save() {
        Map<Long, Integer> idToIndex = new HashMap<>();
        int index = OrderedEntity.START_INDEX;

        for (ProductItem productItem : availableProductItems) {
            if (index != productItem.index) {
                idToIndex.put(productItem.getId(), index);
            }
            index++;
        }
        for (ProductItem productItem : forOrderAndExampleProductItems) {
            if (index != productItem.index) {
                idToIndex.put(productItem.getId(), index);
            }
            index++;
        }
        if (idToIndex.isEmpty()) {
            messageManager.addErrorMessage(BUNDLE_NAME, "nothing_reordered");
            return null;
        }

        // TODO: Может не грузить товары по id, а кэшировать товары, на основе которых строятся ProductItem'ы???
        // TODO: Сравнить варианты по памяти и скорости. Проверить случай перемещения всех товаров сразу.
        IdentifiableList<Product> reorderProducts = new IdentifiableList<>(productService.loadBase(idToIndex.keySet()));
        Map<Product, Integer> productToIndex = new HashMap<>();
        for (Map.Entry<Long, Integer> idAndIndex : idToIndex.entrySet()) {
            productToIndex.put(reorderProducts.getById(idAndIndex.getKey()), idAndIndex.getValue());
        }
        orderedEntityServiceHelper.reorder(productToIndex);

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success");
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "product_list_reorder?faces-redirect=true&mode=" + modeIndex;
    }

    private int extractIndex(Pattern pattern, String id) {
        Matcher matcher = pattern.matcher(id);
        matcher.find();
        return Integer.parseInt(matcher.group(1));
    }

    private List<ProductItem> createProductItems(List<Product> products) {
        List<ProductItem> productItems = new ArrayList<>();
        for (Product product : products) {
            ProductItem productItem = new ProductItem(
                    product.getId(),
                    product.getName(),
                    imageUrlCreator.createUrl(product.getPreview(), ImageUseOption.PRODUCT_PREVIEW_IMAGE_IN_CART),
                    product.getIndex());

            productItems.add(productItem);
        }
        return productItems;
    }

    // getters & setters

    public int getAvailableProductsModeIndex() {
        return AVAILABLE_PRODUCTS_MODE_INDEX;
    }

    public int getForOrderAndExampleProductsModeIndex() {
        return FOR_ORDER_AND_EXAMPLE_PRODUCTS_MODE_INDEX;
    }

    public List<ProductItem> getProductItems() {
        return modeIndex == AVAILABLE_PRODUCTS_MODE_INDEX ? availableProductItems : forOrderAndExampleProductItems;
    }

    // inner entities

    public static class ProductItem implements Serializable {

        private final long id;
        private final String name;
        private final String previewUrl;
        private final int index;

        public ProductItem(long id, String name, String previewUrl, int index) {
            this.id = id;
            this.name = name;
            this.previewUrl = previewUrl;
            this.index = index;
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getPreviewUrl() {
            return previewUrl;
        }
    }
}