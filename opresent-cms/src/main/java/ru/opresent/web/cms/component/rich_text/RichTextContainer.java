package ru.opresent.web.cms.component.rich_text;

/**
 * User: artem
 * Date: 17.07.15
 * Time: 1:52
 */
public interface RichTextContainer {

    public String getText();

    public void setText(String text);

}
