package ru.opresent.web.cms.bean.section;

import org.primefaces.event.FileUploadEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.web.cms.bean.ImageFileBean;
import ru.opresent.web.cms.component.rich_text.RichTextContainer;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 21:35
 */
@Component("sectionItemBean")
@Scope("view")
public class SectionItemBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.section.section_item";

    @Resource(name = "sectionService")
    private transient SectionService sectionService;

    @Resource(name = "imageFileBean")
    private transient ImageFileBean imageFileBean;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private Mode mode;
    private SectionItem sectionItem;

    @PostConstruct
    private void init() {
        Long sectionId = WebUtils.getLongRequestParameter("section_id");
        Long sectionItemId = WebUtils.getLongRequestParameter("id");

        if (sectionId == null && sectionItemId == null) {
            throw new IllegalArgumentException("Section id or section item id must be defined as request parameter!");
        }
        if (sectionId != null && sectionItemId != null) {
            throw new IllegalArgumentException("Section id and section item id not be defined together as request parameters!");
        }

        if (sectionId != null) {
            mode = Mode.ADD;
            sectionItem = new SectionItem();
            sectionItem.setSection(sectionService.getAll().getById(sectionId));
        }

        if (sectionItemId != null) {
            mode = Mode.EDIT;
            sectionItem = sectionService.loadItem(sectionItemId);
        }
    }

    public void handleSectionItemImageUpload(FileUploadEvent event) {
        Image image = imageFileBean.saveImage(event.getFile(), ImageUse.SECTION_ITEM_PREVIEW_IMAGE);
        sectionItem.setPreview(image);
    }

    public String save() {
        try {
            sectionItem = sectionService.save(sectionItem);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            return null;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", sectionItem.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "section_item?faces-redirect=true&id=" + sectionItem.getId();
    }

    // setters & getters (BEGIN)

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public SectionItem getSectionItem() {
        return sectionItem;
    }

    public RichTextContainer getShortTextContainer() {
        return new RichTextContainer() {
            @Override
            public String getText() {
                return sectionItem.getShortText();
            }

            @Override
            public void setText(String text) {
                sectionItem.setShortText(text);
            }
        };
    }

    public RichTextContainer getFullTextContainer() {
        return new RichTextContainer() {
            @Override
            public String getText() {
                return sectionItem.getFullText();
            }

            @Override
            public void setText(String text) {
                sectionItem.setFullText(text);
            }
        };
    }

    // setters & getters (END)

    public enum Mode {
        ADD, EDIT
    }
}
