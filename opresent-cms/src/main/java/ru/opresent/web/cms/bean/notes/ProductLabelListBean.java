package ru.opresent.web.cms.bean.notes;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.ProductLabel;
import ru.opresent.core.domain.ProductLabelType;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.ProductLabelService;
import ru.opresent.web.cms.EditingList;
import ru.opresent.web.cms.bean.ImageFileBean;
import ru.opresent.web.cms.component.dialog.EditDialog;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 25.05.14
 * Time: 19:26
 */
@Component("productLabelListBean")
@Scope("view")
public class ProductLabelListBean extends EditingList<ProductLabel> implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.notes.product_label_list";

    @Resource(name = "productLabelService")
    private transient ProductLabelService productLabelService;

    @Resource(name = "imageFileBean")
    private transient ImageFileBean imageFileBean;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private ProductLabelEditDialog dialog = new ProductLabelEditDialog();

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public List<ProductLabel> loadList() {
        return productLabelService.getAll().getAll();
    }

    @Override
    public ProductLabelEditDialog getDialog() {
        return dialog;
    }

    @Override
    public void delete() {
        ProductLabel deletingLabel = getSelected();

        try {
            productLabelService.delete(deletingLabel);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success");
        reloadList();
    }

    // setters & getters

    public List<ProductLabelType> getAvailableLabelTypes() {
        return Arrays.asList(ProductLabelType.values());
    }

    // inner classes

    public class ProductLabelEditDialog extends EditDialog implements Serializable {

        private ProductLabel productLabel;
        private String title;

        @Override
        public void prepareToAdd() {
            productLabel = new ProductLabel();
            title = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
        }

        @Override
        public void prepareToEdit() {
            productLabel = new ProductLabel(getSelected());
            title = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
        }

        @Override
        public void save() {
            try {
                productLabelService.save(productLabel);
            } catch (ConstraintViolationException e) {
                messageManager.addErrorMessage(e);
                FacesContext.getCurrentInstance().validationFailed();
                return;
            }

            messageManager.addInfoMessage(BUNDLE_NAME, "save_success");
            reloadList();
            RequestContext.getCurrentInstance().update(Arrays.asList("productLabelListForm", "messages"));
        }

        public void handleImageUpload(FileUploadEvent event) {
            Image image = imageFileBean.saveImage(event.getFile(), ImageUse.PRODUCT_LABEL_IMAGE);
            productLabel.setImage(image);
        }

        @Override
        public String getTitle() {
            return title;
        }

        public ProductLabel getProductLabel() {
            return productLabel;
        }

        public boolean isProductLabelWithDiscount() {
            return productLabel.isWithDiscount();
        }

        public void setProductLabelWithDiscount(boolean withDiscount) {
            productLabel.setWithDiscount(withDiscount);
            if (!withDiscount) {
                productLabel.setDiscountPercent(null);
            }
        }
    }
}
