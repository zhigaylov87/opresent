package ru.opresent.web.cms.bean.size;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Size;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.SizeService;
import ru.opresent.web.cms.EditingList;
import ru.opresent.web.cms.component.dialog.EditDialog;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 24.10.13
 * Time: 0:25
 */
@Component("sizeListBean")
@Scope("view")
public class SizeListBean extends EditingList<Size> implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.size.size_list";

    @Resource(name = "sizeService")
    private transient SizeService sizeService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private SizeEditDialog sizeEditDialog = new SizeEditDialog();

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public List<Size> loadList() {
        return sizeService.getAll().getAll();
    }

    @Override
    public void delete() {
        Size deletingSize = getSelected();

        try {
            sizeService.delete(deletingSize);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", deletingSize.getDescription());
        reloadList();
    }

    @Override
    public SizeEditDialog getDialog() {
        return sizeEditDialog;
    }

    // inner classes

    public class SizeEditDialog extends EditDialog implements Serializable {

        private Size size;
        private String title;

        @Override
        public void prepareToAdd() {
            size = new Size();
            title = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
        }

        @Override
        public void prepareToEdit() {
            size = new Size(getSelected());
            title = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
        }

        @Override
        public void save() {
            try {
                sizeService.save(size);
            } catch (ConstraintViolationException e) {
                messageManager.addErrorMessage(e);
                FacesContext.getCurrentInstance().validationFailed();
                return;
            }

            messageManager.addInfoMessage(BUNDLE_NAME, "save_success", size.getDescription());
            reloadList();
            RequestContext.getCurrentInstance().update(Arrays.asList("listForm", "messages"));
        }

        @Override
        public String getTitle() {
            return title;
        }

        public Size getSize() {
            return size;
        }
    }
}
