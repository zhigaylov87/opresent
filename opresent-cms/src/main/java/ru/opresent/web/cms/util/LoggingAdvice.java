package ru.opresent.web.cms.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.core.util.stringify.Stringifiable;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * User: artem
 * Date: 25.11.14
 * Time: 0:52
 */
@Component
@Aspect
public class LoggingAdvice implements Serializable {

    @Resource(name = "log")
    private Logger log;

    // JSF Bean
    @Pointcut("@annotation(org.springframework.context.annotation.Scope)")
    public void jsfBeanInvoked() {}

    @Before(value = "jsfBeanInvoked()", argNames = "joinPoint")
    public void beforeJsfBeanInvoked(JoinPoint joinPoint) {
        logStart(joinPoint);
    }

    @AfterReturning(value = "jsfBeanInvoked()", argNames = "joinPoint, returnValue", returning = "returnValue")
    public void afterReturningJsfBeanInvoked(JoinPoint joinPoint, Object returnValue) {
        logEnd(joinPoint, returnValue);
    }

    @AfterThrowing(value = "jsfBeanInvoked()", argNames = "joinPoint, throwable", throwing = "throwable")
    public void afterThrowingJsfBeanInvoked(JoinPoint joinPoint, Throwable throwable) {
        logError(joinPoint, throwable);
    }

    // Service
    @Pointcut("within(@org.springframework.stereotype.Service *)")
    public void serviceInvoked() {}

    @Before(value = "serviceInvoked()", argNames = "joinPoint")
    public void beforeServiceInvoked(JoinPoint joinPoint) {
        logStart(joinPoint);
    }

    @AfterReturning(value = "serviceInvoked()", argNames = "joinPoint, returnValue", returning = "returnValue")
    public void afterReturningServiceInvoked(JoinPoint joinPoint, Object returnValue) {
        logEnd(joinPoint, returnValue);
    }

    @AfterThrowing(value = "serviceInvoked()", argNames = "joinPoint, throwable", throwing = "throwable")
    public void afterThrowingServiceInvoked(JoinPoint joinPoint, Throwable throwable) {
        logError(joinPoint, throwable);
    }

    // Dao
    @Pointcut("execution(public * ru.opresent.core.dao..*Dao.*(..))")
    public void daoInvoked() {}

    @Before(value = "daoInvoked()", argNames = "joinPoint")
    public void beforeDaoInvoked(JoinPoint joinPoint) {
        logStart(joinPoint);
    }

    @AfterReturning(value = "daoInvoked()", argNames = "joinPoint, returnValue", returning = "returnValue")
    public void afterReturningDaoInvoked(JoinPoint joinPoint, Object returnValue) {
        logEnd(joinPoint, returnValue);
    }

    @AfterThrowing(value = "daoInvoked()", argNames = "joinPoint, throwable", throwing = "throwable")
    public void afterThrowingDaoInvoked(JoinPoint joinPoint, Throwable throwable) {
        logError(joinPoint, throwable);
    }

    private void logStart(JoinPoint joinPoint) {
        StringBuilder sb = new StringBuilder("START");
        appendJoinPoint(sb, joinPoint);
        log.info(sb.toString());
    }

    private void logEnd(JoinPoint joinPoint, Object returnValue) {
        StringBuilder sb = new StringBuilder("END");
        appendJoinPoint(sb, joinPoint).append("|");
        appendValue(sb, returnValue);
        log.info(sb.toString());
    }

    private void logError(JoinPoint joinPoint, Throwable throwable) {
        StringBuilder sb = new StringBuilder("ERROR");
        appendJoinPoint(sb, joinPoint);
        log.error(sb.toString(), throwable);
    }

    private StringBuilder appendJoinPoint(StringBuilder sb, JoinPoint joinPoint) {
        sb.append("|").append(joinPoint.getSignature().toShortString()).append("|(");
        join(sb, Arrays.asList(joinPoint.getArgs()));
        sb.append(")");
        return sb;
    }

    private StringBuilder appendValue(StringBuilder sb, Object value) {
        if (value == null) {
            return sb.append("null");
        }

        Class valueClass = value.getClass();
        boolean cond =
                valueClass.isPrimitive() ||
                valueClass.isEnum() ||
                value instanceof String ||
                value instanceof Number ||
                value instanceof Boolean;
        if (cond) {
            return sb.append(value);
        }

        if (value instanceof Stringifiable) {
            return sb.append(((Stringifiable) value).stringify(false));
        }
        if (value instanceof Identifiable) {
            return appendIdentifiable(sb, ((Identifiable) value));
        }
        if (value instanceof IdentifiableList) {
            return joinWithSquareBrackets(sb, ((IdentifiableList) value).getAll());
        }
        if (value instanceof Collection) {
            return joinWithSquareBrackets(sb, (Collection) value);
        }

        return sb.append(value.toString());
    }

    private StringBuilder appendIdentifiable(StringBuilder sb, Identifiable value) {
        return sb.append("#").append(value.getId());
    }

    private StringBuilder join(StringBuilder sb, Collection items) {
        boolean first = true;
        for (Object item : items) {
            if (!first) {
                sb.append(", ");
            }
            first = false;
            appendValue(sb, item);
        }
        return sb;
    }

    private StringBuilder joinWithSquareBrackets(StringBuilder sb, Collection items) {
        sb.append("[");
        join(sb, items);
        return sb.append("]");
    }
}
