package ru.opresent.web.cms.bean.banner;

import org.primefaces.event.FileUploadEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.banner.BannerItem;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.api.BannerService;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.web.cms.bean.ImageFileBean;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("bannerItemBean")
@Scope("view")
public class BannerItemBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.banner.banner_item";

    @Resource(name = "bannerService")
    private transient BannerService bannerService;

    @Resource(name = "productService")
    private transient ProductService productService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "imageFileBean")
    private transient ImageFileBean imageFileBean;

    private Mode mode;
    private BannerItem bannerItem;

    @PostConstruct
    private void init() {
        Long bannerItemId = WebUtils.getLongRequestParameter("id");
        if (bannerItemId == null) {
            mode = Mode.ADD;
            bannerItem = new BannerItem();
        } else {
            mode = Mode.EDIT;
            bannerItem = new BannerItem(bannerService.getBanner().getAllItems().getById(bannerItemId));
        }
    }

    public void handleNormalImageUpload(FileUploadEvent event) {
        Image image = imageFileBean.saveImage(event.getFile(), ImageUse.BANNER_ITEM_NORMAL_IMAGE);
        bannerItem.setNormalImage(image);
    }

    public void handleMobileImageUpload(FileUploadEvent event) {
        Image image = imageFileBean.saveImage(event.getFile(), ImageUse.BANNER_ITEM_MOBILE_IMAGE);
        bannerItem.setMobileImage(image);
    }

    public String save() {
        long bannerItemId;
        try {
            bannerItemId = bannerService.save(bannerItem);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return null;
        }

        bannerItem = bannerService.getBanner().getAllItems().getById(bannerItemId);
        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", bannerItem.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "banner_item?faces-redirect=true&id=" + bannerItem.getId();
    }

    // getters & setters

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public BannerItem getBannerItem() {
        return bannerItem;
    }

    // inners classes

    private enum Mode {
        ADD, EDIT
    }
}

