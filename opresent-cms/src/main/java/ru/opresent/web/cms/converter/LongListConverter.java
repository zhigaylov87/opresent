package ru.opresent.web.cms.converter;

import javax.faces.convert.FacesConverter;

/**
 * User: artem
 * Date: 18.05.14
 * Time: 19:15
 */
@FacesConverter(value = "LongListConverter")
public class LongListConverter extends ListConverter<Long> {

    @Override
    protected String getStringByElement(Long element) {
        return element.toString();
    }

    @Override
    protected Long getElementByString(String s) {
        return Long.valueOf(s);
    }
}
