package ru.opresent.web.cms.component.galleria;

import ru.opresent.core.domain.image.Image;
import ru.opresent.web.cms.converter.IdentifiableConverter;

import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 08.02.14
 * Time: 1:36
 */
public class ImagesListModel implements Serializable {

    private List<Image> images;
    private Image selectedImage;

    public ImagesListModel() {
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
        selectedImage = images == null || images.isEmpty() ? null : images.get(0);
    }

    public Image getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(Image selectedImage) {
        if (!images.contains(selectedImage)) {
            throw new IllegalArgumentException("Image not found in model: " + selectedImage);
        }
        this.selectedImage = selectedImage;
    }

    public IdentifiableConverter<Image> getImageConverter() {
        return new IdentifiableConverter<>(images);
    }

    public void selectFirstImage() {
        setSelectedImage(images.get(0));
    }

    public void selectLastImage() {
        setSelectedImage(images.get(images.size() - 1));
    }

}
