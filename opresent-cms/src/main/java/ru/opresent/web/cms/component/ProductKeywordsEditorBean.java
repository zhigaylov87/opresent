package ru.opresent.web.cms.component;

import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.model.fluidgrid.FluidGridItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductKeyword;
import ru.opresent.core.service.api.ProductKeywordThesarusService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * User: artem
 * Date: 19.12.14
 * Time: 1:01
 */
@Component("productKeywordsEditorBean")
@Scope("view")
public class ProductKeywordsEditorBean implements Serializable {

    @Resource(name = "productKeywordThesarusService")
    private transient ProductKeywordThesarusService productKeywordThesarusService;

    private Product product;
    private List<FluidGridItem> productKeywordThesarus;

    @PostConstruct
    private void init() {
        reloadProductKeywordThesarus();
    }

    public List<ProductKeyword> completeKeyword(String query) {
        List<ProductKeyword> equals = new ArrayList<>();
        List<ProductKeyword> contains = new ArrayList<>();

        String queryLowerCase = query.toLowerCase();
        for (ProductKeyword keyword : productKeywordThesarusService.getAll()) {
            if (keyword.getKeyword().equals(queryLowerCase)) {
                equals.add(keyword);
            } else if (keyword.getKeyword().contains(queryLowerCase)) {
                contains.add(keyword);
            }
        }

        List<ProductKeyword> keywordsForQuery = new ArrayList<>();
        if (equals.isEmpty()) {
            keywordsForQuery.add(new ProductKeyword(query));
        } else {
            keywordsForQuery.addAll(equals);
        }
        keywordsForQuery.addAll(contains);

        return keywordsForQuery;
    }

    public void addKeywordToThesaurus(SelectEvent selectEvent) {
        ProductKeyword keyword = (ProductKeyword) selectEvent.getObject();
        if (!productKeywordThesarusService.getAll().contains(keyword)) {
            productKeywordThesarusService.add(keyword);
            reloadProductKeywordThesarus();
        }
    }

    public void removeKeywordFromThesaurus(ProductKeyword keyword) {
        productKeywordThesarusService.remove(keyword);
        reloadProductKeywordThesarus();
    }

    public void addKeywordToProduct(ProductKeyword keyword) {
        product.addKeyword(keyword);
    }

    private void reloadProductKeywordThesarus() {
        productKeywordThesarus = new ArrayList<>();
        Character currentFirstChar = null;
        List<ProductKeyword> currentKeywords = null;
        for (ProductKeyword keyword : productKeywordThesarusService.getAll()) {
            Character newFirstChar = keyword.getKeyword().toUpperCase().charAt(0);
            if (currentFirstChar == null || !currentFirstChar.equals(newFirstChar)) {
                currentFirstChar = newFirstChar;
                currentKeywords = new ArrayList<>();
                productKeywordThesarus.add(new FluidGridItem(new KeywordThesarusItemData(currentFirstChar, currentKeywords)));
            }
            currentKeywords.add(keyword);
        }
    }

    // setters & getters

    public List<ProductKeyword> getKeywords() {
        List<ProductKeyword> keywords = new ArrayList<>(product.getKeywords());
        Collections.sort(keywords);
        return keywords;
    }

    public void setKeywords(List<ProductKeyword> keywords) {
        product.setKeywords(keywords == null ? new HashSet<ProductKeyword>() : new HashSet<>(keywords));
    }

    public List<FluidGridItem> getProductKeywordThesarus() {
        return productKeywordThesarus;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public static class KeywordThesarusItemData implements Serializable {

        private Character firstChar;
        private List<ProductKeyword> keywords;

        public KeywordThesarusItemData(Character firstChar, List<ProductKeyword> keywords) {
            this.firstChar = firstChar;
            this.keywords = keywords;
        }

        public Character getFirstChar() {
            return firstChar;
        }

        public List<ProductKeyword> getKeywords() {
            return keywords;
        }
    }
}
