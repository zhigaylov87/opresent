package ru.opresent.web.cms.bean.livemaster;

import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PushEndpoint;
import ru.opresent.web.cms.util.JacksonJsonEncoder;

/**
 * User: artem
 * Date: 07.12.14
 * Time: 15:17
 */
@PushEndpoint(LivemasterSyncBean.PUSH_CHANNEL_PATH)
public class LivemasterDataPushEndPoint {

    @OnMessage(encoders = {JacksonJsonEncoder.class})
    public CmsSyncEvent onMessage(CmsSyncEvent syncEvent) {
        return syncEvent;
    }

}
