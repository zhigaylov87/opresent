package ru.opresent.web.cms.bean.image_synchronization;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.image.ImageFileProcessMode;
import ru.opresent.core.synchronization.ImageFileSynchronizer;
import ru.opresent.core.synchronization.ImageSynchronizationListener;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 16.07.14
 * Time: 23:48
 */
@Component("imageFileSynchronizationBean")
@Scope("view")
public class ImageFileSynchronizationBean implements ImageSynchronizationListener, Serializable {

    public static final String PUSH_CHANNEL_PATH = "/pages/image_synchronization/image_synchronization";

    @Resource(name = "imageFileSynchronizer")
    private transient ImageFileSynchronizer imageFileSynchronizer;

    private ImageFileProcessMode imageFileProcessMode = ImageFileProcessMode.LIGHT;
    private ImageFileSyncSession syncSession;

    public void start() {
        syncSession = new ImageFileSyncSession();

        final ImageSynchronizationListener listener = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                imageFileSynchronizer.synchronize(listener);
            }
        }).start();
    }

    @Override
    public void cleaningStarted() {
        syncSession.setMessage("Cleaning started");
        pushData();
    }

    @Override
    public void cleaningFinished() {
        syncSession.setMessage("Cleaning finished");
        pushData();
    }

    @Override
    public void error(Exception e) {
        syncSession.setMessage(ExceptionUtils.getStackTrace(e));
        pushData();
    }

    @Override
    public void consumeTotalCount(int totalCount) {
        syncSession.setTotalCount(totalCount);
        syncSession.setMessage("Total count: " + totalCount);
        pushData();
    }

    @Override
    public void consumeProcessedCount(int processedCount) {
        syncSession.setProcessedCount(processedCount);
        syncSession.setMessage("Processed count: " + processedCount);
        pushData();
    }

    @Override
    public void complete() {
        syncSession.setMessage("Synchronization complete.");
        pushData();
    }

    @Override
    public ImageFileProcessMode getImageFileProcessMode() {
        return imageFileProcessMode;
    }

    private void pushData() {
        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish(PUSH_CHANNEL_PATH, syncSession);
    }

    // setters & getters

    public void setImageFileProcessMode(ImageFileProcessMode imageFileProcessMode) {
        this.imageFileProcessMode = imageFileProcessMode;
    }

    public List<ImageFileProcessMode> getAvailableImageFileProcessModes() {
        return Arrays.asList(ImageFileProcessMode.values());
    }

    public String getPushChannelPath() {
        return PUSH_CHANNEL_PATH;
    }

}
