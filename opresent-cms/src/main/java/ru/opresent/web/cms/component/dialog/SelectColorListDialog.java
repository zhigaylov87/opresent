package ru.opresent.web.cms.component.dialog;

import ru.opresent.core.domain.Color;

import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 27.10.13
 * Time: 15:52
 */
public class SelectColorListDialog implements Serializable {

    private Handler handler;
    private Map<Color, Boolean> selection;

    public SelectColorListDialog(Handler handler) {
        this.handler = handler;
    }

    public void prepareToShow() {
        selection = new HashMap<>();
        Collection<Color> selectedColors = handler.getSelectedColors();

        if (selectedColors != null) {
            for (Color color : selectedColors) {
                selection.put(color, true);
            }
        }
    }

    public void applySelection() {
        Set<Color> selected = new HashSet<Color>();
        for (Color color : selection.keySet()) {
            if (!selection.get(color)) {
                continue;
            }

            selected.add(color);
        }

        handler.consumeSelectedColors(selected);
    }

    // setters & getters

    public Handler getHandler() {
        return handler;
    }

    public Map<Color, Boolean> getSelection() {
        return selection;
    }

    // inner entities

    public interface Handler extends Serializable {

        public List<Color> getAvailableColors();

        /**
         * @return selected colors or empty collection or {@code null}
         */
        public Set<Color> getSelectedColors();

        public void consumeSelectedColors(Set<Color> selected);

    }
}

