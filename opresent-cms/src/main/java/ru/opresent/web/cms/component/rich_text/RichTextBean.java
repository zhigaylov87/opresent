package ru.opresent.web.cms.component.rich_text;

import org.primefaces.event.FileUploadEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.web.cms.bean.ImageFileBean;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * User: artem
 * Date: 08.06.14
 * Time: 2:36
 */
@Component("richTextBean")
@Scope("view")
public class RichTextBean implements Serializable {

    @Resource(name = "imageFileBean")
    private transient ImageFileBean imageFileBean;

    private boolean withoutCopyright = false;
    private Image loadedImage;

    public void handleImageUpload(FileUploadEvent event) {
        loadedImage = imageFileBean.saveImage(event.getFile(), ImageUse.RICH_TEXT_IMAGE,
                withoutCopyright ? HasCopyrightOption.NO : HasCopyrightOption.INHERIT_FROM_IMAGE_USE);
    }

    // getters & setters

    public boolean isWithoutCopyright() {
        return withoutCopyright;
    }

    public void setWithoutCopyright(boolean withoutCopyright) {
        this.withoutCopyright = withoutCopyright;
    }

    public Image getLoadedImage() {
        return loadedImage;
    }


    // inner entities
}
