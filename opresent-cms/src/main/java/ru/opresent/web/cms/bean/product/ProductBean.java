package ru.opresent.web.cms.bean.product;


import org.primefaces.event.FileUploadEvent;
import org.primefaces.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.CoreException;
import ru.opresent.core.domain.*;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.service.api.*;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.web.cms.Deleter;
import ru.opresent.web.cms.bean.ImageFileBean;
import ru.opresent.web.cms.component.dialog.SelectCategoryListDialog;
import ru.opresent.web.cms.component.dialog.SelectColorListDialog;
import ru.opresent.web.cms.component.galleria.ImagesListModel;
import ru.opresent.web.cms.component.rich_text.RichTextContainer;
import ru.opresent.web.cms.converter.IdentifiableConverter;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

/**
 * User: artem
 * Date: 04.11.13
 * Time: 15:06
 */
@Component("productBean")
@Scope("view")
public class ProductBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.product.product";

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "productService")
    private transient ProductService productService;

    @Resource(name = "productTypeService")
    private transient ProductTypeService productTypeService;

    @Resource(name = "sizeService")
    private transient SizeService sizeService;

    @Resource(name = "categoryService")
    private transient CategoryService categoryService;

    @Resource(name = "colorService")
    private transient ColorService colorService;

    @Resource(name = "productLabelService")
    private transient ProductLabelService productLabelService;

    @Resource(name = "imageFileBean")
    private transient ImageFileBean imageFileBean;

    @Resource(name = "productStatusService")
    private transient ProductStatusService productStatusService;

    @Resource(name = "searchService")
    private transient SearchService searchService;

    private Product product;

    private SelectCategoryListDialog selectCategoryListDialog;
    private SelectColorListDialog selectColorListDialog;

    private ImagesListModel viewsListModel;

    private Tab currentTab = Tab.MAIN;

    @PostConstruct
    private void init() {
        Integer tabIndex = WebUtils.getIntegerRequestParameter("tab");
        currentTab = tabIndex == null ? currentTab : Tab.valueOfIndex(tabIndex);

        Long productId = WebUtils.getLongRequestParameter("id");
        if (productId == null) {
            product = new Product();
            product.setVisible(true);
        } else {
            product = productService.load(productId);
        }

        selectCategoryListDialog = new SelectCategoryListDialog(new SelectCategoryListDialogHandler());
        selectColorListDialog = new SelectColorListDialog(new SelectColorListDialogHandler());

        viewsListModel = new ImagesListModel();
        viewsListModel.setImages(product.getViews());
    }

    public String save() {
        try {
            product = productService.save(product);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            return null;
        }
        searchService.updateIndexAfterProductSave(product.getId());

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", product.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "product_list?faces-redirect=true";
    }

    public void handlePreviewImageUpload(FileUploadEvent event) {
        Image image = imageFileBean.saveImage(event.getFile(), ImageUse.PRODUCT_PREVIEW_IMAGE);
        product.setPreview(image);
    }

    public void handleViewImageUpload(FileUploadEvent event) {
        Image image = imageFileBean.saveImage(event.getFile(), ImageUse.PRODUCT_VIEW_IMAGE);
        product.getViews().add(image);

        viewsListModel.setImages(product.getViews());
        viewsListModel.selectLastImage();
    }

    public void moveViewImage(ViewMovingType movingType) {
        Image movingView = viewsListModel.getSelectedImage();
        List<Image> views = product.getViews();
        if (!views.contains(movingView)) {
            throw new CoreException("Moving view not contains in views list!");
        }
        int movingViewIndex = views.indexOf(movingView);
        boolean isFirst = movingViewIndex == 0;
        boolean isLast = movingViewIndex == views.size() - 1;

        if ((isFirst && (movingType == ViewMovingType.BEGIN || movingType == ViewMovingType.LEFT)) ||
            (isLast && (movingType == ViewMovingType.RIGHT || movingType == ViewMovingType.END))) {
            throw new IllegalArgumentException(
                    MessageFormat.format("Can not move view to {0} because it is {1}! [view: {2}]",
                            movingType, isFirst ? "first" : "last", movingView));
        }

        int newIndex;
        switch (movingType) {
            case BEGIN: newIndex = 0;
                break;
            case LEFT: newIndex = movingViewIndex - 1;
                break;
            case RIGHT: newIndex = movingViewIndex + 1;
                break;
            case END: newIndex = views.size() - 1;
                break;
            default:
                throw new IllegalArgumentException("Unknown element of " +
                        ViewMovingType.class.getName() + ": " + movingType);
        }

        views.remove(movingView);
        views.add(newIndex, movingView);
        viewsListModel.setImages(views);
        viewsListModel.setSelectedImage(movingView);
    }

    public void cleanProductLabel() {
        product.setLabel(null);
        product.setLabelNote(null);
    }

    public void defaultStatusNote() {
        ProductStatusInfo statusInfo = productStatusService.getAll().get(product.getStatus());
        product.setStatusNote(statusInfo == null ? null : statusInfo.getDefaultNote());
    }

    public void defaultLabelNote() {
        product.setLabelNote(product.getLabel().getDefaultNote());
    }

    private void deleteViewImage() {
        Image deletingView = viewsListModel.getSelectedImage();
        int deletingViewIndex = viewsListModel.getImages().indexOf(deletingView);
        boolean isLastViewDeleting = deletingViewIndex == viewsListModel.getImages().size() - 1;

        List<Image> views = product.getViews();
        views.remove(deletingView);
        viewsListModel.setImages(views);
        if (!views.isEmpty()) {
            if (isLastViewDeleting) {
                viewsListModel.selectLastImage();
            } else {
                viewsListModel.setSelectedImage(views.get(deletingViewIndex));
            }
        }
    }

    public ProductStatus getProductStatus() {
        return product.getStatus();
    }

    public void setProductStatus(ProductStatus status) {
        product.setStatus(status);
        if (status != ProductStatus.FOR_ORDER) {
            product.setProductionTime(null);
        }
    }

    // setters & getters

    public boolean isAddition() {
        return product.getId() == Identifiable.NOT_SAVED_ID;
    }

    public boolean isEditing() {
        return product.getId() != Identifiable.NOT_SAVED_ID;
    }

    public Tab getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(Tab currentTab) {
        this.currentTab = currentTab;
    }

    /**
     * Get tabs info as JSON:<br/>
     * <pre>
     * {
     *     "nameToIndex": {
     *                        "MAIN": 0,
     *                        ...
     *                    },
     *
     *     "indexToName": {
     *                        0: "MAIN",
     *                        ...
     *                    }
     * }
     * </pre>
     *
     * @return tabs info as JSON
     */
    public String getTabsInfo() {
        Map<String, Integer> nameToIndex = new HashMap<>();
        Map<Integer, String> indexToName = new HashMap<>();

        for (Tab tab : Tab.values()) {
            nameToIndex.put(tab.name(), tab.getIndex());
            indexToName.put(tab.getIndex(), tab.name());
        }

        Map<String, Map> tabsInfo = new HashMap<>();
        tabsInfo.put("nameToIndex", nameToIndex);
        tabsInfo.put("indexToName", indexToName);

        return new JSONObject(tabsInfo).toString();
    }

    public Product getProduct() {
        return product;
    }

    public List<ProductType> getAvailableProductTypes() {
        return productTypeService.getAll().getAll();
    }

    public IdentifiableConverter<ProductType> getProductTypesConverter() {
        return new IdentifiableConverter<>(productTypeService.getAll().getAll());
    }

    public List<ProductStatus> getAvailableStatuses() {
        return Arrays.asList(ProductStatus.values());
    }

    public List<Size> getAvailableSizes() {
        return sizeService.getAll().getAll();
    }

    public IdentifiableConverter<Size> getSizesConverter() {
        return new IdentifiableConverter<>(sizeService.getAll().getAll(), true);
    }

    public List<ProductLabel> getAvailableLabels() {
        return productLabelService.getAll().getAll();
    }

    public IdentifiableConverter<ProductLabel> getLabelConverter() {
        return new IdentifiableConverter<>(productLabelService.getAll().getAll(), true);
    }

    public List<Category> getSortedProductCategories() {
        final IdentifiableList<Category> allCategories = categoryService.getAll();
        List<Category> productCategories = new ArrayList<>(product.getCategories());
        Collections.sort(productCategories, new Comparator<Category>() {
            @Override
            public int compare(Category c1, Category c2) {
                int index1 = allCategories.getAll().indexOf(c1);
                int index2 = allCategories.getAll().indexOf(c2);
                return Integer.compare(index1, index2);
            }
        });

        return productCategories;
    }

    public List<Color> getProductColorsAsList() {
        return new ArrayList<>(product.getColors());
    }

    public SelectCategoryListDialog getSelectCategoryListDialog() {
        return selectCategoryListDialog;
    }

    public SelectColorListDialog getSelectColorListDialog() {
        return selectColorListDialog;
    }

    public ImagesListModel getViewsListModel() {
        return viewsListModel;
    }

    public Deleter getViewImageDeleter() {
        return new Deleter() {
            @Override
            public void delete() {
                deleteViewImage();
            }
        };
    }

    public RichTextContainer getDesriptionContainer() {
        return new RichTextContainer() {
            @Override
            public String getText() {
                return product.getDescription();
            }

            @Override
            public void setText(String text) {
                product.setDescription(text);
            }
        };
    }

    // inner entities

    public enum Tab {
        MAIN(0),
        VIEWS(1),
        LINKS(2);

        private int index;

        Tab(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }

        public static Tab valueOfIndex(int tabIndex) {
            for (Tab tab : values()) {
                if (tab.getIndex() == tabIndex) {
                    return tab;
                }
            }
            throw new IllegalArgumentException("Illegal tab index: " + tabIndex);
        }
    }

    public enum ViewMovingType {
        BEGIN,
        LEFT,
        RIGHT,
        END
    }

    private class SelectCategoryListDialogHandler implements SelectCategoryListDialog.Handler {
        @Override
        public List<Category> getAvailableCategories() {
            return categoryService.getAll().getAll();
        }

        @Override
        public Set<Category> getSelectedCategories() {
            return product.getCategories();
        }

        @Override
        public void consumeSelectedCategories(Set<Category> selected) {
            product.setCategories(selected);
        }
    }

    private class SelectColorListDialogHandler implements SelectColorListDialog.Handler {

        @Override
        public List<Color> getAvailableColors() {
            return colorService.getAll().getAll();
        }

        @Override
        public Set<Color> getSelectedColors() {
            return product.getColors();
        }

        @Override
        public void consumeSelectedColors(Set<Color> selected) {
            product.setColors(selected);
        }
    }


}
