package ru.opresent.web.cms.bean.product;

import org.springframework.stereotype.Component;
import ru.opresent.core.domain.*;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.service.api.SizeService;
import ru.opresent.web.cms.component.table.FilterActionListener;
import ru.opresent.web.cms.converter.IdentifiableConverter;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 22.04.14
 * Time: 0:03
 */
@Component("productFilterBean")
//@Scope("session")
public class ProductFilterBean implements Serializable {

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "sizeService")
    private SizeService sizeService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    private DefaultProductFilter filter = new DefaultProductFilter();

    private FilterActionListener filterActionListener;

    public void invokeFiltering() {
        filterActionListener.filteringInvoked();
    }

    public void invokeReset() {
        filter = new DefaultProductFilter();
        filterActionListener.resetInvoked();
    }

    // setters & getters

    public DefaultProductFilter getFilter() {
        return filter;
    }

    public void setFilterActionListener(FilterActionListener filterActionListener) {
        this.filterActionListener = filterActionListener;
    }

    public List<Category> getCategories() {
        return categoryService.getAll().getAll();
    }

    public IdentifiableConverter<Category> getCategoryConverter() {
        return new IdentifiableConverter<>(getCategories(), true);
    }

    public List<Size> getSizes() {
        return sizeService.getAll().getAll();
    }

    public IdentifiableConverter<Size> getSizeConverter() {
        return new IdentifiableConverter<>(getSizes(), true);
    }

    public List<ProductType> getProductTypes() {
        return productTypeService.getAll().getAll();
    }

    public IdentifiableConverter<ProductType> getProductTypeConverter() {
        return new IdentifiableConverter<>(getProductTypes(), true);
    }

    public List<ProductStatus> getStatuses() {
        return Arrays.asList(ProductStatus.values());
    }

}