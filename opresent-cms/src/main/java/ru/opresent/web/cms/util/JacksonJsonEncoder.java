package ru.opresent.web.cms.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.atmosphere.config.managed.Encoder;
import ru.opresent.core.CoreException;
import ru.opresent.core.util.ConfiguredObjectMapper;

import java.io.IOException;

/**
 * {@link org.primefaces.push.impl.JSONEncoder} not correctly works with {@link java.lang.Enum}, but this works correct
 * User: artem
 * Date: 07.12.14
 * Time: 17:36
 */
public class JacksonJsonEncoder implements Encoder<Object, String> {

    private static final ObjectMapper JSON_MAPPER = new ConfiguredObjectMapper();

    @Override
    public String encode(Object object) {
        try {
            return JSON_MAPPER.writeValueAsString(object);
        } catch (IOException e) {
            throw new CoreException(e);
        }
    }
}
