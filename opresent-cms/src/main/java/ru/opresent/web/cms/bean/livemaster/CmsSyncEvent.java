package ru.opresent.web.cms.bean.livemaster;

import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.opresent.core.livemaster.sync.event.LivemasterSyncEventType;

import java.util.Objects;

/**
 * User: artem
 * Date: 23.04.2017
 * Time: 2:14
 */
public class CmsSyncEvent {

    private LivemasterSyncEventType type;
    private ObjectNode data;

    public CmsSyncEvent(LivemasterSyncEventType type) {
        this(type, null);
    }

    public CmsSyncEvent(LivemasterSyncEventType type, ObjectNode data) {
        this.type = Objects.requireNonNull(type);
        this.data = data;
    }

    public LivemasterSyncEventType getType() {
        return type;
    }

    public ObjectNode getData() {
        return data;
    }
}
