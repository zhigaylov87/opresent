package ru.opresent.web.cms.bean.image_synchronization;

import java.io.Serializable;
import java.util.Date;

/**
 * User: artem
 * Date: 16.12.14
 * Time: 5:28
 */
public class ImageFileSyncSession implements Serializable {

    private int totalCount;
    private int processedCount;
    private String message;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getProcessedCount() {
        return processedCount;
    }

    public void setProcessedCount(int processedCount) {
        this.processedCount = processedCount;
    }

    public int getProcessedPercent() {
        return totalCount == 0 ? 0 : processedCount * 100 / totalCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = "[" + new Date() + "] " + message;
    }
}
