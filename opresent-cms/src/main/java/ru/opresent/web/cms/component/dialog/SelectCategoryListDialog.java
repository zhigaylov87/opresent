package ru.opresent.web.cms.component.dialog;

import ru.opresent.core.domain.Category;

import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 27.10.13
 * Time: 15:52
 */
public class SelectCategoryListDialog implements Serializable {

    private Handler handler;
    private Map<Category, Boolean> selection;

    public SelectCategoryListDialog(Handler handler) {
        this.handler = handler;
    }

    public void prepareToShow() {
        selection = new HashMap<>();
        Collection<Category> selectedCategories = handler.getSelectedCategories();

        if (selectedCategories != null) {
            for (Category category : selectedCategories) {
                selection.put(category, true);
            }
        }
    }

    public void applySelection() {
        Set<Category> selected = new HashSet<>();
        for (Map.Entry<Category, Boolean> entry : selection.entrySet()) {
            if (Boolean.TRUE.equals(entry.getValue())) {
                selected.add(entry.getKey());
            }
        }
        handler.consumeSelectedCategories(selected);
    }

    // setters & getters

    public Handler getHandler() {
        return handler;
    }

    public Map<Category, Boolean> getSelection() {
        return selection;
    }

    // inner entities

    public interface Handler extends Serializable {

        public List<Category> getAvailableCategories();

        /**
         * @return selected categories or empty collection or {@code null}
         */
        public Set<Category> getSelectedCategories();

        public void consumeSelectedCategories(Set<Category> selected);

    }
}

