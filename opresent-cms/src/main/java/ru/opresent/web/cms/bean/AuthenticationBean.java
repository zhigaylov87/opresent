package ru.opresent.web.cms.bean;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.MessageDigest;

/**
 * User: artem
 * Date: 20.05.15
 * Time: 1:55
 */
@Component("authenticationBean")
@Scope("session")
public class AuthenticationBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.authentification";

    private static final String USERNAME_DIGEST_BASE64 = "xUfJbU1Ki8rnFCXWLVZIsA==";
    private static final String PASSWORD_DIGEST_BASE64 = "gmc6XckVzu8StEU1b5VEWA==";

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "log")
    private transient Logger log;

    private String username;
    private String password;

    public String login() {
        if (accessGranted()) {
            getSession().setAttribute("user", username);
            return "welcome?faces-redirect=true";
        } else {
            messageManager.addErrorMessage(BUNDLE_NAME, "authentication_failed");
            return null;
        }
    }

    public String logout() {
        getSession().invalidate();
        return "/pages/authentication?faces-redirect=true";
    }

    private HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    private boolean accessGranted() {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return false;
        }
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] usernameDigest = md.digest(username.getBytes("UTF-8"));
            byte[] passwordDigest = md.digest(password.getBytes("UTF-8"));

            String usernameDigestBase64 = Base64.encodeBase64String(usernameDigest);
            String passwordDigestBase64 = Base64.encodeBase64String(passwordDigest);

            return USERNAME_DIGEST_BASE64.equals(usernameDigestBase64) && PASSWORD_DIGEST_BASE64.equals(passwordDigestBase64);
        } catch (Exception e) {
            log.error("Error while login", e);
            return false;
        }
    }

    // getters & setters

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
