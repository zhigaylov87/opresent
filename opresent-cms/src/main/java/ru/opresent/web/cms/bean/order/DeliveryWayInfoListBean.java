package ru.opresent.web.cms.bean.order;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.order.DeliveryWayInfo;
import ru.opresent.core.service.api.DeliveryWayService;
import ru.opresent.core.util.EnumHelper;
import ru.opresent.web.cms.EditingList;
import ru.opresent.web.cms.component.dialog.EditDialog;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 20.02.15
 * Time: 2:15
 */
@Component("deliveryWayInfoListBean")
@Scope("view")
public class DeliveryWayInfoListBean extends EditingList<DeliveryWayInfo> implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.order.delivery_way_info_list";

    @Resource(name = "deliveryWayService")
    private transient DeliveryWayService deliveryWayService;

    @Resource(name = "enumHelper")
    private transient EnumHelper enumHelper;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private DeliveryWayInfoEditDialog dialog = new DeliveryWayInfoEditDialog();

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public List<DeliveryWayInfo> loadList() {
        return deliveryWayService.getAll().getAll();
    }

    @Override
    public DeliveryWayInfoEditDialog getDialog() {
        return dialog;
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Delivery way info can not be deleted!");
    }

    // inner classes

    public class DeliveryWayInfoEditDialog extends EditDialog implements Serializable {

        private DeliveryWayInfo deliveryWayInfo;
        private String title;

        @Override
        public void prepareToAdd() {
            throw new UnsupportedOperationException("Delivery way info can not be added!");
        }

        @Override
        public void prepareToEdit() {
            deliveryWayInfo = new DeliveryWayInfo(getSelected());
            title = messageManager.getMessage(BUNDLE_NAME,
                    "edit_dialog_title", enumHelper.getDescription(deliveryWayInfo.getDeliveryWay()));
        }

        @Override
        public void save() {
            try {
                deliveryWayService.save(deliveryWayInfo);
            } catch (ConstraintViolationException e) {
                messageManager.addErrorMessage(e);
                FacesContext.getCurrentInstance().validationFailed();
                return;
            }

            messageManager.addInfoMessage(BUNDLE_NAME,
                    "save_success", enumHelper.getDescription(deliveryWayInfo.getDeliveryWay()));
            reloadList();
            RequestContext.getCurrentInstance().update(Arrays.asList("deliveryWayInfoListForm", "messages"));
        }

        @Override
        public String getTitle() {
            return title;
        }

        public DeliveryWayInfo getDeliveryWayInfo() {
            return deliveryWayInfo;
        }
    }
}
