package ru.opresent.web.cms.util.exceptionhandler;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * User: artem
 * Date: 14.02.14
 * Time: 1:27
 */
public class ErrorPageRedirectNotAjaxExceptionHandlerFactory extends ExceptionHandlerFactory {


    private ExceptionHandlerFactory parent;

    public ErrorPageRedirectNotAjaxExceptionHandlerFactory(ExceptionHandlerFactory parent) {
        this.parent = parent;
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new ErrorPageRedirectNotAjaxExceptionHandler(parent.getExceptionHandler());

    }
}
