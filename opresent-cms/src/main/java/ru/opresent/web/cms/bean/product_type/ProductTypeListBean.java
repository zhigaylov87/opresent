package ru.opresent.web.cms.bean.product_type;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("productTypeListBean")
@Scope("view")
public class ProductTypeListBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.product_type.product_type_list";

    @Resource(name = "productTypeService")
    private transient ProductTypeService productTypeService;

    @Resource(name = "livemasterAliasService")
    private transient LivemasterAliasService livemasterAliasService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private IdentifiableList<LivemasterAliases> productTypesAliases;
    private ProductType selectedProductType;

    @PostConstruct
    private void init() {
        reloadProductTypesAliases();
    }

    public String add() {
        return "product_type?faces-redirect=true";
    }

    public String edit() {
        return "product_type?faces-redirect=true&id=" + selectedProductType.getId();
    }

    public void delete() {
        ProductType productType = selectedProductType;

        try {
            productTypeService.delete(productType);
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        selectedProductType = null;
        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", productType.getName());
        reloadProductTypesAliases();
    }

    private void reloadProductTypesAliases() {
        productTypesAliases = new IdentifiableList<>(livemasterAliasService.load(LivemasterAliasType.PRODUCT_TYPE));
    }

    public List<String> getLivemasterAliases(ProductType productType) {
        if (!productTypesAliases.containsById(productType.getId())) {
            return null;
        }
        List<String> aliases = new ArrayList<>(productTypesAliases.getById(productType.getId()).getAliases());
        Collections.sort(aliases);
        return aliases;
    }

    public void moveUp(ProductType productType) {
         moveUpDown(productType, true);
    }

    public void moveDown(ProductType productType) {
        moveUpDown(productType, false);
    }

    private void moveUpDown(ProductType productType, boolean isUp) {
        if (isUp) {
            productTypeService.moveUp(productType);
        } else {
            productTypeService.moveDown(productType);
        }

        reloadProductTypesAliases();
    }

    // getters & setters

    public List<ProductType> getProductTypes() {
        return productTypeService.getAll().getAll();
    }

    public ProductType getSelectedProductType() {
        return selectedProductType;
    }

    public void setSelectedProductType(ProductType selectedProductType) {
        this.selectedProductType = selectedProductType;
    }
}

