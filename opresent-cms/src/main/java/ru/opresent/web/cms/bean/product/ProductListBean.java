package ru.opresent.web.cms.bean.product;

import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.Product;
import ru.opresent.core.pagination.Page;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.service.ServiceException;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SearchService;
import ru.opresent.web.cms.Deleter;
import ru.opresent.web.cms.component.table.PaginationDataModel;
import ru.opresent.web.cms.util.MessageManager;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * User: artem
 * Date: 06.01.14
 * Time: 23:26
 */
@Component("productListBean")
//@Scope("session")
public class ProductListBean implements PaginationDataModel.Handler<Product>, Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.product.product_list";

    @Resource(name = "productService")
    private transient ProductService productService;

    @Resource(name = "searchService")
    private transient SearchService searchService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "productFilterBean")
    private ProductFilterBean productFilterBean;

    private PaginationDataModel<Product> productsModel = new PaginationDataModel<>(this);
    private Product selectedProduct;

    public String edit() {
        return "product?faces-redirect=true&id=" + selectedProduct.getId();
    }

    private void delete() {
        try {
            productService.delete(selectedProduct.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }
        searchService.updateIndexAfterProductDelete(selectedProduct.getId());

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", selectedProduct.getName());
        selectedProduct = null;
    }

    public boolean isOnlyOneCategory(Product product) {
        return product.getCategories().size() == 1;
    }

    public Category getFirstCategory(Product product) {
        return product.getCategories().iterator().next();
    }

    public List<Category> getCategoriesList(Product product) {
        List<Category> categories = new ArrayList<>(product.getCategories());
        Collections.sort(categories, new Comparator<Category>() {
            @Override
            public int compare(Category c1, Category c2) {
                return c1.getName().compareTo(c2.getName());
            }
        });
        return categories;
    }

    @Override
    public String getDataTableId() {
        return ":tableForm:productsTable";
    }

    @Override
    public Page<Product> loadPage(PageSettings pageSettings) {
        return productService.loadWithCategories(productFilterBean.getFilter(), pageSettings);
    }

    @Override
    public void deselect() {
        selectedProduct = null;
    }


    // setters & getters

    public PaginationDataModel<Product> getProductsModel() {
        return productsModel;
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public Deleter getDeleter() {
        return new Deleter() {
            @Override
            public void delete() {
                ProductListBean.this.delete();
            }
        };
    }

    // inner entities
}