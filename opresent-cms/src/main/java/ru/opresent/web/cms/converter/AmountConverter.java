package ru.opresent.web.cms.converter;

import ru.opresent.core.util.FormatUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * User: artem
 * Date: 02.09.14
 * Time: 1:18
 * @see ru.opresent.core.util.FormatUtils#formatAmount(int)
 * @see ru.opresent.core.util.FormatUtils#formatAmount(long)
 */
@FacesConverter(value = "amountConverter")
public class AmountConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new UnsupportedOperationException("AmountConverter is not working in this direction!");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Integer) {
            return FormatUtils.formatAmount((Integer) value);
        }
        if (value instanceof Long) {
            return FormatUtils.formatAmount((Long) value);
        }

        throw new ConverterException("Invalid value: " + value +
                ", value class: " + (value == null ? null : value.getClass().getName()));

    }
}
