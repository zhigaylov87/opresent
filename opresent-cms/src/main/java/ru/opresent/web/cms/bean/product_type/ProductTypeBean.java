package ru.opresent.web.cms.bean.product_type;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.web.cms.bean.ImageFileBean;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 17:55
 */
@Component("productTypeBean")
@Scope("view")
public class ProductTypeBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.product_type.product_type";

    @Resource(name = "productTypeService")
    private transient ProductTypeService productTypeService;

    @Resource(name = "livemasterAliasService")
    private transient LivemasterAliasService livemasterAliasService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "imageFileBean")
    private transient ImageFileBean imageFileBean;

    private Mode mode;
    private ProductType productType;
    private LivemasterAliases livemasterAliases;

    @PostConstruct
    private void init() {
        Long productTypeId = WebUtils.getLongRequestParameter("id");
        if (productTypeId == null) {
            mode = Mode.ADD;
            productType = new ProductType();
            livemasterAliases = new LivemasterAliases(LivemasterAliasType.PRODUCT_TYPE);
        } else {
            mode = Mode.EDIT;
            productType = new ProductType(productTypeService.getAll().getById(productTypeId));
            livemasterAliases = livemasterAliasService.load(productTypeId, LivemasterAliasType.PRODUCT_TYPE);
        }
    }

    public void handleProductTypeImageUpload(FileUploadEvent event) {
        Image image = imageFileBean.saveImage(event.getFile(), ImageUse.PRODUCT_TYPE_IMAGE);
        productType.setImage(image);
    }

    public String save() {
        long productTypeId;
        try {
            productTypeId = productTypeService.save(productType, livemasterAliases);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return null;
        }

        productType = productTypeService.getAll().getById(productTypeId);
        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", productType.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "product_type?faces-redirect=true&id=" + productType.getId();
    }

    public List<String> completeAlias(String query) {
        return Arrays.asList(query);
    }

    public void onAliasUnselect(UnselectEvent unselectEvent) {
        livemasterAliases.removeAlias((String)unselectEvent.getObject());
    }

    // getters & setters

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public ProductType getProductType() {
        return productType;
    }

    public List<String> getLivemasterAliases() {
        List<String> aliases = new ArrayList<>(livemasterAliases.getAliases());
        Collections.sort(aliases);
        return aliases;
    }

    public void setLivemasterAliases(List<String> aliases) {
        if (aliases != null) {
            for (String alias : aliases) {
                if (!livemasterAliases.getAliases().contains(alias)) {
                    livemasterAliases.addAlias(alias);
                }
            }
        }
    }

    // inners classes

    private enum Mode {
        ADD, EDIT
    }
}

