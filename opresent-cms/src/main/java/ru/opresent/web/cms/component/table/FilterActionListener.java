package ru.opresent.web.cms.component.table;

/**
 * User: artem
 * Date: 23.04.14
 * Time: 1:46
 */
public interface FilterActionListener {

    public void filteringInvoked();

    public void resetInvoked();

}
