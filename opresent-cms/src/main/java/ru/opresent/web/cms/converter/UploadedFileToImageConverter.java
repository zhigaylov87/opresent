package ru.opresent.web.cms.converter;

import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.image.ImageConstraints;
import ru.opresent.core.domain.image.HasCopyrightOption;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUse;

import javax.annotation.Resource;
import javax.faces.convert.ConverterException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 30.03.14
 * Time: 22:52
 */
@Component("uploadedFileToImageConverter")
public class UploadedFileToImageConverter {

    private static final String IMAGE_CONTENT_TYPE_PREFIX = "image/";

    private static final String IS_NOT_IMAGE_MSG =
            "File ''{0}'' is not image file. MIME-type: ''{1}''.";

    private static final String VERY_BIG_MSG =
            "Image file is very big: {0} bytes. Max image file size: {1} bytes.";

    private static final String INVALID_FORMAT =
            "Image file format ''{0}'' not supported. Supported image file formats: {1}.";

    private static final String READ_IMAGE_CONTENT_ERROR = "Error while reading image content.";

    @Resource(name = "imageConstraints")
    private ImageConstraints imageConstraints;

    public Image convert(UploadedFile uploadedFile, ImageUse imageUse, HasCopyrightOption hasCopyrightOption) throws ConverterException {
        String fileName = uploadedFile.getFileName();
        String contentType = uploadedFile.getContentType();

        if (!contentType.startsWith(IMAGE_CONTENT_TYPE_PREFIX)) {
            throw new ConverterException(MessageFormat.format(IS_NOT_IMAGE_MSG, fileName, contentType));
        }

        long size = uploadedFile.getSize();
        int maxSize = imageConstraints.getMaxSize();
        if (size > maxSize) {
            throw new ConverterException(MessageFormat.format(VERY_BIG_MSG, size, maxSize));
        }

        String format = contentType.substring(contentType.indexOf("/") + 1);
        List<String> supportedFormats = imageConstraints.getSupportedFormats();
        if (!supportedFormats.contains(format)) {
            throw new ConverterException(MessageFormat.format(INVALID_FORMAT,
                    format, Arrays.toString(supportedFormats.toArray())));
        }

        Image image = new Image(format, imageUse, hasCopyrightOption);
        try {
            InputStream inputStream = null;
            try {
                inputStream = uploadedFile.getInputstream();
                image.setContent(IOUtils.toByteArray(inputStream));
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        } catch (IOException e) {
            throw new ConverterException(READ_IMAGE_CONTENT_ERROR, e);
        }

        return image;
    }

}