package ru.opresent.web.cms.bean.category;

import org.primefaces.event.UnselectEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Category;
import ru.opresent.core.livemaster.alias.LivemasterAliasType;
import ru.opresent.core.livemaster.alias.LivemasterAliases;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.LivemasterAliasService;
import ru.opresent.web.cms.util.MessageManager;
import ru.opresent.web.cms.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 21:09
 */
@Component("categoryBean")
@Scope("view")
public class CategoryBean implements Serializable {

    private static final String BUNDLE_NAME = "ru.opresent.web.cms.i18n.category.category";

    @Resource(name = "categoryService")
    private transient CategoryService categoryService;

    @Resource(name = "livemasterAliasService")
    private transient LivemasterAliasService livemasterAliasService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private Mode mode;
    private Category category;
    private LivemasterAliases livemasterAliases;

    @PostConstruct
    private void init() {
        Long categoryId = WebUtils.getLongRequestParameter("id");
        if (categoryId == null) {
            mode = Mode.ADD;
            category = new Category();
            livemasterAliases = new LivemasterAliases(LivemasterAliasType.CATEGORY);
        } else {
            mode = Mode.EDIT;
            category = new Category(categoryService.getAll().getById(categoryId));
            livemasterAliases = livemasterAliasService.load(categoryId, LivemasterAliasType.CATEGORY);
        }
    }

    public String save() {
        try {
            categoryService.save(category, livemasterAliases);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return null;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", category.getName());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "category_list?faces-redirect=true";
    }

    public List<String> completeAlias(String query) {
        return Arrays.asList(query);
    }

    public void onAliasUnselect(UnselectEvent unselectEvent) {
        livemasterAliases.removeAlias((String)unselectEvent.getObject());
    }

    // getters & setters

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public Category getCategory() {
        return category;
    }

    public List<String> getLivemasterAliases() {
        List<String> aliases = new ArrayList<>(livemasterAliases.getAliases());
        Collections.sort(aliases);
        return aliases;
    }

    public void setLivemasterAliases(List<String> aliases) {
        if (aliases != null) {
            for (String alias : aliases) {
                if (!livemasterAliases.getAliases().contains(alias)) {
                    livemasterAliases.addAlias(alias);
                }
            }
        }
    }

    // inners classes

    private enum Mode {
        ADD, EDIT
    }




}
