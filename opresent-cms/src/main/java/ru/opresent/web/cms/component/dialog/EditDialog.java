package ru.opresent.web.cms.component.dialog;

import ru.opresent.core.CoreException;

import java.io.Serializable;

/**
 * User: artem
 * Date: 24.10.13
 * Time: 0:19
 */
public abstract class EditDialog implements Serializable {
    private Mode mode;

    public final void prepareToShow(Mode mode) {
        this.mode = mode;
        switch (mode) {
            case ADD:
                prepareToAdd();
                break;
            case EDIT:
                prepareToEdit();
                break;
            default:
                throw new CoreException("Unknown dialog mode: " + mode);
        }
    }

    public abstract void prepareToAdd();

    public abstract void prepareToEdit();

    public abstract void save();

    public abstract String getTitle();

    public Mode getMode() {
        return mode;
    }

    public boolean isAddition() {
        return mode == Mode.ADD;
    }

    public boolean isEditing() {
        return mode == Mode.EDIT;
    }

    public enum Mode {
        ADD, EDIT
    }
}
