package ru.opresent.web.cms.bean.image_synchronization;

import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PushEndpoint;
import ru.opresent.web.cms.util.JacksonJsonEncoder;

/**
 * User: artem
 * Date: 07.12.14
 * Time: 15:17
 */
@PushEndpoint(ImageFileSynchronizationBean.PUSH_CHANNEL_PATH)
public class ImageFileSyncPushEndPoint {

    @OnMessage(encoders = {JacksonJsonEncoder.class})
    public ImageFileSyncSession onMessage(ImageFileSyncSession data) {
        return data;
    }

}
