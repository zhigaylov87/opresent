
var ALL_SYNC_EVENT_ELEMENTS_IDS = ["syncStart",
    "syncProductsDeterminingStart", "syncProductsDeterminingComplete", "syncProductsDeterminingFailed",
    "syncProductsProcessingStart", "syncProductsProcessingContainer", "syncProductsProcessingComplete",
    "notActualLivemasterIdsProcessingStart", "notActualLivemasterIdsCount",
    "notActualLivemasterIdsProcessingContainer", "notActualLivemasterIdsProcessingComplete",
    "syncStopped", "syncComplete"];

var SYNC_PRODUCT_RESULT_TYPE_ALL = "ALL";
var SYNC_PRODUCT_RESULT_TYPE_PRODUCT_ADDED = "PRODUCT_ADDED";
var SYNC_PRODUCT_RESULT_TYPES = [SYNC_PRODUCT_RESULT_TYPE_ALL, SYNC_PRODUCT_RESULT_TYPE_PRODUCT_ADDED,
    "PRODUCT_STATUS_UPDATED", "PRODUCT_STATUS_AND_PRICE_UPDATED", "PRODUCT_PRICE_UPDATED",
    "NOTHING_DONE", "NOTHING_DONE_FOR_DELETED", "ERROR"];


function prepareForSyncStart() {
    _hideSyncEventElements();
}

function _hideSyncEventElements() {
    ALL_SYNC_EVENT_ELEMENTS_IDS.forEach(function (id) {
        $("#" + id).hide();
    });
}

function _setSyncProductsProgress(count, totalCount) {
    var percents = parseInt(count * 100 / totalCount);
    PF("syncProductsProgressBarWidget").setValue(percents);
}

function _setNotActualLivemasterIdsProgress(count, totalCount) {
    var percents = parseInt(count * 100 / totalCount);
    PF("notActualLivemasterIdsProgressBarWidget").setValue(percents);
}

function _initSyncProductsProcessingContainer() {
    var count = 0;
    var totalCount = parseInt($("#syncProductsDeterminingComplete").find(".count").text());

    var syncProductProcessed = $("#syncProductProcessed");
    syncProductProcessed.find(".count").text(count);
    syncProductProcessed.find(".total-count").text(totalCount);

    _setSyncProductsProgress(count, totalCount);

    var syncProductResultFilters = $("#syncProductResultFilters");
    syncProductResultFilters.find(".count").text(0);
    syncProductResultFilters.find(".warningsCount").text(0);

    $("#syncProducts").find("tbody").empty();
}

function _initNotActualLivemasterIdsProcessingContainer(notActualLivemasterIdsCount) {
    var count = 0;
    var totalCount = notActualLivemasterIdsCount;

    var notActualLivemasterIdProcessed = $("#notActualLivemasterIdProcessed");
    notActualLivemasterIdProcessed.find(".count").text(count);
    notActualLivemasterIdProcessed.find(".total-count").text(totalCount);

    _setNotActualLivemasterIdsProgress(count, totalCount);

    $("#notActualLivemasterIdsProducts").find("tbody").empty();
}

function _appendSyncProductTableRow(syncProduct, syncProductResult) {
    var tr = $("<tr>").attr("data-result-type", syncProductResult.type)
        .append(
            $("<td>").append($("table#syncProducts > tbody > tr").size() + 1)
        )
        .append(
            $("<td>").append(
                $("<a>")
                    .attr("href", syncProduct.pageUrl)
                    .attr("target", "_blank")
                    .text(syncProduct.livemasterId)
            )
        );

    // product
    var productTd = $("<td>").addClass("market-product");
    var product = syncProductResult.product;
    if (product) {
        productTd.append(_createProductView(product));
    }
    tr.append(productTd);

    // result
    var resultTd = $("<td>");
    if (syncProductResult.error) {
        var constraintViolations = syncProductResult.error.constraintViolations;
        if (constraintViolations) {
            resultTd.append($("<p class='result-message'>" + syncProductResult.message + "</p>"));
            var violationsUl = $("<ul>").addClass("constraint-violations");
            constraintViolations.forEach(function (violation) {
                violationsUl.append($("<li>").text(violation));
            });
            resultTd.append(violationsUl);
        } else {
            var errorTextId = "error-" + syncProduct.livemasterId;
            resultTd
                .append($("<a>")
                    .addClass("result-message error-link")
                    .attr("onclick", "$('#" + errorTextId + "').toggle()")
                    .text(syncProductResult.message)
                )
                .append($("<p>")
                    .attr("id", errorTextId)
                    .addClass("error-stack-trace")
                    .text(syncProductResult.error.stackTrace)
                );
        }
    } else {
        resultTd.append($("<p class='result-message'>" + syncProductResult.message + "</p>"));
    }
    var warnings = syncProductResult.warnings;
    if (warnings) {
        for (var i = 0; i < warnings.length; i++) {
            var warn = warnings[i];
            var warnInfoId = "warning-text-" + syncProduct.livemasterId + "-" + i;
            resultTd
                .append($("<br>"))
                .append($("<br>"))
                .append($("<a>")
                    .addClass("warning-message warning-link")
                    .attr("onclick", "$('#" + warnInfoId + "').toggle()")
                    .append("Missed image").append("<br/>").append(warn.missedImageUrlAbbrev)
                )
                .append($("<div>")
                    .attr("id", warnInfoId)
                    .addClass("warning-info")
                    .append("<a href='" + warn.missedImageUrl + "' target='_blank'>" + warn.missedImageUrl + "</a>")
                    .append("<p>" + warn.errorStackTrace + "</p>")
                );
        }
    }
    tr.append(resultTd);

    $("table#syncProducts > tbody").append(tr);
}

function _appendNotActualLivemasterIdTableRow(livemasterId, product) {
    var tr = $("<tr>")
        .append(
            $("<td>").append($("table#notActualLivemasterIdsProducts > tbody > tr").size() + 1)
        )
        .append(
            $("<td>").append(livemasterId)
        )
        .append(
            $("<td>").addClass("market-product").append(_createProductView(product))
        );

    $("table#notActualLivemasterIdsProducts > tbody").append(tr);
}

function _createProductView(product) {
    return $("<table><tbody><tr>" +
                "<td>" +
                    "<a href='" + product.url + "' target='_blank'>" +
                        "<img src ='" + product.previewUrl + "'/>" +
                    "</a>" +
                "</td>" +
                "<td>" +
                    "<a href='" + product.url + "' target='_blank'>" + product.name + "</a>" +
                "</td>" +
            "</tr></tbody></table>");
}

var SYNC_EVENTS_HANDLERS = {

    "SYNC_START": function() {
        $("#syncStart").show();
    },

    "SYNC_PRODUCTS_DETERMINING_START": function() {
        $("#syncProductsDeterminingStart").show();
    },
    "SYNC_PRODUCTS_DETERMINING_COMPLETE": function(syncEventData) {
        var syncProductsDeterminingComplete = $("#syncProductsDeterminingComplete");
        syncProductsDeterminingComplete.find(".count").text(syncEventData.syncProductsCount);
        syncProductsDeterminingComplete.show();
    },
    "SYNC_PRODUCTS_DETERMINING_FAILED": function(syncEventData) {
        var syncProductsDeterminingFailed = $("#syncProductsDeterminingFailed");
        var errorStackTrace = syncProductsDeterminingFailed.find(".error-stack-trace");
        errorStackTrace.text(syncEventData.errorStackTrace);

        syncProductsDeterminingFailed.show();
        errorStackTrace.show();
    },

    "SYNC_PRODUCTS_PROCESSING_START": function() {
        $("#syncProductsProcessingStart").show();
        _initSyncProductsProcessingContainer();
        $("#syncProductsProcessingContainer").show();
    },
    "SYNC_PRODUCT_PROCESSED": function(syncEventData) {
        var syncProductProcessed = $("#syncProductProcessed");
        var count = 1 + parseInt(syncProductProcessed.find(".count").text());
        syncProductProcessed.find(".count").text(count);
        var totalCount = parseInt(syncProductProcessed.find(".total-count").text());
        _setSyncProductsProgress(count, totalCount);

        var syncProduct = syncEventData.syncProduct;
        var syncProductResult = syncEventData.syncProductResult;

        [SYNC_PRODUCT_RESULT_TYPE_ALL, syncProductResult.type].forEach(function (resultType) {
            var resultTypeCount = $("table#syncProductResultFilters a[data-result-type=" + resultType + "] span.count");
            var typeCount = 1 + parseInt(resultTypeCount.text());
            resultTypeCount.text(typeCount);
        });

        if (syncProductResult.warnings) {
            var warningsCountHolder = $("table#syncProductResultFilters .warningsCount");
            var warningsCount = 1 + parseInt(warningsCountHolder.text());
            warningsCountHolder.text(warningsCount);
        }

       _appendSyncProductTableRow(syncProduct, syncProductResult);
    },
    "SYNC_PRODUCTS_PROCESSING_COMPLETE": function() {
        $("#syncProductsProcessingComplete").show();
    },

    "NOT_ACTUAL_LIVEMASTER_IDS_PROCESSING_START": function() {
        $("#notActualLivemasterIdsProcessingStart").show();
    },
    "NOT_ACTUAL_LIVEMASTER_IDS_DETERMINED": function(syncEventData) {
        var notActualLivemasterIdsCount = $("#notActualLivemasterIdsCount");
        notActualLivemasterIdsCount.find(".count").text(syncEventData.notActualLivemasterIdsCount);
        notActualLivemasterIdsCount.show();

        _initNotActualLivemasterIdsProcessingContainer(syncEventData.notActualLivemasterIdsCount);
        $("#notActualLivemasterIdsProcessingContainer").show();
    },
    "NOT_ACTUAL_LIVEMASTER_ID_PROCESSED": function(syncEventData) {
        var notActualLivemasterIdProcessed = $("#notActualLivemasterIdProcessed");
        var count = 1 + parseInt(notActualLivemasterIdProcessed.find(".count").text());
        notActualLivemasterIdProcessed.find(".count").text(count);
        var totalCount = parseInt(notActualLivemasterIdProcessed.find(".total-count").text());

        _setNotActualLivemasterIdsProgress(count, totalCount);
        _appendNotActualLivemasterIdTableRow(syncEventData.livemasterId, syncEventData.product);
    },
    "NOT_ACTUAL_LIVEMASTER_ID_PROCESSING_COMPLETE": function() {
        $("#notActualLivemasterIdsProcessingComplete").show();
    },

    "SYNC_STOPPED": function() {
        $("#syncStopped").show();
    },
    "SYNC_COMPLETE": function() {
        $("#syncComplete").show();
    }
};

function handleSyncEvent(syncEvent) {
    var handler = SYNC_EVENTS_HANDLERS[syncEvent.type];
    if (!handler) {
        throw new Error("Handler not found for event type: " + syncEvent.type);
    }
    handler(syncEvent.data);
}

$(function() {
    _hideSyncEventElements();

    $("table#syncProductResultFilters a[data-result-type]").click(function() {
        var _this = $(this);
        var resultType = _this.data("result-type");
        $("table#syncProducts > tbody > tr").each(function() {
            var row = $(this);
            var needDisplay = resultType == SYNC_PRODUCT_RESULT_TYPE_ALL || resultType == row.data("result-type");
            row.css("display", needDisplay ? "table-row" : "none");
        });
    });

    $("table#syncProductResultFilters a.has-warning-filter").click(function() {
        $("table#syncProducts > tbody > tr").each(function() {
            var row = $(this);
            var needDisplay = SYNC_PRODUCT_RESULT_TYPE_PRODUCT_ADDED == row.data("result-type") &&
                row.find("a.warning-message").size() > 0 ;
            row.css("display", needDisplay ? "table-row" : "none");
        });
    });
});