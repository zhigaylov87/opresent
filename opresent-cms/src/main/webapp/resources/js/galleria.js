function Galleria() {
    this._SCROLL_STEP = 100;

    this._imageSelectionListeners = undefined;
    this._scrollListImagesPanelLastScrollLeft = undefined;

    this._init = function() {
        // No use "{}" instead "new Array()", because "forEach" function not found for "{}"
        this._imageSelectionListeners = new Array();
        this._initListeners();
        this.refresh();
    };

    this._initListeners = function() {
        var _this = this;

        $(".galleriaLeftArrowPanel").click(function () {
            _this._scrollLeft();
        });

        $(".galleriaRightArrowPanel").click(function () {
            _this._scrollRight();
        });

        $(".galleriaScrollListImagesPanel").bind('mousewheel', function(e, direction){
            if(direction > 0) {
                _this._scrollLeft();
            } else{
                _this._scrollRight();
            }
        });

        $(".galleriaListImage").load(function() {
            _this._alignListImageVertically($(this));
        });

        this.addImageSelectionListener(new ScrollingGalleriaImageSelectionListener(this));
    };

    this._alignAllListImagesVertically = function() {
        var _this = this;
        $(".galleriaListImagePanel img").each(function() {
            _this._alignListImageVertically($(this));
        });
    };

    this._alignListImageVertically = function(image) {
        var listImagePanelHeight = $(".galleriaListImagePanel").height();
        var marginTop = (listImagePanelHeight - image.height()) / 2;
        image.css("margin-top", marginTop);
    };

    this._getListImagePanel = function(imageId) {
        return $(".galleriaListImagePanel_" + imageId);
    };

    this._getSelectedListImagePanel = function() {
        return $(".galleriaSelectedListImagePanel");
    };

    this._updateSelectedListImagePanel = function () {
        this.setSelectedImageId(this.getSelectedImageId());
    };

    this._getSelectedImageInput = function() {
        return $("input[id$='galleriaSelectedImageInput']");
    };

    this._activateLeftArrow = function() {
        $(".galleriaLeftArrowPanel span").removeClass("galleriaLeftInactiveArrow").addClass("galleriaLeftActiveArrow");
    };

    this._deactivateLeftArrow = function() {
        $(".galleriaLeftArrowPanel span").removeClass("galleriaLeftActiveArrow").addClass("galleriaLeftInactiveArrow");
    };

    this._activateRightArrow = function () {
        $(".galleriaRightArrowPanel span").removeClass("galleriaRightInactiveArrow").addClass("galleriaRightActiveArrow");
    };

    this._deactivateRightArrow = function() {
        $(".galleriaRightArrowPanel span").removeClass("galleriaRightActiveArrow").addClass("galleriaRightInactiveArrow");
    };

    this._scrollBegin = function() {
        var galleriaScrollListImagesPanel = $(".galleriaScrollListImagesPanel");
        galleriaScrollListImagesPanel.scrollLeft(0);

        this._listImagesScrollingListener();
    };

    this._scrollLeft = function() {
        var galleriaScrollListImagesPanel = $(".galleriaScrollListImagesPanel");
        galleriaScrollListImagesPanel.scrollLeft(galleriaScrollListImagesPanel.scrollLeft() - this._SCROLL_STEP);

        this._listImagesScrollingListener();
    };

    this._scrollRight = function() {
        var galleriaScrollListImagesPanel = $(".galleriaScrollListImagesPanel");
        galleriaScrollListImagesPanel.scrollLeft(galleriaScrollListImagesPanel.scrollLeft() + this._SCROLL_STEP);

        this._listImagesScrollingListener();
    };

    this._scrollEnd =  function() {
        var scrollListImagesPanel = $(".galleriaScrollListImagesPanel");
        var scrollListImagesPanelWidth = scrollListImagesPanel.width();
        var scrollListImagesPanelContentWidth = $(".galleriaScrollListImagesPanel table").width();
        var scrollLeft = scrollListImagesPanelContentWidth - scrollListImagesPanelWidth;
        scrollListImagesPanel.scrollLeft(scrollLeft);

        this._listImagesScrollingListener();
    };

    this._scrollToLastPosition = function() {
        var lastScrollLeft = this._scrollListImagesPanelLastScrollLeft;
        if (lastScrollLeft) {
            $(".galleriaScrollListImagesPanel").scrollLeft(lastScrollLeft);
        }
    };

    /**
     * @param {GalleriaImageSelectionEvent} event
     */
    this._correctScrollingIfNeeded = function(event) {
        var scrollListImagesPanel = $(".galleriaScrollListImagesPanel");
        var scrollListImagesPanelWidth = scrollListImagesPanel.width();
        var scrollListImagesPanelLeft = scrollListImagesPanel.offset().left;

        var listImageTD = this._getListImagePanel(event.getImageId()).parent();
        var listImageTDLeft = listImageTD.offset().left;
        var listImageTDWidth = listImageTD.outerWidth(true);

        if ((listImageTDLeft + listImageTDWidth) > (scrollListImagesPanelWidth + scrollListImagesPanelLeft)) {
            this._scrollRight();
            return;
        }

        if (listImageTDLeft < scrollListImagesPanelLeft) {
            this._scrollLeft();
        }
    };

    this._updateArrowsActivity = function () {
        var scrollListImagesPanel = $(".galleriaScrollListImagesPanel");
        var scrollListImagesPanelWidth = scrollListImagesPanel.width();
        var scrollLeft = scrollListImagesPanel.scrollLeft();
        var scrollListImagesPanelContentWidth = $(".galleriaScrollListImagesPanel table").width();

        var isStart = scrollLeft == 0;
        isStart ? this._deactivateLeftArrow() : this._activateLeftArrow();

        var isScrollNotNeeded = scrollListImagesPanelWidth >= scrollListImagesPanelContentWidth;
        var isEnd = scrollListImagesPanelWidth + scrollLeft == scrollListImagesPanelContentWidth;
        isEnd || isScrollNotNeeded ? this._deactivateRightArrow() : this._activateRightArrow();
    };

    this._listImagesScrollingListener = function() {
        this._updateArrowsActivity();
        this._scrollListImagesPanelLastScrollLeft = $(".galleriaScrollListImagesPanel").scrollLeft();
    };

    /**
     * @param {GalleriaImageSelectionEvent} event
     */
    this._notifyImageSelectionListener = function(event) {
        this._imageSelectionListeners.forEach(function(listener) {
            listener.imageSelectionInvoked(event);
        });
    };

    // public functions (BEGIN)

    this.getSelectedImageId = function() {
        return this._getSelectedImageInput().val();
    };

    this.setSelectedImageId = function(imageId) {
        if (!imageId) {
            return;
        }

        var oldSelectedListImagePanel = this._getSelectedListImagePanel();
        oldSelectedListImagePanel.removeClass("galleriaSelectedListImagePanel");

        var newSelectedListImagePanel = this._getListImagePanel(imageId);
        newSelectedListImagePanel.addClass("galleriaSelectedListImagePanel");

        var newSelectedListImage = newSelectedListImagePanel.children("img");
        var newImageSrc = newSelectedListImage.attr("src");

        var selectedImage = $(".galleriaSelectedImage");
        selectedImage.attr("src", newImageSrc);

        this._getSelectedImageInput().val(imageId);

        // notify listener
        var imageSelectionEvent = new GalleriaImageSelectionEvent();
        imageSelectionEvent._imageId = imageId;
        var listImageTD = newSelectedListImagePanel.parent();

        var listImagesTDS = $(".galleriaScrollListImagesPanel table td");
        var imageIndex = listImagesTDS.index(listImageTD);
        imageSelectionEvent._imageIndex = imageIndex;
        imageSelectionEvent._isFirst = imageIndex == 0;
        imageSelectionEvent._isLast = listImagesTDS.length == (imageIndex + 1);

        this._notifyImageSelectionListener(imageSelectionEvent);
    };

    /**
     * @param {GalleriaImageSelectionListener} listener
     */
    this.addImageSelectionListener = function (listener) {
        this._imageSelectionListeners.push(listener);
    };

    /**
     * @param {Array} listeners array of {@link GalleriaImageSelectionListener}
     */
    this.addImageSelectionListeners = function (listeners) {
        this._imageSelectionListeners = this._imageSelectionListeners.concat(listeners);
    };

    this.refresh = function() {
        this._updateSelectedListImagePanel();
        this._alignAllListImagesVertically();
        this._updateArrowsActivity();
    };

    // public functions (END)

    // invoke galleria initializing after all functions defining
    this._init();
}

function GalleriaImageSelectionEvent() {
    this._imageId = undefined;
    this._imageIndex = undefined;
    this._isFirst = undefined;
    this._isLast = undefined;

    this.getImageId = function() {
        return this._imageId;
    };

    /**
     * @return {boolean} true, if selected image is first
     */
    this.isFirst = function() {
        return this._isFirst;
    };

    /**
     * @return {boolean} true, if selected image is last
     */
    this.isLast = function() {
        return this._isLast;
    };

    this.getImageIndex = function() {
        return this._imageIndex;
    };

}

function GalleriaImageSelectionListener() {

    /**
     * @param {GalleriaImageSelectionEvent} event
     */
    this.imageSelectionInvoked = function(event) {

    }

}

/**
 * Scroll to selected image. This inner class used private functions of {@link Galleria}
 * @constructor
 * @param {Galleria} galleria
 */
function ScrollingGalleriaImageSelectionListener(galleria) {

    this._galleria = galleria;

    /**
     * @param {GalleriaImageSelectionEvent} event
     */
    this.imageSelectionInvoked = function(event) {
        if (event.isFirst()) {
            this._galleria._scrollBegin();
            return;
        }

        if (event.isLast()) {
            this._galleria._scrollEnd();
            return;
        }

        this._galleria._scrollToLastPosition();

        this._galleria._correctScrollingIfNeeded(event);
    }

}