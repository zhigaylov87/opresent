PrimeFaces.widget.ProgressBar.prototype.setValue = function(value) {
    if(value >= 0 && value <= 100 && value != this.value) {
        if(value == 0) {
            this.jqValue.hide().css('width', '0%').removeClass('ui-corner-right');

            this.jqLabel.hide();
        }
        else {
            this.jqValue.show().css('width', value + '%');

            if(this.cfg.labelTemplate) {
                var formattedLabel = this.cfg.labelTemplate.replace(/{value}/gi, value);

                this.jqLabel.html(formattedLabel).show();
            }
        }

        this.value = value;
        this.jq.attr('aria-valuenow', value);
    }
};

function placeCursorAtTheEnd(inputElement) {
    var val = inputElement.value;
    inputElement.value='';
    inputElement.value= val;
}