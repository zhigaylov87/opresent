var ProductPreviewPanel = {

    initListeners: function() {

        if ($(".product-preview-panel").size() == 0) {
            return;
        }

        $(".product-preview-panel .product-action-buy, " +
          ".product-preview-panel .product-action-order, " +
          ".product-preview-panel .product-action-more-info").click(function() {

            var _this = $(this);
            var product = _this.closest(".product-preview-panel");
            var productId = product.data("product-id");
            var productStatus = product.data("product-status");
            var productStatusNote = product.data("product-status-note");

            switch (productStatus) {
                case ProductStatus.AVAILABLE:
                case ProductStatus.FOR_ORDER:
                    DeviceType.isMobile() ? MobileNavCart.addProduct(productId) : NavCart.addProduct(productId);
                    _this.addClass("hide");
                    product.find(".product-action-in-cart").removeClass("hide");
                    break;
                case ProductStatus.EXAMPLE:
                    PageManager.getCurrentPageHandler().getIncomingMessageDialog().show(
                        new IncomingMessageDialogParams(productStatusNote,
                            new IncomingMessagePanelParams(productId, IncomingMessageSubjectType.PRODUCT)));
                    break;

                default:
                    throw "Unknown product status: '" + productStatus + "'";
            }
        });

        if (DeviceType.isNormal()) {
            NavCart.addRemoveProductListener(function(productId) {
                var product = $(".product-preview-panel[data-product-id=" + productId + "]");
                product.find(".product-action-in-cart").addClass("hide");
                product.find(".product-action-buy, .product-action-order").removeClass("hide");
            });
        }
    }

};