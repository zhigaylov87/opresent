var OrderPageHandler = BasePageHandler.extend({

    initPageListeners: function() {

        var orderForm = {

            customerFullName: $("#customer-full-name"),
            customerPhone: $("#customer-phone"),
            customerEmail: $("#customer-email"),
            addressType: $("[name=address-type]"),
            deliveryWay: $("#delivery-way"),
            customerCity: $("#customer-city"),
            customerPostcode: $("#customer-postcode"),
            customerAddress: $("#customer-address"),
            customerComment: $("#customer-comment"),

            deliveryWayNote: $(".delivery-way-note"),
            hasErrors: false,

            getGroup: function(formField) {
                return formField.closest(".form-group");
            },

            getFieldValue: function(formField) {
                var fieldValue = formField.val();
                return fieldValue ? fieldValue.trim() : null;
            },

            getRequiredFieldValue: function(formField, checkVisible) {
                if (checkVisible && !formField.is(":visible")) {
                    return null;
                }
                var fieldValue = this.getFieldValue(formField);
                if (fieldValue) {
                    return fieldValue;
                } else {
                    var group = this.getGroup(formField);
                    group.addClass("has-error");
                    group.find(".help-block.field-required").show();
                    this.hasErrors = true;
                    return null;
                }
            },

            clearErrors: function() {
                var groups = $(".order-info-form .form-group");
                groups.removeClass("has-error");
                groups.find(".help-block.field-required").hide();
                this.hasErrors = false;
            }
        };

        var hideDependentFields = function() {
            [orderForm.customerCity, orderForm.customerPostcode, orderForm.customerAddress].forEach(function(formField) {
                orderForm.getGroup(formField).hide();
            });
        };

        var allDeliveryWayOptions = orderForm.deliveryWay.find("option:enabled");
        var updateDeliveryWays = function() {
            orderForm.deliveryWay.val("none");
            orderForm.deliveryWayNote.text("");

            var addressType = orderForm.addressType.filter(":checked").val();
            orderForm.deliveryWay.find("option:enabled").detach();
            allDeliveryWayOptions.each(function(index, option) {
                var $option = $(option);
                if ($option.data("address-type") == addressType) {
                    $option.appendTo(orderForm.deliveryWay);
                }
            });
        };

        // initial view
        orderForm.addressType.first().prop('checked', true);
        updateDeliveryWays();
        hideDependentFields();

        // listeners
        orderForm.addressType.change(function() {
            hideDependentFields();
            updateDeliveryWays();
        });

        orderForm.deliveryWay.change(function() {
            var _this = $(this);
            var selectedOption = _this.find("option[value=" + _this.val() + "]");

            orderForm.deliveryWayNote.text(selectedOption.data("delivery-way-price") + " " + selectedOption.data("delivery-way-note"));

            var group = orderForm.getGroup(orderForm.customerCity);
            orderForm.addressType.filter(":checked").data("city-required") ? group.show() : group.hide();

            group = orderForm.getGroup(orderForm.customerPostcode);
            selectedOption.data("postcode-required") ? group.show() : group.hide();

            group = orderForm.getGroup(orderForm.customerAddress);
            selectedOption.data("address-required") ? group.show() : group.hide();
        });

        $(".order-info-form .form-control").bind("change paste keyup blur", function() {
            var field = $(this);
            var fieldValue = field.val();
            fieldValue = fieldValue ? fieldValue.trim() : null;
            if (fieldValue) {
                var formGroup = field.closest(".form-group");
                formGroup.removeClass("has-error");
                formGroup.find(".help-block.field-required").hide();
            }
        });

        $("a.add-order").click(function() {
            orderForm.clearErrors();

            var orderInfo = {
                customerFullName: orderForm.getRequiredFieldValue(orderForm.customerFullName),
                customerPhone: orderForm.getRequiredFieldValue(orderForm.customerPhone),
                customerEmail: orderForm.getRequiredFieldValue(orderForm.customerEmail),
                addressType: orderForm.addressType.filter(":checked").val(),
                deliveryWay: orderForm.getRequiredFieldValue(orderForm.deliveryWay),
                customerCity: orderForm.getRequiredFieldValue(orderForm.customerCity, true),
                customerPostcode: orderForm.getRequiredFieldValue(orderForm.customerPostcode, true),
                customerAddress: orderForm.getRequiredFieldValue(orderForm.customerAddress, true),
                customerComment: orderForm.getFieldValue(orderForm.customerComment)
            };

            if (orderForm.hasErrors) {
                return;
            }

            AppTransport.addOrder(orderInfo, function(responseData) {
                var orderAddResult = $(responseData);
                var orderAddResultType = orderAddResult.data("order-add-result-type");

                switch (orderAddResultType) {
                    case "SUCCESS":
                        var orderPage = $(".order-page");
                        orderPage.empty();
                        orderPage.append(orderAddResult.children());
                        DeviceType.isMobile() ? MobileNavCart.setProductsCount(0) : NavCart.setProductsCount(0);
                        break;

                    case "FAIL":
                        var orderAddFailMessage = orderAddResult.data("order-add-fail-message");
                        var invalidMessageFilling = $(".invalid-message-filling");
                        invalidMessageFilling.text(orderAddFailMessage);
                        invalidMessageFilling.parent().addClass("has-error");
                        invalidMessageFilling.show();
                        break;

                    default:
                        throw "Invalid order add result type: '" + orderAddResultType + "'";
                }

            });
        });

    }
});