var SectionPageHandler = BasePageHandler.extend({

    _sectionId: undefined,

    initPageListeners: function() {
        this._sectionId = $(".section-page").data("section-id");

        var msgPanelHolder = $(".incoming-message-panel-holder");
        if (msgPanelHolder.size()) {
            var askMoreQuestionLink = $(".ask-more-question");

            var messagePanelJq = new IncomingMessagePanel(msgPanelHolder);
            messagePanelJq.setParams(new IncomingMessagePanelParams(this._sectionId, IncomingMessageSubjectType.SECTION));
            messagePanelJq.setMessageSendSuccessListener(function() {
                askMoreQuestionLink.show();
            });

            askMoreQuestionLink.click(function() {
                messagePanelJq.hideSendMessageResult();
                $(this).hide();
                messagePanelJq.showSendMessagePanel();
            });
        }
    }
});