var WaitPopup = Class.extend({

    _SHOW_TIMEOUT: 500,
    _SHOW_DURATION_MIN: 2000,

    _STATES: {
        HIDDEN: "HIDDEN",
        SHOW_AFTER_TIMEOUT: "SHOW_AFTER_TIMEOUT",
        SHOWN: "SHOWN"
    },

    _popupJq: null,
    _state: null,
    _showTimeoutId: null,
    _showTimeMillis: null,

    init: function() {
        this._popupJq = $(".wait-popup");
        if (this._popupJq.size() == 0) {
            throw new Error(".wait-popup not found");
        }
        if (this._popupJq.size() > 1) {
            throw new Error(".wait-popup not a single");
        }
        this._state = this._STATES.HIDDEN;
    },

    show: function () {
        this._state = this._STATES.SHOW_AFTER_TIMEOUT;
        var _this = this;
        this._showTimeoutId = setTimeout(
            function () {
                if (_this._state == _this._STATES.SHOW_AFTER_TIMEOUT) {
                    _this._showTimeMillis = new Date().getTime();
                    _this._popupJq.modal({show: true, backdrop: "static", keyboard: false});
                    _this._state = _this._STATES.SHOWN;
                }
            },
            this._SHOW_TIMEOUT);
    },

    hide: function (afterHide) {

        if (this._showTimeoutId) {
            clearTimeout(this._showTimeoutId);
            this._showTimeoutId = null;
        }

        if (this._state == this._STATES.SHOW_AFTER_TIMEOUT) {
            this._state = this._STATES.HIDDEN;
            afterHide();
            return;
        }

        if (this._state == this._STATES.SHOWN) {
            var _this = this;
            var _hide = function () {
                _this._showTimeMillis = null;
                _this._popupJq.modal('hide');
                _this._state = _this._STATES.HIDDEN;
                afterHide();
            };

            var showDuration = new Date().getTime() - this._showTimeMillis;
            if (showDuration < this._SHOW_DURATION_MIN) {
                setTimeout(_hide, _this._SHOW_DURATION_MIN - showDuration);
            } else {
                _hide();
            }
            return;
        }

        afterHide();
    }

});