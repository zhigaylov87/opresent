/**
 *
 * @param subjectId
 * @param {IncomingMessageSubjectType} subjectType
 * @constructor
 */
function IncomingMessagePanelParams(subjectId, subjectType) {
    this.subjectId = subjectId;
    this.subjectType = subjectType;
}