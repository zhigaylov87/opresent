var PageManager = {

    _currentPageType: undefined,
    _currentPageHandler: undefined,

    getCurrentPageType: function () {
        if (!this._currentPageType) {
            var pageTypeClass = "data-page-type";
            var pageType = $("[" + pageTypeClass + "]");
            if (pageType.size() == 0) {
                throw "There are no element with style class '" + pageTypeClass + "'";
            }
            if (pageType.size() > 1) {
                throw "There are more than one element with style class '" + pageTypeClass + "'";
            }
            this._currentPageType = pageType.data("page-type");
        }
        return this._currentPageType;
    },

    /**
     * @returns {BasePageHandler}
     */
    getCurrentPageHandler: function() {
        if (!this._currentPageHandler) {
            var currentPageType = this.getCurrentPageType();
            switch (currentPageType) {
                case PageType.WELCOME:
                    this._currentPageHandler = new BasePageHandler();
                    break;

                case PageType.PRODUCT_LIST:
                    this._currentPageHandler = new ProductListPageHandler();
                    break;

                case PageType.PRODUCT:
                    this._currentPageHandler = new ProductPageHandler();
                    break;

                case PageType.CART:
                    this._currentPageHandler = new CartPageHandler();
                    break;

                case PageType.ORDER:
                    this._currentPageHandler = new OrderPageHandler();
                    break;

                case PageType.SECTION:
                    this._currentPageHandler = new SectionPageHandler();
                    break;

                case PageType.SECTION_ITEM:
                    this._currentPageHandler = new SectionItemPageHandler();
                    break;

                case PageType.SEARCH:
                    this._currentPageHandler = new SearchPageHandler();
                    break;

                default:
                    throw "Unknown page type: '" + currentPageType + "'";
            }
        }
        return this._currentPageHandler;
    }
};