var NavCart = {

    SLIDE_DURATION: 200,
    HIDE_AFTER_ADD_TIMEOUT: 4000,
    HIDE_AFTER_OVER_TIMEOUT: 2000,

    _hideTimeoutId: null,
    _removeProductListeners: [],

    addProduct: function(productId) {
        var _this = this;
        AppTransport.addProductToNavCart(productId, function(responseData) {
            $("div#nav-cart").replaceWith(responseData);
            _this.initListeners();
            _this.show();
            _this._hideTimeoutId = setTimeout(function(){_this.hide()}, _this.HIDE_AFTER_ADD_TIMEOUT);
        });
    },

    setProductsCount: function(productsCount) {
        var navCart = $("div#nav-cart");
        var navCartCount = $(".nav-cart-count");
        navCartCount.text(" (" + productsCount + ")");
        if (productsCount) {
            navCart.addClass("not-empty");
            navCartCount.removeClass("hide");
        } else {
            navCart.removeClass("not-empty");
            navCartCount.addClass("hide");
        }
    },

    setTitle: function(cartTitle) {
        $("div#nav-cart .nav-cart-link").attr("title", cartTitle);
    },

    removeProduct: function(productId) {
        var _this = this;
        AppTransport.removeProductFromNavCart(productId, function(responseData) {
            $("div#nav-cart").replaceWith(responseData);
            _this.initListeners();
            $(".nav-cart-content").show(); // immediately show
            _this._removeProductListeners.forEach(function(listener) {
                listener(productId);
            });
        });
    },

    /**
     *
     * @param listener <code>function(productId)</code>
     */
    addRemoveProductListener: function(listener) {
        this._removeProductListeners.push(listener);
    },

    /**
     * Invoked after product removed from nav cart. Do nothing by default. Must be overrided if needed.
     * @param productId
     */
    removeProductListener: function(productId) {
    },

    show: function() {
        $(".nav-cart-content").slideDown(this.SLIDE_DURATION);
    },

    hide: function() {
        $(".nav-cart-content").slideUp(this.SLIDE_DURATION);
    },

    initListeners: function() {
        var _this = this;
        $(".nav-cart-link, .nav-cart-content").hover(
            function(event) {
                if (_this._hideTimeoutId) {
                    clearTimeout(_this._hideTimeoutId);
                    _this._hideTimeoutId = null;
                }
                var cartVisible = $(".nav-cart-content").is(":visible");
                if (event.type == "mouseenter") {
                    // mouseenter
                    if (!cartVisible) {
                        _this.show();
                    }
                } else {
                    // mouseleave
                    if (cartVisible) {
                        _this._hideTimeoutId = setTimeout(function(){_this.hide()}, _this.HIDE_AFTER_OVER_TIMEOUT);
                    }
                }
            }
        );

        $(".remove-nav-cart-item").click(
            function() {
                var productId = $(this).closest(".nav-cart-item").data("product-id");
                _this.removeProduct(productId);
            }
        );
    }
};
