var PersonalOrderDialog = AbstractDialog.extend({

    _dialogJq: undefined,
    _invalidFillingJq: undefined,

    init: function() {
        this._dialogJq = $(".personal-order-dialog");
        this._invalidFillingJq = this._dialogJq.find(".invalid-order-filling");

        var _this = this;
        this._dialogJq.find("button.close-personal-order-dialog").click(function() {
            _this.close();
        });

        this._dialogJq.find("button.send-personal-order").click(function() {
            _this.clearErrors();

            var orderInfo = {
                "personalOrder": true
            };
            var allFieldsFilled =
                _this.fillOrderField(orderInfo, "customerFullName", "customer-full-name") &&
                    _this.fillOrderField(orderInfo, "customerPhone", "customer-phone") &&
                    _this.fillOrderField(orderInfo, "customerEmail", "customer-email") &&
                    _this.fillOrderField(orderInfo, "customerComment", "customer-comment");

            if (!allFieldsFilled) {
                return;
            }

            AppTransport.addPersonalOrder(orderInfo, function() {
                var msgElement = _this._dialogJq.find(".create-order-success");
                var msgText = msgElement.data("message-template")
                    .replace("{customerFullName}", orderInfo.customerFullName);
                msgElement.text(msgText);

                _this._dialogJq.find(".order-content-panel").hide();
                _this._dialogJq.find(".create-order-result").show();
                _this._dialogJq.find(".customer-comment").val("");
            });
        });
    },

    clearErrors: function() {
        this._invalidFillingJq.text("");
        this._dialogJq.find(".has-error").removeClass("has-error");
    },

    /**
     * @param orderInfo
     * @param orderField
     * @param valueSourceClass
     * @returns {boolean} true if field was filled or false if field is empty
     */
    fillOrderField: function(orderInfo, orderField, valueSourceClass) {
        var valueSource = this._dialogJq.find("." + valueSourceClass);
        var fieldValue = valueSource.val();
        fieldValue = fieldValue ? fieldValue.trim() : fieldValue;

        if (fieldValue) {
            orderInfo[orderField] = fieldValue;
            return true;
        } else {
            this._invalidFillingJq.text(valueSource.data("empty-message"));
            this._invalidFillingJq.parent().addClass("has-error");
            valueSource.parent().addClass("has-error");
            return false;
        }
    },

    prepareToShow: function() {
        this._dialogJq.find(".order-content-panel").show();
        this._dialogJq.find(".create-order-result").hide();
        this.clearErrors();
    }

});
