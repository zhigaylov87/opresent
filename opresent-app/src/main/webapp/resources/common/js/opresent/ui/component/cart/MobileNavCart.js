var MobileNavCart = {

    HIDE_AFTER_ADD_TIMEOUT: 2000,

    hideTimeoutId: null,

    addProduct: function(productId) {
        var _this = this;
        AppTransport.addProductToNavCart(productId, function(responseData) {
            if (_this.hideTimeoutId) {
                clearTimeout(_this.hideTimeoutId);
                _this.hideTimeoutId = null;
            }

            var newCount = $(responseData).find(".nav-cart-count").text();
            _this.setProductsCount(newCount);
            if (newCount) {
                var navCartLink = $(".nav-cart-link");
                navCartLink.tooltip("show");
                _this.hideTimeoutId = setTimeout(
                    function(){navCartLink.tooltip("hide");},
                    _this.HIDE_AFTER_ADD_TIMEOUT);
            }

        });
    },

    setProductsCount: function(productsCount) {
        var navCartCount = $(".nav-cart-count");
        navCartCount.text(productsCount);
        if (productsCount) {
            navCartCount.removeClass("hide");
        } else {
            navCartCount.addClass("hide");
        }
    },

    setTitle: function(cartTitle) {

    }
};