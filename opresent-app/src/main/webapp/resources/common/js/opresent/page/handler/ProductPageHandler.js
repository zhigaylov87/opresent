var ProductPageHandler = BasePageHandler.extend({

    initPageListeners: function() {
        var product = $("div.product-page");
        var productId = product.data("product-id");
        var productStatus = product.data("product-status");
        var productStatusNote = product.data("product-status-note");

        $(".product-main a.ask-question").click(function() {
            PageManager.getCurrentPageHandler().getIncomingMessageDialog().show(
                new IncomingMessageDialogParams(productStatusNote,
                    new IncomingMessagePanelParams(productId, IncomingMessageSubjectType.PRODUCT)));
        });

        switch (productStatus) {
            case ProductStatus.AVAILABLE:
            case ProductStatus.FOR_ORDER:
                $(".product-main .product-action-buy, .product-main .product-action-order").click(function() {
                    DeviceType.isMobile() ? MobileNavCart.addProduct(productId) : NavCart.addProduct(productId);
                    $(this).addClass("hide");
                    $(".product-main .product-action-in-cart").removeClass("hide");
                });
                break;
            case ProductStatus.EXAMPLE:
                break;
            default:
                throw "Unknown product status: '" + productStatus + "'";
        }

        NavCart.addRemoveProductListener(function(removedProductId) {
            if (productId == removedProductId) {
                $(".product-main .product-action-in-cart").addClass("hide");
                $(".product-main .product-action-buy, .product-main .product-action-order").removeClass("hide");
            }
        });

        if (DeviceType.isNormal()) {
            //Image switching on product pages
            $(".product-views-small a").click(function(e){
                var imgToShow = $(this).data("img");

                var currentImage = $(".product-views-large img:visible");
                if(currentImage.prop("id") != imgToShow) {
                    currentImage.fadeOut(function() {
                        $("#" + imgToShow).fadeIn();
                    });
                }

                e.preventDefault();
            });

            $(".product-views-large ").magnificPopup({
                delegate: 'a',
                type: 'image',
                removalDelay: 300,
                mainClass: 'mfp-with-fade',
                // other options
                gallery: {
                    enabled: true,
                    navigateByImgClick: true
                },
                callbacks: {
                    open: function() {
                        $('header').css('z-index', 100);
                    },
                    close: function() {
                        $('header').css('z-index', 1030);
                    }
                }
            });
        }
    }
});