var DeviceType = {
    NORMAL: "NORMAL",
    MOBILE: "MOBILE",
    // TABLET: "TABLET", not used now

    CURRENT: null, // TODO Убрать это отсюда. Это состояние сеанса работы, должно быть в другом месте

    isNormal: function() {
        return this.CURRENT == this.NORMAL;
    },

    isMobile: function() {
        return this.CURRENT == this.MOBILE;
    }
};