var CartPageHandler = BasePageHandler.extend({

    initPageListeners: function() {
        var cartPageHandler = this;

        $(".cart-remove").click(function() {
            var productId = DeviceType.isMobile() ?
                $(this).closest("div[data-product-id]").data("product-id") :
                $(this).closest("tr[data-product-id]").data("product-id");

            AppTransport.removeProductFromCartPage(productId, function(responseData) {
                $(".cart-content").replaceWith(responseData);
                var cartTitle = $("input[id=cart-title]").val();

                if (DeviceType.isMobile()) {
                    MobileNavCart.setProductsCount($("div[data-product-id]").size());
                    MobileNavCart.setTitle(cartTitle);
                } else {
                    NavCart.setProductsCount($("tr[data-product-id]").size());
                    NavCart.setTitle(cartTitle);
                }

                // re-init listeners
                cartPageHandler.initPageListeners();
            });
        });
    }
});