var IncomingMessageDialog = AbstractDialog.extend({

    _dialogJq: undefined,
    _messagePanel: undefined,

    init: function() {
        this._dialogJq = $(".incoming-message-dialog");
        if (this._dialogJq.size() == 0) {
            throw new Error(".incoming-message-dialog not found");
        }
        if (this._dialogJq.size() > 1) {
            throw new Error(".incoming-message-dialog not a single");
        }
        this._messagePanel = new IncomingMessagePanel(this._dialogJq);

        var _this = this;
        this._dialogJq.find("button.close-incoming-message-dialog").click(function() {
            _this.close();
        });
    },

    /**
     * @param params {IncomingMessageDialogParams} dialogParams
     */
    prepareToShow: function(params) {
        this._messagePanel.showSendMessagePanel();
        this._messagePanel.hideSendMessageResult();
        this._messagePanel.clearErrors();

        this._dialogJq.find("p.incoming-message-dialog-text").text(params.dialogText);
        this._messagePanel.setParams(params.panelParams);
    }
});