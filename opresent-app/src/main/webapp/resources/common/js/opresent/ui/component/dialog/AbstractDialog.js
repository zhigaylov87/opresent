var AbstractDialog = Class.extend({

    _dialogJq: undefined,

    prepareToShow: function(params) {},

    show: function(params) {
        this.prepareToShow(params);
        this._dialogJq.modal({show: true});
    },

    close: function() {
        this._dialogJq.modal('hide');
    }

});