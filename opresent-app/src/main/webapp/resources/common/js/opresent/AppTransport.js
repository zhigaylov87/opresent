var AppTransport = {

    BASE_URL: "",

    _waitPopup: undefined,

    sendJson: function(jsonObject, url, responseHandler) {
        if (!this._waitPopup) {
            this._waitPopup = new WaitPopup();
        }
        this._waitPopup.show();
        var _this = this;
        $.ajax(
            {url: this.BASE_URL + url,
                type: "POST",
                data: JSON.stringify(jsonObject),
                contentType: "application/json",
                headers: {"TARGET_PAGE_TYPE": PageManager.getCurrentPageType()}
            }).done(function(responseData) {
                _this._waitPopup.hide(function () {
                    responseHandler(responseData);
                });
            });
    },

    // CartController
    addProductToNavCart: function(productId, responseHandler) {
        this.sendJson({"productId": productId}, "/navcart/add", responseHandler);
    },
    removeProductFromNavCart: function(productId, responseHandler) {
        this.sendJson({"productId": productId}, "/navcart/remove", responseHandler);
    },
    removeProductFromCartPage: function(productId, responseHandler) {
        this.sendJson({"productId": productId}, "/cart/remove", responseHandler);
    },

    // OrderController
    addOrder: function(orderInfo, responseHandler) {
        this.sendJson(orderInfo, "/order/add", responseHandler);
    },
    addPersonalOrder: function(orderInfo, responseHandler) {
        this.sendJson(orderInfo, "/order/add-personal", responseHandler);
    },

    // IncomingMessageController
    addIncomingMessage: function(incomingMessage, responseHandler) {
        this.sendJson(incomingMessage, "/message/incoming/add", responseHandler);
    },

    // SearchController
    getSearchContent: function(searchQuery, responseHandler) {
        this.sendJson({"query": searchQuery}, "/search-content", responseHandler);
    },
    goToSearchPage: function(searchQuery) {
        var url = this.BASE_URL + "/search/" + encodeURIComponent(searchQuery);
        $(location).attr('href', url);
    }
};