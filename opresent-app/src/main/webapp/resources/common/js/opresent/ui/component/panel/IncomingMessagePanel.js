var IncomingMessagePanel = Class.extend({

    _formJq: undefined,
    _invalidFillingJq: undefined,
    _messageSendSuccessListener: undefined,
    _params: undefined,

    init: function(panelContainer) {
        var panelJq = panelContainer.find(".incoming-message-panel");
        this._formJq = panelJq.find(".incoming-message-form");
        this._invalidFillingJq = this._formJq.find(".invalid-message-filling");

        var _this = this;
        panelJq.find("button.send-incoming-message").click(function() {
            _this.clearErrors();
            var incomingMessage = _this.createIncomingMessage();

            var allFieldsFilled =
                _this.fillMessageField(incomingMessage, "customerName", "customer-name") &&
                    _this.fillMessageField(incomingMessage, "customerEmailOrPhone", "customer-email-or-phone") &&
                    _this.fillMessageField(incomingMessage, "messageText", "message-text");

            if (!allFieldsFilled) {
                return;
            }

            AppTransport.addIncomingMessage(incomingMessage, function() {
                var msgElement = _this._formJq.find(".send-message-success");
                var msgText = msgElement.data("message-template")
                    .replace("{customerName}", incomingMessage.customerName);
                msgElement.text(msgText);

                _this._formJq.find(".send-message-panel").hide();
                _this._formJq.find(".send-message-result").show();
                _this.clearMessageText();

                if (_this._messageSendSuccessListener) {
                    _this._messageSendSuccessListener();
                }
            });
        });
    },

    clearErrors: function() {
        this._invalidFillingJq.text("");
        this._formJq.find(".has-error").removeClass("has-error");
    },

    /**
     * @returns {IncomingMessage}
     */
    createIncomingMessage: function() {
        var incomingMessage = new IncomingMessage();
        incomingMessage.subjectId = this._params.subjectId;
        incomingMessage.subjectType = this._params.subjectType;
        return incomingMessage;
    },

    /**
     * @param {IncomingMessagePanelParams} params
     */
    setParams: function(params) {
        this._params = params;
    },

    setMessageSendSuccessListener: function(listener) {
        this._messageSendSuccessListener = listener;
    },

    /**
     * @param incomingMessage
     * @param messageField
     * @param valueSourceClass
     * @returns {boolean} true if field was filled or false if field is empty
     */
    fillMessageField: function(incomingMessage, messageField, valueSourceClass) {
        var valueSource = this._formJq.find("." + valueSourceClass);
        var fieldValue = valueSource.val();
        fieldValue = fieldValue ? fieldValue.trim() : fieldValue;

        if (fieldValue) {
            incomingMessage[messageField] = fieldValue;
            return true;
        } else {
            this._invalidFillingJq.text(valueSource.data("empty-message"));
            this._invalidFillingJq.parent().addClass("has-error");
            valueSource.parent().addClass("has-error");
            return false;
        }
    },

    showSendMessagePanel: function() {
        this._formJq.find(".send-message-panel").show();
    },

    hideSendMessageResult: function() {
        this._formJq.find(".send-message-result").hide();
    },

    clearMessageText: function() {
        this._formJq.find(".message-text").val("");
    }

});