var BasePageHandler = Class.extend({

    _incomingMessageDialog: undefined,

    initBaseListeners: function() {
        switch(DeviceType.CURRENT) {
            case DeviceType.NORMAL:
                $(document).on('mouseleave', '#main-nav a.flat', function(e){
                    $(this).parent().removeClass('activehover');
                });

                var pageType = PageManager.getCurrentPageType();

                if (pageType != PageType.CART && pageType != PageType.ORDER) {
                    NavCart.initListeners();
                }
                if (pageType != PageType.SEARCH) {
                    this._initNavSearchListeners();
                }
                break;

            case DeviceType.MOBILE:
                $(".nav-cart-link").tooltip({
                    placement: "bottom",
                    trigger: "manual"
                });
                break;
        }

        ProductPreviewPanel.initListeners();

        if ($(".incoming-message-dialog").size() != 0) {
            this._incomingMessageDialog = new IncomingMessageDialog();
        }
    },

    initPageListeners: function() {},

    getIncomingMessageDialog: function() {
        return this._incomingMessageDialog;
    },

    addSearchKeywordHandler: function($input) {
        var keywords = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: AppTransport.BASE_URL + '/search-keyword/%QUERY',
                wildcard: '%QUERY',
                transform: function(productKeywordSearchResult) {
                    return productKeywordSearchResult.keywords;
                }
            }
        });

        $input.typeahead(null,
            {
                name: 'keywords',
                source: keywords,
                limit: 10
            });
    },

    _initNavSearchListeners: function() {
        var $navSearchInput = $("#nav-search-input");

        this.addSearchKeywordHandler($navSearchInput);

        $("#nav-search-button").click(function() {
            AppTransport.goToSearchPage($navSearchInput.val());
        });

        $navSearchInput.keydown(function(e) {
            if (e.keyCode == 13) { // Enter key
                AppTransport.goToSearchPage($navSearchInput.val());
                return false;
            }
            return true;
        });
    }

});