var SearchPageHandler = BasePageHandler.extend({

    _getSearchInput: function() {
        return $("#search-query-input");
    },

    _getSearchButton: function() {
        return $("#search-query-button");
    },

    _executeSearchQuery: function() {
        var searchQuery = this._getSearchInput().val();

        AppTransport.getSearchContent(searchQuery, function(responseData) {
            var searchContent = $("#search-content");
            var newSearchContent = $(responseData);
            searchContent.empty();
            searchContent.append(newSearchContent.children());
        });
    },

    initPageListeners: function() {
        var _this = this;

        this._getSearchButton().click(function() {
            _this._executeSearchQuery();
        });

        var $searchQueryInput = this._getSearchInput();
        this.addSearchKeywordHandler($searchQueryInput);

        $searchQueryInput.keydown(function(e) {
            if (e.keyCode == 13) { // Enter key
                _this._executeSearchQuery();
                return false;
            }
            return true;
        });
    }

});
