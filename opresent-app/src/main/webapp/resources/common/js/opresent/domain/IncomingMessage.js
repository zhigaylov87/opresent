var IncomingMessage = Class.extend({
    subjectId: undefined,
    subjectType: undefined,
    customerName: undefined,
    customerEmailOrPhone: undefined,
    messageText: undefined
});