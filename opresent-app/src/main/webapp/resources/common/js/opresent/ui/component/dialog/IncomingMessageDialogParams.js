/**
 * @param dialogText
 * @param {IncomingMessagePanelParams} panelParams
 * @constructor
 */
function IncomingMessageDialogParams(dialogText, panelParams) {
    this.dialogText = dialogText;
    this.panelParams = panelParams;
}