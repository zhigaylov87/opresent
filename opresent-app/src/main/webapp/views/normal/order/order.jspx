<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.0"
          xmlns:opresentn="urn:jsptagdir:/WEB-INF/tags/normal"
          xmlns:c="http://java.sun.com/jsp/jstl/core"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:opresentfn="http://opresent/functions"
          xmlns:s="http://www.springframework.org/tags">

    <jsp:directive.page contentType="text/html;charset=UTF-8"/>
    <jsp:output omit-xml-declaration="yes"/>

    <opresentn:template>

        <s:message code="order.devivery_way_price" var="order_devivery_way_price"/>

        <div class="order-page row" data-page-type="ORDER">

            <div class="col-md-8">
                <h1><s:message code="order.header"/></h1>

                <form class="form-horizontal order-info-form">
                    <div>
                        <div class="form-group">
                            <p class="help-block invalid-message-filling"><jsp:text/></p>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-full-name">
                                <s:message code="order.customer_full_name"/>
                            </label>
                            <div class="col-sm-9">
                                <input id="customer-full-name"
                                       maxlength="${entitiesConstraints.orderInfoCustomerFullNameMaxLength}"
                                       tabindex="2" class="form-control" type="text"/>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-phone">
                                <s:message code="order.customer_phone"/>
                            </label>
                            <div class="col-sm-9">
                                <input id="customer-phone"
                                       maxlength="${entitiesConstraints.orderInfoCustomerPhoneMaxLength}"
                                       tabindex="3" class="form-control" type="text"/>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-email">
                                <s:message code="order.customer_email"/>
                            </label>
                            <div class="col-sm-9">
                                <input id="customer-email"
                                       maxlength="${entitiesConstraints.orderInfoCustomerEmailMaxLength}"
                                       tabindex="4" class="form-control" type="text"/>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-sm-3"><jsp:text/></span>
                            <div class="col-sm-9">
                                <c:forEach items="${addressTypeList}" var="addressType">
                                    <label class="radio-inline">
                                    <input type="radio" name="address-type"
                                           data-city-required="${addressType.cityRequired}"
                                           value="${addressType.name()}"/>${enumHelper.getDescription(addressType)}
                                    </label>
                                </c:forEach>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="delivery-way">
                                <s:message code="order.delivery_way"/>
                            </label>
                            <div class="col-sm-9">
                                <select id="delivery-way" tabindex="5" class="form-control">
                                    <option value="none" disabled="disabled" selected="selected">
                                        <s:message code="order.select_delivery_way"/>
                                    </option>

                                    <c:forEach items="${deliveryWayInfoList}" var="deliveryWayInfo">
                                        <option value="${deliveryWayInfo.deliveryWay.name()}"
                                                data-address-type="${deliveryWayInfo.addressType.name()}"
                                                data-postcode-required="${deliveryWayInfo.deliveryWay.postcodeRequired}"
                                                data-address-required="${deliveryWayInfo.deliveryWay.addressRequired}"
                                                data-delivery-way-price="${order_devivery_way_price} ${deliveryWayInfo.price}"
                                                data-delivery-way-note="${fn:escapeXml(deliveryWayInfo.note)}"
                                                >${enumHelper.getDescription(deliveryWayInfo.deliveryWay)}</option>
                                    </c:forEach>
                                </select>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                                <span class="help-block delivery-way-note"><jsp:text/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-city">
                                <s:message code="order.customer_city"/>
                            </label>
                            <div class="col-sm-9">
                                <input id="customer-city"
                                       maxlength="${entitiesConstraints.orderInfoCustomerCityMaxLength}"
                                       tabindex="6" class="form-control" type="text"/>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-postcode">
                                <s:message code="order.customer_postcode"/>
                            </label>
                            <div class="col-sm-9">
                                <input id="customer-postcode"
                                       maxlength="${entitiesConstraints.orderInfoCustomerPostcodeMaxLength}"
                                       tabindex="7" class="form-control" type="text"/>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-address">
                                <s:message code="order.customer_address"/>
                            </label>
                            <div class="col-sm-9">
                                <textarea id="customer-address"
                                          maxlength="${entitiesConstraints.orderInfoCustomerAddressMaxLength}"
                                          tabindex="8" class="form-control"><jsp:text/></textarea>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="customer-comment">
                                <s:message code="order.customer_comment"/>
                            </label>
                            <div class="col-sm-9">
                                <textarea id="customer-comment"
                                          maxlength="${entitiesConstraints.orderInfoCustomerCommentMaxLength}"
                                          tabindex="8" class="form-control"><jsp:text/></textarea>
                                <span class="help-block field-required"><s:message code="field_required"/></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"><jsp:text/></div>

                            <div class="col-sm-9">
                                <a class="btn btn-large pull-left add-order">
                                    <s:message code="order.add_order"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="order-content col-md-4">

                <p class="order-total">
                    <small><s:message code="order.your_order"/></small>
                    <br/>
                    <span>${opresentfn:formatAmount(cart.totalPrice)} <s:message code="rub"/></span>
                </p>

                <div class="order-items">
                    <c:forEach items="${cart.products}" var="product">
                        <c:set var="productName" value="${fn:escapeXml(product.name)}"/>
                        <c:set var="productUrl" value="${appNavigationManager.getProductUrl(product)}"/>
                        <c:set var="previewUrl"
                               value="${imageUrlCreator.createUrl(product.preview, 'PRODUCT_PREVIEW_IMAGE_IN_CART')}"/>

                        <div class="item">
                            <a href="${productUrl}" title="${productName}">
                                <img src="${previewUrl}" alt="${productName}"/>
                            </a>

                            <div class="desc">
                                <p class="name"><a href="${productUrl}" title="${productName}">${productName}</a></p>
                                <p class="price">${opresentfn:formatAmount(product.price)} <s:message code="rub"/></p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

        </div>

    </opresentn:template>

</jsp:root>