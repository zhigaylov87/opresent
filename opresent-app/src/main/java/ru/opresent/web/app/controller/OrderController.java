package ru.opresent.web.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.opresent.core.domain.order.AddressType;
import ru.opresent.core.domain.order.DeliveryWayInfo;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.domain.order.OrderInfo;
import ru.opresent.core.service.api.DeliveryWayService;
import ru.opresent.core.service.api.OrderService;
import ru.opresent.web.app.util.AppConstants;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Arrays;

/**
 * User: artem
 * Date: 16.11.14
 * Time: 16:26
 */
@Controller
@RequestMapping(value = "/order")
public class OrderController extends BaseController {

    @Resource(name = "orderService")
    private OrderService orderService;

    @Resource(name = "deliveryWayService")
    private DeliveryWayService deliveryWayService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String orderPage(Model model) {
        model.addAttribute("addressTypeList", Arrays.asList(AddressType.values()));
        model.addAttribute("deliveryWayInfoList", deliveryWayService.getAll().getAll());

        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createOrderPageParams());
        return "order/order";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public String orderAdd(Model model,
                           @RequestBody @Valid OrderInfo orderInfo,
                           @CookieValue(AppConstants.TRACKING_ID_COOKIE_NAME) String trackingId) {
        if (orderInfo.isPersonalOrder()) {
            throw new BadRequestException();
        }
        try {
            Order order = orderService.save(orderInfo, trackingId);
            model.addAttribute("order", order);
            model.addAttribute("deliveryWayInfo",
                    deliveryWayService.getAll().getById(
                            new DeliveryWayInfo(order.getAddressType(), order.getDeliveryWay()).getId()));
            return "order/order_add_success";
        } catch (ConstraintViolationException e) {
            model.addAttribute("orderAddFailMessage", e.getConstraintViolations().iterator().next().getMessage());
            return "order/order_add_fail";
        }
    }

    @RequestMapping(value = "/add-personal", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(value = HttpStatus.OK)
    public void add(@RequestBody @Valid OrderInfo orderInfo,
                    @CookieValue(AppConstants.TRACKING_ID_COOKIE_NAME) String trackingId) {
        if (!orderInfo.isPersonalOrder()) {
            throw new BadRequestException();
        }
        orderService.save(orderInfo, trackingId);
    }
}