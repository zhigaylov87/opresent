package ru.opresent.web.app.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.opresent.core.service.api.CartService;
import ru.opresent.web.app.util.AppConstants;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 16.11.14
 * Time: 16:26
 */
@Controller
@RequestMapping
public class CartController extends BaseController {

    @Resource(name = "cartService")
    private CartService cartService;

    @RequestMapping(value = "/navcart/add", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public String navcartAddProduct(@CookieValue(AppConstants.TRACKING_ID_COOKIE_NAME) String trackingId,
                                    @RequestBody ProductIdHolder productIdHolder) {
        cartService.addProduct(trackingId, productIdHolder.getProductId());
        return "cart/navcart";
    }

    @RequestMapping(value = "/navcart/remove", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public String navcartRemoveProduct(@CookieValue(AppConstants.TRACKING_ID_COOKIE_NAME) String trackingId,
                                       @RequestBody ProductIdHolder productIdHolder) {
        cartService.removeProduct(trackingId, productIdHolder.getProductId());
        return "cart/navcart";
    }

    @RequestMapping(value = "/cart/remove", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public String cartRemoveProduct(@CookieValue(AppConstants.TRACKING_ID_COOKIE_NAME) String trackingId,
                                    @RequestBody ProductIdHolder productIdHolder) {
        cartService.removeProduct(trackingId, productIdHolder.getProductId());
        return "cart/cart_content";
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String cartPage(Model model) {
        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createCartPageParams());
        return "cart/cart";
    }
}