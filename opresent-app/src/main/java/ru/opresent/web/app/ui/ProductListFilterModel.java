package ru.opresent.web.app.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 03.11.16
 * Time: 0:42
 */
public class ProductListFilterModel {

    private Item selectedProductTypeItem;
    private Item selectedCategoryItem;
    private Item selectedProductStatusItem;

    private List<Item> productTypeItems = new ArrayList<>();
    private List<Item> categoryItems = new ArrayList<>();
    private List<Item> productStatusItems = new ArrayList<>();

    public void addProductTypeItem(String name, String href, boolean selected) {
        Item item = new Item(name, href, selected);
        productTypeItems.add(item);
        if (selected) {
            selectedProductTypeItem = item;
        }
    }

    public void addCategoryItem(String name, String href, boolean selected) {
        Item item = new Item(name, href, selected);
        categoryItems.add(item);
        if (selected) {
            selectedCategoryItem = item;
        }
    }

    public void addProductStatusItem(String name, String href, boolean selected, int count) {
        Item item = new Item(name, href, selected, count);
        productStatusItems.add(item);
        if (selected) {
            selectedProductStatusItem = item;
        }
    }

    public Item getSelectedProductTypeItem() {
        return selectedProductTypeItem;
    }

    public Item getSelectedCategoryItem() {
        return selectedCategoryItem;
    }

    public Item getSelectedProductStatusItem() {
        return selectedProductStatusItem;
    }

    public List<Item> getProductTypeItems() {
        return Collections.unmodifiableList(productTypeItems);
    }

    public List<Item> getCategoryItems() {
        return Collections.unmodifiableList(categoryItems);
    }

    public List<Item> getProductStatusItems() {
        return Collections.unmodifiableList(productStatusItems);
    }

    public static class Item {

        private String name;
        private String href;
        private boolean selected;
        private Integer count;

        private Item(String name, String href, boolean selected) {
            this(name, href, selected, null);
        }

        private Item(String name, String href, boolean selected, Integer count) {
            this.name = name;
            this.href = href;
            this.selected = selected;
            this.count = count;
        }

        public String getName() {
            return name;
        }

        public String getHref() {
            return href;
        }

        public boolean isSelected() {
            return selected;
        }

        public Integer getCount() {
            return count;
        }
    }

}
