package ru.opresent.web.app.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.opresent.core.CoreException;
import ru.opresent.core.service.impl.SitemapService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * User: artem
 * Date: 11.12.15
 * Time: 1:47
 */
@Controller
@RequestMapping
public class RobotsController {

    private static final String ROBOTS_TXT =
            "User-agent: *\n" +
            "Disallow: /cart\n" +
            "Disallow: /order\n" +
            "Disallow: /search\n" +
            "Disallow: /404\n" +
            "Disallow: /500\n" +
            "Host: www.o-present.ru\n" +
            "Sitemap: http://www.o-present.ru/sitemap.xml";

    @RequestMapping(value = "/robots.txt", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
    @ResponseBody
    public String getRobotsTxt() {
        return ROBOTS_TXT;
    }

    @RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getSitemapXml() {
        try {
            return Files.readAllBytes(Paths.get(SitemapService.SITEMAP_XML_PATH));
        } catch (IOException e) {
            throw new CoreException(e);
        }
//        return SitemapService.SITEMAP_XML_PATH;
//        return new FileSystemResource(SitemapService.SITEMAP_XML_PATH);
    }

}