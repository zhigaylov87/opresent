package ru.opresent.web.app.ui;

import org.springframework.stereotype.Component;
import ru.opresent.core.domain.ProductFilter;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.util.AppNavigationManager;

import javax.annotation.Resource;

/**
 * Max page items is nine.<br/>
 * Display options:
 * <ol>
 * <li>
 *     <p>|1|2|3|4|5|</p>
 *     <p>|1|2|3|4|5|6|7|8|9|</p>
 * </li>
 * <li>
 *     <p>|1|2|3|<b>4</b>|5|6|...|99|100|</p>
 *     <p>|1|2|3|4|<b>5</b>|6|...|99|100|</p>
 * </li>
 * <li>
 *     <p>|1|2|...|95|96|<b>97</b>|98|99|100|</p>
 *     <p>|1|2|...|95|<b>96</b>|97|98|99|100|</p>
 * </li>
 * <li>
 *     <p>|1|2|...|5|<b>6</b>|7|...|99|100|</p>
 *     <p>|1|2|...|94|<b>95</b>|96|...|99|100|</p>
 *     <p>|1|2|...|55|<b>56</b>|57|...|99|100|</p>
 * </li>
 * </ol>
 *
 */
@Component("productListPaginationModelCreator")
public class ProductListPaginationModelCreator {

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    /**
     * @return pagination model or {@code null} if pagination not needed
     */
    public ProductListPaginationModel createModel(ProductPage currentPage, ProductFilter filter, boolean mobile) {
        int totalProductsCount = currentPage.getTotalItemsCount();
        int pageSize = currentPage.getSettings().getPageSize();
        if (totalProductsCount <= pageSize) {
            return null;
        }

        ProductListPaginationModel model = new ProductListPaginationModel();
        int selectedPageNumber = currentPage.getSettings().getPageNumber();
        int pagesCount = (totalProductsCount / pageSize) + (totalProductsCount % pageSize == 0 ? 0 : 1);

        if (selectedPageNumber != PageSettings.FIRST_PAGE_NUMBER) {
            model.addPrevItem(getPageUrl(filter, selectedPageNumber - 1));
        }

        int totalItemsCount = mobile ? 5 : 9;
        int firstOrLastItemsCount = mobile ? 1 : 2;
        int middleItemsCount = mobile ? 1 : 3;
        int middleRange = middleItemsCount / 2;
        int headOrTailItemsCount = firstOrLastItemsCount + middleItemsCount + 1;

        if (pagesCount <= totalItemsCount) {
            addItemsInterval(filter, model, PageSettings.FIRST_PAGE_NUMBER, pagesCount, selectedPageNumber);
        } else if (selectedPageNumber < headOrTailItemsCount) {
            addItemsInterval(filter, model, PageSettings.FIRST_PAGE_NUMBER, headOrTailItemsCount, selectedPageNumber);
            model.addDotsItem();
            addItemsInterval(filter, model, pagesCount - firstOrLastItemsCount + 1, pagesCount, selectedPageNumber);
        } else if (selectedPageNumber > pagesCount - headOrTailItemsCount + 1) {
            addItemsInterval(filter, model, PageSettings.FIRST_PAGE_NUMBER, firstOrLastItemsCount, selectedPageNumber);
            model.addDotsItem();
            addItemsInterval(filter, model, pagesCount - headOrTailItemsCount + 1, pagesCount, selectedPageNumber);
        } else {
            addItemsInterval(filter, model, PageSettings.FIRST_PAGE_NUMBER, firstOrLastItemsCount, selectedPageNumber);
            model.addDotsItem();
            addItemsInterval(filter, model, selectedPageNumber - middleRange, selectedPageNumber + middleRange, selectedPageNumber);
            model.addDotsItem();
            addItemsInterval(filter, model, pagesCount - firstOrLastItemsCount + 1, pagesCount, selectedPageNumber);
        }

        if (selectedPageNumber != pagesCount) {
            model.addNextItem(getPageUrl(filter, selectedPageNumber + 1));
        }

        return model;
    }

    private void addItemsInterval(ProductFilter filter, ProductListPaginationModel model,
                                  int startPageNumber, int endPageNumber, int selectedPageNumber) {
        for (int pageNumber = startPageNumber; pageNumber <= endPageNumber; pageNumber++) {
            model.addPageItem(getPageUrl(filter, pageNumber), pageNumber, pageNumber == selectedPageNumber);
        }
    }

    private String getPageUrl(ProductFilter filter, int pageNumber) {
        return appNavigationManager.getCatalogUrl(filter.getProductType(), filter.getCategory(), filter.getStatus(), pageNumber);
    }

}
