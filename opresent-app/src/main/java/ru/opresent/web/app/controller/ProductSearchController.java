package ru.opresent.web.app.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.opresent.core.search.ProductKeywordSearchQuery;
import ru.opresent.core.search.ProductKeywordSearchResult;
import ru.opresent.core.search.ProductSearchQuery;
import ru.opresent.core.service.api.SearchService;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 30.05.15
 * Time: 3:53
 */
@Controller
@RequestMapping
public class ProductSearchController extends BaseController {

    @Resource(name = "searchService")
    private SearchService searchService;

    @RequestMapping(value = "/search/{query}", method = RequestMethod.GET)
    public String searchProducts(Model model, @PathVariable("query") String query) {
        ProductSearchQuery searchQuery = new ProductSearchQuery();
        searchQuery.setQuery(query);
        model.addAttribute("productSearchQuery", searchQuery);
        model.addAttribute("productSearchResult", searchService.search(searchQuery));
        model.addAttribute("hideNavSearch", true);

        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createSearchPageParams());
        return "search/search";
    }

    @RequestMapping(value = {"/search/", "/search"}, method = RequestMethod.GET)
    public String searchProducts(Model model) {
        model.addAttribute("hideNavSearch", true);

        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createSearchPageParams());
        return "search/search";
    }

    @RequestMapping(value = "/search-content", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public String searchContent(Model model, @RequestBody ProductSearchQuery searchQuery) {
        model.addAttribute("productSearchResult", searchService.search(searchQuery));
        return "search/search_content";
    }

    @RequestMapping(value = "/search-keyword/{query}", method = RequestMethod.GET)
    public @ResponseBody ProductKeywordSearchResult searchKeywords(@PathVariable String query) {
        ProductKeywordSearchQuery searchQuery = new ProductKeywordSearchQuery();
        searchQuery.setQuery(query);
        return searchService.search(searchQuery);
    }
}