package ru.opresent.web.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * User: artem
 * Date: 19.10.15
 * Time: 1:48
 */
@Controller
public class ErrorController extends BaseController {

    @RequestMapping(value = "/404")
    public String error404() {
        return "error/404";
    }

    @RequestMapping(value = "/500")
    public String error500() {
        return "error/500";
    }
}
