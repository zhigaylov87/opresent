package ru.opresent.web.app.controller;

import ru.opresent.core.util.stringify.EntityStringifier;
import ru.opresent.core.util.stringify.Stringifiable;

/**
 * User: artem
 * Date: 30.08.15
 * Time: 4:41
 */
public class ProductIdHolder implements Stringifiable {

    private long productId;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @Override
    public EntityStringifier stringify(boolean fully) {
        return new EntityStringifier(fully)
                .add("productId", productId);
    }

    @Override
    public String toString() {
        return stringify(false).toString();
    }
}
