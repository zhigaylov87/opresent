package ru.opresent.web.app.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.opresent.core.service.api.CartService;
import ru.opresent.core.util.NullableImmutableMap;
import ru.opresent.web.app.controller.BaseController;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.UUID;

/**
 * TODO: При индексировании сайта поисковые пауки наверняка(уточнить) не хранят куки,
 * TODO: поэтому при обращении к каждой странице будет создаваться новая кука и по логам будет выглядеть,
 * TODO: что зашли на одну страницу и сразу ушли с сайта без перехода на какую-то ещё страницу.
 * TODO: Как отличить робота-паука от реальных юзеров?
 * TODO: https://support.google.com/webmasters/answer/1061943?hl=en
 * TODO: http://www.useragentstring.com/pages/Crawlerlist/
 *
 * TODO: Для tracking id хорошо бы запилить какую-нибудь проверку, что значение валидно,
 * TODO: а то могут "подхачить" значение на клиенте. По большому счету в этом ничего страшного - максимум чужую корзину посмотрят.
 *
 * TODO: проверять trackingId на валидность(можно по формату просто) и если пришла левая фигня,
 * TODO: то писать в лог об этом в WARN и записывать свой новый trackingId
 *
 * User: artem
 * Date: 29.08.15
 * Time: 4:05
 */
public class AppHandlerInterceptor extends HandlerInterceptorAdapter {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "jsonMapper")
    private ObjectMapper jsonMapper;

    @Resource(name = "cartService")
    private CartService cartService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String trackingId = getTrackingId(request);
        if (trackingId == null) {
            trackingId = UUID.randomUUID().toString();
            Cookie cookie = new Cookie(AppConstants.TRACKING_ID_COOKIE_NAME, trackingId);
            cookie.setHttpOnly(true); // no need to use in client-side scripting code
            cookie.setMaxAge(AppConstants.TRACKING_ID_COOKIE_MAX_AGE);
            cookie.setPath("/");
            response.addCookie(cookie);
        }

        Map<String, String> appRequestInfo = NullableImmutableMap.<String, String>builder()
                .put("trackingId", trackingId)
                .put("requestURI", request.getRequestURI())
                .put("remoteAddr", request.getRemoteAddr())
                .put("userAgent", getUserAgent(request))
                .put("referer", getReferer(request))
                .put("deviceType", DeviceUtils.getCurrentDeviceType().name())
                .build();
        String info = jsonMapper.writeValueAsString(appRequestInfo);
        log.info("AppHandlerInterceptor.preHandle|" + info);

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // NOTE: modelAndView can be null handler methods, which not return view(void, responsebody and so on)
        if (modelAndView != null) {
            String trackingId = getTrackingId(request);
            if (trackingId != null) {
                modelAndView.getModelMap().addAttribute(BaseController.CART_ATTR, cartService.load(trackingId));
            }
            modelAndView.getModelMap().addAttribute(BaseController.PAGE_REFERER_ATTR, getReferer(request));
        }
        log.info("AppHandlerInterceptor.postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("AppHandlerInterceptor.afterCompletion");
    }

    private String getTrackingId(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }
        for (Cookie cookie : cookies) {
            if (AppConstants.TRACKING_ID_COOKIE_NAME.equals(cookie.getName())) {
                return cookie.getValue();
            }
        }
        return null;
    }

    private String getUserAgent(HttpServletRequest request) {
        return request.getHeader(HttpHeaders.USER_AGENT);
    }

    private String getReferer(HttpServletRequest request) {
        return request.getHeader(HttpHeaders.REFERER);
    }

}
