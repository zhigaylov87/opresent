package ru.opresent.web.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.opresent.core.component.api.image.ImageUrlCreator;
import ru.opresent.core.config.MarketConfig;
import ru.opresent.core.config.ServerConfig;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.core.util.EnumHelper;
import ru.opresent.core.util.UrlUtils;
import ru.opresent.core.validation.EntitiesConstraints;
import ru.opresent.web.app.page.PageHelper;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.util.List;

/**
 * User: artem
 * Date: 30.09.14
 * Time: 0:54
 */
@Controller
@RequestMapping
public abstract class BaseController {

    public static final String PAGE_PARAMS_ATTR = "pageParams";
    public static final String PAGE_REFERER_ATTR = "pageReferer";
    public static final String ACTIVE_SECTION_ATTR = "activeSection";
    public static final String CART_ATTR = "cart";

    @Resource(name = "enumHelper")
    private EnumHelper enumHelper;

    @Resource(name = "deviceDependencyImageUrlCreator")
    private ImageUrlCreator imageUrlCreator;

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    @Resource(name = "pageHelper")
    protected PageHelper pageHelper;

    @Resource(name = "entitiesConstraints")
    private EntitiesConstraints entitiesConstraints;

    @Resource(name = "marketConfig")
    private MarketConfig marketConfig;

    @Resource(name = "serverConfig")
    private ServerConfig serverConfig;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @ModelAttribute("enumHelper")
    public EnumHelper getEnumHelper() {
        return enumHelper;
    }

    @ModelAttribute("imageUrlCreator")
    public ImageUrlCreator getImageUrlCreator() {
        return imageUrlCreator;
    }

    @ModelAttribute("appNavigationManager")
    public AppNavigationManager getAppNavigationManager() {
        return appNavigationManager;
    }

    @ModelAttribute("pageHelper")
    public PageHelper getPageHelper() {
        return pageHelper;
    }

    @ModelAttribute("entitiesConstraints")
    public EntitiesConstraints getEntitiesConstraints() {
        return entitiesConstraints;
    }

    @ModelAttribute("marketConfig")
    public MarketConfig getMarketConfig() {
        return marketConfig;
    }

    @ModelAttribute("serverConfig")
    public ServerConfig getServerConfig() {
        return serverConfig;
    }

    @ModelAttribute("sections")
    public List<Section> getSections() {
        return sectionService.getAllVisible().getAll();
    }

    protected long extractEntityIdFromNamedUrl(String namedUrl) {
        try {
            return UrlUtils.extractIdFromNamedUrl(namedUrl);
        } catch (MalformedURLException e) {
            throw new NotFoundException();
        }
    }

    protected void setMovedPermanentlyRedirectInView(ModelAndView modelAndView, String redirectUrl) {
        RedirectView redirectView = new RedirectView(redirectUrl);
        redirectView.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        modelAndView.setView(redirectView);
    }
}
