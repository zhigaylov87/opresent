package ru.opresent.web.app.component;

import org.springframework.stereotype.Component;
import ru.opresent.core.component.api.image.ImageFileManager;
import ru.opresent.core.component.api.image.ImageUrlCreator;
import ru.opresent.core.domain.image.Image;
import ru.opresent.core.domain.image.ImageUseOption;
import ru.opresent.web.app.util.DeviceUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 19.07.15
 * Time: 23:02
 */
@Component("deviceDependencyImageUrlCreator")
public class DeviceDependencyImageUrlCreator implements ImageUrlCreator {

    @Resource(name = "imageFileManager")
    private ImageFileManager imageFileManager;

    @Override
    public String createUrl(Image image) {
        return imageFileManager.createUrl(image);
    }

    @Override
    public String createUrl(Image image, ImageUseOption useOption) {
        return imageFileManager.createUrl(image, useOption, DeviceUtils.getCurrentDeviceType());
    }
}
