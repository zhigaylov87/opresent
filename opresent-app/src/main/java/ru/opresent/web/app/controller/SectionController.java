package ru.opresent.web.app.controller;

import org.springframework.mobile.device.DeviceType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.service.api.DeliveryWayService;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.service.api.SeeAlsoProductsService;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.core.util.IdentifiableNamedEntityList;
import ru.opresent.web.app.util.DeviceUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 14.03.15
 * Time: 7:08
 */
@Controller
@RequestMapping
public class SectionController extends BaseController {

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Resource(name = "deliveryWayService")
    private DeliveryWayService deliveryWayService;

    @Resource(name = "seeAlsoProductsService")
    private SeeAlsoProductsService seeAlsoProductsService;

    @RequestMapping(value = "/{sectionEntityName}", method = RequestMethod.GET)
    public String sectionByEntityName(Model model, @PathVariable("sectionEntityName") String sectionEntityName) {
        IdentifiableNamedEntityList<Section> sections = sectionService.getAllVisible();
        if (!sections.containsByEntityName(sectionEntityName)) {
            throw new NotFoundException();
        }

        Section section = sections.getByEntityName(sectionEntityName);
        model.addAttribute("section", section);
        model.addAttribute(ACTIVE_SECTION_ATTR, section);
        switch (section.getType()) {
            case LIST:
                model.addAttribute("sectionItems",  sectionService.loadVisibleItems(section.getId()));
                break;

            case DELIVERY:
                model.addAttribute("deliveryWayInfos", deliveryWayService.getAll().getAll());
                break;
        }

        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createSectionPageParams(section));
        return "section/section";
    }

    @RequestMapping(value = "/{sectionEntityName}/{sectionNamedUrl}", method = RequestMethod.GET)
    public ModelAndView sectionItemById(ModelAndView modelAndView,
                                        @PathVariable("sectionEntityName") String sectionEntityName,
                                        @PathVariable("sectionNamedUrl") String sectionNamedUrl) {
        SectionItem sectionItem = sectionService.loadItem(extractEntityIdFromNamedUrl(sectionNamedUrl));
        if (sectionItem == null || !sectionItem.isVisible()) {
            throw new NotFoundException();
        }
        Section section = sectionItem.getSection();
        if (!section.isVisible()) {
            throw new NotFoundException();
        }

        String actualUrl = appNavigationManager.getSectionItemUrl(sectionItem);
        if (!section.getEntityName().equals(sectionEntityName) || !actualUrl.endsWith("/" + sectionNamedUrl)) {
            setMovedPermanentlyRedirectInView(modelAndView, actualUrl);
            return modelAndView;
        }

        modelAndView.addObject(ACTIVE_SECTION_ATTR, section);
        modelAndView.addObject("sectionItem", sectionItem);
        modelAndView.setViewName("section/section_item");

        if (DeviceUtils.getCurrentDeviceType() == DeviceType.NORMAL) {
            modelAndView.addObject("seeAlsoProducts", seeAlsoProductsService.getForSectionItem(sectionItem.getId()));
        }

        modelAndView.addObject(PAGE_PARAMS_ATTR, pageHelper.createSectionItemPageParams(sectionItem));
        return modelAndView;
    }

}
