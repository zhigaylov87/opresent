package ru.opresent.web.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.service.api.BannerService;
import ru.opresent.core.service.api.CatalogService;
import ru.opresent.core.service.api.NewsService;
import ru.opresent.core.service.api.ProductTypeService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 28.08.14
 * Time: 22:59
 */
@Controller
@RequestMapping
public class WelcomeController extends BaseController {

    private static final int WELCOME_PRODUCT_TYPES_MAX_COUNT = 6;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "catalogService")
    private CatalogService catalogService;

    @Resource(name = "bannerService")
    private BannerService bannerService;

    @Resource(name = "newsService")
    private NewsService newsService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome(Model model) {
        model.addAttribute("bannerItems", bannerService.getBanner().getAllVisibleItems().getAll());
        model.addAttribute("welcomeProductTypes", getWelcomeProductTypes());
        model.addAttribute("newsItems", newsService.getNews().getAllVisibleItems().getAll());

        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createWelcomePageParams());
        return "welcome";
    }

    private List<ProductType> getWelcomeProductTypes() {
        List<ProductType> productTypes = new ArrayList<>();

        Set<ProductType> catalogProductTypes = catalogService.getCatalog().getProductTypes();
        for (ProductType productType : productTypeService.getAll().getAll()) {
            if (productTypes.size() == WELCOME_PRODUCT_TYPES_MAX_COUNT) {
                break;
            }
            if (catalogProductTypes.contains(productType)) {
                productTypes.add(productType);
            }
        }

        return productTypes;
    }
}
