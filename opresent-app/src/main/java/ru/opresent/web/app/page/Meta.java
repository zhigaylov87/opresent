package ru.opresent.web.app.page;

/**
 * User: artem
 * Date: 31.10.15
 * Time: 2:30
 */
public class Meta {

    private String name;
    private String content;

    Meta(String name, String content) {
        this.name = name;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

}
