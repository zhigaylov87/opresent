package ru.opresent.web.app.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.servlet.*;
import java.io.IOException;
import java.util.UUID;

/**
 *
 * User: artem
 * Date: 16.10.16
 * Time: 15:42
 */
public class AppRequestIdFilter implements Filter {

    private static final String MDC_REQUEST_ID = "requestId";
    private static final Logger LOG = LoggerFactory.getLogger(AppRequestIdFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // do nothing yet
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            MDC.put(MDC_REQUEST_ID, UUID.randomUUID().toString());
            LOG.info("START|AppRequestIdFilter");
            chain.doFilter(request, response);
        } finally {
            LOG.info("END|AppRequestIdFilter");
            MDC.remove(MDC_REQUEST_ID);
        }
    }

    @Override
    public void destroy() {
        // do nothing yet
    }
}
