package ru.opresent.web.app.controller;

import com.google.common.collect.ImmutableMap;
import org.springframework.mobile.device.DeviceType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.DefaultProductFilter;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.catalog.Catalog;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.pagination.DefaultPageSettings;
import ru.opresent.core.pagination.PageSettings;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.service.api.*;
import ru.opresent.core.util.IdentifiableNamedEntityList;
import ru.opresent.web.app.ui.ProductListFilterModelCreator;
import ru.opresent.web.app.ui.ProductListPaginationModelCreator;
import ru.opresent.web.app.util.DeviceUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * curl -v -H "Accept: application/json" http://localhost:8080/web-market-restful/restful/product/page/0/1/1
 * <p/>
 * User: artem
 * Date: 16.05.14
 * Time: 22:38
 */
@Controller
@RequestMapping(value = "/" + Section.CATALOG_SECTION_ENTITY_NAME)
public class ProductListController extends BaseController {

    private static final Map<DeviceType, Integer> DEVICE_TYPE_TO_PAGE_SIZE = ImmutableMap.of(
            DeviceType.NORMAL, 31,
            DeviceType.MOBILE, 30
    );

    private static final Map<DeviceType, ? extends Map<Integer, List<Integer>>> DEVICE_TYPE_TO_PRODUCT_COLUMN_INDEX = ImmutableMap.of(
            DeviceType.NORMAL, ImmutableMap.of(0, Arrays.asList(4, 7, 10, 13, 16, 19, 22, 25, 28),
                                               1, Arrays.asList(0, 2, 5, 8, 11, 14, 17, 20, 23, 26, 29),
                                               2, Arrays.asList(1, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30)),

            DeviceType.MOBILE, ImmutableMap.of(0, Arrays.asList(0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28),
                                               1, Arrays.asList(1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29))
    );

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Resource(name = "catalogService")
    private CatalogService catalogService;

    @Resource(name = "orderService")
    private OrderService orderService;

    @Resource(name = "productListFilterModelCreator")
    private ProductListFilterModelCreator productListFilterModelCreator;

    @Resource(name = "productListPaginationModelCreator")
    private ProductListPaginationModelCreator productListPaginationModelCreator;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String list(Model model) {
        return list(model, createParams());
    }

    @RequestMapping(value = "/{param1}", method = RequestMethod.GET)
    public String list(Model model, @PathVariable("param1") String param1) {
        return list(model, createParams(param1));
    }

    @RequestMapping(value = "/{param1}/{param2}", method = RequestMethod.GET)
    public String list(Model model, @PathVariable("param1") String param1, @PathVariable("param2") String param2) {
        return list(model, createParams(param1, param2));
    }

    @RequestMapping(value = "/{param1}/{param2}/{param3}", method = RequestMethod.GET)
    public String list(Model model, @PathVariable("param1") String param1, @PathVariable("param2") String param2,
                       @PathVariable("param3") String param3) {
        return list(model, createParams(param1, param2, param3));
    }

    @RequestMapping(value = "/{param1}/{param2}/{param3}/{param4}", method = RequestMethod.GET)
    public String list(Model model, @PathVariable("param1") String param1, @PathVariable("param2") String param2,
                       @PathVariable("param3") String param3, @PathVariable("param4") String param4) {
        return list(model, createParams(param1, param2, param3, param4));
    }

    private String list(Model model, Params params) {
        DefaultProductFilter filter = new DefaultProductFilter();
        filter.setProductType(params.productType);
        filter.setCategory(params.category);
        filter.setStatus(params.status);
        filter.setVisible(true);

        DeviceType deviceType = DeviceUtils.getCurrentDeviceType();
        ProductPage productPage = productService.load(filter,
                new DefaultPageSettings(params.pageNumber, DEVICE_TYPE_TO_PAGE_SIZE.get(deviceType)));

        model.addAttribute(ACTIVE_SECTION_ATTR, sectionService.getAllVisible().getByEntityName(Section.CATALOG_SECTION_ENTITY_NAME));
        model.addAttribute("orderSettings", orderService.getOrderSettings());
        model.addAttribute("productPage", productPage);
        model.addAttribute("productColumnIndexes", DEVICE_TYPE_TO_PRODUCT_COLUMN_INDEX.get(deviceType));

        boolean mobile = deviceType == DeviceType.MOBILE;
        model.addAttribute("filterModel", productListFilterModelCreator.createModel(productPage, filter, mobile));
        model.addAttribute("paginationModel", productListPaginationModelCreator.createModel(productPage, filter, mobile));

        model.addAttribute(PAGE_PARAMS_ATTR, pageHelper.createProductListPageParams());

        return "product/product_list";
    }

    private Params createParams(String... strParams) {
        if (strParams.length > 4) {
            throw new NotFoundException();
        }

        IdentifiableNamedEntityList<ProductType> productTypes = productTypeService.getAll();
        IdentifiableNamedEntityList<Category> categories = categoryService.getAll();
        Catalog catalog = catalogService.getCatalog();
        Params params = new Params();

        for (int i = 0; i < strParams.length; i++) {
            String paramValue = strParams[i];

            if (i == 0 && productTypes.containsByEntityName(paramValue)) {
                ProductType productType = productTypes.getByEntityName(paramValue);
                if (!catalog.getProductTypes().contains(productType)) {
                    throw new NotFoundException();
                }
                params.productType = productType;
            } else if (i <= 1 && params.category == null && categories.containsByEntityName(paramValue)) {
                Category category = categories.getByEntityName(paramValue);
                if (!catalog.getAllCategories().contains(category)) {
                    throw new NotFoundException();
                }
                params.category = category;
            } else if (i <= 2 && params.status == null && ProductStatus.containsByEntityName(paramValue)) {
                params.status = ProductStatus.valueOfEntityName(paramValue);
            } else {
                try {
                    params.pageNumber = Integer.parseInt(paramValue);
                } catch (NumberFormatException e) {
                    throw new NotFoundException();
                }
            }
        }

        return params;
    }

    private static class Params {
        ProductType productType;
        Category category;
        ProductStatus status;
        int pageNumber = PageSettings.FIRST_PAGE_NUMBER;
    }
}