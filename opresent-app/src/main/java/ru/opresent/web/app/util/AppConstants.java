package ru.opresent.web.app.util;

/**
 * User: artem
 * Date: 14.03.16
 * Time: 1:01
 */
public interface AppConstants {

    String TRACKING_ID_COOKIE_NAME = "OPRESENT_ID";

    int TRACKING_ID_COOKIE_MAX_AGE = 3 * 365 * 24 * 60 * 60; //three years in seconds
}
