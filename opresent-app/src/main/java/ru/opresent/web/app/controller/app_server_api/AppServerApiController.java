package ru.opresent.web.app.controller.app_server_api;

import org.slf4j.Logger;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.opresent.core.CoreException;
import ru.opresent.core.component.api.crypto.Cryptographer;
import ru.opresent.core.service.api.SearchService;
import ru.opresent.core.service.impl.SitemapService;
import ru.opresent.core.service.impl.search.SearchIndexUpdateConfig;
import ru.opresent.web.app.controller.BadRequestException;

import javax.annotation.Resource;
import java.text.MessageFormat;

/**
 *
 * User: artem
 * Date: 26.12.15
 * Time: 21:10
 */
@Controller
@RequestMapping(value = "/api")
public class AppServerApiController {

    @Resource(name = "log")
    private Logger log;

    @Resource(name = "cryptographer")
    private Cryptographer cryptographer;

    @Resource(name = "cacheManager")
    private CacheManager cacheManager;

    @Resource(name = "searchService")
    private SearchService searchService;

    @Resource(name = "sitemapService")
    private SitemapService sitemapService;


    // TODO: Придумать какое-то другое решение, чтобы кэш обновлялся самостоятельно на сервере
    // TODO: Возможно надо следить за какой-нибудь характеристикой сущностей типа modify_time или какой-то специальной настройкой в БД
    @RequestMapping(value = "/cache/clear", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void clearCache(@RequestBody String encryptedCacheName) {
        String logMsg = " of clear cache";
        try {
            log.info("Start" + logMsg);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0} [encrypted cache name: ''{1}'']", logMsg, encryptedCacheName));
            }

            String cacheName = cryptographer.decrypt(encryptedCacheName, String.class);
            logMsg = MessageFormat.format("{0} [cache name: ''{1}'']", logMsg, cacheName);
            log.info("Process" + logMsg);

            cacheManager.getCache(cacheName).clear();

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new BadRequestException();
        }
    }

    // TODO: Сделать регуляргую задачу, которая будет смотреть на modify_time товаров и обновлять индекс при необходимости
    @RequestMapping(value = "/search/index-update", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateSearchIndex(@RequestBody String encryptedSearchIndexUpdateConfig) {
        String logMsg = " of update search index";
        try {
            log.info("Start" + logMsg);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0} [encrypted search index update config: ''{1}'']",
                        logMsg, encryptedSearchIndexUpdateConfig));
            }

            SearchIndexUpdateConfig updateConfig = cryptographer.decrypt(encryptedSearchIndexUpdateConfig, SearchIndexUpdateConfig.class);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0} [decrypted search index update config: ''{1}'']",
                        logMsg, updateConfig));
            }

            logMsg = MessageFormat.format("{0} [search index update config: ''{1}'']", logMsg, updateConfig);
            log.info("Process" + logMsg);

            switch (updateConfig.getMode()) {
                case WHOLE:
                    searchService.updateWholeIndex();
                    break;

                case PRODUCT_SAVE:
                    // TODO: Сейчас частичное обновление индекса работает некорректно
//                    searchService.updateIndexAfterProductSave(updateConfig.getProductId());
//                    break;

                case PRODUCT_DELETE:
                    // TODO: Сейчас частичное обновление индекса работает некорректно
//                    searchService.updateIndexAfterProductDelete(updateConfig.getProductId());
//                    break;

                default:
                    throw new CoreException("Unknown update config mode: " + updateConfig.getMode());
            }

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new BadRequestException();
        }
    }

    // TODO: Сделать регуляргую задачу, которая будет смотреть на modify_time товаров и обновлять sitemap при необходимости
    @RequestMapping(value = "/sitemap/update", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateSitemap(@RequestBody String encryptedUpdateParams) {
        String logMsg = " of update sitemap";
        try {
            log.info("Start" + logMsg);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0} [encrypted update params: ''{1}'']",
                        logMsg, encryptedUpdateParams));
            }

            String decryptedUpdateParams = cryptographer.decrypt(encryptedUpdateParams, String.class);
            if (log.isDebugEnabled()) {
                log.debug(MessageFormat.format("Process{0} [decrypted update params: ''{1}'']",
                        logMsg, decryptedUpdateParams));
            }

            sitemapService.updateSitemap(decryptedUpdateParams);

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new BadRequestException();
        }
    }
}
