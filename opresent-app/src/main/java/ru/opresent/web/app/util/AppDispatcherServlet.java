package ru.opresent.web.app.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: artem
 * Date: 23.10.16
 * Time: 6:47
 */
public class AppDispatcherServlet extends DispatcherServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AppDispatcherServlet.class);

    @Override
    protected void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOG.info("START|AppDispatcherServlet.doService");
        super.doService(request, response);
        LOG.info("END|AppDispatcherServlet.doService");
    }

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOG.info("START|AppDispatcherServlet.doDispatch");
        super.doDispatch(request, response);
        LOG.info("END|AppDispatcherServlet.doDispatch");
    }

    @Override
    protected void render(ModelAndView mv, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOG.info("START|AppDispatcherServlet.render");
        super.render(mv, request, response);
        LOG.info("END|AppDispatcherServlet.render");
    }
}
