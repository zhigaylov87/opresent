package ru.opresent.web.app.page;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Product;
import ru.opresent.core.domain.ProductStatusInfo;
import ru.opresent.core.domain.section.Section;
import ru.opresent.core.domain.section.SectionItem;
import ru.opresent.core.service.api.ProductStatusService;
import ru.opresent.core.service.api.SectionService;
import ru.opresent.core.util.LocaleUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 10.01.15
 * Time: 8:19
 */
@Component("pageHelper")
public class PageHelper {

    @Resource(name = "productStatusService")
    private ProductStatusService productStatusService;

    @Resource(name = "sectionService")
    private SectionService sectionService;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    public String getProductStatusNote(Product product) {
        if (product.getStatusNote() != null) {
            return product.getStatusNote();
        }

        ProductStatusInfo statusInfo = productStatusService.getAll().get(product.getStatus());
        return statusInfo == null ? null : statusInfo.getDefaultNote();
    }

    public PageParams createWelcomePageParams() {
        return PageParams.builder()
                .setTitle(createTitle(null))
                .addRobotsIndexFollowMeta()
                .addDescriptionMeta(getMsg("welcome.description"))
                .addKeywordsMeta(getMsg("welcome.keywords"))
                .build();
    }

    public PageParams createProductListPageParams() {
        return PageParams.builder()
                .setTitle(createTitle(sectionService.getAllVisible().getByEntityName(Section.CATALOG_SECTION_ENTITY_NAME).getName()))
                .addRobotsIndexFollowMeta()
                .build();
    }

    public PageParams createProductPageParams(Product product) {
        return PageParams.builder()
                .setTitle(createTitle(getMsg("buy") + " " + product.getName()))
                .addDescriptionMeta(getMsg("buy") + " " + product.getName())
                .addKeywordsMeta(product.getKeywords())
                .addRobotsIndexFollowMeta()
                .build();
    }

    public PageParams createCartPageParams() {
        return PageParams.builder()
                .setTitle(createTitle(getMsg("cart")))
                .addRobotsNoIndexFollowMeta()
                .build();
    }

    public PageParams createOrderPageParams() {
        return PageParams.builder()
                .setTitle(createTitle(getMsg("order.header")))
                .addRobotsNoIndexFollowMeta()
                .build();
    }

    public PageParams createSectionPageParams(Section section) {
        return PageParams.builder()
                .setTitle(createTitle(section.getName()))
                .addRobotsIndexFollowMeta()
                .build();
    }

    public PageParams createSectionItemPageParams(SectionItem sectionItem) {
        return PageParams.builder()
                .setTitle(createTitle(sectionItem.getName()))
                .addRobotsIndexFollowMeta()
                .build();
    }

    public PageParams createSearchPageParams() {
        return PageParams.builder()
                .setTitle(createTitle(getMsg("search")))
                .addRobotsNoIndexFollowMeta()
                .build();
    }

    private String createTitle(String titleStart) {
        return titleStart == null ? getMsg("default_page_title") : titleStart + " | " + getMsg("default_page_title");
    }

    private String getMsg(String code) {
        return messageSource.getMessage(code, null, LocaleUtils.RUSSIAN);
    }
}
