package ru.opresent.web.app.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import ru.opresent.core.domain.Identifiable;
import ru.opresent.core.domain.cart.Cart;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.domain.order.Order;
import ru.opresent.core.util.IdentifiableList;
import ru.opresent.core.util.stringify.Stringifiable;
import ru.opresent.web.app.controller.BadRequestException;
import ru.opresent.web.app.controller.NotFoundException;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * TODO: Убрать логирование методов BaseController'a если они быстро отрабатывают
 * TODO: Логировать результаты выполнения методов. Для коллекций можно только размер печатать.
 * User: artem
 * Date: 25.11.14
 * Time: 0:52
 */
@Component
@Aspect
public class LoggingAdvice implements Serializable {

    @Resource(name = "log")
    private Logger log;

    // Controller
    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void controllerInvoked() {}

    @Before(value = "controllerInvoked()", argNames = "joinPoint")
    public void beforeControllerInvoked(JoinPoint joinPoint) {
        logStart(joinPoint);
    }

    @AfterReturning(value = "controllerInvoked()", argNames = "joinPoint, returnValue", returning = "returnValue")
    public void afterReturningControllerInvoked(JoinPoint joinPoint, Object returnValue) {
        logEnd(joinPoint, returnValue);
    }

    @AfterThrowing(value = "controllerInvoked()", argNames = "joinPoint, throwable", throwing = "throwable")
    public void afterThrowingControllerInvoked(JoinPoint joinPoint, Throwable throwable) {
        if (throwable instanceof NotFoundException || throwable instanceof BadRequestException) {
            logEnd(joinPoint, throwable.getClass().getSimpleName());
        } else {
            logError(joinPoint, throwable);
        }
    }

    // Service
    @Pointcut("within(@org.springframework.stereotype.Service *)")
    public void serviceInvoked() {}

    @Before(value = "serviceInvoked()", argNames = "joinPoint")
    public void beforeServiceInvoked(JoinPoint joinPoint) {
        logStart(joinPoint);
    }

    @AfterReturning(value = "serviceInvoked()", argNames = "joinPoint, returnValue", returning = "returnValue")
    public void afterReturningServiceInvoked(JoinPoint joinPoint, Object returnValue) {
        logEnd(joinPoint, returnValue);
    }

    @AfterThrowing(value = "serviceInvoked()", argNames = "joinPoint, throwable", throwing = "throwable")
    public void afterThrowingServiceInvoked(JoinPoint joinPoint, Throwable throwable) {
        logError(joinPoint, throwable);
    }

    // Dao
    @Pointcut("execution(public * ru.opresent.core.dao..*Dao.*(..))")
    public void daoInvoked() {}

    @Before(value = "daoInvoked()", argNames = "joinPoint")
    public void beforeDaoInvoked(JoinPoint joinPoint) {
        logStart(joinPoint);
    }

    @AfterReturning(value = "daoInvoked()", argNames = "joinPoint, returnValue", returning = "returnValue")
    public void afterReturningDaoInvoked(JoinPoint joinPoint, Object returnValue) {
        logEnd(joinPoint, returnValue);
    }

    @AfterThrowing(value = "daoInvoked()", argNames = "joinPoint, throwable", throwing = "throwable")
    public void afterThrowingDaoInvoked(JoinPoint joinPoint, Throwable throwable) {
        logError(joinPoint, throwable);
    }

    private void logStart(JoinPoint joinPoint) {
        StringBuilder sb = new StringBuilder("START");
        appendJoinPoint(sb, joinPoint);
        log.info(sb.toString());
    }

    private void logEnd(JoinPoint joinPoint, Object returnValue) {
        StringBuilder sb = new StringBuilder("END");
        appendJoinPoint(sb, joinPoint).append("|");
        appendValue(sb, returnValue);
        log.info(sb.toString());
    }

    private void logError(JoinPoint joinPoint, Throwable throwable) {
        StringBuilder sb = new StringBuilder("ERROR");
        appendJoinPoint(sb, joinPoint);
        log.error(sb.toString(), throwable);
    }

    private StringBuilder appendJoinPoint(StringBuilder sb, JoinPoint joinPoint) {
        sb.append("|").append(joinPoint.getSignature().toShortString()).append("|(");
        join(sb, Arrays.asList(joinPoint.getArgs()));
        sb.append(")");
        return sb;
    }

    private static final List<Class<? extends Stringifiable>> CHOOSE_STRINGIFIABLE_INSTEAD_IDENTIFIABLE =
            Arrays.<Class<? extends Stringifiable>>asList(
                    IncomingMessage.class, Cart.class, Order.class
    );

    private StringBuilder appendValue(StringBuilder sb, Object value) {
        if (value == null) {
            return sb.append("null");
        }

        Class valueClass = value.getClass();
        boolean cond =
                valueClass.isPrimitive() ||
                valueClass.isEnum() ||
                value instanceof String ||
                value instanceof Number ||
                value instanceof Boolean;
        if (cond) {
            return sb.append(value);
        }

        if (value instanceof Identifiable &&
                !(value instanceof Stringifiable && CHOOSE_STRINGIFIABLE_INSTEAD_IDENTIFIABLE.contains(value.getClass()))) {
            return appendIdentifiable(sb, ((Identifiable) value));
        }
        if (value instanceof IdentifiableList) {
            return joinWithSquareBrackets(sb, ((IdentifiableList) value).getAll());
        }
        if (value instanceof Collection) {
            return joinWithSquareBrackets(sb, (Collection) value);
        }
        // Model must be before Map
        if (value instanceof Model) {
            return sb.append(valueClass.getSimpleName());
        }
        if (value instanceof Map) {
            return sb.append("count: ").append(((Map) value).size());
        }
        if (value instanceof Stringifiable) {
            return sb.append(((Stringifiable) value).stringify(false));
        }

        return sb.append(valueClass.getSimpleName());
    }

    private StringBuilder appendIdentifiable(StringBuilder sb, Identifiable value) {
        return sb.append("#").append(value.getId());
    }

    private StringBuilder join(StringBuilder sb, Collection items) {
        boolean first = true;
        for (Object item : items) {
            if (!first) {
                sb.append(", ");
            }
            first = false;
            appendValue(sb, item);
        }
        return sb;
    }

    private StringBuilder joinWithSquareBrackets(StringBuilder sb, Collection items) {
        sb.append("[");
        join(sb, items);
        return sb.append("]");
    }
}
