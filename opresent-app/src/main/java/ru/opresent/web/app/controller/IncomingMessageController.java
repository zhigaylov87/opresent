package ru.opresent.web.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.opresent.core.domain.message.incoming.IncomingMessage;
import ru.opresent.core.service.api.IncomingMessageService;
import ru.opresent.web.app.util.AppConstants;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * User: artem
 * Date: 22.11.14
 * Time: 16:33
 */
@Controller
@RequestMapping(value = "/message/incoming")
public class IncomingMessageController extends BaseController {

    @Resource(name = "incomingMessageService")
    private IncomingMessageService incomingMessageService;

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(value = HttpStatus.OK)
    public void add(@RequestBody @Valid IncomingMessage message,
                    @CookieValue(AppConstants.TRACKING_ID_COOKIE_NAME) String trackingId) {
        incomingMessageService.save(message, trackingId);
    }

}
