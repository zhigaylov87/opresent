package ru.opresent.web.app.ui;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.opresent.core.domain.Category;
import ru.opresent.core.domain.ProductFilter;
import ru.opresent.core.domain.ProductStatus;
import ru.opresent.core.domain.ProductType;
import ru.opresent.core.domain.catalog.Catalog;
import ru.opresent.core.pagination.ProductPage;
import ru.opresent.core.service.api.CatalogService;
import ru.opresent.core.service.api.CategoryService;
import ru.opresent.core.service.api.ProductTypeService;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.core.util.EnumHelper;
import ru.opresent.core.util.LocaleUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 03.11.16
 * Time: 0:42
 */
@Component("productListFilterModelCreator")
public class ProductListFilterModelCreator {

    @Resource(name = "catalogService")
    private CatalogService catalogService;

    @Resource(name = "productTypeService")
    private ProductTypeService productTypeService;

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    @Resource(name = "enumHelper")
    private EnumHelper enumHelper;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    public ProductListFilterModel createModel(ProductPage page, ProductFilter filter, boolean mobile) {
        ProductListFilterModel model = new ProductListFilterModel();
        Catalog catalog = catalogService.getCatalog();

        ProductType selectedProductType = filter.getProductType();
        model.addProductTypeItem(getMsg(mobile ? "product_list.filter.all_product_types" : "product_list.filter.all_item_name"),
                appNavigationManager.getCatalogUrl(), selectedProductType == null);
        for (ProductType productType : productTypeService.getAll().getAll()) {
            if (catalog.getProductTypes().contains(productType)) {
                model.addProductTypeItem(productType.getName(), appNavigationManager.getCatalogUrl(productType),
                        productType.equals(selectedProductType));
            }
        }

        Category selectedCategory = filter.getCategory();
        model.addCategoryItem(getMsg(mobile ? "product_list.filter.all_categories" : "product_list.filter.all_item_name"),
                appNavigationManager.getCatalogUrl(selectedProductType), selectedCategory == null);
        for (Category category : categoryService.getAll().getAll()) {
            if (selectedProductType == null ?
                    catalog.getAllCategories().contains(category) :
                    catalog.getProductTypeCategories(selectedProductType).contains(category)) {
                model.addCategoryItem(category.getName(), appNavigationManager.getCatalogUrl(selectedProductType, category),
                        category.equals(selectedCategory));
            }
        }

        ProductStatus selectedProductStatus = filter.getStatus();
        model.addProductStatusItem(getMsg(mobile ? "product_list.filter.all_statuses" : "product_list.filter.all_item_name"),
                appNavigationManager.getCatalogUrl(selectedProductType, selectedCategory),
                selectedProductStatus == null, page.getTotalCountForAllStatuses());
        for (ProductStatus productStatus : ProductStatus.values()) {
            model.addProductStatusItem(enumHelper.getDescription(productStatus),
                    appNavigationManager.getCatalogUrl(selectedProductType, selectedCategory, productStatus),
                    productStatus == selectedProductStatus, page.getTotalCountForStatus(productStatus));
        }

        return model;
    }

    private String getMsg(String code) {
        return messageSource.getMessage(code, null, LocaleUtils.RUSSIAN);
    }

}
