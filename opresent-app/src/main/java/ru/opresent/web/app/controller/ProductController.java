package ru.opresent.web.app.controller;

import org.springframework.mobile.device.DeviceType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.opresent.core.domain.Product;
import ru.opresent.core.service.api.ProductService;
import ru.opresent.core.service.api.SeeAlsoProductsService;
import ru.opresent.core.util.AppNavigationManager;
import ru.opresent.web.app.util.DeviceUtils;

import javax.annotation.Resource;

/**
 * User: artem
 * Date: 23.09.14
 * Time: 1:06
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController extends BaseController {

    @Resource(name = "appNavigationManager")
    private AppNavigationManager appNavigationManager;

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "seeAlsoProductsService")
    private SeeAlsoProductsService seeAlsoProductsService;

    @RequestMapping(value = "/{productNamedUrl}", method = RequestMethod.GET)
    public ModelAndView byId(ModelAndView modelAndView, @PathVariable("productNamedUrl") String productNamedUrl) {
        Product product = productService.load(extractEntityIdFromNamedUrl(productNamedUrl));
        if (product == null || !product.isVisible()) {
            throw new NotFoundException();
        }

        String actualUrl = appNavigationManager.getProductUrl(product);
        if (!actualUrl.endsWith("/" + productNamedUrl)) {
            setMovedPermanentlyRedirectInView(modelAndView, actualUrl);
            return modelAndView;
        }

        modelAndView.addObject("product", product);
        modelAndView.addObject(PAGE_PARAMS_ATTR, pageHelper.createProductPageParams(product));

        if (DeviceUtils.getCurrentDeviceType() == DeviceType.NORMAL) {
            modelAndView.addObject("seeAlsoProducts", seeAlsoProductsService.getForProduct(product.getId()));
        }

        modelAndView.setViewName("product/product");
        return modelAndView;
    }

}
