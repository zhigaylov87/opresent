package ru.opresent.web.app.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductListPaginationModel {

    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return Collections.unmodifiableList(items);
    }

    public void addPrevItem(String href) {
        items.add(new Item(ItemType.PREV, href, null, null));
    }

    public void addPageItem(String href, int pageNumber, boolean pageSelected) {
        items.add(new Item(ItemType.PAGE, href, pageNumber, pageSelected));
    }

    public void addDotsItem() {
        items.add(new Item(ItemType.DOTS, null, null, null));
    }

    public void addNextItem(String href) {
        items.add(new Item(ItemType.NEXT, href, null, null));
    }

    public enum ItemType {PREV, DOTS, PAGE, NEXT}

    public static class Item {

        private ItemType type;
        private String href;
        private Boolean pageSelected;
        private Integer pageNumber;

        private Item(ItemType type, String href, Integer pageNumber, Boolean pageSelected) {
            this.type = type;
            this.href = href;
            this.pageNumber = pageNumber;
            this.pageSelected = pageSelected;
        }

        public ItemType getType() {
            return type;
        }

        public String getHref() {
            return href;
        }

        public Boolean getPageSelected() {
            return pageSelected;
        }

        public Integer getPageNumber() {
            return pageNumber;
        }
    }
}
