package ru.opresent.web.app.util;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceType;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * User: artem
 * Date: 08.11.15
 * Time: 5:30
 */
public class DeviceUtils {

    public static DeviceType getCurrentDeviceType() {
        RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
        Assert.isInstanceOf(ServletRequestAttributes.class, attrs);
        HttpServletRequest request = ((ServletRequestAttributes) attrs).getRequest();
        Device device = org.springframework.mobile.device.DeviceUtils.getCurrentDevice(request);
        return device.isMobile() ? DeviceType.MOBILE : DeviceType.NORMAL;
    }

}
