package ru.opresent.web.app.page;

import org.apache.taglibs.standard.functions.Functions;
import ru.opresent.core.domain.ProductKeyword;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 31.10.15
 * Time: 1:59
 */
public class PageParams {

    private String title;
    private List<Meta> metaList = new ArrayList<>();

    private PageParams() {
    }

    public String getTitle() {
        return title;
    }

    public List<Meta> getMetaList() {
        return metaList;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private static final Meta ROBOTS_INDEX_FOLLOW_META = new Meta("robots", "index,follow");
        private static final Meta ROBOTS_NOINDEX_FOLLOW_META = new Meta("robots", "noindex,follow");

        private PageParams params = new PageParams();

        private Builder() {
        }

        public Builder setTitle(String title) {
            params.title = Functions.escapeXml(title);
            return this;
        }

        public Builder addRobotsIndexFollowMeta() {
            return addMeta(ROBOTS_INDEX_FOLLOW_META);
        }

        public Builder addRobotsNoIndexFollowMeta() {
            return addMeta(ROBOTS_NOINDEX_FOLLOW_META);
        }

        public Builder addKeywordsMeta(String keywords) {
            return addMeta(new Meta("keywords", Functions.escapeXml(keywords)));
        }

        public Builder addKeywordsMeta(Collection<ProductKeyword> keywords) {
            StringBuilder keywordsContent = new StringBuilder();
            boolean first = true;
            for (ProductKeyword keyword : keywords) {
                if (first) {
                    first = false;
                } else {
                    keywordsContent.append(",");
                }
                keywordsContent.append(keyword.getKeyword());
            }
            return addKeywordsMeta(keywordsContent.toString());
        }

        public Builder addDescriptionMeta(String descriptionContent) {
            return addMeta(new Meta("description", Functions.escapeXml(descriptionContent)));
        }

        public PageParams build() {
            params.metaList = Collections.unmodifiableList(params.metaList);
            return params;
        }

        private Builder addMeta(Meta meta) {
            for (Meta m : params.metaList) {
                if (m.getName().equals(meta.getName())) {
                    throw new IllegalArgumentException("Meta with name = '" + meta.getName() + "' already exist!");
                }
            }
            params.metaList.add(meta);
            return this;
        }
    }
}
